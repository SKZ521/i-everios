//
//  KEYPullDownMenu.m
//  Keydown
//
//  Created by mmackh on 10/11/13.
//  Copyright (c) 2013 Maximilian Mackh. All rights reserved.
//

#import "KEYPullDownMenu.h"
#import "SKBounceAnimation.h"

#define kKEYPullDownAnimationDuration 0.3
#define kKEYPullDownAnimationBounceHeight 20
#define kKEYPullDownViewTag 198312

#pragma mark - 下拉列表cell
@interface KEYPullDownMenuCell : UITableViewCell
@property(nonatomic, strong)UILabel *itemLabel;
@property(nonatomic, strong)UIView *line;

- (void)setCellWithMenuItem:(KEYPullDownMenuItem *)item;
- (void)showHighLightLine:(BOOL)flag;
@end

@interface KEYPullDownMenu () <UITableViewDataSource,UITableViewDelegate>
// 下拉列表
@property (nonatomic,strong) UITableView *tableView;
// 下拉列表将来显示到这
@property (nonatomic,weak) UIViewController *parentViewController;
// 下拉数据
@property (nonatomic,strong) NSMutableArray *items;
@property (nonatomic,readonly) CGRect initialFrame;
@property (nonatomic,readonly) CGRect finalFrame;

@property (copy) dismissBlock dismissBlock;
@property (copy) reorderBlock reorderBlock;
@property (copy) deleteBlock deleteBlock;

@end

@interface KEYPullDownMenuItem ()

@property (nonatomic) BOOL dummy;

@end

@implementation KEYPullDownMenu

- (id)init
{
    self = [super init];
    if (self)
    {
        self.backgroundColor = [UIColor colorWithWhite:.0f alpha:.4f];
        [self setAutoresizingMask:UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth];

        self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        [self.tableView registerClass:[KEYPullDownMenuCell class] forCellReuseIdentifier:@"Cell"];
        [self.tableView setRowHeight:44.0f];
        [self.tableView setBackgroundColor:[UIColor whiteColor]];
        [self.tableView setSeparatorColor:[UIColor clearColor]];
        [self.tableView setDelegate:self];
        [self.tableView setDataSource:self];
        [self addSubview:self.tableView];
    }
    return self;
}

#pragma mark -
#pragma mark TableView

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    KEYPullDownMenuCell *mCell = (KEYPullDownMenuCell *)cell;
    [mCell showHighLightLine:([self.items[indexPath.row] isActive])?UITableViewCellAccessoryCheckmark:UITableViewCellAccessoryNone];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    KEYPullDownMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    [cell setCellWithMenuItem:self.items[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    self.dismissBlock(self.items[indexPath.row],indexPath.row);
}

#pragma mark -
#pragma mark Frames

- (void)layoutSubviews
{
    [super layoutSubviews];
    if (self.frame.origin.y == self.initialFrame.origin.y)
    {
        self.frame = self.initialFrame;
        return;
    }
    self.frame = self.finalFrame;
}

- (CGRect)initialFrame
{
    return CGRectMake(0, -self.parentViewController.view.frame.size.height, self.parentViewController.view.frame.size.width, self.parentViewController.view.frame.size.height);
}

- (CGRect)finalFrame
{
    return CGRectMake(0, self.parentViewController.topLayoutGuide.length, self.parentViewController.view.frame.size.width, self.parentViewController.view.frame.size.height - self.parentViewController.topLayoutGuide.length);
}

#pragma mark - Private Methods
- (void)backGroundViewTaped:(KEYPullDownMenu *)menu
{
    [KEYPullDownMenu dismissInViewController:menu.parentViewController];
}

#pragma mark - Public Methods

+ (instancetype)openMenuInViewController:(UIViewController *)viewController items:(NSArray *)menuItems dismissBlock:dismissBlock reorderBlock:reorderBlock deleteBlock:deleteBlock
{
    [self blockUserInteraction:YES];
    KEYPullDownMenu *pullDownView = [[KEYPullDownMenu alloc] init];
    pullDownView.dismissBlock = dismissBlock;
    pullDownView.reorderBlock = reorderBlock;
    pullDownView.deleteBlock = deleteBlock;
    [pullDownView setParentViewController:viewController];
    [pullDownView addTarget:pullDownView action:@selector(backGroundViewTaped:) forControlEvents:UIControlEventTouchUpInside];
    [pullDownView setFrame:pullDownView.initialFrame];
    [pullDownView.tableView setFrame:CGRectMake(.0f, .0f, CGRectGetWidth(pullDownView.frame), 176.0f)];
    [pullDownView setItems:menuItems.mutableCopy];
    [pullDownView setTag:kKEYPullDownViewTag];
    [viewController.view addSubview:pullDownView];
    [UIView animateWithDuration:kKEYPullDownAnimationDuration delay:0 options:UIViewAnimationOptionCurveEaseOut animations:
    ^{
        [pullDownView setFrame:CGRectZero];
    }
    completion:^(BOOL finished)
    {
         NSString *keyPath = @"position.y";
         CABasicAnimation *positionAnimation = [CABasicAnimation animationWithKeyPath:keyPath];;
         positionAnimation.fromValue = @(CGRectGetMidY(pullDownView.frame));
         positionAnimation.toValue = @(CGRectGetMidY(pullDownView.frame)-kKEYPullDownAnimationBounceHeight);
         positionAnimation.duration = 0.1;
         positionAnimation.beginTime = 0.0;
         
         id finalValue = @(CGRectGetMidY(pullDownView.finalFrame));
         [pullDownView.layer setValue:finalValue forKeyPath:keyPath];
         SKBounceAnimation *bounceAnimation = [SKBounceAnimation animationWithKeyPath:keyPath];
         bounceAnimation.fromValue = [NSNumber numberWithFloat:CGRectGetMidY(pullDownView.finalFrame) -kKEYPullDownAnimationBounceHeight];
         bounceAnimation.toValue = finalValue;
         bounceAnimation.numberOfBounces = 2;
         bounceAnimation.shouldOvershoot = NO;
         bounceAnimation.beginTime = 0.1;
         bounceAnimation.duration = 0.5;
         
         CAAnimationGroup *group = [CAAnimationGroup animation];
         [group setDuration:.57];
         [group setAnimations:[NSArray arrayWithObjects:positionAnimation, bounceAnimation, nil]];
         
         [pullDownView.layer addAnimation:group forKey:@"bounceAnimation"];
         [self blockUserInteraction:NO];
     }];
    
    return pullDownView;
}

+ (instancetype)dismissInViewController:(UIViewController *)viewController
{
    [self blockUserInteraction:YES];
    KEYPullDownMenu *pullDownView = (KEYPullDownMenu *)[viewController.view viewWithTag:kKEYPullDownViewTag];
    [pullDownView.layer removeAllAnimations];
    [UIView animateWithDuration:kKEYPullDownAnimationDuration animations:^{
        [pullDownView setFrame:pullDownView.initialFrame];
    } completion:^(BOOL finished) {
        [pullDownView removeFromSuperview];
        [self blockUserInteraction:NO];
    }];
    
    return pullDownView;
}

+ (void)blockUserInteraction:(BOOL)block
{
    [[[UIApplication sharedApplication] keyWindow] setUserInteractionEnabled:!block];
}

+ (BOOL)isOpenInViewController:(UIViewController *)viewController
{
    KEYPullDownMenu *pullDownView = (KEYPullDownMenu *)[viewController.view viewWithTag:kKEYPullDownViewTag];
    return pullDownView?YES:NO;
}

@end

@implementation KEYPullDownMenuItem
{
    NSString *_name;
}

- (id)initWithName:(NSString *)name
{
    self = [super init];
    if (!self) return nil;
    
    _name = name;
    self.dictionary = [NSMutableDictionary new];
    
    return self;
}

+ (instancetype)menuItemNamed:(NSString *)name
{
    return [[KEYPullDownMenuItem alloc] initWithName:name];
}

- (NSString *)name
{
    return _name;
}

@end

@implementation KEYPullDownMenuCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (!self) return nil;
    
    _itemLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 25)];
    _itemLabel.center = self.contentView.center;
    _itemLabel.backgroundColor = CLEAR;
    _itemLabel.font = TextFont;
    _itemLabel.textColor = BLACK;
    [self.contentView addSubview:_itemLabel];

    _line = [[UIView alloc] initWithFrame:CGRectMake((CGRectGetWidth(self.frame)-50.0f)/2, CGRectGetHeight(self.frame)-5.0f, 50.0f, 1.5f)];
    [_line setBackgroundColor:[UIColor colorWithRed:224.0f/225.0f green:155.0f/225.0f blue:189.0f/225.0f alpha:1.0f]];
    [self.contentView addSubview:_line];
    
    return self;
}

- (void)setCellWithMenuItem:(KEYPullDownMenuItem *)item
{
    _itemLabel.text = [item name];
}

- (void)showHighLightLine:(BOOL)flag
{
    _line.hidden = !flag;
}

@end
