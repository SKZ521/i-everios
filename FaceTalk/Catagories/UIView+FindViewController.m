//
//  UIView+FindViewController.m
//  ZGChat
//
//  Created by 'Sooongz on 14-2-14.
//  Copyright (c) 2014年 Sooongz. All rights reserved.
//

#import "UIView+FindViewController.h"

@implementation UIView (FindViewController)

- (UIViewController *)firstAvailableUIViewController
{
    // convenience function for casting and to "mask" the recursive function
    return (UIViewController *)[self traverseResponderChainForUIViewController];
}

- (id)traverseResponderChainForUIViewController
{
    id nextResponder = [self nextResponder];
    if ([nextResponder isKindOfClass:[UIViewController class]]) {
        return nextResponder;
    } else if ([nextResponder isKindOfClass:[UIView class]]) {
        return [nextResponder traverseResponderChainForUIViewController];
    } else {
        return nil;
    }
}

@end
