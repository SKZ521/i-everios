//
//  NSObject+Reflection.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-17.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Reflection)
- (BOOL)reflectDataFromObject:(NSObject *)dataSource;
@end
