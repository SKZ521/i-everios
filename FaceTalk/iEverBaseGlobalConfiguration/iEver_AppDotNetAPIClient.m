//
//  iEver_AppDotNetAPIClient.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-15.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_AppDotNetAPIClient.h"


//#ifdef DEBUG
//static NSString * const APIBaseURLString = @"http://api.meetnana.com/"; //内网  阿里云服务器 iever ：http://114.215.173.39:8080/   内网地址：http://192.168.1.110:8082/
//#else
//static NSString * const APIBaseURLString = @"http://api.meetnana.com/;
//#endif
@implementation iEver_AppDotNetAPIClient

/*
 * 单例 iEver_AppDotNetAPIClient
 */
+ (iEver_AppDotNetAPIClient *)sharedClient
{
    static iEver_AppDotNetAPIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{

       _sharedClient = [[iEver_AppDotNetAPIClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://192.168.1.110:8082/"]];
       //_sharedClient = [[iEver_AppDotNetAPIClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://api.meetnana.com/"]];
       });
      return _sharedClient;

}
/*
 * 初始化baseURL
 * 自定义request head
 *
 */
- (id)initWithBaseURL:(NSURL *)url
{

    self = [super initWithBaseURL:url];
    if (!self) {
        return nil;
    }
	[self registerHTTPOperationClass:[AFJSONRequestOperation class]];
    self.parameterEncoding = AFJSONParameterEncoding;
    return self;
}


- (RACSignal *)enqueueRequestWithMethod:(NSString *)method path:(NSString *)path parameters:(NSDictionary *)parameters
{

	return [self enqueueRequestWithMethod:method path:[NSString stringWithFormat:@"iever-api/%@",path] parameters:parameters timeOut:30.0];
    //return [self enqueueRequestWithMethod:method path:[NSString stringWithFormat:@"v110/%@",path] parameters:parameters timeOut:30.0];
}


- (RACSignal *)enqueueRequestWithMethod:(NSString *)method path:(NSString *)path parameters:(NSDictionary *)parameters timeOut:(double)time
{
    RACReplaySubject *subject = [RACReplaySubject subject];
    NSMutableURLRequest *request = [self requestWithMethod:method path:path parameters:parameters];

    request.timeoutInterval = time;

	AFHTTPRequestOperation *operation = [self HTTPRequestOperationWithRequest:request
																	  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                    NSLog(@"statusCode************** %@", @(operation.response.statusCode));
																		  NSLog(@"responseObject %@", operation.responseString);
																		  id json = [NSJSONSerialization JSONObjectWithData:responseObject
																													options:NSJSONReadingMutableContainers
																													  error:nil];
																		  [subject sendNext:json];
																		  [subject sendCompleted];

																	  } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
																		  // Use a cached response if it exists (iOS6 bug)
																		  NSLog(@"failure......... error : %@", error);
																		  [SVProgressHUD showImage:nil status:@"请求失败!"];

																		  NSCachedURLResponse *cachedResponse = [[NSURLCache sharedURLCache] cachedResponseForRequest:request];
																		  if (cachedResponse != nil && [[cachedResponse data] length] > 0){
																			  NSError *JSONError = nil;
																			  id JSON = [NSJSONSerialization JSONObjectWithData:cachedResponse.data options:0 error:&JSONError];
																			  if (!JSONError){
																				  [subject sendNext:JSON];
                                                                              } else {
																				  [subject sendError:error];
																			  }
																		  } else {
																			  [subject sendError:error];
																		  }

																		  [subject sendCompleted];
																	  }];

	[self enqueueHTTPRequestOperation:operation];

	return [subject deliverOn:[RACScheduler scheduler]];
}

@end
