//
//  iEver_Global.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-15.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_Global.h"

@implementation iEver_Global
//notification
NSString * const ReceiveRemoteNotification = @"ReceiveRemoteNotification";
/*
 * 单例 iEver_Global
 */
+(iEver_Global *)Instance
{
	static iEver_Global *instance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		instance = [[iEver_Global alloc] init];
	});
	return instance;
}

- (BOOL)isExistenceNetwork
{
    // Create zero addy
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;

    // Recover reachability flags
    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
    SCNetworkReachabilityFlags flags;

    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    CFRelease(defaultRouteReachability);

    if (!didRetrieveFlags) {
        return NO;
    }

    BOOL isReachable = flags & kSCNetworkFlagsReachable;
    BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;

    return (isReachable && !needsConnection) ? YES : NO;
}
/*
 * 解析数据成字典
 */
+ (NSDictionary *)parserToJsonDicData:(NSString *)responderStr
{

	DLog(@"responderStr : %@", responderStr);

	NSError *error = nil;
	NSData *data = [responderStr dataUsingEncoding:NSUTF8StringEncoding];

	NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:data
                                                            options:NSJSONReadingMutableContainers
                                                              error:&error];
	DLog(@"json dic %@", [jsonDic description]);

	return jsonDic;
}

/*
 * 解析数据成数组
 */
+ (NSArray *)parserToJsonArrayData:(NSString *)responderStr
{

	NSError *error = nil;
	NSData *data = [responderStr dataUsingEncoding:NSUTF8StringEncoding];

	NSArray *jsonArr = [NSJSONSerialization JSONObjectWithData:data
                                                       options:NSJSONReadingMutableContainers
                                                         error:&error];
	DLog(@"json array %@", [jsonArr description]);

	return jsonArr;
}


/*
 * 上传头像
 */
- (void)uploadImage:(UIImage *)image
{
	//NSDictionary *dic = @{@"memberId": [MTGlobal getPUUserInfoToLifeyoyoObject].memberID};
    NSDictionary *dic = @{@"memberId": @"2333333333333"};
	AFHTTPClient *httpClient = [iEver_AppDotNetAPIClient sharedClient];
	NSData *imageData = UIImageJPEGRepresentation(image, 0.8);
	NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"upload" parameters:dic constructingBodyWithBlock: ^(id formData) {
		[formData appendPartWithFileData:imageData name:@"avatar" fileName:@"avatar.jpg" mimeType:@"image/jpeg"];
	}];
	AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
	[operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
		NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
	}];
	[operation start];

	[operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"图片上传成功 === %@",operation.responseString);
        NSDictionary *dic = [iEver_Global parserToJsonDicData:operation.responseString];
        [[NSUserDefaults standardUserDefaults] setObject:[dic objectForKey:@"info"] forKey:@"USERAVATAR"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"headImageModifySusseedNotification" object:image];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failure: %@", error);
    }];
}


+ (NSString *)timeString
{

    NSString *timeSp = [NSString stringWithFormat:@"%lld", (long long)[[NSDate date] timeIntervalSince1970]*1000];

    NSLog(@"timeSp:%@",timeSp);
    return timeSp;
}

+ (NSString *)intervalSinceNow: (long long) theDate
{

    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval now=[dat timeIntervalSince1970]*1;
    NSString *timeString=@"";

    NSTimeInterval cha= theDate -now;

    if (cha <0) {

        return timeString = @"已结束";
    }

    if (cha/3600<1) {
        timeString = [NSString stringWithFormat:@"%f", cha/60];
        timeString = [timeString substringToIndex:timeString.length-7];
        timeString=[NSString stringWithFormat:@"%@分钟", timeString];

    }
    if (cha/3600>1&&cha/86400<1) {
        timeString = [NSString stringWithFormat:@"%f", cha/3600];
        timeString = [timeString substringToIndex:timeString.length-7];
        timeString=[NSString stringWithFormat:@"%@小时", timeString];
    }
    if (cha/86400>1)
    {
        timeString = [NSString stringWithFormat:@"%f", cha/86400];
        timeString = [timeString substringToIndex:timeString.length-7];
        timeString=[NSString stringWithFormat:@"%@天", timeString];

    }
    return timeString;
}
/*
 * 计算发布时间间隔
 */
+ (NSString *)commentIntervalSinceNow: (long long) theDate{

    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval now=[dat timeIntervalSince1970]*1;
    NSString *timeString=@"";

    NSTimeInterval cha= now - theDate;

    if (cha/3600<1) {
        timeString = [NSString stringWithFormat:@"%f", cha/60];
        timeString = [timeString substringToIndex:timeString.length-7];
        if ([timeString isEqualToString:@"0"]) {
            timeString=@"刚刚";
        }else{
            timeString=[NSString stringWithFormat:@"%@分钟前", timeString];
        }
    }
    if (cha/3600>1&&cha/86400<1) {
        timeString = [NSString stringWithFormat:@"%f", cha/3600];
        timeString = [timeString substringToIndex:timeString.length-7];
        timeString=[NSString stringWithFormat:@"%@小时前", timeString];
    }
    if (cha/86400>1)
    {
        timeString = [NSString stringWithFormat:@"%f", cha/86400];
        timeString = [timeString substringToIndex:timeString.length-7];
        timeString=[NSString stringWithFormat:@"%@天前", timeString];

    }
    return timeString;
}
- (BOOL)validateMobile:(NSString *)mobileNum
{
//    /**
//     * 手机号码
//     * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
//     * 联通：130,131,132,152,155,156,185,186
//     * 电信：133,1349,153,180,189
//     */
//    NSString * MOBILE = @"^1(3[0-9]|5[0-35-9]|8[025-9])\\d{8}$";
//    /**
//     10         * 中国移动：China Mobile
//     11         * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
//     12         */
//    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d)\\d{7}$";
//    /**
//     15         * 中国联通：China Unicom
//     16         * 130,131,132,152,155,156,185,186
//     17         */
//    NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
//    /**
//     20         * 中国电信：China Telecom
//     21         * 133,1349,153,180,189
//     22         */
//    NSString * CT = @"^1((33|53|8[09])[0-9]|349)\\d{7}$";
//    /**
//     25         * 大陆地区固话及小灵通
//     26         * 区号：010,020,021,022,023,024,025,027,028,029
//     27         * 号码：七位或八位
//     28         */
//    // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";

    NSString *str_C = @"^1[3456789][0-9]\\d{8}$";
//
//    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
//    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
//    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
//    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];

    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", str_C];

    if ([regextestct evaluateWithObject:mobileNum] == YES)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

/*
 * 验证错误类型
 */
- (NSString *)confirmationResultCode:(int)resultCode{

    NSString *returnStr;
    /**
     * 未知错误
     */
    if (resultCode == -9999) {
        returnStr = @"服务器开小差了";
    }/**
      * loginKey为空或错误
      */
    else if (resultCode == -1000) {
        returnStr = @"登录异常";
    }/**
      * 参数为空
      */
    else if (resultCode == -1001) {
        returnStr = @"参数为空";
    }/**
      * 参数解析异常
      */
    else if (resultCode == -1002) {
        returnStr = @"参数解析异常";
    }/**
      * 参数数据有误
      */
    else if (resultCode == -1003) {
        returnStr = @"参数数据有误";
    }
    /**
     * 获取上传token出错
     */
    else if (resultCode == -1004) {
        returnStr = @"获取上传token出错";
    }/**
      * 更新出错
      */
    else if (resultCode == -2002) {
        returnStr = @"提交失败";
    }
    /**
     * 重复录入
     */
    else if (resultCode == -2001) {
        returnStr = @"重复录入";
    }/**
      * 用户不存在
      */
    else if (resultCode == -3001) {
        returnStr = @"用户不存在";
    }/**
      * 用户名密码不匹配
      */
    else if (resultCode == -3002) {
        returnStr = @"用户名密码不匹配";
    }/**
      * 昵称为空
      */
    else if (resultCode == -3003) {
        returnStr = @"昵称为空";
    }/**
      * 昵称重复
      */
    else if (resultCode == -3004) {
        returnStr = @"昵称重复";
    }/**
      * email为空
      */
    else if (resultCode == -3005) {
        returnStr = @"email为空或格式错误";
    }/**
      * email已存在
      */
    else if (resultCode == -3006) {
        returnStr = @"email已存在";
    }/**
      * 手机号为空
      */
    else if (resultCode == -3007) {
        returnStr = @"手机号为空";
    }/**
      * 手机号已存在
      */
    else if (resultCode == -3008) {
        returnStr = @"手机号已存在";
    }/**
      * 注册信息不完整或有误
      */
    else if (resultCode == -3009) {
        returnStr = @"注册信息不完整或有误";
    }/**
      * 登陆key为空
      */
    else if (resultCode == -3010) {
        returnStr = @"登陆key为空";
    }
    /**
     * 用户手机号码未绑定
     */
    else if (resultCode == -3011) {
        returnStr = @"用户手机号码未绑定";
    }
    /**
     * 用户被禁用
     */
    else if (resultCode == -3012) {
        returnStr = @"用户被禁用";
    }
    /**
     * 验证码为空
     */
    else if (resultCode == -3013) {
        returnStr = @"验证码为空";
    }
    /**
     * 手机号码和校验码不匹配
     */
    else if (resultCode == -3014) {
        returnStr = @"手机号码和校验码不匹配";
    }
    /**
     * 密码错误
     */
    else if (resultCode == -3015) {
        returnStr = @"密码错误";
    }
    /**
     * 程序出错
     */
    else if (resultCode == -9999) {
        returnStr = @"服务器开小差了";
    }
    return returnStr;
}

/*
 * 获得当前时间是
 */
- (NSInteger)requestCurrentDate:(NSString *)mothod{

    int returnInt;
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *now;
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekdayCalendarUnit |
    NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    now=[NSDate date];
    comps = [calendar components:unitFlags fromDate:now];
    if([mothod isEqualToString:@"weekday"]){

        returnInt = [comps weekday];
    }else if([mothod isEqualToString:@"month"]){

        returnInt = [comps month];
    }else if([mothod isEqualToString:@"day"]){

        returnInt = [comps day];
    }else if([mothod isEqualToString:@"hour"]){

        returnInt = [comps hour];
    }else if([mothod isEqualToString:@"minute"]){

        returnInt = [comps minute];
    }else if([mothod isEqualToString:@"second"]){

        returnInt = [comps second];
    }
    else if([mothod isEqualToString:@"year"]){

        returnInt = [comps year];
    }
    return returnInt;
}



- (UIColor *) colorWithHexString: (NSString *)color
{
    NSString *cString = [[color stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];

    // String should be 6 or 8 characters
    if ([cString length] < 6) {
        return [UIColor clearColor];
    }

    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"])
        cString = [cString substringFromIndex:2];
    if ([cString hasPrefix:@"#"])
        cString = [cString substringFromIndex:1];
    if ([cString length] != 6)
        return [UIColor clearColor];

    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;

    //r
    NSString *rString = [cString substringWithRange:range];

    //g
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];

    //b
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];

    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];

    return [UIColor colorWithRed:((float) r / 255.0f) green:((float) g / 255.0f) blue:((float) b / 255.0f) alpha:1.0f];
}


/*
 函数描述：字符串筛选,去掉不需要的特殊字符串
 参数描述：target         原字符串
 replacement   需要替换的字符串
 options       默认传2:NSLiteralSearch,区分大小写
 _replaceArray 需要排除的Array
 返回值： 筛选完的String
 备注:   使用方法:replaceOccurrencesOfString:@"1(2*3" withString:@"" options:2 replaceArray:[NSArray arrayWithObjects:@"(",@"*", nil]
 输出:123
 */
+ (NSString *)replaceOccurrencesOfString:(NSString *)target withString:(NSString *)replacement options:(NSStringCompareOptions)options replaceArray:(NSArray *)_replaceArray {
    NSMutableString *tempStr = [NSMutableString stringWithString:target];
    NSArray *replaceArray = [NSArray arrayWithArray:_replaceArray];
    for(int i = 0; i < [replaceArray count]; i++){
        NSRange range = [target rangeOfString: [replaceArray objectAtIndex:i]];
        if(range.location != NSNotFound){
            [tempStr replaceOccurrencesOfString: [replaceArray objectAtIndex:i]
                                     withString: replacement
                                        options: options
                                          range: NSMakeRange(0, [tempStr length])];
        }
    }
    return tempStr;
}


/*
 * 判断是否开启推送
 */
+ (int)enabledRemoteNotificationTypes{

    return [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
}


//邮箱
+ (BOOL) validateEmail:(NSString *)email
{
    NSString *emailRegex = @"\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}


//手机号码验证
+ (BOOL) validateMobile:(NSString *)mobile
{
    //手机号以13， 15，18开头，八个 \d 数字字符
    NSString *phoneRegex = @"^((13[0-9])|(15[^4,\\D])|(18[0,0-9]))\\d{8}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    return [phoneTest evaluateWithObject:mobile];
}


//车牌号验证
+ (BOOL) validateCarNo:(NSString *)carNo
{
    NSString *carRegex = @"^[\u4e00-\u9fa5]{1}[a-zA-Z]{1}[a-zA-Z_0-9]{4}[a-zA-Z_0-9_\u4e00-\u9fa5]$";
    NSPredicate *carTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",carRegex];
    NSLog(@"carTest is %@",carTest);
    return [carTest evaluateWithObject:carNo];
}


//车型
+ (BOOL) validateCarType:(NSString *)CarType
{
    NSString *CarTypeRegex = @"^[\u4E00-\u9FFF]+$";
    NSPredicate *carTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",CarTypeRegex];
    return [carTest evaluateWithObject:CarType];
}


//用户名
+ (BOOL) validateUserName:(NSString *)name
{
    NSString *userNameRegex = @"^[A-Za-z0-9]{6,20}+$";
    NSPredicate *userNamePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",userNameRegex];
    BOOL B = [userNamePredicate evaluateWithObject:name];
    return B;
}


//密码
+ (BOOL) validatePassword:(NSString *)passWord
{
    NSString *passWordRegex = @"^[a-zA-Z0-9]{6,20}+$";
    NSPredicate *passWordPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",passWordRegex];
    return [passWordPredicate evaluateWithObject:passWord];
}


//昵称
+ (BOOL) validateNickname:(NSString *)nickname
{
    NSString *nicknameRegex = @"^[\u4e00-\u9fa5]{4,8}$";
    NSPredicate *passWordPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",nicknameRegex];
    return [passWordPredicate evaluateWithObject:nickname];
}
@end
