//
//  iEver_AppPostJsonAPIClient.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-11.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_AppPostJsonAPIClient.h"

@implementation iEver_AppPostJsonAPIClient

//#ifdef DEBUG
//static NSString * const APIBaseURLString = @"http://api.meetnana.com/"; //内网  阿里云服务器 iever ：http://114.215.173.39:8080/   内网地址：http://192.168.1.110:8082/
//#else
//static NSString * const APIBaseURLString = @"http://api.meetnana.com/"; //上线
//#endif

/*
 * 单例 iEver_AppDotNetAPIClient
 */
+ (iEver_AppPostJsonAPIClient *)sharedClient
{
    static iEver_AppPostJsonAPIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{

         _sharedClient = [[iEver_AppPostJsonAPIClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://192.168.1.110:8082/"]];
        // _sharedClient = [[iEver_AppPostJsonAPIClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://api.meetnana.com/"]];
    });
    return _sharedClient;
}

/*
 * 初始化baseURL
 * 自定义request head
 *
 */
- (id)initWithBaseURL:(NSURL *)url
{

    self = [super initWithBaseURL:url];
    if (!self) {
        return nil;
    }
	[self registerHTTPOperationClass:[AFJSONRequestOperation class]];
    self.parameterEncoding = AFJSONParameterEncoding;
    [self setDefaultHeader:@"Content-Type" value:@"application/json"];
    return self;
}

- (RACSignal *)enqueueRequestWithMethod:(NSString *)method path:(NSString *)path parameters:(NSDictionary *)parameters
{

     return [self enqueueRequestWithMethod:method path:[NSString stringWithFormat:@"iever-api/%@",path] parameters:parameters timeOut:30.0];
    //return [self enqueueRequestWithMethod:method path:[NSString stringWithFormat:@"v110/%@",path] parameters:parameters timeOut:30.0];
}


- (RACSignal *)enqueueRequestWithMethod:(NSString *)method path:(NSString *)path parameters:(NSDictionary *)parameters timeOut:(double)time
{
    RACReplaySubject *subject = [RACReplaySubject subject];
    NSMutableURLRequest *request = [self requestWithMethod:method path:path parameters:parameters];

    NSMutableDictionary *cookieProperties = [NSMutableDictionary dictionary];
    [cookieProperties setObject:[[NSUserDefaults standardUserDefaults] objectForKey: @"loginKey"] forKey:@"loginKey"];
    [cookieProperties setObject:[[NSUserDefaults standardUserDefaults] objectForKey: @"userId"] forKey:@"userId"];
    NSHTTPCookie * userCookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:userCookie];


    NSURL *dataUrl = [NSURL URLWithString:path relativeToURL:self.baseURL];
    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:dataUrl];//id: NSHTTPCookie
    NSDictionary *sheaders = [NSHTTPCookie requestHeaderFieldsWithCookies:cookies];
    [request setAllHTTPHeaderFields:sheaders];

    
	NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:DeviceToken];
	NSString *memberID = @"0";
    NSString *currentAppVersion = [[NSBundle mainBundle] infoDictionary][@"CFBundleVersion"];
	NSString *systemVersion = [[UIDevice currentDevice] systemVersion];
	NSString *value = [NSString stringWithFormat:@"%@/%@/iOS%@ %@", token, memberID, currentAppVersion, systemVersion];


    NSString * encodingString = [value stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //	DLog(@"value   %@", value);
	[request setValue:encodingString forHTTPHeaderField:@"User-Agent"];
	request.timeoutInterval = time;
	//[request setCachePolicy:NSURLRequestReturnCacheDataElseLoad];

	AFHTTPRequestOperation *operation = [self HTTPRequestOperationWithRequest:request
																	  success:^(AFHTTPRequestOperation *operation, id responseObject) {

                                                                          NSDictionary *dictionary = operation.response.allHeaderFields;
                                                                          NSLog(@"response cookies:%@",dictionary);

                                                                          NSLog(@"statusCode******************************** %@", @(operation.response.statusCode));
																		  NSLog(@"responseObject %@", operation.responseString);
																		  id json = [NSJSONSerialization JSONObjectWithData:responseObject
																													options:NSJSONReadingMutableContainers
																													  error:nil];
																		  [subject sendNext:json];
																		  [subject sendCompleted];

																	  } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
																		  // Use a cached response if it exists (iOS6 bug)
																		  NSLog(@"failure....123..... error : %@", error);
																		  //[SVProgressHUD showImage:nil status:@"请求失败!"];

																		  NSCachedURLResponse *cachedResponse = [[NSURLCache sharedURLCache] cachedResponseForRequest:request];
																		  if (cachedResponse != nil && [[cachedResponse data] length] > 0){
																			  NSError *JSONError = nil;
																			  id JSON = [NSJSONSerialization JSONObjectWithData:cachedResponse.data options:0 error:&JSONError];
																			  if (!JSONError){
																				  [subject sendNext:JSON];
                                                                              } else {
																				  [subject sendError:error];
																			  }
																		  } else {
																			  [subject sendError:error];
																		  }
                                                                          
																		  [subject sendCompleted];
																	  }];
    
	[self enqueueHTTPRequestOperation:operation];
    
	return [subject deliverOn:[RACScheduler scheduler]];
}

@end
