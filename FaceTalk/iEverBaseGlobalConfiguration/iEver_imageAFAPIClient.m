//
//  iEver_imageAFAPIClient.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-13.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_imageAFAPIClient.h"
#import "AFJSONRequestOperation.h"
@implementation iEver_imageAFAPIClient

+ (iEver_imageAFAPIClient *)sharedClient:(NSString *)url {
    static iEver_imageAFAPIClient *_sharedClient = nil;
    _sharedClient = [[iEver_imageAFAPIClient alloc] initWithBaseURL:[NSURL URLWithString:url]];
    return _sharedClient;}
- (id)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if (!self) {
        return nil;
    }
    [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
    self.parameterEncoding = AFJSONParameterEncoding;
    return self;
}

@end
