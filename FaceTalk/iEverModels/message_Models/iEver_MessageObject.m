//
//  iEver_MessageObject.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-7.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_MessageObject.h"

@implementation iEver_MessageObject

- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }
    self.resultCode                         = [[attributes objectForKey:@"resultCode"] integerValue];
    /* coverList object  */
    NSMutableArray *array_messageList_object = [[NSMutableArray alloc] initWithCapacity:0];
    array_messageList_object                 = [attributes objectForKey:@"msgList"];
    self._messageList                    = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_messageList_object count]; i++) {
        iEver_messageList_object *message_object = [[iEver_messageList_object alloc] initWithAttributes:[array_messageList_object objectAtIndex:i]];
        [self._messageList addObject:message_object];
    }

    return self;
}


/* 消息列表 */
- (RACSignal *)messageList:(NSDictionary *)dic path:(NSString *)pathUrl{

    return [[[iEver_AppDotNetAPIClient sharedClient] enqueueRequestWithMethod:@"GET"
                                                                           path:pathUrl
                                                                     parameters:dic
             ]

            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                iEver_MessageObject * object = [[iEver_MessageObject alloc] initWithAttributes:responseObject];
                return object;
            }];

}

/* 设定消息已读 */
- (RACSignal *)readedMessage:(NSDictionary *)dic path:(NSString *)pathUrl{

    return [[[iEver_AppDotNetAPIClient sharedClient] enqueueRequestWithMethod:@"GET"
                                                                         path:pathUrl
                                                                   parameters:dic
             ]

            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
}
@end
/* quesList object  */
@implementation iEver_messageList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.M_id              = [[attributes objectForKey:@"id"] integerValue];
    self.userId            = [[attributes objectForKey:@"userId"] integerValue];
    self.businessId        = [[attributes objectForKey:@"businessId"] integerValue];
    self.type              = [[attributes objectForKey:@"type"] integerValue];
    self.sendUserId        = [[attributes objectForKey:@"sendUserId"] integerValue];
    self.msgContent        = [attributes objectForKey:@"msgContent"];
    self.readStatus        = [[attributes objectForKey:@"readStatus"] integerValue];
    self.status            = [[attributes objectForKey:@"status"] integerValue];
    self.createTime        = [[attributes objectForKey:@"createTime"] longLongValue];
    self.updateTime        = [[attributes objectForKey:@"updateTime"] longLongValue];
    self.userHeadImg       = [attributes objectForKey:@"userHeadImg"];
    self.sendUserNickName  = [attributes objectForKey:@"sendUserNickName"];
    return self;
}
@end
