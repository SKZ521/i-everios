//
//  iEver_MessageObject.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-7.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iEver_MessageObject : NSObject
@property (nonatomic, assign) int                        resultCode;       /* resultCode */
@property (nonatomic, retain) NSMutableArray             *_messageList;
/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes ;
/* 消息列表 */
- (RACSignal *)messageList:(NSDictionary *)dic path:(NSString *)pathUrl;

/* 设定消息已读 */
- (RACSignal *)readedMessage:(NSDictionary *)dic path:(NSString *)pathUrl;
@end
/* quesList object  */
@interface iEver_messageList_object : NSObject

@property (nonatomic, assign) int                        M_id;          /* 消息ID */
@property (nonatomic, assign) int                        userId;        /* 用户ID */
@property (nonatomic, assign) int                        businessId;    /* 业务ID */
@property (nonatomic, assign) int                        type;          /* 类型ID */
@property (nonatomic, assign) int                        sendUserId;    /* 发送消息ID */
@property (nonatomic, copy  ) NSString                   *msgContent;   /* 问题内容 */
@property (nonatomic, assign) int                        readStatus;    /* 消息是否阅读状态 */
@property (nonatomic, assign) int                        status;        /* 状态 */
@property (nonatomic, assign) long long                  createTime;    /* 创建时间 */
@property (nonatomic, assign) long long                  updateTime;    /* 更新时间 */
@property (nonatomic, copy  ) NSString                   *userHeadImg;  /* 用户头像 */
@property (nonatomic, copy  ) NSString                   *sendUserNickName;  /* 用户昵称 */

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes ;
@end
