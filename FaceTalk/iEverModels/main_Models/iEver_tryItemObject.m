//
//  iEver_tryItemObject.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-30.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_tryItemObject.h"

@implementation iEver_tryItemObject

- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }

    self.pageSize                            = [[attributes objectForKey:@"pageSize"] integerValue];
    self.resultCode                          = [[attributes objectForKey:@"resultCode"] integerValue];
    self._itemTryList                        = [[NSMutableArray alloc] initWithCapacity:0];

    NSMutableArray *array_itemTryList_object = [[NSMutableArray alloc] init];
    array_itemTryList_object                 = [attributes objectForKey:@"itemTryList"];
    for (int i = 0; i < [array_itemTryList_object count]; i ++) {
        iEver_itemTryList_object * content_object = [[iEver_itemTryList_object alloc] initWithAttributes:[array_itemTryList_object objectAtIndex:i]];
        [self._itemTryList addObject:content_object];
    }
	return self;
}

/* 商品排行榜接口 */
- (RACSignal *)questTryItemData:(NSDictionary *)dic path:(NSString *)pathUrl{

    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"Get"
                                                                           path:pathUrl
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                iEver_tryItemObject * object = [[iEver_tryItemObject alloc] initWithAttributes:responseObject];
                return object;
            }];
}

@end



/* itemTopList object */
@implementation iEver_itemTryList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }
    self.T_ID                  = [[attributes objectForKey:@"id"] integerValue];
    self.tryImg                = [attributes objectForKey:@"tryImg"];
    self.tryName               = [attributes objectForKey:@"tryName"];
    self.itemId                = [[attributes objectForKey:@"itemId"] integerValue];
    self.startTime             = [[attributes objectForKey:@"startTime"] longLongValue];
    self.endTime               = [[attributes objectForKey:@"endTime"] longLongValue];
    self.tryNum                = [[attributes objectForKey:@"tryNum"] integerValue];
    self.tryExplain            = [attributes objectForKey:@"tryExplain"];
    self.status                = [[attributes objectForKey:@"status"] integerValue];
    self.applyTotal            = [[attributes objectForKey:@"applyTotal"] integerValue];
    self.isApply               = [[attributes objectForKey:@"isApply"] integerValue];
    self.itemName              = [attributes objectForKey:@"itemName"];
    self.itemImg               = [attributes objectForKey:@"itemImg"];
    self.price                 = [[attributes objectForKey:@"price"] integerValue];



	return self;
}

@end
