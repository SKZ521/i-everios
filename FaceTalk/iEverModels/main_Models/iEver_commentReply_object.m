//
//  iEver_commentReply_object.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/5/15.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_commentReply_object.h"

@implementation iEver_commentReply_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    /* acList object  */
    NSMutableArray *array_acList_object = [[NSMutableArray alloc] initWithCapacity:0];
    self.pageSize                         = [[attributes objectForKey:@"pageSize"] integerValue];
    self.resultCode                       = [[attributes objectForKey:@"resultCode"] integerValue];
    array_acList_object                   = [attributes objectForKey:@"acList"];
    self._acList                          = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_acList_object count]; i++) {
        iEver_acList_object *acList_object = [[iEver_acList_object alloc] initWithAttributes:[array_acList_object objectAtIndex:i]];
        [self._acList addObject:acList_object];
    }

    return self;
}

/* 评论回复 */
- (RACSignal *)queryCommentReply:(NSDictionary *)dic path:(NSString *)pathUrl{

    return [[[iEver_AppDotNetAPIClient sharedClient] enqueueRequestWithMethod:@"GET"
                                                                         path:pathUrl
                                                                   parameters:dic
             ]

            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                iEver_commentReply_object * object = [[iEver_commentReply_object alloc] initWithAttributes:responseObject];
                return object;
            }];
    
}


@end


/* _acList object */
@implementation iEver_acList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.acObject_id          = [[attributes objectForKey:@"id"] integerValue];
    self.cover_id             = [[attributes objectForKey:@"coverId"] integerValue];
    self.parent_id            = [[attributes objectForKey:@"parentId"] integerValue];
    self.userId               = [[attributes objectForKey:@"userId"] integerValue];
    self.atUserId             = [[attributes objectForKey:@"atUserId"] integerValue];
    self.commentContent       = [attributes  objectForKey:@"commentContent"];
    self.type                 = [[attributes objectForKey:@"type"] integerValue];
    self.status               = [[attributes objectForKey:@"status"] integerValue];
    self.createTime           = [[attributes objectForKey:@"createTime"] longLongValue];
    self.updateTime           = [[attributes objectForKey:@"updateTime"] longLongValue];
    self.nickName             = [attributes  objectForKey:@"nickName"];
    self.atNickName           = [attributes  objectForKey:@"atNickName"];
    self.headImg              = [attributes  objectForKey:@"headImg"];
    self.feature              = [attributes  objectForKey:@"feature"];
    self.likeTotal            = [[attributes objectForKey:@"likeTotal"] integerValue];

    return self;
}

@end

