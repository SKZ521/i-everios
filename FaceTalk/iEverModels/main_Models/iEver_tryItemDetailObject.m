//
//  iEver_tryItemDetailObject.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-30.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_tryItemDetailObject.h"

@implementation iEver_tryItemDetailObject

- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }
    self._itemDetailObject = [[iEver_itemTry_object alloc] initWithAttributes:[attributes objectForKey:@"itemTry"]];

    self._itemTryDetailArray = [[NSMutableArray alloc] initWithCapacity:0];
    //试用品名称
    NSString *nameStr = [NSString stringWithFormat:@"试用品    : %@",self._itemDetailObject.tryName];
    [self._itemTryDetailArray addObject:nameStr];
    //试用申请时间
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY.MM.dd"];
    NSString *startTimeStr = [NSString stringWithFormat:@"%lld",self._itemDetailObject.startTime/1000];
    NSString *endTimeStr = [NSString stringWithFormat:@"%lld",self._itemDetailObject.endTime/1000];
    NSTimeInterval startTimeInterval =[startTimeStr doubleValue];
    NSTimeInterval endTimeInterval =[endTimeStr doubleValue];
    NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:startTimeInterval];
    NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:endTimeInterval];
    NSString *startTime = [formatter stringFromDate:startDate];
    NSString *endTime = [formatter stringFromDate:endDate];
    NSString *timeStr = [NSString stringWithFormat:@"申请时间 : %@ - %@",startTime,endTime];
    [self._itemTryDetailArray addObject:timeStr];
    //试用数量
    NSString *numStr = [NSString stringWithFormat:@"申请数量 : %d",self._itemDetailObject.tryNum];
    [self._itemTryDetailArray addObject:numStr];
    //试用说明
    NSString *explainStr = [NSString stringWithFormat:@"申请说明 : %@",self._itemDetailObject.tryExplain];
    [self._itemTryDetailArray addObject:explainStr];
    /* successList object  */
    NSMutableArray *array_successList_object = [[NSMutableArray alloc] init];
    array_successList_object                 = [attributes objectForKey:@"successList"];
    self._itemTryNameList                    = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_successList_object count]; i++) {
        iEver_successList_object *content_object = [[iEver_successList_object alloc] initWithAttributes:[array_successList_object objectAtIndex:i]];
        [self._itemTryNameList addObject:content_object];
    }
     return self;
}
/* 查询试用申请信息 */
- (RACSignal *)itemTryApplyQueryByUserId:(NSDictionary *)dic path:(NSString *)pathUrl{

    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"Get"
                                                                           path:pathUrl
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                iEver_itemTryApply_object * object;

                if ([[responseObject objectForKey:@"itemTryApply"] isKindOfClass:[NSDictionary class]]) {
                    object = [[iEver_itemTryApply_object alloc] initWithAttributes:[responseObject objectForKey:@"itemTryApply"]];
                }else{

                    object = [[iEver_itemTryApply_object alloc] init];
                    object.itemTryApply = @"NULL";

                }

                return object;
            }];

}
/* 根据试用id查询 */
- (RACSignal *)queryByItemTryId:(NSDictionary *)dic path:(NSString *)pathUrl{


    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"Get"
                                                                           path:pathUrl
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                iEver_tryItemDetailObject * object = [[iEver_tryItemDetailObject alloc] initWithAttributes:responseObject];
                return object;
            }];

}


/* 申请试用 */
- (RACSignal *)itemTryApply:(NSDictionary *)dic path:(NSString *)pathUrl{

    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:@"/itemTryApply/insert"
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
    
}

@end


/* item object */
@implementation iEver_itemTry_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }
    self.T_ID                  = [[attributes objectForKey:@"id"] integerValue];
    self.tryImg                = [attributes objectForKey:@"tryImg"];
    self.tryName               = [attributes objectForKey:@"tryName"];
    self.itemId                = [[attributes objectForKey:@"itemId"] integerValue];
    self.startTime             = [[attributes objectForKey:@"startTime"] longLongValue];
    self.endTime               = [[attributes objectForKey:@"endTime"] longLongValue];
    self.tryNum                = [[attributes objectForKey:@"tryNum"] integerValue];
    self.tryExplain            = [attributes objectForKey:@"tryExplain"];
    self.status                = [[attributes objectForKey:@"status"] integerValue];
    self.applyTotal            = [[attributes objectForKey:@"applyTotal"] integerValue];
    self.isApply               = [[attributes objectForKey:@"isApply"] integerValue];
    self.itemName              = [attributes objectForKey:@"itemName"];
    self.webUrl                = [attributes objectForKey:@"webUrl"];
    self.itemImg               = [attributes objectForKey:@"itemImg"];
    self.price                 = [[attributes objectForKey:@"price"] integerValue];
    self.itemSpec              = [attributes objectForKey:@"itemSpec"];

	return self;
}

@end

/* item object */
@implementation iEver_successList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.S_ID                  = [[attributes objectForKey:@"id"] integerValue];
    self.nickName              = [attributes objectForKey:@"nickName"];
    self.headImg               = [attributes objectForKey:@"headImg"];
    self.point                 = [[attributes objectForKey:@"point"] integerValue];
    self.createTime            = [[attributes objectForKey:@"createTime"] longLongValue];
    self.feature               = [attributes objectForKey:@"feature"];
    self.commentContent        = [attributes objectForKey:@"commentContent"];

    return self;
}

@end


/* item object */
@implementation iEver_itemTryApply_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.realName              = [attributes objectForKey:@"realName"];
    self.address               = [attributes objectForKey:@"address"];
    self.phone               = [attributes objectForKey:@"phone"];

    return self;
}

@end