//
//  iEver_tryItemObject.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-30.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iEver_tryItemObject : NSObject

@property (nonatomic, assign) int                        resultCode;       /* resultCode */

@property (nonatomic, assign) int                        pageSize;        /* pageSize */

/*  试用列表 */
@property (nonatomic, retain) NSMutableArray             *_itemTryList;

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;

/* 查询试用列表接口 */
- (RACSignal *)questTryItemData:(NSDictionary *)dic path:(NSString *)pathUrl;
@end


/* itemTryList object */
@interface iEver_itemTryList_object : NSObject

@property (nonatomic, assign) int                        T_ID;                 /* 数据id */
@property (nonatomic, copy  ) NSString                   *tryImg;              /* 活动图片 */
@property (nonatomic, copy  ) NSString                   *tryName;             /* 活动名称 */
@property (nonatomic, assign) int                        itemId;               /* 商品ID */
@property (nonatomic, assign) long long                  startTime;            /* 开始时间 */
@property (nonatomic, assign) long long                  endTime;              /* 结束时间 */
@property (nonatomic, assign) int                        tryNum;               /* 试用个数 */
@property (nonatomic, copy  ) NSString                   *tryExplain;          /* 活动描述 */
@property (nonatomic, assign) int                        status;               /* 活动状态 0正常状态、-1删除、10已结束 */
@property (nonatomic, assign) int                        applyTotal;           /* 申请个数 */
@property (nonatomic, assign) int                        isApply;              /* 是否申请 */
@property (nonatomic, copy  ) NSString                   *itemName;            /* 商品名称 */
@property (nonatomic, copy  ) NSString                   *itemImg;             /* 商品图片 */
@property (nonatomic, assign) int                        price;                /* 使用价格 */

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;

@end
