//
//  iEver_mainDetail_Object.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-6.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_mainDetail_Object.h"
#import "iEver_imageAFAPIClient.h"
#import "JSONKit.h"
#import "AFJSONRequestOperation.h"
#import "AFNetworking.h"

@implementation iEver_mainDetail_Object

- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }
    self._arrayCountnumber = 0;
    self.pageSize                         = [[attributes objectForKey:@"pageSize"] integerValue];
    self.resultCode                       = [[attributes objectForKey:@"resultCode"] integerValue];
    self.articlecommentTotal              = [[attributes objectForKey:@"articlecommentTotal"] integerValue];

    /* _itemList object */
    NSMutableArray *array_item_object = [[NSMutableArray alloc] init];
    array_item_object                 = [attributes objectForKey:@"itemList"];
    self._itemList                    = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_item_object count]; i++) {
        iEver_detail_itemList_object *category_object = [[iEver_detail_itemList_object alloc] initWithAttributes:[array_item_object objectAtIndex:i]];
        [self._itemList addObject:category_object];
    }

    if ([self._itemList count] > 0) {
        self._arrayCountnumber ++;
    }

    /* _articleCover object  */
    iEver_detail_articleCover_object *articleCover_object = [[iEver_detail_articleCover_object alloc] initWithAttributes:[attributes objectForKey:@"articleCover"]];
    self._articleCover = articleCover_object;


     /* _picList object  */
    NSMutableArray *array_picList_object = [[NSMutableArray alloc] init];
    array_picList_object                 = [attributes objectForKey:@"picList"];
    self._picList                        = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_picList_object count]; i++) {
        iEver_detail_picList_object *content_object = [[iEver_detail_picList_object alloc] initWithAttributes:[array_picList_object objectAtIndex:i]];
        [self._picList addObject:content_object];
     }
    if ([self._picList count] > 0) {
        self._arrayCountnumber ++;
    }

    /* recommendCoverList object  */
    NSMutableArray *array_videoList_object = [[NSMutableArray alloc] init];
    array_videoList_object                 = [attributes objectForKey:@"recommendCoverList"];
    self._recommendCoverList                        = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_videoList_object count]; i++) {
        iEver_detail_recommendCoverList_object *content_object = [[iEver_detail_recommendCoverList_object alloc] initWithAttributes:[array_videoList_object objectAtIndex:i] ];
        [self._recommendCoverList addObject:content_object];
    }
    if ([self._recommendCoverList count] > 0) {
        self._arrayCountnumber ++;
    }

    /* articleCommentList object  */
    NSMutableArray *array_articleCommentList_object = [[NSMutableArray alloc] init];
    array_articleCommentList_object                 = [attributes objectForKey:@"articleCommentList"];
    self._commentList                        = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_articleCommentList_object count]; i++) {
        iEver_detail_articleCommentList_object *content_object = [[iEver_detail_articleCommentList_object alloc] initWithAttributes:[array_articleCommentList_object objectAtIndex:i]];
        [self._commentList addObject:content_object];
    }
    if ([self._commentList count] > 0) {
        self._arrayCountnumber ++;
    }
    //self._arrayCountnumber = 4;
	return self;
}

/*  content LIST Object array */
- (RACSignal *)acquireDetailData:(NSDictionary *)dic path:(NSString *)pathUrl{

    /* restful request URL article/queryCoverById/15 */
    return [[[iEver_AppDotNetAPIClient sharedClient] enqueueRequestWithMethod:@"GET"
                                                                         path:pathUrl
                                                                   parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                iEver_mainDetail_Object * object = [[iEver_mainDetail_Object alloc] initWithAttributes:responseObject];
                return object;
            }];
}

@end

/* _itemList object */
@implementation iEver_detail_itemList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }
    self.Item_id           = [[attributes objectForKey:@"id"] integerValue];
    self.rootCategoryId    = [[attributes objectForKey:@"rootCategoryId"] integerValue];
    self.secondCategoryId  = [[attributes objectForKey:@"secondCategoryId"] integerValue];
    self.threeCategoryId   = [[attributes objectForKey:@"threeCategoryId"] integerValue];
    self.leafLevel         = [[attributes objectForKey:@"leafLevel"] integerValue];
    self.itemName          = [attributes  objectForKey:@"itemName"];
    self.itemDesc          = [attributes  objectForKey:@"itemDesc"];
    self.itemColor         = [attributes  objectForKey:@"itemColor"];
    self.itemSpec          = [attributes  objectForKey:@"itemSpec"];
    self.itemImg           = [attributes  objectForKey:@"itemImg"];
    self.price             = [[attributes objectForKey:@"price"] integerValue];
    self.itemLink          = [attributes  objectForKey:@"itemLink"];
    self.videoLink         = [attributes  objectForKey:@"videoLink"];
    self.videoDesc         = [attributes  objectForKey:@"videoDesc"];
    self.sortLevel         = [attributes  objectForKey:@"sortLevel"];
    self.pvTotal           = [[attributes objectForKey:@"pvTotal"] integerValue];
    self.status            = [[attributes objectForKey:@"status"] integerValue];
    self.itemTryId         = [[attributes objectForKey:@"itemTryId"] integerValue];
    self.createTime        = [[attributes objectForKey:@"createTime"] longLongValue];
    self.updateTime        = [[attributes objectForKey:@"updateTime"] longLongValue];
	return self;
}

@end


/* _articleCover object  */
@implementation iEver_detail_articleCover_object

- (BOOL)isNilObj:(id)obj {

    if (!obj) {
        return NO;
    }
    if ([obj  isKindOfClass:[NSNull  class]]) {
        return NO;
    }
    NSString  *str = [NSString  stringWithFormat:@"%@",obj];
    if ([str length] > 0) {
        return YES;
    }
    return NO;

}
- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }
    self.Cover_id             = [[attributes objectForKey:@"id"] integerValue];
    self.rootCategoryId       = [[attributes objectForKey:@"rootCategoryId"] integerValue];
    self.secondCategoryId     = [[attributes objectForKey:@"secondCategoryId"] integerValue];
    self.threeCategoryId      = [[attributes objectForKey:@"threeCategoryId"] integerValue];
    self.articleTitle         = [attributes  objectForKey:@"articleTitle"];
    self.articleDesc          = [attributes  objectForKey:@"articleDesc"];
    self.coverImg             = [attributes  objectForKey:@"coverImg"];
    self.coverDesc            = [attributes  objectForKey:@"coverDesc"];
    self.releaseStatus        = [[attributes objectForKey:@"releaseStatus"] integerValue];
    self.timerDate            = [[attributes objectForKey:@"timerDate"] longLongValue];
    self.type                 = [[attributes objectForKey:@"type"] integerValue];
    self.authorUserId         = [[attributes objectForKey:@"authorUserId"] integerValue];
    self.authorNickName       = [attributes  objectForKey:@"authorNickName"];
    self.status               = [[attributes objectForKey:@"status"] integerValue];
    self.videoLink            = [attributes  objectForKey:@"videoLink"];
    self.webUrl               = [attributes  objectForKey:@"webUrl"];
    self.V_videoLink          = @"";
    if (self.videoLink.length > 3) {

        NSString   *requestURL = [[NSString  stringWithFormat:@"http://vpface.flvcd.com/talk_ios.php?url=%@&format=super",self.videoLink]                                                   stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableURLRequest *request = [[iEver_imageAFAPIClient sharedClient:requestURL] requestWithMethod:@"GET" path:nil parameters:nil];
        AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [requestOperation start];
        [requestOperation waitUntilFinished];
        NSDictionary *result = [requestOperation.responseString objectFromJSONStringWithParseOptions:JKParseOptionLooseUnicode];
        if([[result  objectForKey:@"TYPE"]  isEqualToString:@"DIRECT"]) {
            NSArray  *playList = [result  objectForKey:@"V"];
            if ([playList  count] >= 1) {

                NSDictionary  *playUnit = [playList  lastObject];
                if ([self  isNilObj:[playUnit   objectForKey:@"U"]]) {
                    self.V_videoLink = [playUnit   objectForKey:@"U"];
                    //self.V_videoLink = @"http://7xih0q.com2.z0.glb.qiniucdn.com/201504080004.mp4";

                    if (self.V_videoLink.length == 0) {
                        self.V_videoLink = [attributes objectForKey:@"mVideoLink"];
                    }
                }
            }
        }
    }else{
        self.V_videoLink = [attributes objectForKey:@"mVideoLink"];
    }
    self.createTime           = [[attributes objectForKey:@"createTime"] longLongValue];
    self.updateTime           = [[attributes objectForKey:@"updateTime"] longLongValue];
    self.pvTotal              = [[attributes objectForKey:@"pvTotal"] integerValue];
    self.likeTotal            = [[attributes objectForKey:@"likeTotal"] integerValue];
    self.webPvTotal            = [[attributes objectForKey:@"webPvTotal"] integerValue];
    self.commentTotal         = [[attributes objectForKey:@"commentTotal"] integerValue];
    if ([[attributes objectForKey:@"isLike"] integerValue] >0) {
        self._selected            = YES;
    }else{
        self._selected            = NO;
    }
    self.isBrowse             = [[attributes objectForKey:@"isBrowse"] integerValue];
    self.isComment            = [[attributes objectForKey:@"isComment"] integerValue];
    /* V_person add */
    self.tags                = [attributes  objectForKey:@"tags"];
    self.userId              = [[attributes objectForKey:@"userId"] integerValue];
    self.releaseTime         = [[attributes objectForKey:@"releaseTime"] longLongValue];
    self.nickName            = [attributes  objectForKey:@"nickName"];
    self.headImg             = [attributes  objectForKey:@"headImg"];
    self.categoryName        = [attributes  objectForKey:@"categoryName"];

    /* _tagsList object */
    NSMutableArray *array_tags_object = [[NSMutableArray alloc] init];
    array_tags_object                 = [attributes objectForKey:@"tagsList"];
    self._tagsList                    = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_tags_object count]; i++) {
        iEver_detail_tagsList_object *tags_object = [[iEver_detail_tagsList_object alloc] initWithAttributes:[array_tags_object objectAtIndex:i]];
        [self._tagsList addObject:tags_object];
    }

	return self;
}
@end

/* _tagsList object */
@implementation iEver_detail_tagsList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.tagId                = [[attributes objectForKey:@"tagId"] integerValue];
    self.businessId           = [[attributes objectForKey:@"businessId"] integerValue];
    self.tagName              = [attributes  objectForKey:@"tagName"];

    return self;
}

@end

/* _picList object  */
@implementation iEver_detail_picList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }

    self.Pic_id               = [[attributes objectForKey:@"id"] integerValue];
    self.articleCoverId       = [[attributes objectForKey:@"articleCoverId"] integerValue];
    self.imgUrl               = [attributes  objectForKey:@"imgUrl"];
    self.image_width          = 0;
    self.image_height         = 0;
    self.image_width          = [[attributes objectForKey:@"imgWidth"] integerValue];
    self.image_height         = [[attributes objectForKey:@"imgHeight"] integerValue];
    self.imgDesc              = [attributes  objectForKey:@"imgDesc"];
    self.imgTitle             = [attributes  objectForKey:@"imgTitle"];
    self.sortLevel            = [[attributes objectForKey:@"sortLevel"] integerValue];
    self.status               = [[attributes objectForKey:@"status"] integerValue];
    self.createTime           = [[attributes objectForKey:@"createTime"] longLongValue];
    self.updateTime           = [[attributes objectForKey:@"updateTime"] longLongValue];
	return self;
}
@end


/* recommendCoverList object  */
@implementation iEver_detail_recommendCoverList_object
- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }
    self.RecomCover_id        = [[attributes objectForKey:@"id"] integerValue];
    self.rootCategoryId       = [[attributes objectForKey:@"rootCategoryId"] integerValue];
    self.secondCategoryId     = [[attributes objectForKey:@"secondCategoryId"] integerValue];
    self.threeCategoryId      = [[attributes objectForKey:@"threeCategoryId"] integerValue];
    self.articleTitle         = [attributes  objectForKey:@"articleTitle"];
    self.articleDesc          = [attributes  objectForKey:@"articleDesc"];
    self.coverImg             = [attributes  objectForKey:@"coverImg"];
    self.coverDesc            = [attributes  objectForKey:@"coverDesc"];
    self.releaseStatus        = [[attributes objectForKey:@"releaseStatus"] integerValue];
    self.timerDate            = [[attributes objectForKey:@"timerDate"] longLongValue];
    self.type                 = [[attributes objectForKey:@"type"] integerValue];
    self.authorUserId         = [[attributes objectForKey:@"authorUserId"] integerValue];
    self.authorNickName       = [attributes  objectForKey:@"authorNickName"];
    self.status               = [[attributes objectForKey:@"status"] integerValue];
    self.videoLink            = [attributes  objectForKey:@"videoLink"];
    self.videoDesc            = [attributes  objectForKey:@"videoDesc"];
    self.itemId               = [[attributes objectForKey:@"itemId"] integerValue];
    self.createTime           = [[attributes objectForKey:@"createTime"] longLongValue];
    self.updateTime           = [[attributes objectForKey:@"updateTime"] longLongValue];
    self.pvTotal              = [[attributes objectForKey:@"pvTotal"] integerValue];
	return self;
}


@end

/* _articleCommentList object */
@implementation iEver_detail_articleCommentList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }
    self.Comment_id           = [[attributes objectForKey:@"id"] integerValue];
    self.userId               = [[attributes objectForKey:@"userId"] integerValue];
    self.commentContent       = [attributes  objectForKey:@"commentContent"];
    self.feature              = [attributes  objectForKey:@"feature"];
    self.type                 = [[attributes objectForKey:@"type"] integerValue];
    self.replyTotal           = [[attributes objectForKey:@"replyTotal"] integerValue];
    self.status               = [[attributes objectForKey:@"status"] integerValue];
    self.nickName             = [attributes  objectForKey:@"nickName"];
    self.headImg              = [attributes  objectForKey:@"headImg"];
    self.createTime           = [[attributes objectForKey:@"createTime"] longLongValue];
    self.updateTime           = [[attributes objectForKey:@"updateTime"] longLongValue];
    if ([[attributes objectForKey:@"isLike"] integerValue] >0) {
        self._selected            = YES;
    }else{
        self._selected            = NO;
    }
    self.likeTotal            = [[attributes objectForKey:@"likeTotal"] integerValue];


    /* _replyList object  */
    NSMutableArray *array_replyList_object = [[NSMutableArray alloc] init];
    array_replyList_object                 = [attributes objectForKey:@"replyList"];
    self._replyList                      = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_replyList_object count]; i++) {
        iEver_detail_comment_ReplyList_object *_comment_ReplyList_object = [[iEver_detail_comment_ReplyList_object alloc] initWithAttributes:[array_replyList_object objectAtIndex:i]];
        [self._replyList addObject:_comment_ReplyList_object];
    }

	return self;
}

@end


/* _replyList object */
@implementation iEver_detail_comment_ReplyList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.ReplyComment_id      = [[attributes objectForKey:@"id"] integerValue];
    self.cover_id             = [[attributes objectForKey:@"coverId"] integerValue];
    self.parent_id            = [[attributes objectForKey:@"parentId"] integerValue];
    self.userId               = [[attributes objectForKey:@"userId"] integerValue];
    self.atUserId             = [[attributes objectForKey:@"atUserId"] integerValue];
    self.commentContent       = [attributes  objectForKey:@"commentContent"];
    self.type                 = [[attributes objectForKey:@"type"] integerValue];
    self.status               = [[attributes objectForKey:@"status"] integerValue];
    self.createTime           = [[attributes objectForKey:@"createTime"] longLongValue];
    self.updateTime           = [[attributes objectForKey:@"updateTime"] longLongValue];
    self.nickName             = [attributes  objectForKey:@"nickName"];
    self.atNickName           = [attributes  objectForKey:@"atNickName"];
    self.headImg              = [attributes  objectForKey:@"headImg"];
    self.feature              = [attributes  objectForKey:@"feature"];
    self.likeTotal            = [[attributes objectForKey:@"likeTotal"] integerValue];

    return self;
}

@end



