//
//  iEver_rankItemObject.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-28.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iEver_rankItemObject : NSObject
/* 商品排行榜接口 */
- (RACSignal *)questDataQueryCategory:(NSDictionary *)dic path:(NSString *)pathUrl;

/*  分类类别 */
@property (nonatomic, retain) NSMutableArray             *_itemTopCateList;

/*  时间分类类别 */
@property (nonatomic, retain) NSMutableArray             *_topTimeList;

/*  时间分类类别1 */
@property (nonatomic, retain) NSMutableArray             *_topTimeList1;

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;

@end


/* itemTopList object */
@interface iEver_itemTopList_object : NSObject

@property (nonatomic, assign) int                        R_ID;                 /* 数据id */
@property (nonatomic, assign) int                        topCategoryId;        /* 归属分类 */
@property (nonatomic, assign) int                        itemId;               /* 商品ID */
@property (nonatomic, assign) int                        sortLevel;            /* 短分类 */
@property (nonatomic, assign) int                        beforeLevel;          /* 前期排名 */
@property (nonatomic, assign) float                      startGrade;           /* 分数 */
@property (nonatomic, assign) int                        price;                /* 价格 */
@property (nonatomic, assign) int                        status;               /* 状态 */
@property (nonatomic, assign) int                        useTotal;             /* 使用认识 */
@property (nonatomic, copy  ) NSString                   *itemName;            /* 商品名称 */
@property (nonatomic, copy  ) NSString                   *itemSpec;            /* 商品规格 */
@property (nonatomic, copy  ) NSString                   *itemImg;             /* 商品图片 */
@property (nonatomic, assign) BOOL                       _selected;            /* 是否点赞 */
@property (nonatomic, assign) int                        pvTotal;              /* 浏览数量 */
@property (nonatomic, assign) int                        likeTotal;            /* 点赞数量 */
@property (nonatomic, assign) int                        rowNum;               /* cell个数 */

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;

@end

/* itemTopCateList object */
@interface iEver_itemTopCateList_object : NSObject

@property (nonatomic, assign) int                        C_ID;                 /* 数据id */
@property (nonatomic, copy  ) NSString                   *categoryName;        /* 商品名称 */
@property (nonatomic, assign) int                        sortLevel;            /* 短分类 */
@property (nonatomic, assign) int                        status;               /* 状态 */
@property (nonatomic, assign) long long                  createTime;           /* 创建时间 */
@property (nonatomic, assign) long long                  updateTime;           /* 更新时间 */
@property (nonatomic, assign) long long                  topTime;
/*  当前内容数据数组 */
@property (nonatomic, retain) NSMutableArray             *_itemTopListArray;

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;

@end
