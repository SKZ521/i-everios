//
//  iEver_mainContent.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-19.
//  Copyright (c) 2014年 iEver. All rights reserved.
//


/* bannerList object */
@interface iEver_bannerList_object : NSObject

@property (nonatomic, assign) int                        Banner_id;       /* id */
@property (nonatomic, assign) int                        homeType;        /* 分类类型 */
@property (nonatomic, assign) int                        businessId;      /* 业务ID */
@property (nonatomic, copy  ) NSString                   *coverImg;       /* 封面图片 */
@property (nonatomic, assign) int                        sortLevel;       /*  */
/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;
@end

/* videoBannerList object */
@interface iEver_videoBannerList_object : NSObject

@property (nonatomic, assign) int                        articleId;       /* id */
@property (nonatomic, copy  ) NSString                   *coverImg;       /* 封面图片 */
@property (nonatomic, assign) int                        homeType;
/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;
@end

/* specialBannerList object */
@interface iEver_specialBannerList_object : NSObject

@property (nonatomic, assign) int                        businessId;           /* id */
@property (nonatomic, copy  ) NSString                   *specialTitle;       /* 标题 */
@property (nonatomic, copy  ) NSString                   *coverImg;           /* 封面图片 */
@property (nonatomic, copy  ) NSString                   *rootCategoryName;   /* 一级分类 */
@property (nonatomic, copy  ) NSString                   *secondCategoryName; /* 二级分类 */
@property (nonatomic, copy  ) NSString                   *articleTitle;       /* 文章标题 */
@property (nonatomic, copy  ) NSString                   *videoLink;          /* 视频链接 */
@property (nonatomic, copy  ) NSString                   *mVideoLink;
@property (nonatomic, copy  ) NSString                   *expertUserNickName;
@property (nonatomic, assign) int                        homeType;
@property (nonatomic, assign) int                        pvTotal;             /* 手机浏览量 */
@property (nonatomic, assign) int                        webPvTotal;          /* web浏览量 */
@property (nonatomic, assign) long long                  bannerReleaseTime;   /* 发布时间 */
/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;
@end



/* coverList object  */
@interface iEver_mainContentList_object : NSObject

@property (nonatomic, assign) int                        Cover_id;                /* id */
@property (nonatomic, assign) BOOL                       _selected;               /* 是否点赞 */
@property (nonatomic, assign) int                        rootCategoryId;          /* 一级分类 */
@property (nonatomic, assign) int                        secondCategoryId;        /* 二级分类 */
@property (nonatomic, assign) int                        threeCategoryId;         /* 三级分类 */
@property (nonatomic, assign) int                        useCategoryId;           /* 使用的分类 */
@property (nonatomic, assign) int                        categoryType;            /* 使用的分类 */
@property (nonatomic, copy  ) UIColor                    *defineCOLOR;            /* 预定义颜色值 */
@property (nonatomic, copy  ) NSMutableString            *rootCategoryName;       /* 一级分类标题 */
@property (nonatomic, copy  ) NSMutableString            *secondCategoryName;     /* 二级分类标题 */
@property (nonatomic, copy  ) NSMutableString            *threeCategoryName;      /* 三级分类标题 */
@property (nonatomic, copy  ) NSMutableString            *tagCategoryName;        /*  分类标题 */
@property (nonatomic, copy  ) NSString                   *useCategoryColor;       /* 二级分类颜色 */
@property (nonatomic, copy  ) NSMutableString            *buttonName;             /* 按钮标题 */
@property (nonatomic, assign) int                        buttonCategoryId;        /* 按钮ID */
@property (nonatomic, copy  ) NSString                   *articleTitle;           /* 文章标题 */
@property (nonatomic, copy  ) NSString                   *articleDesc;            /* 文章描述 */
@property (nonatomic, copy  ) NSString                   *coverImg;               /* 封面图片 */
@property (nonatomic, copy  ) NSString                   *coverDesc;              /* 封面描述 */
@property (nonatomic, assign) int                        releaseStatus;           /* 发布状态 */
@property (nonatomic, assign) long long                  timerDate;               /* 定时时间 */
@property (nonatomic, assign) int                        type;                    /* 10官方文章、20达人文章 */
@property (nonatomic, assign) int                        authorUserId;            /* 作者ID */
@property (nonatomic, copy  ) NSString                   *authorNickName;         /* 作者昵称 */
@property (nonatomic, assign) int                        status;                  /* 文章状态 */
@property (nonatomic, copy  ) NSString                   *videoLink;              /* 视频链接 */
@property (nonatomic, copy  ) NSString                   *mVideoLink;
@property (nonatomic, copy  ) NSString                   *videoDesc;              /* 视频描述 */
@property (nonatomic, assign) int                        itemId;                  /* 商品ID  */
@property (nonatomic, assign) long long                  createTime;              /* 创建时间 */
@property (nonatomic, assign) long long                  releaseTime;             /* 发布时间 */
@property (nonatomic, assign) long long                  updateTime;              /* 更新时间 */
@property (nonatomic, assign) int                        isLike;                  /* 是否喜欢  */
@property (nonatomic, assign) int                        isBrowse;                /* 是否浏览  */
@property (nonatomic, assign) int                        isComment;               /* 是否评论  */
@property (nonatomic, assign) int                        pvTotal;                 /* 浏览次数  */
@property (nonatomic, assign) int                        likeTotal;               /* 喜欢次数  */
@property (nonatomic, assign) int                        commentTotal;            /* 评论次数  */
@property (nonatomic, assign) int                        didSelectIndexPath;      /* 选中cell的indexpath  */
@property (nonatomic, copy  ) NSString                   *webUrl;

@property (nonatomic, assign) int                        webPvTotal;


/*  tagsList Object array */
@property (nonatomic, retain) NSMutableArray            *_tagsList;
/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;
@end

/* _tagsList object */
@interface iEver_mainList_tagsList_object : NSObject

@property (nonatomic, assign) int                        tagId;         /* tagid */
@property (nonatomic, assign) int                        businessId;    /* 事物id */
@property (nonatomic, copy  ) NSString                   *tagName;      /* 标签名称 */
/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;
@end


/* secondCategoryList object */
@interface iEver_secondCategoryList_object : NSObject

@property (nonatomic, assign) int                        Category_id;             /* id */
@property (nonatomic, copy  ) NSString                   *categoryName;           /* 分类名称 */
@property (nonatomic, assign) int                        parentCategoryId;        /* 分类ID */
@property (nonatomic, assign) int                        secondCategoryId;        /* 二级分类 */
@property (nonatomic, assign) int                        type;                    /* 类型 */
@property (nonatomic, assign) int                        status;                  /* 状态 */
@property (nonatomic, copy  ) NSString                   *color;                  /* 颜色 */
/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;
@end


#import <Foundation/Foundation.h>
@class iEver_defineDicObject;
@interface iEver_mainContent : NSObject
@property (nonatomic, assign) int                        refreshCount;     /* refreshCount */
@property (nonatomic, assign) int                        pageSize;         /* pageSize */
@property (nonatomic, assign) int                        resultCode;       /* resultCode */
/*  bannerList LIST Object array */
@property (nonatomic, retain) NSMutableArray             *_bannerList;

/*  videoBannerList LIST Object array */
@property (nonatomic, retain) NSMutableArray             *_videoBannerList;

/*  specialBannerList LIST Object array */
@property (nonatomic, retain) NSMutableArray             *_specialBannerList;

/*  content LIST Object array */
@property (nonatomic, retain) NSMutableArray             *_contentList;

/*  secondCategoryList LIST Object array */
@property (nonatomic, retain) NSMutableArray             *_secondCategoryList;

/*  Color LIST Object array */
@property (nonatomic, retain) NSMutableDictionary        *_ColorDic;

/*  name LIST Object array */
@property (nonatomic, retain) NSMutableDictionary        *_nameDic;


/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;
- (RACSignal *)acquireContentListsData:(NSDictionary *)dic path:(NSString *)pathUrl;

/*  CoverBySubCate data mothod */
- (RACSignal *)acquireCoverBySubCateListsData:(NSDictionary *)dic path:(NSString *)pathUrl;
@end

