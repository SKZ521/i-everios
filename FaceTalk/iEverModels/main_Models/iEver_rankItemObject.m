//
//  iEver_rankItemObject.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-28.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_rankItemObject.h"

@implementation iEver_rankItemObject

- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }

    self._topTimeList                            = [[NSMutableArray alloc] initWithCapacity:0];
    self._topTimeList1                           = [[NSMutableArray alloc] initWithCapacity:0];
    NSMutableArray *array_itopTimeList_object    = [[NSMutableArray alloc] init];
    array_itopTimeList_object                    = [attributes objectForKey:@"topTimeList"];
    for (int i = 0; i < [array_itopTimeList_object count]; i ++) {

        long long dateType = [[array_itopTimeList_object objectAtIndex:i] longLongValue];

        NSDateFormatter *formatter               = [[NSDateFormatter alloc] init];
        [formatter                                 setDateFormat:@"YYYY-MM"];
        NSString *dateLoca                       = [NSString stringWithFormat:@"%lld",dateType/1000];
        NSTimeInterval time                      = [dateLoca doubleValue];
        NSDate *detaildate                       = [NSDate dateWithTimeIntervalSince1970:time];
        NSString *timestr                        = [formatter stringFromDate:detaildate];
        [self._topTimeList addObject:timestr];

    }


    for (int i = 0; i < [array_itopTimeList_object count]; i ++) {

        long long dateType = [[array_itopTimeList_object objectAtIndex:i] longLongValue];

        NSDateFormatter *formatter               = [[NSDateFormatter alloc] init];
        [formatter                                 setDateFormat:@"YYYY-MM-dd"];
        NSString *dateLoca                       = [NSString stringWithFormat:@"%lld",dateType/1000];
        NSTimeInterval time                      = [dateLoca doubleValue];
        NSDate *detaildate                       = [NSDate dateWithTimeIntervalSince1970:time];
        NSString *timestr                        = [formatter stringFromDate:detaildate];
        [self._topTimeList1 addObject:timestr];
        
    }

    self._itemTopCateList                        = [[NSMutableArray alloc] initWithCapacity:0];

    NSMutableArray *array_itemTopCateList_object = [[NSMutableArray alloc] init];
    if ([[attributes objectForKey:@"itemTopCateList"] isKindOfClass:[NSString class]]) {
    }else{
        array_itemTopCateList_object                 = [attributes objectForKey:@"itemTopCateList"];
    }
    for (int i = 0; i < [array_itemTopCateList_object count]; i ++) {

        iEver_itemTopCateList_object * content_object = [[iEver_itemTopCateList_object alloc] initWithAttributes:[array_itemTopCateList_object objectAtIndex:i]];
        [self._itemTopCateList addObject:content_object];

    }
	return self;
}

/* 商品排行榜接口 */
- (RACSignal *)questDataQueryCategory:(NSDictionary *)dic path:(NSString *)pathUrl{

    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"Get"
                                                                           path:pathUrl
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                iEver_rankItemObject * object = [[iEver_rankItemObject alloc] initWithAttributes:responseObject];
                return object;
            }];
}

@end


/* itemTopList object */
@implementation iEver_itemTopList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }
    self.R_ID                  = [[attributes objectForKey:@"id"] integerValue];
    self.topCategoryId         = [[attributes objectForKey:@"topCategoryId"] integerValue];
    self.itemId                = [[attributes objectForKey:@"itemId"] integerValue];
    self.sortLevel             = [[attributes objectForKey:@"sortLevel"] integerValue];
    self.beforeLevel           = [[attributes objectForKey:@"beforeLevel"] integerValue];
    self.startGrade            = [[attributes objectForKey:@"startGrade"] floatValue];
    self.price                 = [[attributes objectForKey:@"price"] integerValue];
    self.status                = [[attributes objectForKey:@"status"] integerValue];
    self.useTotal              = [[attributes objectForKey:@"useTotal"] integerValue];
    self.itemName              = [attributes objectForKey:@"itemName"];
    self.itemSpec              = [attributes objectForKey:@"itemSpec"];
    self.itemImg               = [attributes objectForKey:@"itemImg"];
    self.pvTotal               = [[attributes objectForKey:@"pvTotal"] integerValue];
    self.likeTotal             = [[attributes objectForKey:@"likeTotal"] integerValue];
    if ([[attributes objectForKey:@"isLike"] integerValue] >0) {
        self._selected            = YES;
    }else{

        self._selected            = NO;
    }

	return self;
}

@end


/* itemTopCateList object */
@implementation iEver_itemTopCateList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }
    self.C_ID                   = [[attributes objectForKey:@"id"] integerValue];
    self.categoryName           = [attributes objectForKey:@"categoryName"];
    self.sortLevel              = [[attributes objectForKey:@"sortLevel"] integerValue];
    self.status                 = [[attributes objectForKey:@"status"] integerValue];
    self.createTime             = [[attributes objectForKey:@"createTime"] longLongValue];
    self.updateTime             = [[attributes objectForKey:@"updateTime"] longLongValue];
    self.topTime                = [[attributes objectForKey:@"topTime"] longLongValue];
    self._itemTopListArray      = [[NSMutableArray alloc] initWithCapacity:0];
    NSMutableArray *array_itemTopList_object = [[NSMutableArray alloc] init];
    array_itemTopList_object    = [attributes objectForKey:@"itemTopList"];

    for (int i = 0; i < [array_itemTopList_object count]; i++) {
        iEver_itemTopList_object *content_object = [[iEver_itemTopList_object alloc] initWithAttributes:[array_itemTopList_object objectAtIndex:i]];
        [self._itemTopListArray addObject:content_object];
    }


	return self;
}

@end

