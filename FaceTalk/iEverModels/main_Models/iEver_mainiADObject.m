//
//  iEver_mainiADObject.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-17.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_mainiADObject.h"
#import "iEver_IAD.h"
@implementation iEver_mainiADObject
- (id)initWithAttributes:(NSDictionary *)attributes
{
	self = [super init];
    if (!self) {
        return nil;
    }
    self.resultCode = [[attributes objectForKey:@"resultCode"] integerValue];
	//广告数据
	if (![[attributes objectForKey:@"bannerList"] isKindOfClass:[NSNull class]]) {
		self.adsList = [[NSMutableArray alloc] initWithCapacity:0];
		for (id object in [attributes objectForKey:@"bannerList"]) {
			iEver_IAD *iad = [[iEver_IAD alloc] initWithAttributes:object];
			[self.adsList addObject:iad];
		}
	}

	return self;
}

- (RACSignal *)acquireIADSListsData:(NSDictionary *)dic
{

	return [[[iEver_AppDotNetAPIClient sharedClient] enqueueRequestWithMethod:@"GET"
                                                                     path:@"home/getHome"
                                                               parameters:dic
             ]

            map:^id(NSDictionary *responseObject) {
                NSLog(@"responseObject---->%@",responseObject);
                iEver_mainiADObject *activityiAd = [[iEver_mainiADObject alloc] initWithAttributes:responseObject];
                return activityiAd;
            }];
}

@end
