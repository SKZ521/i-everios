//
//  iEver_detailCommentObject.h
//  FaceTalk
//
//  Created by 开仲 司 on 14/12/23.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iEver_detailCommentObject : NSObject
/*  _articleCommentList Object array */
@property (nonatomic, assign) int                                pageSize;         /* pageSize */
@property (nonatomic, assign) int                                resultCode;       /* resultCode */
@property (nonatomic, retain) NSMutableArray                     *_commentList;

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes ;
/* 评论列表 */
- (RACSignal *)articleComment:(NSDictionary *)dic path:(NSString *)pathUrl;
@end

/* _articleCommentList object */
@interface iEver_articleCommentList_object : NSObject

@property (nonatomic, assign) int                        Comment_id;       /* 评论id */
@property (nonatomic, assign) int                        itemId;           /* 商品ID */
@property (nonatomic, assign) int                        userId;           /* 用户ID */
@property (nonatomic, copy  ) NSString                   *commentContent;  /* 评论内容 */
@property (nonatomic, assign) int                        type;             /* 类型 */
@property (nonatomic, assign) int                        status;           /* 状态 */
@property (nonatomic, assign) long long                  createTime;       /* 创建时间 */
@property (nonatomic, assign) long long                  updateTime;       /* 更新时间 */
@property (nonatomic, copy  ) NSString                   *nickName;        /* 昵称 */
@property (nonatomic, copy  ) NSString                   *feature;
@property (nonatomic, copy  ) NSString                   *headImg;         /* 头像图片 */
@property (nonatomic, assign) int                        point;            /* 积分 */
@property (nonatomic, assign) int                        isLike;           /* 是否点赞 */
@property (nonatomic, assign) int                        likeTotal;        /* 点赞个数 */
@property (nonatomic, assign) int                        replyTotal;       /* 回复个数 */
@property (nonatomic, assign) BOOL                       _selected;        /* 是否点赞 */
@property (nonatomic, assign) int                        endCell;          /* 是否最后一个单元格 */
@property (nonatomic, assign) int                        intoFromWhere; /* 1: 来自官方文章；2：来自达人文章 */

@property (nonatomic, retain) NSMutableArray             *_replyList;      /* 评论回复的数组 */

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;

@end


/* _articleCommentList object */
@interface iEver_comment_ReplyList_object : NSObject

@property (nonatomic, assign) int                        ReplyComment_id;  /* 评论id */
@property (nonatomic, assign) int                        cover_id;         /* 封面id */
@property (nonatomic, assign) int                        parent_id;        /* 父数据id */
@property (nonatomic, assign) int                        userId;           /* 用户ID */
@property (nonatomic, assign) int                        atUserId;         /* @用户ID */
@property (nonatomic, copy  ) NSString                   *commentContent;  /* 评论内容 */
@property (nonatomic, assign) int                        type;             /* 类型 */
@property (nonatomic, assign) int                        status;           /* 状态 */
@property (nonatomic, assign) long long                  createTime;       /* 创建时间 */
@property (nonatomic, assign) long long                  updateTime;       /* 更新时间 */
@property (nonatomic, copy  ) NSString                   *nickName;        /* 昵称 */
@property (nonatomic, copy  ) NSString                   *atNickName;      /* @用户昵称 */
@property (nonatomic, copy  ) NSString                   *feature;
@property (nonatomic, copy  ) NSString                   *headImg;         /* 头像图片 */
@property (nonatomic, assign) int                        point;            /* 积分 */
@property (nonatomic, assign) int                        isLike;           /* 是否点赞 */
@property (nonatomic, assign) int                        likeTotal;        /* 点赞个数 */

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;

@end
