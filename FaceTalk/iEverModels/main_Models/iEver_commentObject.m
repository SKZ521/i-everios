//
//  iEver_commentObject.m
//  FaceTalk
//
//  Created by 开仲 司 on 14/12/1.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_commentObject.h"

@implementation iEver_commentObject
/* 插入商品评论 */
- (RACSignal *)insertItemComment:(NSDictionary *)dic path:(NSString *)pathUrl{

    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:@"itemComment/insert"
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
}
/* 插入文章评论 */
- (RACSignal *)insertArticleComment:(NSDictionary *)dic path:(NSString *)pathUrl{

    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:pathUrl
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];

}

/* 插入活动评论 */
- (RACSignal *)insertTryComment:(NSDictionary *)dic path:(NSString *)pathUrl{

    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:pathUrl
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];

}

/* 插入问题的中回答的评论 */
- (RACSignal *)insertAnswerComment:(NSDictionary *)dic path:(NSString *)pathUrl {

    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"POST"
                                                                           path:pathUrl
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                return responseObject;
            }];
    

}

@end
