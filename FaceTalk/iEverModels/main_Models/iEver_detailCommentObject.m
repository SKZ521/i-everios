//
//  iEver_detailCommentObject.m
//  FaceTalk
//
//  Created by 开仲 司 on 14/12/23.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_detailCommentObject.h"

@implementation iEver_detailCommentObject

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    /* coverList object  */
    NSMutableArray *array_messageList_object = [[NSMutableArray alloc] initWithCapacity:0];
    self.pageSize                         = [[attributes objectForKey:@"pageSize"] integerValue];
    self.resultCode                       = [[attributes objectForKey:@"resultCode"] integerValue];
    array_messageList_object                 = [attributes objectForKey:@"acList"];
    self._commentList                    = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_messageList_object count]; i++) {
        iEver_articleCommentList_object *message_object = [[iEver_articleCommentList_object alloc] initWithAttributes:[array_messageList_object objectAtIndex:i]];
        [self._commentList addObject:message_object];
    }

    return self;
}

/* 评论列表 */
- (RACSignal *)articleComment:(NSDictionary *)dic path:(NSString *)pathUrl{

    return [[[iEver_AppDotNetAPIClient sharedClient] enqueueRequestWithMethod:@"GET"
                                                                         path:pathUrl
                                                                   parameters:dic
             ]

            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                iEver_detailCommentObject * object = [[iEver_detailCommentObject alloc] initWithAttributes:responseObject];
                return object;
            }];

}
@end
/* _articleCommentList object */
@implementation iEver_articleCommentList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.Comment_id           = [[attributes objectForKey:@"id"] integerValue];
    self.itemId               = [[attributes objectForKey:@"itemId"] integerValue];
    self.userId               = [[attributes objectForKey:@"userId"] integerValue];
    self.commentContent       = [attributes  objectForKey:@"commentContent"];
    self.type                 = [[attributes objectForKey:@"type"] integerValue];
    self.status               = [[attributes objectForKey:@"status"] integerValue];
    self.replyTotal           = [[attributes objectForKey:@"replyTotal"] integerValue];
    self.nickName             = [attributes  objectForKey:@"nickName"];
    self.feature              = [attributes  objectForKey:@"feature"];
    self.headImg              = [attributes  objectForKey:@"headImg"];
    self.createTime           = [[attributes objectForKey:@"createTime"] longLongValue];
    self.updateTime           = [[attributes objectForKey:@"updateTime"] longLongValue];
    if ([[attributes objectForKey:@"isLike"] integerValue] >0) {
        self._selected            = YES;
    }else{
        self._selected            = NO;
    }
    self.likeTotal            = [[attributes objectForKey:@"likeTotal"] integerValue];

    /* _replyList object  */
    NSMutableArray *array_replyList_object = [[NSMutableArray alloc] init];
    array_replyList_object                 = [attributes objectForKey:@"replyList"];
    self._replyList                      = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_replyList_object count]; i++) {
        iEver_comment_ReplyList_object *_comment_ReplyList_object = [[iEver_comment_ReplyList_object alloc] initWithAttributes:[array_replyList_object objectAtIndex:i]];
        [self._replyList addObject:_comment_ReplyList_object];
    }


    return self;
}

@end


/* _replyList object */
@implementation iEver_comment_ReplyList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.ReplyComment_id      = [[attributes objectForKey:@"id"] integerValue];
    self.cover_id             = [[attributes objectForKey:@"coverId"] integerValue];
    self.parent_id            = [[attributes objectForKey:@"parentId"] integerValue];
    self.userId               = [[attributes objectForKey:@"userId"] integerValue];
    self.atUserId             = [[attributes objectForKey:@"atUserId"] integerValue];
    self.commentContent       = [attributes  objectForKey:@"commentContent"];
    self.type                 = [[attributes objectForKey:@"type"] integerValue];
    self.status               = [[attributes objectForKey:@"status"] integerValue];
    self.createTime           = [[attributes objectForKey:@"createTime"] longLongValue];
    self.updateTime           = [[attributes objectForKey:@"updateTime"] longLongValue];
    self.nickName             = [attributes  objectForKey:@"nickName"];
    self.atNickName           = [attributes  objectForKey:@"atNickName"];
    self.headImg              = [attributes  objectForKey:@"headImg"];
    self.feature              = [attributes  objectForKey:@"feature"];
    self.likeTotal            = [[attributes objectForKey:@"likeTotal"] integerValue];

    return self;
}

@end