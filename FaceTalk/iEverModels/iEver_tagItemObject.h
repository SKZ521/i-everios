//
//  iEver_tagItemObject.h
//  FaceTalk
//
//  Created by 开仲 司 on 15/2/5.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iEver_tagItemObject : NSObject

@property (nonatomic, assign) int                        resultCode;       /* resultCode */

@property (nonatomic, assign) int                        pageSize;        /* pageSize */

/* 商品排行榜接口 */
- (RACSignal *)queryItemByTagId:(NSDictionary *)dic path:(NSString *)pathUrl;

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;

/*  分类类别 */
@property (nonatomic, retain) NSMutableArray             *_tagItemList;
@end


/* itemTopList object */
@interface iEver_tagItemList_object : NSObject

@property (nonatomic, assign) int                        T_ID;                 
@property (nonatomic, assign) int                        rootCategoryId;        
@property (nonatomic, assign) int                        secondCategoryId;              
@property (nonatomic, assign) int                        threeCategoryId;           
@property (nonatomic, assign) int                        leafLevel;         
@property (nonatomic, copy  ) NSString                   *itemName;           
@property (nonatomic, copy  ) NSString                   *itemDesc;            
@property (nonatomic, copy  ) NSString                   *itemColor;            
@property (nonatomic, copy  ) NSString                   *itemSpec;             
@property (nonatomic, copy  ) NSString                   *itemImg;             
@property (nonatomic, assign) int                        price;                
@property (nonatomic, copy  ) NSString                   *itemLink;            
@property (nonatomic, copy  ) NSString                   *videoLink;            
@property (nonatomic, copy  ) NSString                   *videoDesc;            
@property (nonatomic, assign) int                        status;             
@property (nonatomic, assign) int                        startGrade;              
@property (nonatomic, copy  ) NSString                   *gradeDesc;             
@property (nonatomic, assign) long long                  createTime;          
@property (nonatomic, assign) long long                  updateTime;           
@property (nonatomic, assign) int                        pvTotal;
@property (nonatomic, assign) int                        webPvTotal;
@property (nonatomic, assign) int                        likeTotal;
@property (nonatomic, assign) int                        commentTotal;              
@property (nonatomic, assign) int                        isBrowse;              
@property (nonatomic, assign) int                        isLike;

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes;

@end