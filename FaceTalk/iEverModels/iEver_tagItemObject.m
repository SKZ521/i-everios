//
//  iEver_tagItemObject.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/2/5.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_tagItemObject.h"

@implementation iEver_tagItemObject

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }

    self.pageSize                           = [[attributes objectForKey:@"pageSize"] integerValue];
    self.resultCode                         = [[attributes objectForKey:@"resultCode"] integerValue];

    self._tagItemList                        = [[NSMutableArray alloc] initWithCapacity:0];
    NSMutableArray *array_tagItemList_object = [[NSMutableArray alloc] init];
    array_tagItemList_object                 = [attributes objectForKey:@"itemList"];
    for (int i = 0; i < [array_tagItemList_object count]; i ++) {

        iEver_tagItemList_object * content_object = [[iEver_tagItemList_object alloc] initWithAttributes:[array_tagItemList_object objectAtIndex:i]];
        [self._tagItemList addObject:content_object];
        
    }
    return self;
}


/* 商品排行榜接口 */
- (RACSignal *)queryItemByTagId:(NSDictionary *)dic path:(NSString *)pathUrl{

    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"Get"
                                                                           path:pathUrl
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                iEver_tagItemObject * object = [[iEver_tagItemObject alloc] initWithAttributes:responseObject];
                return object;
            }];
}
@end


/* itemTopList object */
@implementation iEver_tagItemList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.T_ID                  = [[attributes objectForKey:@"id"] integerValue];
    self.rootCategoryId        = [[attributes objectForKey:@"rootCategoryId"] integerValue];
    self.secondCategoryId      = [[attributes objectForKey:@"secondCategoryId"] integerValue];
    self.threeCategoryId       = [[attributes objectForKey:@"threeCategoryId"] integerValue];
    self.leafLevel             = [[attributes objectForKey:@"leafLevel"] integerValue];
    self.itemName              = [attributes objectForKey:@"itemName"];
    self.itemDesc              = [attributes objectForKey:@"itemDesc"];
    self.itemColor             = [attributes objectForKey:@"itemColor"];
    self.itemSpec              = [attributes objectForKey:@"itemSpec"];
    self.itemImg               = [attributes objectForKey:@"itemImg"];
    self.price                 = [[attributes objectForKey:@"price"] integerValue];
    self.itemLink              = [attributes objectForKey:@"itemLink"];
    self.videoLink             = [attributes objectForKey:@"videoLink"];
    self.videoDesc             = [attributes objectForKey:@"videoDesc"];
    self.status                = [[attributes objectForKey:@"status"] integerValue];
    self.startGrade            = [[attributes objectForKey:@"startGrade"] integerValue];
    self.gradeDesc             = [attributes objectForKey:@"gradeDesc"];
    self.createTime            = [[attributes objectForKey:@"createTime"] longLongValue];
    self.updateTime            = [[attributes objectForKey:@"updateTime"] integerValue];
    self.pvTotal               = [[attributes objectForKey:@"pvTotal"] integerValue];
    self.webPvTotal            = [[attributes objectForKey:@"webPvTotal"] integerValue];
    self.likeTotal             = [[attributes objectForKey:@"likeTotal"] integerValue];
    self.commentTotal          = [[attributes objectForKey:@"commentTotal"] integerValue];
    self.isBrowse              = [[attributes objectForKey:@"isBrowse"] integerValue];
    self.isLike                = [[attributes objectForKey:@"isLike"] integerValue];

    return self;
}

@end
