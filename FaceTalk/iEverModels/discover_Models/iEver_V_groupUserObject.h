//
//  iEver_V_groupUserObject.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-14.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iEver_V_groupUserObject : NSObject
/*  expertUserList  array */
@property (nonatomic, retain) NSMutableArray             *_expertUserList;
/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes ;
/* 达人分组列表接口 */
- (RACSignal *)queryByGroup:(NSDictionary *)dic path:(NSString *)pathUrl;
@end


/* userList object  */
@interface iEver_V_userList_object : NSObject

@property (assign) BOOL                                  opened;
@property (nonatomic,retain) NSMutableArray              *indexPaths; // 临时保存indexpaths

@property (nonatomic, copy  ) NSString                   *categoryName;  /* 分类名称 */
/*  expertUserList  array */
@property (nonatomic, retain) NSMutableArray             *_userList;     /* 组数据 */

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes ;
@end


/* user object  */
@interface iEver_V_user_object : NSObject

@property (nonatomic, assign) int                        User_id;        /* 达人ID */
@property (nonatomic, copy  ) NSString                   *nickName;      /* 达人昵称 */
@property (nonatomic, copy  ) NSString                   *headImg;       /* 达人头像 */
@property (nonatomic, copy  ) NSString                   *intro;         /* 达人简述 */
@property (nonatomic, assign) int                        answerTotal;    /* 回答问题数量 */

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes ;
@end