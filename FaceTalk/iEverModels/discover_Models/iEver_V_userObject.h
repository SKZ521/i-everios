//
//  iEver_V_userObject.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-10.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iEver_V_userObject : NSObject

@property (nonatomic, assign) int                        resultCode;       /* resultCode */

@property (nonatomic, assign) int                        pageSize;        /* pageSize */

/*  content LIST Object array */
@property (nonatomic, retain) NSMutableArray             *_userList;

@property (assign) BOOL                                  opened;
@property (nonatomic,retain) NSMutableArray *indexPaths; // 临时保存indexpaths

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes ;

/* 达人根据回答量排序 */
- (RACSignal *)queryOrderByAnswer:(NSDictionary *)dic path:(NSString *)pathUrl;
@end


/* userList object  */
@interface iEver_discover_userList_object : NSObject

@property (nonatomic, assign) int                        User_id;        /* 达人ID */
@property (nonatomic, copy  ) NSString                   *nickName;      /* 达人昵称 */
@property (nonatomic, copy  ) NSString                   *headImg;       /* 达人头像 */
@property (nonatomic, copy  ) NSString                   *intro;         /* 达人简述 */
@property (nonatomic, assign) int                        answerTotal;    /* 回答问题数量 */
@property (nonatomic, assign) int                        articleTotal;   /* 文章数量 */

/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes ;
@end
