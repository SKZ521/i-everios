//
//  iEver_V_answeredQuestionObject.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-17.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_V_answeredQuestionObject.h"

@implementation iEver_V_answeredQuestionObject

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }

    /* coverList object  */
    NSMutableArray *array_quesList_object = [[NSMutableArray alloc] init];
    self.pageSize                         = [[attributes objectForKey:@"pageSize"] integerValue];
    self.resultCode                       = [[attributes objectForKey:@"resultCode"] integerValue];
    array_quesList_object                 = [attributes objectForKey:@"quesList"];
    self._quesList                    = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_quesList_object count]; i++) {
        iEver_V_quesList_object *quest_object = [[iEver_V_quesList_object alloc] initWithAttributes:[array_quesList_object objectAtIndex:i]];
        [self._quesList addObject:quest_object];
    }

    return self;
}
/*  expertArticle data mothod */
- (RACSignal *)queryAnsweredByUser:(NSDictionary *)dic path:(NSString *)pathUrl{

    /* restful request URL /expertQuestion/queryAnswered/{达人用户id}/{分页页码} */
    return [[[iEver_AppDotNetAPIClient sharedClient] enqueueRequestWithMethod:@"GET"
                                                                         path:pathUrl
                                                                   parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                iEver_V_answeredQuestionObject * object = [[iEver_V_answeredQuestionObject alloc] initWithAttributes:responseObject];
                return object;
            }];
    
}
@end


/* quesList object  */
@implementation iEver_V_quesList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) return nil;

    // 提问者相关
    self.ques_id        = [[attributes objectForKey:@"id"] integerValue];/* 1、问题ID */
    self.qCategory_id   = [[attributes objectForKey:@"qCategoryId"] integerValue];/* 2、问题分类ID */
    self.qUserId        = [[attributes objectForKey:@"qUserId"] integerValue];/* 3、问题用户ID */
    self.qContent       = [attributes objectForKey:@"qContent"];/* 4、问题内容 */
    self.createTime     = [[attributes objectForKey:@"createTime"] longLongValue];/* 5、创建时间 */
    self.qNickName      = [attributes objectForKey:@"qNickName"];/* 6、问者昵称 */
    self.qHeadImg       = [attributes objectForKey:@"qHeadImg"];/* 7、问者头像 */
    self.answerTotal    = [[attributes objectForKey:@"answerTotal"] integerValue];/* 8、回答总数 */

    // 回答者相关
    self.aId            = [[attributes objectForKey:@"aId"] integerValue];/* 1、回答Id */
    self.aUserId        = [[attributes objectForKey:@"aUserId"] integerValue];/* 2、回答用户id */
    self.aNickName      = [attributes objectForKey:@"aNickName"];/* 3、答者昵称 */
    self.aHeadImg       = [attributes objectForKey:@"aHeadImg"];/* 4、答者头像 */
    self.aUserType      = [[attributes objectForKey:@"aUserType"] integerValue];/* 5、回答用户类型 10普通用户 20达人用户 */
    self.aContent       = [attributes objectForKey:@"aContent"];/* 6、回答内容 */
    self.aCreateTime    = [[attributes objectForKey:@"aCreateTime"] longLongValue];/* 7、回答时间 */
    /* 8、回答是否被点赞 */
    NSInteger praiseNumber = [[attributes objectForKey:@"isPraise"] integerValue];
    if (praiseNumber > 1)
    {
        _isPraise = YES;
    } else {
        _isPraise = NO;
    }
    self.praiseTotal    = [[attributes objectForKey:@"praiseTotal"] integerValue];/* 9、点赞总数 */

    /*  10提问者图片、20回答者图片 */
    NSArray *quesPicList = [attributes objectForKey:@"quesPicList"];
    if ([quesPicList count] > 0) {
        self._QquesPicList = [[NSMutableArray alloc] init];
        self._AquesPicList = [[NSMutableArray alloc] init];
        for (int i = 0; i < [quesPicList count]; i ++) {
            NSDictionary *dic = [quesPicList objectAtIndex:i];
            if ([[dic objectForKey:@"type"] integerValue] == 10) {
                [self._QquesPicList addObject:[dic objectForKey:@"imgUrl"]];
            }else if ([[dic objectForKey:@"type"] integerValue] == 20) {
                [self._AquesPicList addObject:[dic objectForKey:@"imgUrl"]];
            }
        }
    }
    return self;
}
@end
