//
//  iEver_V_personMessageObject.m
//  FaceTalk
//
//  Created by 开仲 司 on 14/11/19.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_V_personMessageObject.h"

@implementation iEver_V_personMessageObject
/*  parse data mothod */
- (id)initWithAttributes:(NSDictionary *)attributes{
    self = [super init];
    if (!self) {
        return nil;
    }

    NSDictionary *user_dic = [attributes objectForKey:@"user"];
    self.U_id              = [[user_dic objectForKey:@"id"] integerValue];
    self.categoryName      = [user_dic  objectForKey:@"categoryName"];
    self.headImg           = [user_dic  objectForKey:@"headImg"];
    self.nickName          = [user_dic  objectForKey:@"nickName"];
    self.intro             = [user_dic  objectForKey:@"intro"];
    return self;
}

/* 查询达人个人信息 */
- (RACSignal *)expertUser:(NSDictionary *)dic path:(NSString *)pathUrl{

    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"Get"
                                                                           path:pathUrl
                                                                     parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                iEver_V_personMessageObject *object = [[iEver_V_personMessageObject alloc] initWithAttributes:responseObject];
                object.coverCount        = [[responseObject objectForKey:@"coverCount"] integerValue];
                object.quesCount         = [[responseObject objectForKey:@"quesCount"] integerValue];
                return object;
            }];
}
@end
