//
//  iEver_expertQuestionObject.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/5/24.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_expertQuestionObject.h"

@implementation iEver_expertQuestionObject

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }

    /* quesList object  */
    NSMutableArray *array_quesList_object = [[NSMutableArray alloc] init];
    self.pageSize                         = [[attributes objectForKey:@"pageSize"] integerValue];
    self.resultCode                       = [[attributes objectForKey:@"resultCode"] integerValue];
    array_quesList_object                 = [attributes objectForKey:@"quesList"];
    self._quesList                    = [[NSMutableArray alloc] initWithCapacity:0];
    self.judgmentDate = @"";
    for (int i = 0; i < [array_quesList_object count]; i++) {

        iEver_quesList_object *quest_object = [[iEver_quesList_object alloc] initWithAttributes:[array_quesList_object objectAtIndex:i]];

        if (![self.judgmentDate isEqualToString:quest_object.creeatTimeFormatStr]) {
            quest_object.isSectionFirstData = NO;
            self.judgmentDate = quest_object.creeatTimeFormatStr;
        }else {
            quest_object.isSectionFirstData = YES;
        }
        [self._quesList addObject:quest_object];
    }

    return self;
}

/*  expertQuestion data mothod */
- (RACSignal *)expertQuestionQueryAll:(NSDictionary *)dic path:(NSString *)pathUrl{

    /* restful request URL /expertQuestion/queryAll/{qCategoryId}/{qStatus}/{currPage} */
    return [[[iEver_AppDotNetAPIClient sharedClient] enqueueRequestWithMethod:@"GET"
                                                                         path:pathUrl
                                                                   parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                iEver_expertQuestionObject * object = [[iEver_expertQuestionObject alloc] initWithAttributes:responseObject];
                return object;
            }];

}
@end


/* quesList object  */
@implementation iEver_quesList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.ques_id              = [[attributes objectForKey:@"id"] integerValue];
    self.qCategoryId          = [[attributes objectForKey:@"qCategoryId"] integerValue];
    self.qUserId              = [[attributes objectForKey:@"qUserId"] integerValue];
    self.qTitle               = [attributes objectForKey:@"qTitle"];
    self.qContent             = [attributes objectForKey:@"qContent"];
    self.qStatus              = [[attributes objectForKey:@"qStatus"] integerValue];
    self.status               = [[attributes objectForKey:@"status"] integerValue];
    self.createTime           = [[attributes objectForKey:@"createTime"] longLongValue];
    self.updateTime           = [[attributes objectForKey:@"updateTime"] longLongValue];
    self.qNickName            = [attributes objectForKey:@"qNickName"];
    self.qHeadImg             = [attributes objectForKey:@"qHeadImg"];
    self.answerTotal          = [[attributes objectForKey:@"answerTotal"] integerValue];

    /* 格式创建时间  05-15 */
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY年MM月dd日"];
    NSString *dateLoca = [NSString stringWithFormat:@"%lld",self.createTime/1000];
    NSTimeInterval time =[dateLoca doubleValue];
    NSDate *detaildate = [NSDate dateWithTimeIntervalSince1970:time];
    self.creeatTimeFormatStr = [formatter stringFromDate:detaildate];

    /*  10提问者图片 */
    NSArray *quesPicList = [attributes objectForKey:@"quesPicList"];
    if ([quesPicList count] > 0) {
        self._quesPicList = [[NSMutableArray alloc] init];
        for (int i = 0; i < [quesPicList count]; i ++) {
            NSDictionary *dic = [quesPicList objectAtIndex:i];
            if ([[dic objectForKey:@"type"] integerValue] == 10) {
                [self._quesPicList addObject:[dic objectForKey:@"imgUrl"]];
            }
        }
    }
    return self;
}


@end
