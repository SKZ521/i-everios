//
//  iEver_normalUserUnAnswerObject.m
//  FaceTalk
//
//  Created by 开仲 司 on 14/11/21.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_normalUserUnAnswerObject.h"

@implementation iEver_normalUserUnAnswerObject

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }

    /* coverList object  */
    NSMutableArray *array_quesList_object = [[NSMutableArray alloc] init];
    array_quesList_object                 = [attributes objectForKey:@"quesList"];
    self._quesList                    = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_quesList_object count]; i++) {
        iEver_N_quesList_object *quest_object = [[iEver_N_quesList_object alloc] initWithAttributes:[array_quesList_object objectAtIndex:i]];
        [self._quesList addObject:quest_object];
    }

    return self;
}

/* 查询普通用户提问未回答 */
- (RACSignal *)queryQuestByUnanswered:(NSDictionary *)dic path:(NSString *)pathUrl{

    return [[[iEver_AppPostJsonAPIClient sharedClient] enqueueRequestWithMethod:@"Get"
                                                                           path:pathUrl
                                                                     parameters:dic
             ]

            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                iEver_normalUserUnAnswerObject * object = [[iEver_normalUserUnAnswerObject alloc] initWithAttributes:responseObject];
                return object;
            }];
}
@end
/* quesList object  */
@implementation iEver_N_quesList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.ques_id              = [[attributes objectForKey:@"id"] integerValue];
    self.ques_id              = [[attributes objectForKey:@"id"] integerValue];
    self.ques_id              = [[attributes objectForKey:@"id"] integerValue];
    self.qTitle               = [attributes objectForKey:@"qTitle"];
    self.qContent             = [attributes objectForKey:@"qContent"];
    self.aContent             = [attributes objectForKey:@"aContent"];
    self.qStatus              = [[attributes objectForKey:@"qStatus"] integerValue];
    self.status               = [[attributes objectForKey:@"status"] integerValue];
    self.createTime           = [[attributes objectForKey:@"createTime"] longLongValue];
    self.updateTime           = [[attributes objectForKey:@"updateTime"] longLongValue];
    self.qNickName            = [attributes objectForKey:@"qNickName"];
    self.qHeadImg             = [attributes objectForKey:@"qHeadImg"];
    self.aNickName            = [attributes objectForKey:@"aNickName"];
    self.aHeadImg             = [attributes objectForKey:@"aHeadImg"];
    self.likeTotal            = [[attributes objectForKey:@"likeTotal"] integerValue];
    if ([[attributes objectForKey:@"isLike"] integerValue] >0) {
        self._selected            = YES;
    }else{
        self._selected            = NO;
    }
    /*  10提问者图片、20回答者图片 */
    NSArray *quesPicList = [attributes objectForKey:@"quesPicList"];
    if ([quesPicList count] > 0) {
        self._QquesPicList = [[NSMutableArray alloc] init];
        self._AquesPicList = [[NSMutableArray alloc] init];
        for (int i = 0; i < [quesPicList count]; i ++) {
            NSDictionary *dic = [quesPicList objectAtIndex:i];
            if ([[dic objectForKey:@"type"] integerValue] == 10) {
                [self._QquesPicList addObject:[dic objectForKey:@"imgUrl"]];
            }else if ([[dic objectForKey:@"type"] integerValue] == 20) {
                [self._AquesPicList addObject:[dic objectForKey:@"imgUrl"]];
            }
        }
    }
    return self;
}
@end
