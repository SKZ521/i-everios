//
//  iEverAppDelegate.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-11.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEverAppDelegate.h"

/* drawer VC */
#import "MFSideMenuContainerViewController.h"
/* left menu VC */
#import "iEver_LeftViewController.h"

/* Mian VC */
#import "iEver_OfficialListViewController.h"
#import "iEver_VuserCenterViewController.h"
#import "iEver_AskAnswerViewController.h"
#import "iEver_RankViewController.h"
#import "iEver_ItemTryViewController.h"
#import "iEver_MYViewController.h"
/* setting person */
#import "iEver_SettingViewController.h"

#import "MobClick.h"
#import "UMSocial.h"
#import "UMSocialWechatHandler.h"
#import "UMSocialSinaHandler.h"
#import "UMFeedback.h"

#import "iEver_UserMessageObject.h"
/* Jpush */
#import "APService.h"

#import "CustomNavigationController.h"

//#import <Fabric/Fabric.h>
//#import <Crashlytics/Crashlytics.h>

//for mac
#include <sys/socket.h>
#include <sys/sysctl.h>
#include <net/if.h>
#include <net/if_dl.h>

//for idfa
#import <AdSupport/AdSupport.h>


@interface iEverAppDelegate()<UITabBarControllerDelegate>
{
    UIView *splashView;
    NSDictionary *dic;
}
@end
@implementation iEverAppDelegate

+ (iEverAppDelegate *)shareAppDelegate
{
	return (iEverAppDelegate *)[[UIApplication sharedApplication] delegate];
}

-(void)configThirdPlatformKey
{
    /* UM 统计 */
    [MobClick startWithAppkey        :UMAPPKEY
                         reportPolicy:BATCH
                            channelId:nil];

    /* UM 社会分享 */
    [UMSocialData setAppKey          :UMAPPKEY];

    /* UM WXchat 社会分享&登录授权 */
    [UMSocialWechatHandler setWXAppId:@"wx6d8f7558873e6093"
                            appSecret:@"013dee85e7532ba71b9215bd1a7b5a91"
                                  url:@"http://www.iever.com.cn"];

    /* UM 反馈 */
    [[UMFeedback sharedInstance] setAppkey:UMAPPKEY
                                  delegate:nil];

    /* UM sina 登录授权 */
    [UMSocialSinaHandler openSSOWithRedirectURL:@"http://sns.whalecloud.com/sina2/callback"];

     /* UM 打点激活beta */
    [self umtrack];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{

    //[Fabric with:@[CrashlyticsKit]];

    [self configSplashView:launchOptions];

    return YES;
}

-(void)addRootView:(NSDictionary *)launchOptions{

    /* config User Notification */
    [self configUserNotification:launchOptions];

    /* info plist setting Status bar is initially hidden YES  here setStatusBarHidden NO */
    //[[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];

    /*some third platform config*/
    [self configThirdPlatformKey];

    /*config viewController Paradigm*/
    [self creatTabBar];

    self.tabBarController.selectedIndex = 0;

    iEver_LeftViewController *leftVC = [[iEver_LeftViewController alloc] init];
    MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                    containerWithCenterViewController:self.tabBarController
                                                    leftMenuViewController:leftVC
                                                    rightMenuViewController:nil];



    self.window.rootViewController = container;
    [self.window makeKeyAndVisible];

    /*GIF request anim*/
    [GiFHUD setGifWithImageName:@"pika.gif"];
}


-(void)creatTabBar{

    iEver_OfficialListViewController   *_mainViewController            = [[iEver_OfficialListViewController alloc] init];
    _mainViewController.categoryType                                   = 0;/* 0查询全部文章、10一级分类、11二级分类、12三级分类*/
    _mainViewController.category_id                                    = 0;
    CustomNavigationController         *_mainNavigationController      = [[CustomNavigationController alloc] initWithRootViewController:_mainViewController];

    iEver_AskAnswerViewController    *_VuserCenterViewController          = [[iEver_AskAnswerViewController alloc] init];
    CustomNavigationController         *_VuserCenterNavigationController  = [[CustomNavigationController alloc] initWithRootViewController:_VuserCenterViewController];

    iEver_RankViewController           *_rankViewController            = [[iEver_RankViewController alloc] init];
    CustomNavigationController         * _rankNavigationController     = [[CustomNavigationController alloc] initWithRootViewController:_rankViewController];

    iEver_ItemTryViewController        *_itemTryViewController         = [[iEver_ItemTryViewController alloc] init];
    CustomNavigationController         *_itemTryNavigationController   = [[CustomNavigationController alloc] initWithRootViewController:_itemTryViewController];
//
//    iEver_MYViewController   *_MYViewController    = [[iEver_MYViewController alloc] init];
//    CustomNavigationController         *_MYNavigationController    = [[CustomNavigationController alloc] initWithRootViewController:_MYViewController];

    self.tabBarController          = [[CustomTabBarViewController alloc] init];
    self.tabBarController.delegate = self;
    self.tabBarController.viewControllers = @[_mainNavigationController,
                                              _VuserCenterNavigationController,
                                              _rankNavigationController,
                                              _itemTryNavigationController];

    [[UINavigationBar appearance]  setBackgroundImage:[UIImage imageNamed:@"navigationbar_background_os7"] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance]  setTintColor:[UIColor blackColor]];
    [[UITabBar appearance]         setBackgroundImage:[UIImage imageNamed:@"tabbar_bg"]];
    NSDictionary *attributes       = @{ NSFontAttributeName: [UIFont fontWithName:@"ZHSRXT--GBK1-0" size:21],NSForegroundColorAttributeName: [UIColor blackColor]};
    [[UINavigationBar appearance]  setTitleTextAttributes:attributes];

    UITabBar *tabBar              = _tabBarController.tabBar;

    UITabBarItem *tabBarItem1     = [tabBar.items objectAtIndex:0];
    tabBarItem1.image             = [[UIImage imageNamed:@"tabbar_main_normal"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabBarItem1.selectedImage     = [[UIImage imageNamed:@"tabbar_main_click"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UITabBarItem *tabBarItem2     = [tabBar.items objectAtIndex:1];
    tabBarItem2.image             = [[UIImage imageNamed:@"tabbar_discover_normal"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabBarItem2.selectedImage     = [[UIImage imageNamed:@"tabbar_discover_click"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UITabBarItem *tabBarItem3     = [tabBar.items objectAtIndex:2];
    tabBarItem3.image             = [[UIImage imageNamed:@"tabbar_rank_normal"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabBarItem3.selectedImage     = [[UIImage imageNamed:@"tabbar_rank_click"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UITabBarItem *tabBarItem4     = [tabBar.items objectAtIndex:3];
    tabBarItem4.image             = [[UIImage imageNamed:@"tabbar_gift_normal"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabBarItem4.selectedImage     = [[UIImage imageNamed:@"tabbar_gift_click"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    UITabBarItem *tabBarItem5     = [tabBar.items objectAtIndex:4];
//    tabBarItem5.image             = [[UIImage imageNamed:@"tabbar_personCenter_normal"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    tabBarItem5.selectedImage     = [[UIImage imageNamed:@"tabbar_personCenter_click"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];


    // Change the title color of tab bar items
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       CLEAR, NSForegroundColorAttributeName,nil ,nil] forState:UIControlStateNormal];

    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       CLEAR, NSForegroundColorAttributeName,nil ,nil] forState:UIControlStateHighlighted];
    int offset = 5;
    UIEdgeInsets imageInset = UIEdgeInsetsMake(offset, 0, -offset, 0);
    tabBarItem1.imageInsets = imageInset;
    tabBarItem2.imageInsets = imageInset;
    tabBarItem3.imageInsets = imageInset;
    tabBarItem4.imageInsets = imageInset;
   // tabBarItem5.imageInsets = imageInset;

}


-(void)configUserNotification:(NSDictionary *)launchOptions{

    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
#if __IPHONE_OS_VERSION_MAX_ALLOWED > __IPHONE_7_1
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
        //可以添加自定义categories
        [APService registerForRemoteNotificationTypes:(UIUserNotificationTypeBadge |
                                                       UIUserNotificationTypeSound |
                                                       UIUserNotificationTypeAlert)
                                           categories:nil];
    } else {
        //categories 必须为nil
        [APService registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
                                                       UIRemoteNotificationTypeSound |
                                                       UIRemoteNotificationTypeAlert)
                                           categories:nil];
    }
#else
    //categories 必须为nil
    [APService registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
                                                   UIRemoteNotificationTypeSound |
                                                   UIRemoteNotificationTypeAlert)
                                       categories:nil];
#endif
    // Required
    [APService setupWithOption:launchOptions];
}

#pragma mark -
#pragma mark - JPush 注册推送及相应推送消息，通知到某个控制器

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {

    [APService registerDeviceToken:deviceToken];
    [[NSUserDefaults standardUserDefaults] setObject:[APService registrationID] forKey:@"pushToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {

}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {

    if (application.applicationState == UIApplicationStateActive){
        UIAlertView *alterView = [[UIAlertView alloc] initWithTitle:@"消息通知"
                                                            message:@"您有最新消息，是否现在进行查看？"
                                                           delegate:nil
                                                  cancelButtonTitle:@"取消"
                                                  otherButtonTitles:@"确定", nil];

        [alterView.rac_buttonClickedSignal subscribeNext:^(NSNumber *x) {
            if ([x integerValue]) {

                [[NSNotificationCenter defaultCenter] postNotificationName:ReceiveRemoteNotification object:self userInfo:userInfo];
            }
        }];
        [alterView show];

    }
    else  if ( application.applicationState == UIApplicationStateInactive ) {

        [[NSNotificationCenter defaultCenter] postNotificationName:ReceiveRemoteNotification object:self userInfo:userInfo];
    }
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [APService handleRemoteNotification:userInfo];
    completionHandler(UIBackgroundFetchResultNewData);

}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{

    if (application.applicationState == UIApplicationStateActive){
        UIAlertView *alterView = [[UIAlertView alloc] initWithTitle:@"消息通知"
                                                            message:@"您有最新消息，是否现在进行查看？"
                                                           delegate:nil
                                                  cancelButtonTitle:@"取消"
                                                  otherButtonTitles:@"确定", nil];

        [alterView.rac_buttonClickedSignal subscribeNext:^(NSNumber *x) {
            if ([x integerValue]) {

                [[NSNotificationCenter defaultCenter] postNotificationName:ReceiveRemoteNotification object:self userInfo:userInfo];
            }
        }];
        [alterView show];

    }
    else  if ( application.applicationState == UIApplicationStateInactive ) {

        [[NSNotificationCenter defaultCenter] postNotificationName:ReceiveRemoteNotification object:self userInfo:userInfo];
    }

    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];

    [APService handleRemoteNotification:userInfo];

}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /* 网络提醒 */

    BOOL isNetworkWorking = [[iEver_Global Instance] isExistenceNetwork];
    if (!isNetworkWorking) {
        UIAlertView *myalert = [[UIAlertView alloc] initWithTitle:@"警告"
                                                          message:NetWorkError
                                                         delegate:nil
                                                cancelButtonTitle:@"确认"
                                                otherButtonTitles:nil];
        [myalert show];
    }
    

    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];

   /* 因为问答用到推送，这个在激活的时候将极光推送码再发一遍，确保在线 */

    if ([APService registrationID].length > 0) {
        
        iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
        NSString * pathUrl = [NSString stringWithFormat:@"/push/sendPushToken/%@/ios",[APService registrationID]];
        [[[content sendPushToken:nil path:pathUrl] deliverOn:[RACScheduler mainThreadScheduler]]
         subscribeNext:^(NSDictionary *object) {
             
         }];
    }


}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}



#pragma mark -
#pragma mark - 开机启动动画，依次出现逻辑


-(void)configSplashView:(NSDictionary *)launchOptions {

    /* config cookie null string */
    if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"loginKey"] isKindOfClass:[NSString class]]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"loginKey"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"userId"];
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"userType"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }


    dic = launchOptions;

    splashView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    splashView.backgroundColor = WHITE;

    UIImage *splashView_logo = [UIImage imageNamed:@"splashView_logo"];

    UIImageView *imageview = [[UIImageView alloc]initWithFrame:CGRectMake(118, 519, splashView_logo.size.width, splashView_logo.size.height)];
    imageview.image = splashView_logo;
    [splashView addSubview:imageview];

    self.window                         = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window addSubview:splashView];
    [self.window bringSubviewToFront:splashView];
    [self.window makeKeyAndVisible];

    [self performSelector:@selector(scale_1) withObject:nil afterDelay:0.0f];
    [self performSelector:@selector(scale_2) withObject:nil afterDelay:0.1f];
    [self performSelector:@selector(scale_3) withObject:nil afterDelay:0.2f];
    [self performSelector:@selector(scale_4) withObject:nil afterDelay:0.3f];
    [self performSelector:@selector(scale_5) withObject:nil afterDelay:0.4f];
    [self performSelector:@selector(scale_6) withObject:nil afterDelay:0.5f];
    [self performSelector:@selector(scale_7) withObject:nil afterDelay:0.6f];
    [self performSelector:@selector(scale_8) withObject:nil afterDelay:0.7f];
    [self performSelector:@selector(scale_9) withObject:nil afterDelay:0.8f];
    [self performSelector:@selector(showWord1) withObject:nil afterDelay:1.5f];
    [self performSelector:@selector(showLine) withObject:nil afterDelay:2.0f];
    
}


-(void)scale_1
{
    UIImage *splashView_icon1 = [UIImage imageNamed:@"splashView_icon1"];
    UIImageView *round_1 = [[UIImageView alloc]initWithFrame:CGRectMake(125 + splashView_icon1.size.width*0.1, 140 + splashView_icon1.size.height*0.1, splashView_icon1.size.width*0.8, splashView_icon1.size.height*0.8)];
    round_1.image = splashView_icon1;
    round_1.alpha = 0.0;
    [splashView addSubview:round_1];
    [self setAnimation:round_1];
}

-(void)scale_2
{
    UIImage *plashView_icon2 = [UIImage imageNamed:@"splashView_icon2"];
    UIImageView *round_2 = [[UIImageView alloc]initWithFrame:CGRectMake(154+ plashView_icon2.size.width*0.1, 140 + plashView_icon2.size.height*0.1, plashView_icon2.size.width*0.8, plashView_icon2.size.height*0.8)];
    round_2.image = plashView_icon2;
    round_2.alpha = 0.0;
    [splashView addSubview:round_2];
    [self setAnimation:round_2];
}

-(void)scale_3
{
    UIImage *plashView_icon3 = [UIImage imageNamed:@"splashView_icon3"];
    UIImageView *round_3 = [[UIImageView alloc]initWithFrame:CGRectMake(182+ plashView_icon3.size.width*0.1, 140+ plashView_icon3.size.height*0.1, plashView_icon3.size.width*0.8, plashView_icon3.size.height*0.8)];
    round_3.image = plashView_icon3;
    round_3.alpha = 0.0;
    [splashView addSubview:round_3];
    [self setAnimation:round_3];
}

-(void)scale_4
{
    UIImage *plashView_icon4 = [UIImage imageNamed:@"splashView_icon4"];
    UIImageView *round_4 = [[UIImageView alloc]initWithFrame:CGRectMake(182+ plashView_icon4.size.width*0.1, 170+ plashView_icon4.size.height*0.1, plashView_icon4.size.width*0.8, plashView_icon4.size.height*0.8)];
    round_4.image = plashView_icon4;
    round_4.alpha = 0.0;
    [splashView addSubview:round_4];
    [self setAnimation:round_4];

}

-(void)scale_5
{
    UIImage *plashView_icon5 = [UIImage imageNamed:@"splashView_icon5"];
    UIImageView *round_5 = [[UIImageView alloc]initWithFrame:CGRectMake(182+ plashView_icon5.size.width*0.1, 200+ plashView_icon5.size.height*0.1, plashView_icon5.size.width*0.8, plashView_icon5.size.height*0.8)];
    round_5.image = plashView_icon5;
    round_5.alpha = 0.0;
    [splashView addSubview:round_5];
    [self setAnimation:round_5];
}

-(void)scale_6
{
    UIImage *plashView_icon6 = [UIImage imageNamed:@"splashView_icon6"];
    UIImageView *round_6 = [[UIImageView alloc]initWithFrame:CGRectMake(154+ plashView_icon6.size.width*0.1, 200+ plashView_icon6.size.height*0.1, plashView_icon6.size.width*0.8, plashView_icon6.size.height*0.8)];
    round_6.image = plashView_icon6;
    round_6.alpha = 0.0;
    [splashView addSubview:round_6];
    [self setAnimation:round_6];
}


-(void)scale_7
{
    UIImage *plashView_icon7 = [UIImage imageNamed:@"splashView_icon7"];
    UIImageView *round_7 = [[UIImageView alloc]initWithFrame:CGRectMake(125+ plashView_icon7.size.width*0.1, 200+ plashView_icon7.size.height*0.1, plashView_icon7.size.width*0.8, plashView_icon7.size.height*0.8)];
    round_7.image = plashView_icon7;
    round_7.alpha = 0.0;
    [splashView addSubview:round_7];
    [self setAnimation:round_7];
}

-(void)scale_8
{
    UIImage *plashView_icon8 = [UIImage imageNamed:@"splashView_icon8"];
    UIImageView *round_8 = [[UIImageView alloc]initWithFrame:CGRectMake(125+ plashView_icon8.size.width*0.1, 170+ plashView_icon8.size.height*0.1, plashView_icon8.size.width*0.8, plashView_icon8.size.height*0.8)];
    round_8.image = plashView_icon8;
    round_8.alpha = 0.0;
    [splashView addSubview:round_8];
    [self setAnimation:round_8];
}


-(void)scale_9
{
    UIImage *plashView_icon9 = [UIImage imageNamed:@"splashView_icon9"];
    UIImageView *round_9 = [[UIImageView alloc]initWithFrame:CGRectMake(154+ plashView_icon9.size.width*0.1, 170+ plashView_icon9.size.height*0.1, plashView_icon9.size.width*0.8, plashView_icon9.size.height*0.8)];
    round_9.image = plashView_icon9;
    round_9.alpha = 0.0;
    [splashView addSubview:round_9];
    [self setAnimation:round_9];
}


-(void)setAnimation:(UIImageView *)nowView
{

    [UIView animateWithDuration:0.1f delay:0.0f options:UIViewAnimationOptionCurveLinear
                     animations:^
    {
        // 执行的动画code
        nowView.alpha = 1.0;
        [nowView setFrame:CGRectMake(nowView.frame.origin.x- nowView.frame.size.width*0.1, nowView.frame.origin.y-nowView.frame.size.height*0.1, nowView.frame.size.width*1.2, nowView.frame.size.height*1.2)];
    }
                     completion:^(BOOL finished)
    {
        // 完成后执行code
       // [nowView removeFromSuperview];
    }
     ];


}

-(void)showWord1
{

    UIImage *splashView_s = [UIImage imageNamed:@"splashView_s"];

    UIImageView *imageview = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth/2 - splashView_s.size.width/2, 275, splashView_s.size.width, splashView_s.size.height)];
    imageview.image = splashView_s;
    [splashView addSubview:imageview];

    imageview.alpha = 0.0;
    [UIView animateWithDuration:0.5f delay:0.0f options:UIViewAnimationOptionCurveLinear
                     animations:^
    {
        imageview.alpha = 1.0;
    }
                     completion:^(BOOL finished)
    {

    }
     ];
}

-(void)showLine
{

    UIImage *splashView_last = [UIImage imageNamed:@"splashView_last"];

    UIImageView *imageview = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth/2 - splashView_last.size.width/2, 305, splashView_last.size.width, splashView_last.size.height)];
    imageview.image = splashView_last;
    [splashView addSubview:imageview];

    imageview.alpha = 0.0;
    [UIView animateWithDuration:0.5f delay:0.0f options:UIViewAnimationOptionCurveLinear
                     animations:^
     {
         imageview.alpha = 1.0;
     }
                     completion:^(BOOL finished)
     {

         // 完成后执行code
         [NSThread sleepForTimeInterval:1.0f];
         [splashView removeFromSuperview];
          [self addRootView:dic];
     }
     ];
}


#pragma mark -
#pragma mark - WXchat & sina 授权登录回调

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    return  [UMSocialSnsService handleOpenURL:url];
}
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return  [UMSocialSnsService handleOpenURL:url];
}



#pragma mark -
#pragma mark - UM track 追踪渠道下载量 原理封装了APP store URL 短链接追踪，区分哪个渠道下点击并且下载的数量
-(void)umtrack {

    NSString * appKey = @"551e0958fd98c583530007ea";
    NSString * deviceName = [[[UIDevice currentDevice] name] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString * mac = [self macString];
    NSString * idfa = [self idfaString];
    NSString * idfv = [self idfvString];
    NSString * urlString = [NSString stringWithFormat:@"http://log.umtrack.com/ping/%@/?devicename=%@&mac=%@&idfa=%@&idfv=%@", appKey, deviceName, mac, idfa, idfv];
    [NSURLConnection connectionWithRequest:[NSURLRequest requestWithURL: [NSURL URLWithString:urlString]] delegate:nil];
    
}

- (NSString * )macString{
    int mib[6];
    size_t len;
    char *buf;
    unsigned char *ptr;
    struct if_msghdr *ifm;
    struct sockaddr_dl *sdl;

    mib[0] = CTL_NET;
    mib[1] = AF_ROUTE;
    mib[2] = 0;
    mib[3] = AF_LINK;
    mib[4] = NET_RT_IFLIST;

    if ((mib[5] = if_nametoindex("en0")) == 0) {
        printf("Error: if_nametoindex error\n");
        return NULL;
    }

    if (sysctl(mib, 6, NULL, &len, NULL, 0) < 0) {
        printf("Error: sysctl, take 1\n");
        return NULL;
    }

    if ((buf = malloc(len)) == NULL) {
        printf("Could not allocate memory. error!\n");
        return NULL;
    }

    if (sysctl(mib, 6, buf, &len, NULL, 0) < 0) {
        printf("Error: sysctl, take 2");
        free(buf);
        return NULL;
    }

    ifm = (struct if_msghdr *)buf;
    sdl = (struct sockaddr_dl *)(ifm + 1);
    ptr = (unsigned char *)LLADDR(sdl);
    NSString *macString = [NSString stringWithFormat:@"%02X:%02X:%02X:%02X:%02X:%02X",
                           *ptr, *(ptr+1), *(ptr+2), *(ptr+3), *(ptr+4), *(ptr+5)];
    free(buf);

    return macString;
}

- (NSString *)idfaString {

    NSBundle *adSupportBundle = [NSBundle bundleWithPath:@"/System/Library/Frameworks/AdSupport.framework"];
    [adSupportBundle load];

    if (adSupportBundle == nil) {
        return @"";
    }
    else{

        Class asIdentifierMClass = NSClassFromString(@"ASIdentifierManager");

        if(asIdentifierMClass == nil){
            return @"";
        }
        else{

            //for no arc
            //ASIdentifierManager *asIM = [[[asIdentifierMClass alloc] init] autorelease];
            //for arc
            ASIdentifierManager *asIM = [[asIdentifierMClass alloc] init];

            if (asIM == nil) {
                return @"";
            }
            else{

                if(asIM.advertisingTrackingEnabled){
                    return [asIM.advertisingIdentifier UUIDString];
                }
                else{
                    return [asIM.advertisingIdentifier UUIDString];
                }
            }
        }
    }
}

- (NSString *)idfvString
{
    if([[UIDevice currentDevice] respondsToSelector:@selector( identifierForVendor)]) {
        return [[UIDevice currentDevice].identifierForVendor UUIDString];
    }
    
    return @"";
}


@end
