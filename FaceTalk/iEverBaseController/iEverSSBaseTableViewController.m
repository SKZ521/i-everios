//
//  iEverSSBaseTableViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-13.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEverSSBaseTableViewController.h"
#import "SVPullToRefresh.h"

typedef NS_ENUM( NSUInteger, SSDataSourcesReloadType ) {
    SSDataSourcesReloadDefault = 0,
    SSDataSourcesReloadPull,
    SSDataSourcesReloadDrop,
};
@interface iEverSSBaseTableViewController ()
@property (nonatomic, assign)SSDataSourcesReloadType reloadType;
@end

@implementation iEverSSBaseTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithBarType:(SSNavBarType)barType
{
    self = [super init];
    if (self) {
        self.navBarType = barType;
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [GiFHUD dismiss];
	[super viewWillDisappear:animated];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    /* set default backgroundView */
    UIImageView *_backImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    _backImageView.image = [UIImage imageNamed:@"backImage"];
    self.tableView.backgroundView = _backImageView;
    /* Compatible iOS6 & iOS7 */
#ifdef __IPHONE_7_0
	if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
		self.automaticallyAdjustsScrollViewInsets = NO;
		self.extendedLayoutIncludesOpaqueBars = NO;

		self.view.frame =  CGRectMake(0, 0, ScreenWidth, ScreenHeight - 20);
	}
#endif

    //初始化刷新状态
    self.page = 1;
    self.reloadType = SSDataSourcesReloadPull;

    /* gat page 1 data */
    __weak iEverSSBaseTableViewController *weakSelf = self;
	// setup pull-to-refresh 刷新
	[self.tableView addPullToRefreshWithActionHandler:^{
        NSLog(@"下拉刷新");
		weakSelf.reloadType = SSDataSourcesReloadPull;
        weakSelf.page = 1;
		[weakSelf manualRefreh];
	}];

	/* gat more */
	[self.tableView addInfiniteScrollingWithActionHandler:^{
        NSLog(@"加载更多");
		weakSelf.reloadType = SSDataSourcesReloadDrop;
        weakSelf.page += 1;
		[weakSelf manualRefreh];
    }];

    self.tableView.showsInfiniteScrolling = YES;
    self.tableView.editing = NO;
    self.dataSource = [[SSArrayDataSource alloc] initWithItems:nil];
    self.tableView.dataSource = self.dataSource;
	self.dataSource.tableView = self.tableView;
    self.dataSource.tableView.editing = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;


    [self addLeftBarButtonItem:nil];
}

#pragma mark - 刷新数据

/* gat page 1 data */
- (void)manualRefreh
{
    [self fetchData];
}

/* gat  data */
- (void)fetchData
{

    NSLog(@"fetchData base viewController");
}

//data source
- (void)reloadDatasouce:(NSArray *)objects
{
    [self refreshViewStopAnimating];
    switch (self.reloadType) {
        case SSDataSourcesReloadPull: {
            [self.dataSource updateItems:objects];
        }
            break;
        case SSDataSourcesReloadDrop: {
            [self.dataSource appendItems:objects];
        }
            break;

        default:
            break;
    }

    if (objects.count < ONEPAGECOUNT) {
		self.tableView.showsInfiniteScrolling = NO;
	}else {
		self.tableView.showsInfiniteScrolling = YES;
	}

    //没有数据
    [self nothingView];
}

//加载数据完成 停止动画
- (void)refreshViewStopAnimating
{
	int64_t delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
		__weak iEverSSBaseTableViewController *weakSelf = self;
		[weakSelf.tableView.pullToRefreshView stopAnimating];
		[weakSelf.tableView.infiniteScrollingView stopAnimating];
    });
}

//hide pull refresh
- (void)showsPullToRefresh
{
	self.tableView.showsPullToRefresh = NO;
}

//下拉刷新数据（动画）
- (void)triggerPullToRefresh
{
    [self.tableView triggerPullToRefresh];
}

//空数据替换view
- (void)nothingView
{
	if ([self.dataSource allItems].count == 0) {
		UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_nothing_"]];
		self.tableView.tableFooterView = imageView;
	}else {
		self.tableView.tableFooterView = nil;
	}
}


//left nav bar
////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)addLeftBarButtonItem:(void (^)(UIButton *sender))block
{
    __typeof (self) __weak weakSelf = self;

    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *return_image = [UIImage imageNamed:@"NAV_return"];
    if (self.navBarType) {
        leftButton.frame = CGRectMake(0, 0, 44, 44);
        [leftButton setTitleColor:MY_PURPLECOLOR forState:UIControlStateNormal];
        [leftButton setTitle:@"取消" forState:UIControlStateNormal];
    }else {
        leftButton.frame            = CGRectMake(0, 0, return_image.size.width, return_image.size.height);
        if (iOS7) {
            [leftButton setImage:return_image forState:UIControlStateNormal];
        }else {
            [leftButton setImage:return_image forState:UIControlStateNormal];
        }
    }
    [[leftButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *sender) {
        if (weakSelf.navBarType) {
            [weakSelf dismissViewControllerAnimated:YES completion:^{

            }];
        }else {
            [weakSelf.view endEditing:YES];
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
}

//right nav bar
////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)addRightBarButtonItem:(void (^)(UIButton *sender))block
{
	UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
	rightButton.frame = CGRectMake(0, 10, 36, 44);
    rightButton.titleLabel.font = TextFont;
	//[rightButton setTitle:@"筛选" forState:UIControlStateNormal];
    int offset = 5;
    UIEdgeInsets imageInset = UIEdgeInsetsMake(offset, 0, -offset, 0);
    rightButton.titleEdgeInsets = imageInset;
	[rightButton setTitleColor:WHITE forState:UIControlStateNormal];
    [rightButton setImage:[UIImage imageNamed:@"main_list_fenlei"] forState:UIControlStateNormal];
	rightButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
	[[rightButton rac_signalForControlEvents:UIControlEventTouchUpInside ] subscribeNext:^(UIButton *sender) {
		return block(sender);
	}];
	UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
	self.navigationItem.rightBarButtonItem = rightBarButton;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return UIInterfaceOrientationPortrait;
}

- (BOOL)shouldAutorotate{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{

    return UIInterfaceOrientationPortrait;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView

           editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  UITableViewCellEditingStyleNone;

}
@end
