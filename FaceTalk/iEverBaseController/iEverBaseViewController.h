//
//  iEverBaseViewController.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-13.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, NavBarType) {
	kBackButton = 0,
	kCloseButton
};

@interface iEverBaseViewController : UIViewController

@property (nonatomic, assign) NavBarType navBarType;

- (id)initWithBarType:(NavBarType)barType;
- (void)addLeftBarButtonItem:(void (^)(UIButton *sender))block;
- (void)addRightBarButtonItem:(void (^)(UIButton *sender))block;

/**
 * 用户/达人主页入口
 */
- (void)gotoUserHomeWithUserId:(NSString *)userId;
@end
