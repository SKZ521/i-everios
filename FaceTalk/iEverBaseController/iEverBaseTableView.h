//
//  iEverBaseTableView.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-13.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface iEverBaseTableView : UITableView

@property (strong, nonatomic) NSMutableArray * dataSourceArray;
@property (nonatomic, assign) int              page;

- (id)initWithFrame:(CGRect)frame reloadData:(void (^)(id data, NSError *error))block;

- (void)fetchData;
- (void)addDataToDataSource:(NSArray *)objects;
- (void)manualRefreh;
@end
