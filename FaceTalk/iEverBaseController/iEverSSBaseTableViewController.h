//
//  iEverSSBaseTableViewController.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-13.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SSDataSources.h>

typedef NS_ENUM(NSInteger, SSNavBarType) {
    SSkBackButton = 0,
    SSkCloseButton
};

@interface iEverSSBaseTableViewController : UITableViewController

@property (nonatomic, assign) SSNavBarType navBarType;

- (id)initWithBarType:(SSNavBarType)barType;

@property (nonatomic, assign) NSInteger             page;
@property (nonatomic, strong) SSArrayDataSource     *dataSource;
//@property (nonatomic, strong) SSSectionedDataSource *sectionDataSource;

//deprecated property; use SSArrayDataSource * dataSource instead;
@property (nonatomic, strong) NSMutableArray        *dataSourceArray;
@property (nonatomic, strong) NSMutableArray        *filteredActivities;
@property (nonatomic, strong) NSMutableArray        *activities;

//add data source
- (void)reloadDatasouce:(NSArray *)objects;

//请求数据
- (void)fetchData;

//add nav bar; lfet and right
- (void)addLeftBarButtonItem:(void (^)(UIButton *sender))block;
- (void)addRightBarButtonItem:(void (^)(UIButton *sender))block;

//start pull refresh with animation
- (void)triggerPullToRefresh;

//hide pull refresh
- (void)showsPullToRefresh;
@end
