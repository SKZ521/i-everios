//
//  iEver_applyItemViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-31.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_applyItemViewController.h"
#import "iEver_tryItemDetailObject.h"
#import "iEver_ItemDetailViewController.h"
@interface iEver_applyItemViewController (){

}
@property (strong) IBOutlet UIScrollView *scrollView;

@property (strong) IBOutlet UIImageView  *item_imageview;

@property (strong) IBOutlet UILabel      *item_name;
@property (strong) IBOutlet UILabel      *item_price;

@property (strong) IBOutlet UILabel      *headStr;

@property (strong) IBOutlet UIView       *userDetailBaseView;

@property (strong) IBOutlet UILabel      *nameStr;
@property (strong) IBOutlet UIView       *nameBaseView;
@property (strong) IBOutlet UITextField  *userName_textField;

@property (strong) IBOutlet UILabel      *adressStr;
@property (strong) IBOutlet UILabel      *adressPlacholderStr;
@property (strong) IBOutlet UIView       *adressBaseView;
@property (strong) IBOutlet UITextView   *adress_textField;

@property (strong) IBOutlet UILabel      *phoneStr;
@property (strong) IBOutlet UITextField  *phone_textField;
@property (strong) IBOutlet UIView       *phoneBaseView;

@property (strong) IBOutlet UIButton     *commit_button;

- (IBAction)buttonAction:(id)sender;
@end

@implementation iEver_applyItemViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    /* scrollView  */
    [self backScrollView];
}

-(void)backScrollView{

    _scrollView.scrollEnabled = YES;
    CGSize newSize = CGSizeMake(self.view.frame.size.width, 700.0);
    [_scrollView setContentSize:newSize];
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"申请信息";

    /* item detail */

    [_item_imageview setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/200x",self.imageview]] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
    _item_name.text = self.name;
    _item_price.text = self.price;

    [self fetchData];

    /* fix custom text Font */

    [self customButtonFont];

}

-(void)customButtonFont {

    self.item_name.font = Title_Font;
    self.item_price.font = Title_Font;

    CALayer *userDetailBaseView_layer = [self.userDetailBaseView layer];
    [userDetailBaseView_layer setMasksToBounds:YES];
    [userDetailBaseView_layer setCornerRadius:5.0];
    [userDetailBaseView_layer setBorderWidth:0.5];
    [userDetailBaseView_layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    self.headStr.font = Title_Font;
    self.headStr.textColor = PERSON_red;

    CALayer *nameBaseView_layer = [self.nameBaseView layer];
    [nameBaseView_layer setMasksToBounds:YES];
    [nameBaseView_layer setCornerRadius:5.0];
    [nameBaseView_layer setBorderWidth:0.5];
    [nameBaseView_layer setBorderColor:[[UIColor darkGrayColor] CGColor]];
    self.nameStr.font = Title_Font;
    self.userName_textField.font = Title_Font;

    CALayer *adressBaseView_layer = [self.adressBaseView layer];
    [adressBaseView_layer setMasksToBounds:YES];
    [adressBaseView_layer setCornerRadius:5.0];
    [adressBaseView_layer setBorderWidth:0.5];
    [adressBaseView_layer setBorderColor:[[UIColor darkGrayColor] CGColor]];
    self.adressStr.font = Title_Font;
    self.adressPlacholderStr.font = Title_Font;
    self.adress_textField.font = Title_Font;

    CALayer *phoneBaseView_layer = [self.phoneBaseView layer];
    [phoneBaseView_layer setMasksToBounds:YES];
    [phoneBaseView_layer setCornerRadius:5.0];
    [phoneBaseView_layer setBorderWidth:0.5];
    [phoneBaseView_layer setBorderColor:[[UIColor darkGrayColor] CGColor]];
    self.phoneStr.font = Title_Font;
    self.phone_textField.font = Title_Font;

    self.commit_button.titleLabel.font = Title_V_Fonts;
    self.commit_button.backgroundColor = MY_PURPLECOLOR;
    CALayer *coverButton = [self.commit_button layer];
    [coverButton setMasksToBounds:YES];
    [coverButton setCornerRadius:5.0];
    
}


/*request data*/
-(void)fetchData{

    @weakify(self);
    iEver_tryItemDetailObject *detail_Object = [[iEver_tryItemDetailObject alloc] init];
    NSString *_pathUrl = [NSString stringWithFormat:@"/itemTryApply/queryByUserId/%d",self.itemTryId];
    [[[detail_Object itemTryApplyQueryByUserId:nil path:_pathUrl] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(iEver_itemTryApply_object *object) {

         @strongify(self);

         if (![object.itemTryApply isEqualToString:@"NULL"]) {
             self.userName_textField.text = object.realName;

             if (object.address.length > 0) {
                 self.adressPlacholderStr.hidden = YES;
             }else {
                 self.adressPlacholderStr.hidden = NO;
             }
             self.adress_textField.text = object.address;
             self.phone_textField.text = object.phone;
             [self.commit_button setTitle:@"修改申请资料" forState:UIControlStateNormal];
         }else{

             [self.commit_button setTitle:@"提交" forState:UIControlStateNormal];
         }


     }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)buttonAction:(id)sender{

    UIButton *button = (UIButton *)sender;
	NSInteger index = button.tag - 10;
    switch (index) {
        case 0:{


            if (self.userName_textField.text.length == 0) {
                [SVProgressHUD showImage:Nil status:@"请输入正确的姓名"];
                return;
            }
            if (self.userName_textField.text.length < 2 ) {
                [SVProgressHUD showImage:Nil status:@"昵称太短"];
                return;
            }
            if (self.userName_textField.text.length > 20 ) {
                [SVProgressHUD showImage:Nil status:@"昵称太长"];
                return;
            }
            if (self.adress_textField.text.length == 0 ) {
                [SVProgressHUD showImage:Nil status:@"请输入正确的地址"];
                return;
            }
            if (self.adress_textField.text.length < 6 ) {
                [SVProgressHUD showImage:Nil status:@"地址不够详细吧"];
                return;
            }
            if (self.adress_textField.text.length > 50 ) {
                [SVProgressHUD showImage:Nil status:@"地址也太长了吧"];
                return;
            }
            if (![[iEver_Global Instance] validateMobile:self.phone_textField.text]) {
                [SVProgressHUD showImage:Nil status:@"请输入正确的手机号"];
                return;
            }

            [SVProgressHUD showWithStatus:@"提交中..." maskType:SVProgressHUDMaskTypeBlack];
            iEver_tryItemDetailObject * content = [[iEver_tryItemDetailObject alloc] init];
            NSDictionary *dic = @{@"realName": self.userName_textField.text,
                                  @"address": self.adress_textField.text,
                                  @"phone": self.phone_textField.text,
                                  @"itemTryId": [NSNumber numberWithInt:self.itemTryId],
                                  @"itemId": [NSNumber numberWithInt:self.itemId],
                                  };

            [[[content itemTryApply:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
             subscribeNext:^(NSDictionary *object) {
                 if ([[object objectForKey:@"resultCode"] integerValue] == 1) {

                     [SVProgressHUD showImage:Nil status:@"申请成功"];
                     [self.navigationController popViewControllerAnimated:YES];
                 }else{
                     NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
                     [SVProgressHUD showImage:Nil status:codeStr];
                 }
                 
             }];
        }
            break;
        case 1:{

            iEver_ItemDetailViewController *_itemlDetailViewController = [[iEver_ItemDetailViewController alloc]init];
            _itemlDetailViewController.type_id = self.itemId;
            [self.navigationController pushViewController:_itemlDetailViewController animated:YES];
            IOS7POP;
        }
            break;

            

        default:
            break;
    }

}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark -
#pragma mark UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollview
{
    [self.userName_textField resignFirstResponder];
    [self.adress_textField resignFirstResponder];
    [self.phone_textField resignFirstResponder];
}

#pragma mark - UITextView Delegate Methods

- (void)textViewDidChange:(UITextView *)textView {

    NSInteger number = [textView.text length];
    if (number == 0) {
        self.adressPlacholderStr.hidden = NO;
    }
    if (number > 0) {
        self.adressPlacholderStr.hidden = YES;
    }
}
@end
