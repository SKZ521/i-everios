//
//  iEver_Vlist_detailCommentViewController.h
//  FaceTalk
//
//  Created by 开仲 司 on 15/4/12.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEverBaseTableViewController.h"

@interface iEver_Vlist_detailCommentViewController : iEverBaseTableViewController

@property (nonatomic, assign) int                        type_id;       /* id */

@end
