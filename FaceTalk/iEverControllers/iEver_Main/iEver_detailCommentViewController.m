//
//  iEver_detailCommentViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 14/12/23.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_detailCommentViewController.h"
#import "iEver_detailCommentObject.h"
#import "iEver_comment_cell.h"
#import "iEver_commentObject.h"

@interface iEver_detailCommentViewController ()

@property (retain, nonatomic) UIView                    *mainView;
@property (retain, nonatomic) UIButton                  *bottomButton;
@property (retain, nonatomic) UITextView                *myTextView;
@property (nonatomic, strong) UILabel                   *commentLab;

@end

@implementation iEver_detailCommentViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.title = @"全部评论";
    /* NO show pull refresh */
    self.tableView.showsPullToRefresh = YES;
    self.tableView.showsInfiniteScrolling = YES;
    self.tableView.frame = CGRectMake(0, 0.0, ScreenWidth, ScreenHeight - BARHeight - NAVHeight);
    [self.tableView  setSeparatorStyle:UITableViewCellSeparatorStyleNone];

    /* request data */
    [self fetchData];

    /* comment textview */
    [self creatTextView];
}
/*request data*/
-(void)fetchData{

    @weakify(self);
    iEver_detailCommentObject *comment_Object = [[iEver_detailCommentObject alloc] init];
    NSString *_pathUrl = [NSString stringWithFormat:@"/articleComment/queryByCoverId/%d/%d",self.commentCover_id,self.page];
    [[[comment_Object articleComment:nil path:_pathUrl] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(iEver_detailCommentObject *object) {

         @strongify(self);
         if (object.pageSize < self.page) {
             self.tableView.showsInfiniteScrolling = NO;
         }else{

             [self addDataToDataSource:object._commentList];
             [self.tableView reloadData];
         }

     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataSourceArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"commentCell";
    UITableViewCell *cell = nil;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell = (iEver_comment_cell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    // Configure the cell...
    if (!cell) {

        cell = [[iEver_comment_cell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }

    iEver_comment_cell *message_cell = (iEver_comment_cell *)cell;
    iEver_articleCommentList_object * object = [self.dataSourceArray objectAtIndex:indexPath.row];
    [message_cell setObject:object];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id object = [self.dataSourceArray objectAtIndex:indexPath.row];
    return [iEver_comment_cell heightForObject:object atIndexPath:indexPath tableView:tableView];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


// 根据键盘状态，调整_mainView的位置
- (void) changeContentViewPoint:(NSNotification *)notification{

    NSDictionary *userInfo  = [notification userInfo];
    NSValue *value          = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGFloat keyBoardEndY    = value.CGRectValue.origin.y;  // 得到键盘弹出后的键盘视图所在y坐标
    CGFloat kStateBarHeight = 0.0;

    NSNumber *duration      = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve         = [userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];

    // 添加移动动画，使视图跟随键盘移动
    [UIView animateWithDuration:duration.doubleValue animations:^{
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationCurve:[curve intValue]];
        self.mainView.center = CGPointMake(self.mainView.center.x, keyBoardEndY - kStateBarHeight - self.mainView.bounds.size.height/2.0 - NAVHeight);   // keyBoardEndY的坐标包括了状态栏的高度，要减去  隐藏导航 NAVHeight
    }];

}


-(void)changeKeyboardWillHide:(NSNotification *)notification {

    self.bottomButton.hidden     = YES;
    [self.myTextView resignFirstResponder];
    self.mainView.frame          = CGRectMake(0, ScreenHeight - NAVHeight, ScreenWidth, 145.0);

}


-(void)insertCommentToReplyComment {

    [self.view bringSubviewToFront:self.mainView];
    self.bottomButton.hidden    = NO;
    self.commentLab.text        = [NSString stringWithFormat:@"回复 %@",self.toReplyCommentName];
    [self.myTextView becomeFirstResponder];

}

-(void)creatTextView {

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeContentViewPoint:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

    /* mainView 灰黑 BG*/
    self.bottomButton             = [UIButton buttonWithType:UIButtonTypeCustom];
    self.bottomButton.frame       = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
    self.bottomButton.hidden      = YES;
    self.bottomButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        self.comment_parentId = 0;
        self.bottomButton.hidden  = YES;
        [self.myTextView resignFirstResponder];
        self.mainView.frame       = CGRectMake(0, ScreenHeight, 320, 145.0);
        return [RACSignal empty];
    }];
    self.bottomButton.backgroundColor = BLACK;
    self.bottomButton.alpha           = 0.6;
    [self.view addSubview:self.bottomButton];

    /* mainView */
    self.mainView                     = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight, 320, 145.0)];
    self.mainView.backgroundColor     = BackkGroundColor;
    [self.view addSubview:self.mainView];

    /* textViewImage */
    UIImageView *textViewImage       = [[UIImageView alloc] initWithFrame:CGRectMake(15, 50, 290, 80)];
    textViewImage.image              = [UIImage imageNamed:@"commentContentBG"];
    [self.mainView addSubview:textViewImage];

    /* myTextView */
    self.myTextView                  = [[UITextView alloc]initWithFrame:CGRectMake(15, 50, 290, 80)];
    self.myTextView.font             = TextFont;
    self.myTextView.backgroundColor  = CLEAR;
    [self.mainView addSubview:self.myTextView];

    /* cancelBut */
    UIButton *cancelBut              = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBut.frame                  = CGRectMake(10, 10, 30, 30);
    [cancelBut setBackgroundImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateNormal];
    [cancelBut setBackgroundImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateHighlighted];
    cancelBut.rac_command            = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        self.comment_parentId = 0;
        self.bottomButton.hidden     = YES;
        [self.myTextView resignFirstResponder];
        self.mainView.frame          = CGRectMake(0, ScreenHeight, 320, 145.0);
        return [RACSignal empty];
    }];
    [self.mainView addSubview:cancelBut];

    /* commentLab */
    self.commentLab             = [[UILabel alloc] initWithFrame:CGRectMake(40, 10, 240, 25)];
    self.commentLab.text                 = @"评 论";
    self.commentLab.font                 = TextFont;
    self.commentLab.textAlignment        = NSTextAlignmentCenter;
    [self.mainView addSubview:self.commentLab];

    /* sendBut */
    UIButton *sendBut               = [UIButton buttonWithType:UIButtonTypeCustom];
    sendBut.frame                   = CGRectMake(280, 10, 30, 30);
    [sendBut setBackgroundImage:[UIImage imageNamed:@"send"] forState:UIControlStateNormal];
    [sendBut setBackgroundImage:[UIImage imageNamed:@"send"] forState:UIControlStateHighlighted];
    sendBut.rac_command             = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        /* insert Comment */
        [self insertCommentRequest];
        return [RACSignal empty];
    }];
    [self.mainView addSubview:sendBut];


    /* --- bottomView commentBut ------ */
    UIImage    * detail_bottomView = [UIImage imageNamed:@"detail_bottomView"];
    UIView *commentView            = [[UIView alloc] initWithFrame:CGRectMake(0, ScreenHeight - BARHeight - NAVHeight, detail_bottomView.size.width, detail_bottomView.size.height)];
    commentView.backgroundColor    = WHITE;
    [self.view addSubview:commentView];


    UIImageView *imageview         = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, detail_bottomView.size.width, detail_bottomView.size.height)];
    imageview.image                = detail_bottomView;
    [commentView addSubview:imageview];

    UIImage *detail_comment         = [UIImage imageNamed:@"detail_comment"];
    UIButton *commentBut            = [UIButton buttonWithType:UIButtonTypeCustom];
    commentBut.backgroundColor      = CLEAR;
    [commentBut setBackgroundImage:detail_comment forState:UIControlStateNormal];
    [commentBut setBackgroundImage:detail_comment forState:UIControlStateHighlighted];
    commentBut.frame = CGRectMake(ScreenWidth/2 - detail_comment.size.width/2, detail_bottomView.size.height/2 - detail_comment.size.height/2, detail_comment.size.width, detail_comment.size.height);
    commentBut.rac_command          = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        [self.view bringSubviewToFront:self.mainView];
        self.bottomButton.hidden    = NO;
        [self.myTextView becomeFirstResponder];
        self.commentLab.text        = @"评 论";
        return [RACSignal empty];
    }];
    [commentView addSubview:commentBut];

}
/* insert Comment */
-(void)insertCommentRequest{


    NSString *temp = [self.myTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

    if ([temp isEqualToString:@""]) {
        [SVProgressHUD showImage:Nil status:@"请输评论内容"];
        return ;
    }

    [SVProgressHUD showWithStatus:@"提交中..." maskType:SVProgressHUDMaskTypeBlack];
    iEver_commentObject * content = [[iEver_commentObject alloc] init];

    NSString *_pathUrl = [NSString stringWithFormat:@"/articleComment/insert"];
    NSDictionary *dic;

    /* 评论文章详情 */
    if (self.comment_parentId == 0) {

        dic = @{@"coverId": [NSNumber numberWithInt:_commentCover_id],
                @"commentContent": self.myTextView.text,
                @"type": [NSNumber numberWithInt:10],
                };
    }
    /* 回复评论 */
    else {

        dic = @{@"coverId": [NSNumber numberWithInt:_commentCover_id],
                @"parentId": [NSNumber numberWithInt:_comment_parentId],
                @"atUserId": [NSNumber numberWithInt:_comment_atUserId],
                @"commentContent": self.myTextView.text,
                @"type": [NSNumber numberWithInt:11],
                };
    }


    [[[content insertArticleComment:dic path:_pathUrl] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(NSDictionary *object) {
         if ([[object objectForKey:@"resultCode"] integerValue] == 1) {
             [SVProgressHUD showImage:Nil status:@"提交成功"];

             self.page = 1;

             [self.tableView triggerPullToRefresh];

         }else{
             NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
             [SVProgressHUD showImage:Nil status:codeStr];
         }

     }];
    self.bottomButton.hidden = YES;
    [self.myTextView resignFirstResponder];
    self.commentLab.text = @"评 论";
    self.comment_parentId = 0;
    self.myTextView.text = @"";
    self.mainView.frame = CGRectMake(0, ScreenHeight, 320, 145.0);
}


@end
