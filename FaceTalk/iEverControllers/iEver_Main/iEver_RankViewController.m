//
//  iEver_RankViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-28.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_RankViewController.h"
#import "iEver_rankItemObject.h"
#import "iEver_itemRank_cell.h"
#import "iEver_ItemDetailViewController.h"
#import "iEver_likeActionObject.h"
#import "CWStarRateView.h"
#import "MCPagerView.h"
#import "KEYPullDownMenu.h"

#define KScrollViewHeight 307.0f
#define KScrollViewWidth 270.0f
#define photo_modeHeight 216
#define photo_mode_viewY   ScreenHeight - photo_modeHeight - 44 - 20
#define kTitleMonthTag 1562
@interface iEver_RankViewController ()<UIScrollViewDelegate,MCPagerViewDelegate>
{

    iEver_rankItemObject *_rankOblect;
    int controlPage ;
    UIView *scrollBottomView;

    UIButton *bottomButton;
    UIButton *Confirmation;
    NSArray *pickerData;
    NSString *datePathUrl;

    NSArray *pickerData1;

    UILabel *lastSpecialTitle;
    UILabel *crrentSpecialTitle;
    UILabel *nextSpecialTitle;

    UIView *baseColorView;

    NSInteger  _pages;
}
@property (strong, nonatomic) UIScrollView  *scrollView;
@property (strong, nonatomic) UIPageControl *pageControl;
@property (strong, nonatomic) UILabel       *categoryName_time_lable;
@property (strong, nonatomic) MCPagerView *pagerView;

@property(nonatomic, strong)KEYPullDownMenuItem *selectedItem;
@end

@implementation iEver_RankViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.title = @"榜单";

        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@""
                                                        image:[UIImage imageNamed:@"tabbar_rank_normal"]
                                                selectedImage:[UIImage imageNamed:@"tabbar_rank_click"]];
        // init
        datePathUrl = @"all";
        controlPage = 0;
    }
    return self;
}

#pragma mark - view's lifecycle
- (void)viewWillDisappear:(BOOL)animated
{
    [GiFHUD dismiss];
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self createTitleView];

    /* set default backgroundView */
    self.tableView.backgroundView = ({
        UIImageView *_backImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
        _backImageView.image        = [UIImage imageNamed:@"backImage"];
        _backImageView;
    });

    /* NO show pull refresh */
    self.tableView.showsPullToRefresh = NO;
    self.tableView.showsInfiniteScrolling = NO;
    [self.tableView  setSeparatorStyle:UITableViewCellSeparatorStyleNone];

    CGRect tFrame        = self.view.bounds;
    tFrame.size.height   = tFrame.size.height - 44.0 - 49.0;
    self.tableView.frame = tFrame;

    /* creat ScollView */
    [self creatHeadViewForScollView];

    //request data
    [self fetchData];
}

- (void)createTitleView
{
    self.navigationItem.rightBarButtonItem = nil;
    self.navigationItem.leftBarButtonItem = nil;

    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(.0f, .0f, 80.0f, 40.0f)];
    [titleView setBackgroundColor:CLEAR];

    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(.0f, 10.0f, 40.0f, 25.0f)];
    titleLabel.backgroundColor = CLEAR;
    titleLabel.font = TextBigFont;
    titleLabel.textColor = BLACK;
    titleLabel.text = @"榜单";
    [titleView addSubview:titleLabel];

    UILabel *monthLabel = [[UILabel alloc] initWithFrame:CGRectMake(45.0f, 12.0f, 30.0f, 25.0f)];
    monthLabel.tag = kTitleMonthTag;
    monthLabel.backgroundColor = CLEAR;
    monthLabel.font = TextFont;
    monthLabel.textColor = BLACK;
    NSDate *now = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];//设置成中国阳历
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekdayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    comps = [calendar components:unitFlags fromDate:now];
    long month=[comps month];//获取月对应的长整形字符串
    monthLabel.text = [NSString stringWithFormat:@"0%ld月",month];
    [titleView addSubview:monthLabel];

    UIImage *VuserCenterNav_arrow = [UIImage imageNamed:@"VuserCenterNav_arrow"];
    UIImageView *titleSignImageView = [[UIImageView alloc] initWithFrame:CGRectMake(75.0f, 25.0f, VuserCenterNav_arrow.size.width, VuserCenterNav_arrow.size.height)];
    titleSignImageView.image = VuserCenterNav_arrow;
    titleSignImageView.backgroundColor = CLEAR;
    [titleView addSubview:titleSignImageView];

    titleView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(TapcenterBarView:)];
    tapGesture.numberOfTapsRequired = 1;
    [titleView addGestureRecognizer:tapGesture];

    self.navigationItem.titleView = titleView;
}

- (void)TapcenterBarView:(UITapGestureRecognizer *)gesture
{
    if ([KEYPullDownMenu isOpenInViewController:self])
    {
        [KEYPullDownMenu dismissInViewController:self];
    } else {
        // 无数据不作处理
        if (!pickerData || [pickerData count]==0) return;

        __weak typeof(self) weakSelf = self;
        NSMutableArray *pullDownItems = [NSMutableArray new];

        for (int i=0; i<[pickerData count]; i++)
        {
            KEYPullDownMenuItem *item = [KEYPullDownMenuItem menuItemNamed:pickerData[i]];
            if (_selectedItem)
            {
                if ([_selectedItem.name isEqualToString:item.name])
                {
                    item.active = YES;
                }
            } else {
                if (i==[pickerData count]-1)
                {
                    item.active = YES;
                    self.selectedItem = item;
                }
            }
            [pullDownItems addObject:item];
        }

        [KEYPullDownMenu openMenuInViewController:self items:pullDownItems
                                                                     dismissBlock:^(KEYPullDownMenuItem *selectedItem, NSInteger selectedRow)
                                         {
                                             [KEYPullDownMenu dismissInViewController:self];
                                             self.selectedItem = selectedItem;
                                             NSLog(@"selectedRow:%d",selectedRow);
                                             datePathUrl = pickerData1[selectedRow];
                                             [weakSelf fetchData];
                                         }
                                                                     reorderBlock:nil deleteBlock:nil];
    }
}

- (void)creatHeadViewForScollView
{
    // tableview headview
    scrollBottomView                 = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width,440.0)];
    scrollBottomView.backgroundColor = WHITE;
    self.tableView.tableHeaderView   = scrollBottomView;

    //-----------------颜色背景View
    baseColorView = [[UIView alloc] initWithFrame:CGRectMake(7.0, 4.0, ScreenWidth - 7.0 *2, scrollBottomView.frame.size.height-4)];
    baseColorView.backgroundColor = RANKCOLOR_1;
    [scrollBottomView addSubview:baseColorView];


    //-----------------标题View
    UIView *baseTitleView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, baseColorView.frame.size.width, 42.0)];
    baseTitleView.backgroundColor = CLEAR;
    [baseColorView addSubview:baseTitleView];

    lastSpecialTitle = [[UILabel alloc] initWithFrame:CGRectMake(9.0, 20 , 75, 15)];
    lastSpecialTitle.numberOfLines = 1;
    lastSpecialTitle.backgroundColor = CLEAR;
    lastSpecialTitle.textColor = TIME;
    lastSpecialTitle.font = Title_Font;
    lastSpecialTitle.textAlignment = NSTextAlignmentLeft;

    [baseTitleView addSubview:lastSpecialTitle];

    crrentSpecialTitle = [[UILabel alloc] initWithFrame:CGRectMake(93, 15 , 124, 20)];
    crrentSpecialTitle.numberOfLines = 1;
    crrentSpecialTitle.backgroundColor = CLEAR;
    crrentSpecialTitle.textColor = BLACK;
    crrentSpecialTitle.font = Title_Big2_Font;
    crrentSpecialTitle.textAlignment = NSTextAlignmentCenter;

    [baseTitleView addSubview:crrentSpecialTitle];

    nextSpecialTitle = [[UILabel alloc] initWithFrame:CGRectMake(220, 20 , 75, 15)];
    nextSpecialTitle.numberOfLines = 1;
    nextSpecialTitle.backgroundColor = CLEAR;
    nextSpecialTitle.textColor = TIME;
    nextSpecialTitle.font = Title_Font;
    nextSpecialTitle.textAlignment = NSTextAlignmentRight;

    [baseTitleView addSubview:nextSpecialTitle];


    //---------------------scrollView base view

    UIImage *rankList_rankBigBox = [UIImage imageNamed:@"rankList_rankBigBox"];

    UIImageView *image_rankBigBox = [[UIImageView alloc] initWithFrame:CGRectMake(baseColorView.frame.size.width/2 - rankList_rankBigBox.size.width/2, baseTitleView.frame.size.height, rankList_rankBigBox.size.width , rankList_rankBigBox.size.height)];

    image_rankBigBox.userInteractionEnabled = YES;
    image_rankBigBox.image = rankList_rankBigBox;
    [baseColorView addSubview:image_rankBigBox];


    // 滚动面板
    self.scrollView                                = [[UIScrollView alloc] initWithFrame:CGRectMake(rankList_rankBigBox.size.width/2 - KScrollViewWidth/2, 24.0, KScrollViewWidth, KScrollViewHeight)];

    self.scrollView.backgroundColor                = CLEAR;
    self.scrollView.pagingEnabled                  = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.delegate                       = self;
    [image_rankBigBox addSubview:self.scrollView];

    scrollBottomView.hidden = YES;

    // pageContrlo
    self.pagerView        =  [[MCPagerView alloc] init];

    [image_rankBigBox addSubview:self.pagerView];


}

/* creat category As TableViweHeader */
-(void)creatCoverViewAsTableViweHeader{

    // Pager
    _pages = [_rankOblect._itemTopCateList count];

    [_pagerView setImage:[UIImage imageNamed:@"main_list_point"]
        highlightedImage:[UIImage imageNamed:@"main_list_point_"]
                  forKey:@"a"];

    NSMutableString * string = [[NSMutableString alloc ] init];

    for (int i = 0; i < [_rankOblect._itemTopCateList count]; i ++ ) {
        [string appendFormat:@"a"];
    }

    [_pagerView setPattern:string];

    _pagerView.delegate = self;

    [self.pagerView setCenter:CGPointMake((self.scrollView.frame.size.width / 2.0 - _pages * 18 /2), self.scrollView.frame.size.height + 40)];


    if ([_rankOblect._itemTopCateList count] >0) {
        scrollBottomView.hidden        = NO;
        self.pageControl.numberOfPages = _rankOblect._itemTopCateList.count;
        [self.scrollView setContentSize:CGSizeMake((KScrollViewWidth) * [_rankOblect._itemTopCateList count] , KScrollViewHeight)];

        for (int i = 0; i < [_rankOblect._itemTopCateList count]; i++) {
            iEver_itemTopCateList_object *iAd = [_rankOblect._itemTopCateList objectAtIndex:i];
            [self addImageWithName:iAd atPosition:i];
            
        }
    }

}

- (void)addImageWithName:(iEver_itemTopCateList_object *)iAd atPosition:(int)position

{
	UIView *bottomView            = [[UIView alloc] init];

    //bottomView.backgroundColor    = [UIColor purpleColor];
	bottomView.frame              = CGRectMake(KScrollViewWidth* position, 0, KScrollViewWidth, KScrollViewHeight);


    /*   -------------------      金牌       ---------------------------*/
    iEver_itemTopList_object *object_gold = [iAd._itemTopListArray objectAtIndex:0];
    // 白色背景
    UIView *baseView_gold         = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, KScrollViewWidth, 108.0)];
    baseView_gold.backgroundColor = WHITE;
    [bottomView addSubview:baseView_gold];

    // 金牌标志
    UIImage *rankList_AngleSign_1 = [UIImage imageNamed:[NSString stringWithFormat:@"rankList_-AngleSign_%d",position + 1]];
    UIImageView *image_gold      = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, rankList_AngleSign_1.size.width, rankList_AngleSign_1.size.height)];
    image_gold.image             = rankList_AngleSign_1;
    [baseView_gold addSubview:image_gold];

    //金牌标注-----排名
    UILabel *rank_gold = [[UILabel alloc] initWithFrame:CGRectMake(0, 2, rankList_AngleSign_1.size.width, 15)];
    rank_gold.backgroundColor = CLEAR;
    rank_gold.textAlignment = NSTextAlignmentCenter;
    rank_gold.textColor = WHITE;
    rank_gold.font = TextFont;
    rank_gold.text = @"1";
    [image_gold addSubview:rank_gold];


    //金牌商品升降标志
    UIImage *rank_rise = [UIImage imageNamed:@"rank_rise"];
    UIImage *rank_parallel = [UIImage imageNamed:@"rank_parallel"];
    UIImage *rank_drop = [UIImage imageNamed:@"rank_drop"];
    UIImageView *image_level_gold          = [[UIImageView alloc] initWithFrame:CGRectMake(2.5, rankList_AngleSign_1.size.height + 5.0, 10.0, 10.0)];
    if (object_gold.beforeLevel == 0) {
        image_level_gold.hidden = YES;
    }else {

        if (object_gold.sortLevel > object_gold.beforeLevel) {
            image_level_gold.image             = rank_rise;
        }else if(object_gold.sortLevel == object_gold.beforeLevel){
            image_level_gold.image             = rank_parallel;
        }else if(object_gold.sortLevel < object_gold.beforeLevel){
            image_level_gold.image             = rank_drop;
        }

        image_level_gold.hidden = NO;
    }
    [baseView_gold addSubview:image_level_gold];


    //金牌商品-----图片
    UIImageView *itemIamge_gold    = [[UIImageView alloc] initWithFrame:CGRectMake(rankList_AngleSign_1.size.width, 0.0, 108.0, 108.0)];
    itemIamge_gold.contentMode     = UIViewContentModeScaleAspectFit;


    [itemIamge_gold setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/640x",object_gold.itemImg]] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
    [baseView_gold addSubview:itemIamge_gold];


    //金牌商品name
    UILabel *name_gold           = [[UILabel alloc] init];
    name_gold.backgroundColor    = CLEAR;
    name_gold.textAlignment      = NSTextAlignmentLeft;
    name_gold.textColor          = BLACK;
    name_gold.font               = Title_Font;
    name_gold.text               = object_gold.itemName;
    name_gold.lineBreakMode      = NSLineBreakByTruncatingTail;
    name_gold.numberOfLines      = 0;
    NSDictionary * name_gold_dic = [NSDictionary dictionaryWithObjectsAndKeys:name_gold.font, NSFontAttributeName,nil];
    CGSize name_gold_Size        = [object_gold.itemName boundingRectWithSize:CGSizeMake(110.0, 40.0)
                                                              options:NSStringDrawingUsesLineFragmentOrigin
                                                           attributes:name_gold_dic context:nil].size;
    name_gold.frame              = CGRectMake(itemIamge_gold.frame.origin.x + itemIamge_gold.frame.size.width + 5.0, 15.0, 110.0, name_gold_Size.height);
    [baseView_gold addSubview:name_gold];


    //金牌评分展示
    CWStarRateView *starView_gold = [[CWStarRateView alloc] initWithFrame:CGRectMake(name_gold.frame.origin.x, 54, 65, 12.0) numberOfStars:5];
    starView_gold.scorePercent    = (object_gold.startGrade *2)/10.f;
    [baseView_gold addSubview:starView_gold];


    //金牌分数
    UILabel *startGrade_gold           = [[UILabel alloc] initWithFrame:CGRectMake(name_gold.frame.origin.x + starView_gold.frame.size.width + 4, 54, 48.0, 15.0)];
    startGrade_gold.numberOfLines      = 1;
    startGrade_gold.backgroundColor    = CLEAR;
    startGrade_gold.textAlignment      = NSTextAlignmentLeft;
    startGrade_gold.textColor          = RANK_UPDATE_GOLD;
    startGrade_gold.font               = TextFonts;
    startGrade_gold.text               = [NSString stringWithFormat:@"%.1f",object_gold.startGrade];
    [baseView_gold addSubview:startGrade_gold];


    //金牌多少人关注
    UILabel *pv_gold           = [[UILabel alloc] initWithFrame:CGRectMake(name_gold.frame.origin.x, 67, 100.0, 15.0)];
    pv_gold.numberOfLines      = 1;
    pv_gold.backgroundColor    = CLEAR;
    pv_gold.textAlignment      = NSTextAlignmentLeft;
    pv_gold.textColor          = GRAY_LIGHT;
    pv_gold.font               = TextFonts;
    pv_gold.text               = [NSString stringWithFormat:@"%d人关注",object_gold.pvTotal];
    [baseView_gold addSubview:pv_gold];


    //金牌商品容量&价格
    UILabel *price_gold           = [[UILabel alloc] initWithFrame:CGRectMake(name_gold.frame.origin.x, 83.0, 87.0, 20.0)];
    price_gold.numberOfLines      = 1;
    price_gold.backgroundColor    = CLEAR;
    price_gold.textAlignment      = NSTextAlignmentLeft;
    price_gold.textColor          = DAKEBLACK;
    price_gold.font               = TextDESCFonts;
    price_gold.text               = [NSString stringWithFormat:@"RMB%d/%@",object_gold.price,object_gold.itemSpec];
    [baseView_gold addSubview:price_gold];

    //金牌最外层-----交互按钮
    UIButton *button_gold_lineView                 = [UIButton buttonWithType:UIButtonTypeCustom];
    button_gold_lineView.backgroundColor           = CLEAR;
    button_gold_lineView.frame                     = baseView_gold.bounds;

    [[button_gold_lineView rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *button) {

        iEver_ItemDetailViewController *_itemlDetailViewController = [[iEver_ItemDetailViewController alloc]init];
        _itemlDetailViewController.type_id                         = object_gold.itemId;
        _itemlDetailViewController.hidesBottomBarWhenPushed        = YES;
        [self.navigationController pushViewController:_itemlDetailViewController animated:YES];
        IOS7POP;

    }];
    [baseView_gold addSubview:button_gold_lineView];




    /*   -------------------      银牌       ---------------------------*/
    // 背景视图 ----银奖

    iEver_itemTopList_object *object_silver = [iAd._itemTopListArray objectAtIndex:1];

    UIView *baseView_silver         = [[UIView alloc] initWithFrame:CGRectMake(0.0, baseView_gold.frame.size.height + 7.0, 132.0, 193.0)];
    baseView_silver.backgroundColor = WHITE;
    [bottomView addSubview:baseView_silver];


    //  银牌标志
    UIImageView *image_silver      = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, rankList_AngleSign_1.size.width, rankList_AngleSign_1.size.height)];
    image_silver.image             = rankList_AngleSign_1;
    [baseView_silver addSubview:image_silver];


    //银牌标注-----排名
    UILabel *rank_silver = [[UILabel alloc] initWithFrame:CGRectMake(0, 2, rankList_AngleSign_1.size.width, 15)];
    rank_silver.backgroundColor = CLEAR;
    rank_silver.textAlignment = NSTextAlignmentCenter;
    rank_silver.textColor = WHITE;
    rank_silver.font = TextFont;
    rank_silver.text = @"2";
    [image_silver addSubview:rank_silver];


    //银牌商品升降标志
    UIImageView *image_level_silver          = [[UIImageView alloc] initWithFrame:CGRectMake(2.5, rankList_AngleSign_1.size.height + 5.0, 10.0, 10.0)];
    if (object_silver.beforeLevel == 0) {
        image_level_silver.hidden = YES;
    }else {

        if (object_silver.sortLevel > object_silver.beforeLevel) {
            image_level_silver.image             = rank_rise;
        }else if(object_silver.sortLevel == object_silver.beforeLevel){
            image_level_silver.image             = rank_parallel;
        }else if(object_silver.sortLevel < object_silver.beforeLevel){
            image_level_silver.image             = rank_drop;
        }

        image_level_silver.hidden = NO;
    }
    [baseView_silver addSubview:image_level_silver];


    //银牌商品-----图片
    UIImageView *itemIamge_silver    = [[UIImageView alloc] initWithFrame:CGRectMake(rankList_AngleSign_1.size.width, 0.0, baseView_silver.frame.size.width - rankList_AngleSign_1.size.width * 2, baseView_silver.frame.size.width - rankList_AngleSign_1.size.width * 2)];
    itemIamge_silver.contentMode     = UIViewContentModeScaleAspectFit;


    [itemIamge_silver setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/640x",object_silver.itemImg]] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
    [baseView_silver addSubview:itemIamge_silver];


    //银牌商品name
    UILabel *name_silver           = [[UILabel alloc] init];
    name_silver.numberOfLines      = 0;
    name_silver.backgroundColor    = CLEAR;
    name_silver.textAlignment      = NSTextAlignmentLeft;
    name_silver.textColor          = BLACK;
    name_silver.font               = Title_Font;
    name_silver.text               = object_silver.itemName;
    NSDictionary * name_silver_dic = [NSDictionary dictionaryWithObjectsAndKeys:name_silver.font, NSFontAttributeName,nil];
    CGSize name_silver_Size        = [object_silver.itemName boundingRectWithSize:CGSizeMake(112.0, 40.0)
                                                                      options:NSStringDrawingUsesLineFragmentOrigin
                                                                   attributes:name_silver_dic context:nil].size;
    name_silver.frame              = CGRectMake(10.0, itemIamge_silver.frame.size.height + 10.0, 112.0, name_silver_Size.height);
    [baseView_silver addSubview:name_silver];

    //银牌评分展示
    CWStarRateView *starView_silver = [[CWStarRateView alloc] initWithFrame:CGRectMake(name_silver.frame.origin.x, name_silver.frame.origin.y + name_silver.frame.size.height + 10, 65, 12.0) numberOfStars:5];
    starView_silver.scorePercent    = (object_silver.startGrade *2)/10.f;
    [baseView_silver addSubview:starView_silver];


    //银牌分数
    UILabel *startGrade_silver           = [[UILabel alloc] initWithFrame:CGRectMake(name_silver.frame.origin.x + starView_silver.frame.size.width + 4, name_silver.frame.origin.y + name_silver.frame.size.height + 10, 48.0, 15.0)];
    startGrade_silver.numberOfLines      = 1;
    startGrade_silver.backgroundColor    = CLEAR;
    startGrade_silver.textAlignment      = NSTextAlignmentLeft;
    startGrade_silver.textColor          = RANK_UPDATE_GOLD;
    startGrade_silver.font               = TextFonts;
    startGrade_silver.text               = [NSString stringWithFormat:@"%.1f",object_silver.startGrade];
    [baseView_silver addSubview:startGrade_silver];


    //银牌多少人关注
    UILabel *pv_silver           = [[UILabel alloc] initWithFrame:CGRectMake(name_silver.frame.origin.x, startGrade_silver.frame.origin.y + startGrade_silver.frame.size.height + 3, 100.0, 15.0)];
    pv_silver.numberOfLines      = 1;
    pv_silver.backgroundColor    = CLEAR;
    pv_silver.textAlignment      = NSTextAlignmentLeft;
    pv_silver.textColor          = GRAY_LIGHT;
    pv_silver.font               = TextFonts;
    pv_silver.text               = [NSString stringWithFormat:@"%d人关注",object_silver.pvTotal];
    [baseView_silver addSubview:pv_silver];


    //银牌商品容量&价格
    UILabel *price_silver           = [[UILabel alloc] initWithFrame:CGRectMake(name_silver.frame.origin.x, baseView_silver.frame.size.height - 30.0, 165.0, 20.0)];
    price_silver.numberOfLines      = 1;
    price_silver.backgroundColor    = CLEAR;
    price_silver.textAlignment      = NSTextAlignmentLeft;
    price_silver.textColor          = DAKEBLACK;
    price_silver.font               = TextDESCFonts;
    price_silver.text               = [NSString stringWithFormat:@"RMB%d/%@",object_silver.price,object_silver.itemSpec];
    [baseView_silver addSubview:price_silver];

    //银牌边框按钮
    UIButton *button_silver_lineView                 = [UIButton buttonWithType:UIButtonTypeCustom];
    button_silver_lineView.backgroundColor           = CLEAR;
    button_silver_lineView.frame                     = baseView_silver.bounds;

    [[button_silver_lineView rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *button) {

        iEver_ItemDetailViewController *_itemlDetailViewController = [[iEver_ItemDetailViewController alloc]init];
        _itemlDetailViewController.type_id                         = object_silver.itemId;
        _itemlDetailViewController.hidesBottomBarWhenPushed        = YES;
        [self.navigationController pushViewController:_itemlDetailViewController animated:YES];
        IOS7POP;

    }];
    [baseView_silver addSubview:button_silver_lineView];



    /*   -------------------      铜牌       ---------------------------*/
    // 铜牌边框

    iEver_itemTopList_object *object_copper = [iAd._itemTopListArray objectAtIndex:2];

    UIView *baseView_copper         = [[UIView alloc] initWithFrame:CGRectMake(baseView_silver.frame.size.width + 7.0, baseView_gold.frame.size.height + 7.0, 132.0, 193.0)];
    baseView_copper.backgroundColor = WHITE;
    [bottomView addSubview:baseView_copper];


    //  铜牌标志
    UIImageView *image_copper     = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, rankList_AngleSign_1.size.width, rankList_AngleSign_1.size.height)];
    image_copper.image             = rankList_AngleSign_1;
    [baseView_copper addSubview:image_copper];


    //铜牌标注-----排名
    UILabel *rank_copper = [[UILabel alloc] initWithFrame:CGRectMake(0, 2, rankList_AngleSign_1.size.width, 15)];
    rank_copper.backgroundColor = CLEAR;
    rank_copper.textAlignment = NSTextAlignmentCenter;
    rank_copper.textColor = WHITE;
    rank_copper.font = TextFont;
    rank_copper.text = @"3";
    [image_copper addSubview:rank_copper];


    //铜牌商品升降标志
    UIImageView *image_level_copper          = [[UIImageView alloc] initWithFrame:CGRectMake(2.5, rankList_AngleSign_1.size.height + 5.0, 10.0, 10.0)];
    if (object_copper.beforeLevel == 0) {
        image_level_copper.hidden = YES;
    }else {

        if (object_copper.sortLevel > object_copper.beforeLevel) {
            image_level_copper.image             = rank_rise;
        }else if(object_copper.sortLevel == object_copper.beforeLevel){
            image_level_copper.image             = rank_parallel;
        }else if(object_copper.sortLevel < object_copper.beforeLevel){
            image_level_copper.image             = rank_drop;
        }

        image_level_copper.hidden = NO;
    }
    [baseView_copper addSubview:image_level_copper];


    //铜牌商品-----图片
    UIImageView *itemIamge_copper    = [[UIImageView alloc] initWithFrame:CGRectMake(rankList_AngleSign_1.size.width, 0.0, baseView_copper.frame.size.width - rankList_AngleSign_1.size.width * 2, baseView_copper.frame.size.width - rankList_AngleSign_1.size.width * 2)];
    itemIamge_copper.contentMode     = UIViewContentModeScaleAspectFit;

    [itemIamge_copper setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/640x",object_copper.itemImg]] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
    [baseView_copper addSubview:itemIamge_copper];


    //银牌商品name
    UILabel *name_copper           = [[UILabel alloc] init];
    name_copper.numberOfLines      = 0;
    name_copper.backgroundColor    = CLEAR;
    name_copper.textAlignment      = NSTextAlignmentLeft;
    name_copper.textColor          = BLACK;
    name_copper.font               = Title_Font;
    name_copper.text               = object_copper.itemName;
    NSDictionary * name_copper_dic = [NSDictionary dictionaryWithObjectsAndKeys:name_copper.font, NSFontAttributeName,nil];
    CGSize name_copper_Size        = [object_copper.itemName boundingRectWithSize:CGSizeMake(112.0, 40.0)
                                                                          options:NSStringDrawingUsesLineFragmentOrigin
                                                                       attributes:name_copper_dic context:nil].size;
    name_copper.frame              = CGRectMake(10.0, itemIamge_copper.frame.size.height + 10.0, 112.0, name_copper_Size.height);
    [baseView_copper addSubview:name_copper];

    //银牌评分展示
    CWStarRateView *starView_copper = [[CWStarRateView alloc] initWithFrame:CGRectMake(name_copper.frame.origin.x, name_copper.frame.origin.y + name_copper.frame.size.height + 10, 65, 12.0) numberOfStars:5];
    starView_copper.scorePercent    = (object_copper.startGrade *2)/10.f;
    [baseView_copper addSubview:starView_copper];



    //铜牌分数
    UILabel *startGrade_copper           = [[UILabel alloc] initWithFrame:CGRectMake(name_copper.frame.origin.x + starView_copper.frame.size.width + 4, name_copper.frame.origin.y + name_copper.frame.size.height + 10, 48.0, 15.0)];
    startGrade_copper.numberOfLines      = 1;
    startGrade_copper.backgroundColor    = CLEAR;
    startGrade_copper.textAlignment      = NSTextAlignmentLeft;
    startGrade_copper.textColor          = RANK_UPDATE_GOLD;
    startGrade_copper.font               = TextFonts;
    startGrade_copper.text               = [NSString stringWithFormat:@"%.1f",object_copper.startGrade];
    [baseView_copper addSubview:startGrade_copper];


    //铜牌多少人关注
    UILabel *pv_copper           = [[UILabel alloc] initWithFrame:CGRectMake(name_copper.frame.origin.x, startGrade_copper.frame.origin.y + startGrade_copper.frame.size.height + 3, 100.0, 15.0)];
    pv_copper.numberOfLines      = 1;
    pv_copper.backgroundColor    = CLEAR;
    pv_copper.textAlignment      = NSTextAlignmentLeft;
    pv_copper.textColor          = GRAY_LIGHT;
    pv_copper.font               = TextFonts;
    pv_copper.text               = [NSString stringWithFormat:@"%d人关注",object_copper.pvTotal];
    [baseView_copper addSubview:pv_copper];
    

    //铜牌商品容量&价格
    UILabel *price_copper           = [[UILabel alloc] initWithFrame:CGRectMake(name_copper.frame.origin.x, baseView_copper.frame.size.height - 30.0, 165.0, 20.0)];
    price_copper.numberOfLines      = 1;
    price_copper.backgroundColor    = CLEAR;
    price_copper.textAlignment      = NSTextAlignmentLeft;
    price_copper.textColor          = DAKEBLACK;
    price_copper.font               = TextDESCFonts;
    price_copper.text               = [NSString stringWithFormat:@"RMB%d/%@",object_copper.price,object_copper.itemSpec];
    [baseView_copper addSubview:price_copper];

    //铜牌边框按钮
    UIButton *button_copper_lineView                 = [UIButton buttonWithType:UIButtonTypeCustom];
    button_copper_lineView.backgroundColor           = CLEAR;
    button_copper_lineView.frame                     = baseView_copper.bounds;

    [[button_copper_lineView rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *button) {

        iEver_ItemDetailViewController *_itemlDetailViewController = [[iEver_ItemDetailViewController alloc]init];
        _itemlDetailViewController.type_id                         = object_copper.itemId;
        _itemlDetailViewController.hidesBottomBarWhenPushed        = YES;
        [self.navigationController pushViewController:_itemlDetailViewController animated:YES];
        IOS7POP;

    }];
    [baseView_copper addSubview:button_copper_lineView];


    [self.scrollView addSubview:bottomView];

   }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*request data*/
- (void)fetchData
{
    @weakify(self);

    [GiFHUD show];
    iEver_rankItemObject *detail_Object = [[iEver_rankItemObject alloc] init];
    NSString *_pathUrl = [NSString stringWithFormat:@"/itemTop/queryCategory/%@",datePathUrl];

    [[[detail_Object questDataQueryCategory:nil path:_pathUrl] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(iEver_rankItemObject *object) {

         @strongify(self);
         _rankOblect = object;
         // 无数据
         if ([_rankOblect._itemTopCateList count] == 0)
         {
             [GiFHUD dismiss];
             return ;
         }
         // 清空容器
         if (self.dataSourceArray.count >0)
         {
             [self.dataSourceArray removeAllObjects];
         }

         iEver_itemTopCateList_object *cateList_object = [_rankOblect._itemTopCateList objectAtIndex:controlPage];
         [self addDataToDataSource:cateList_object._itemTopListArray];
         self.tableView.showsInfiniteScrolling = NO;

         if ([object._itemTopCateList count] == 1) {
             iEver_itemTopCateList_object *_specialBanner_object1 = [object._itemTopCateList objectAtIndex:0];
             crrentSpecialTitle.text = _specialBanner_object1.categoryName;

         }

         if ([object._itemTopCateList count] > 1) {
             iEver_itemTopCateList_object *_specialBanner_object1 = [object._itemTopCateList objectAtIndex:0];
             crrentSpecialTitle.text = _specialBanner_object1.categoryName;
             iEver_itemTopCateList_object *_specialBanner_object2 = [object._itemTopCateList objectAtIndex:1];
             nextSpecialTitle.text = _specialBanner_object2.categoryName;
             
         }

         NSDateFormatter *formatter                    = [[NSDateFormatter alloc] init];
         [formatter                                      setDateFormat:@"YYYY.MM.dd"];
         NSString *dateLoca                            = [NSString stringWithFormat:@"%lld",cateList_object.createTime/1000];
         NSTimeInterval time                           = [dateLoca doubleValue];
         NSDate *detaildate                            = [NSDate dateWithTimeIntervalSince1970:time];
         NSString *timestr                             = [formatter stringFromDate:detaildate];
         self.categoryName_time_lable.text             = timestr;

         if (self.dataSourceArray.count >0) {

             [self.tableView reloadData];

             [self loadBootstrap];
         }

         if ([object._topTimeList count] >0) {

             pickerData = object._topTimeList;
             pickerData1 = object._topTimeList1;

             UIView *titleView = (UIView *)self.navigationItem.titleView;
             UILabel *monthLabel = (UILabel *)[titleView viewWithTag:kTitleMonthTag];
             NSString *lastDate = [pickerData lastObject];
             monthLabel.text = [NSString stringWithFormat:@"%@月",[[lastDate componentsSeparatedByString:@"-"] lastObject]];
         }
         [GiFHUD  dismiss];
         /* creat category As TableViweHeader */
         [self creatCoverViewAsTableViweHeader];
     }];
}


-(void)loadBootstrap {
    NSString *rank_previousVersion = [[NSUserDefaults standardUserDefaults] objectForKey:rank_PREVIOUSVERSION];
    if (!rank_previousVersion) {
        rank_previousVersion = @"0";
    }
    if ([rank_CURRENTVERSION compare:rank_previousVersion options:NSNumericSearch]) {
    //newVersionFound
    [[NSUserDefaults standardUserDefaults] setObject:rank_CURRENTVERSION forKey:rank_PREVIOUSVERSION];
    [[NSUserDefaults standardUserDefaults] synchronize];

    UIImage *main_Bootstrap = [UIImage imageNamed:@"rank_Bootstrap"];
    UIImageView *image_Bootstrap = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,ScreenWidth, ScreenHeight)];
    image_Bootstrap.userInteractionEnabled = YES;
    image_Bootstrap.image = main_Bootstrap;

    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(140, 310, 120, 40);
    button.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        image_Bootstrap.hidden = YES;
        return [RACSignal empty];
    }];
    [image_Bootstrap addSubview:button];

    [[iEverAppDelegate shareAppDelegate].window addSubview:image_Bootstrap];

    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataSourceArray count] -3;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *CellIdentifier  = @"ItemRankPicListCell";
    UITableViewCell *cell = nil;
	tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell = (iEver_itemRank_cell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            // Configure the cell...
	if (!cell) {
            cell = [[iEver_itemRank_cell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
            iEver_itemRank_cell *pic_cell                 = (iEver_itemRank_cell *)cell;
            iEver_itemTopList_object *object              = [self.dataSourceArray objectAtIndex:indexPath.row +3];
            object.rowNum                                 = indexPath.row +4;
            [pic_cell setObject:object];

	return cell;
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [iEver_itemRank_cell heightForObject:nil atIndexPath:indexPath tableView:tableView];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];

    iEver_itemTopList_object *object                           = [self.dataSourceArray objectAtIndex:indexPath.row +3];
    iEver_ItemDetailViewController *_itemlDetailViewController = [[iEver_ItemDetailViewController alloc]init];
    _itemlDetailViewController.type_id                         = object.itemId;
    _itemlDetailViewController.hidesBottomBarWhenPushed        = YES;
    [self.navigationController pushViewController:_itemlDetailViewController animated:YES];
    IOS7POP;
}
#pragma mark -
#pragma mark UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollview
{

    int page = floor(scrollview.contentOffset.x / KScrollViewWidth);
    _pagerView.page = page;
}
- (void)pageView:(MCPagerView *)pageView didUpdateToPage:(NSInteger)newPage
{

}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {

    controlPage                                   = floor(self.scrollView.contentOffset.x / self.scrollView.frame.size.width);
    if (controlPage < 0) {
        controlPage = 0;
    }
    if (controlPage > [_rankOblect._itemTopCateList count] - 1) {
        controlPage = [_rankOblect._itemTopCateList count] - 1;
    }
    iEver_itemTopCateList_object *cateList_object = [_rankOblect._itemTopCateList objectAtIndex:controlPage];
    NSDateFormatter *formatter                    = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY.MM.dd"];
    NSString *dateLoca                            = [NSString stringWithFormat:@"%lld",cateList_object.updateTime/1000];
    NSTimeInterval time                           = [dateLoca doubleValue];
    NSDate *detaildate                            = [NSDate dateWithTimeIntervalSince1970:time];
    NSString *timestr                             = [formatter stringFromDate:detaildate];
    self.categoryName_time_lable.text             = timestr;

    if (self.dataSourceArray.count >0) {
        [self.dataSourceArray removeAllObjects];
    }
    [self addDataToDataSource:cateList_object._itemTopListArray];
    self.tableView.showsInfiniteScrolling = NO;
    if (self.dataSourceArray.count >0) {
        [self.tableView reloadData];
    }
    self.pageControl.currentPage                  = controlPage;


    if (controlPage - 1 < 0) {
        lastSpecialTitle.text = @"";
    }else{

        iEver_itemTopCateList_object *_specialBanner_object1 = [_rankOblect._itemTopCateList objectAtIndex:controlPage - 1];
        lastSpecialTitle.text = _specialBanner_object1.categoryName;
    }


    iEver_itemTopCateList_object *_specialBanner_object2 = [_rankOblect._itemTopCateList objectAtIndex:controlPage];
    crrentSpecialTitle.text = _specialBanner_object2.categoryName;

    if (controlPage + 1 > _rankOblect._itemTopCateList.count - 1) {
        nextSpecialTitle.text = @"";
    }else{

        iEver_itemTopCateList_object *_specialBanner_object3 = [_rankOblect._itemTopCateList objectAtIndex:controlPage + 1];
        nextSpecialTitle.text = _specialBanner_object3.categoryName;
    }


    NSArray *colorTypes =@[RANKCOLOR_1,RANKCOLOR_2,RANKCOLOR_3,RANKCOLOR_4,RANKCOLOR_5,RANKCOLOR_6,RANKCOLOR_7,RANKCOLOR_8,RANKCOLOR_9,RANKCOLOR_10,RANKCOLOR_1,RANKCOLOR_2,RANKCOLOR_3,RANKCOLOR_4,RANKCOLOR_5,RANKCOLOR_6,RANKCOLOR_7,RANKCOLOR_8,RANKCOLOR_9,RANKCOLOR_10];

    [UIView beginAnimations:nil context:nil];
    [UIView    setAnimationCurve: UIViewAnimationCurveLinear];
    [UIView    setAnimationDelegate:self];
    [UIView    setAnimationDuration:.6];
    baseColorView.backgroundColor = colorTypes[controlPage];
    [UIView commitAnimations];

}

@end
