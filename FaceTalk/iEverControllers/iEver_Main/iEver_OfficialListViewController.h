//
//  iEver_OfficialListViewController.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-18.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEverSSBaseTableViewController.h"
#import "AAShareBubbles.h"
@interface iEver_OfficialListViewController : iEverSSBaseTableViewController<AAShareBubblesDelegate>
@property (nonatomic, assign) int                        category_id;       /* id */
@property (nonatomic, assign) int                        categoryType;      /* 分类类型*/
@property (nonatomic, assign) long long                  timeDate;          /* 查询时间*/
@property (nonatomic, assign) int                        tagId;             /* 标签查询*/
@property (nonatomic, assign) int                        userLike;          /* 用户喜欢查询*/
@property (nonatomic, assign) int                        didSelectIndexPathComment;/* 选中cell的indexpath  */
@property (nonatomic, retain) iEver_mainContentList_object *detail_Object; /* 详情返回的列表数据 */
-(void)delegateMethod;
@end
