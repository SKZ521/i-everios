//
//  iEver_Vlist_detailCommentViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/4/12.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_Vlist_detailCommentViewController.h"
#import "iEver_detailCommentObject.h"
#import "iEver_comment_cell.h"

@interface iEver_Vlist_detailCommentViewController ()

@end

@implementation iEver_Vlist_detailCommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"更多评论";
    /* NO show pull refresh */
    self.tableView.showsPullToRefresh = NO;
    self.tableView.showsInfiniteScrolling = YES;
    [self.tableView  setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    /* request data */
    [self fetchData];
}

/*request data*/
-(void)fetchData{

    @weakify(self);
    iEver_detailCommentObject *comment_Object = [[iEver_detailCommentObject alloc] init];
    NSString *_pathUrl = [NSString stringWithFormat:@"/expertArticleComment/queryByCoverId/%d/%d",self.type_id,self.page];
    [[[comment_Object articleComment:nil path:_pathUrl] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(iEver_detailCommentObject *object) {

         @strongify(self);
         if (object.pageSize < self.page) {
             self.tableView.showsInfiniteScrolling = NO;
         }else{

             [self addDataToDataSource:object._commentList];
             [self.tableView reloadData];
         }
     }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataSourceArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Vdetail_commentCell";
    UITableViewCell *cell = nil;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell = (iEver_comment_cell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    // Configure the cell...
    if (!cell) {

        cell = [[iEver_comment_cell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }

    iEver_comment_cell *message_cell = (iEver_comment_cell *)cell;
    iEver_articleCommentList_object * object = [self.dataSourceArray objectAtIndex:indexPath.row];
    [message_cell setObject:object];

    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id object = [self.dataSourceArray objectAtIndex:indexPath.row];
    return [iEver_comment_cell heightForObject:object atIndexPath:indexPath tableView:tableView];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
