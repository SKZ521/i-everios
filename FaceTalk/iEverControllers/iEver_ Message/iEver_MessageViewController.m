//
//  iEver_MessageViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-13.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_MessageViewController.h"
#import "iEver_MessageObject.h"
#import "iEver_message_cell.h"
#import "iEver_normalUserAnsweredQesViewController.h"
#import "iEver_ItemTryDetailViewController.h"
#import "iEver_LoginViewController.h"
@interface iEver_MessageViewController ()
{

    iEver_MessageObject *content_object;
}
@end

@implementation iEver_MessageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"我的消息";
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    [self.tableView reloadData];
}
- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    /* NO show pull refresh */
    self.tableView.showsPullToRefresh = YES;
    self.tableView.showsInfiniteScrolling = YES;
    [self.tableView  setSeparatorStyle:UITableViewCellSeparatorStyleNone];

    /* set default backgroundView */
    self.tableView.backgroundView = ({
        UIImageView *_backImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
        _backImageView.image = [UIImage imageNamed:@"backImage"];
        _backImageView;
    });

    /* configure cell */
    [self configureCell];

    /* request data */
    [self fetchData];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)delegateMethod{
    [self fetchData];
}
/* configure cell */
-(void)configureCell{

    __typeof (self) __weak weakSelf = self;

    self.dataSource.cellConfigureBlock = ^(iEver_message_cell *cell,
                                           NSNumber *exampleType,
                                           UITableView *tableView,
                                           NSIndexPath *indexPath) {

        [cell configureCell];
        iEver_messageList_object *listObject = (iEver_messageList_object *)[weakSelf.dataSource itemAtIndexPath:indexPath];
        cell.object =listObject;
    };
    self.dataSource.cellClass = [iEver_message_cell class];
}

/*request data*/
-(void)fetchData{

    @weakify(self);
    [GiFHUD showWithOverlay];
    iEver_MessageObject *detail_Object = [[iEver_MessageObject alloc] init];
    NSString *_pathUrl = [NSString stringWithFormat:@"msg/queryMsg/%d",self.page];
    [[[detail_Object messageList:nil path:_pathUrl] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(iEver_MessageObject *object) {

         @strongify(self);
         if (object.resultCode == 1) {
             [self reloadDatasouce:object._messageList];
             [self.tableView reloadData];
             [GiFHUD dismiss];
         }else{

             iEver_LoginViewController *_loginViewController = [[iEver_LoginViewController alloc]init];
             _loginViewController.delegate = self;
             _loginViewController.hidesBottomBarWhenPushed = YES;
             [self.navigationController pushViewController:_loginViewController animated:YES];
         }

     }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id object = [self.dataSource itemAtIndexPath:indexPath];
    return [iEver_message_cell heightForObject:object atIndexPath:indexPath tableView:tableView];
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /* 消息类型type10回答问题、20试用申请 */
    iEver_messageList_object *object = [self.dataSource itemAtIndexPath:indexPath];

    iEver_MessageObject *message_Object = [[iEver_MessageObject alloc] init];

    [[[message_Object readedMessage:nil path:[NSString stringWithFormat:@"msg/updateReadStatus/%d",object.M_id]] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(NSDictionary *object) {
         
     }];
    
    if (object.type == 10) {

        iEver_normalUserAnsweredQesViewController *_normalUserAnsweredQesViewController = [[iEver_normalUserAnsweredQesViewController alloc]init];
        [self.navigationController pushViewController:_normalUserAnsweredQesViewController animated:YES];
        IOS7POP;

    }else if (object.type == 20){

        iEver_ItemTryDetailViewController *_itemTryDetailViewController = [[iEver_ItemTryDetailViewController alloc]init];
        _itemTryDetailViewController.type_id = object.businessId;
        [self.navigationController pushViewController:_itemTryDetailViewController animated:YES];
        IOS7POP;
    }
    object.readStatus = 1;
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return UIInterfaceOrientationPortrait;
}

- (BOOL)shouldAutorotate{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{

    return UIInterfaceOrientationPortrait;
}

@end
