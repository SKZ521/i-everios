//
//  iEver_TagQuery_ItemViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/2/5.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_TagQuery_ItemViewController.h"
#import "iEver_ItemDetailViewController.h"
#import "iEver_tagItemObject.h"
#import "iEver_tagItem_Cell.h"

@interface iEver_TagQuery_ItemViewController ()

@end

@implementation iEver_TagQuery_ItemViewController


- (void)viewWillAppear:(BOOL)animated
{

    [super viewWillAppear:animated];

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}
-(void)addBar{

    __weak iEver_TagQuery_ItemViewController *weakSelf = self;

    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    leftButton.frame = CGRectMake(0, 0, 44, 44);
    [leftButton setImage:[UIImage imageNamed:@"return"] forState:UIControlStateNormal];
    leftButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        [weakSelf.navigationController popViewControllerAnimated:YES];
        return [RACSignal empty];
    }];
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    self.navigationItem.leftBarButtonItem = leftBarButton;
}


- (void)viewDidLoad {
    [super viewDidLoad];

    /* add left  right bar */
    [self addBar];

    /* configure cell */
    [self configureCell];

    /* set default backgroundView */
    self.tableView.backgroundView = ({
        UIImageView *_backImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
        _backImageView.image = [UIImage imageNamed:@"backImage"];
        _backImageView;
    });
    /* request data */
    [self fetchData];

    /* NO show pull refresh */
    self.tableView.showsPullToRefresh = YES;
    [self.tableView  setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

/* configure cell */
-(void)configureCell{

    __typeof (self) __weak weakSelf = self;

    self.dataSource.cellConfigureBlock = ^(iEver_tagItem_Cell *cell,
                                           NSNumber *exampleType,
                                           UITableView *tableView,
                                           NSIndexPath *indexPath) {

        [cell configureCell];
        iEver_tagItemList_object *listObject = (iEver_tagItemList_object *)[weakSelf.dataSource itemAtIndexPath:indexPath];
        cell.object =listObject;
    };
    self.dataSource.cellClass = [iEver_tagItem_Cell class];
}

/* get request data*/
-(void)fetchData{

    @weakify(self);
    [GiFHUD show];

    iEver_tagItemObject *content = [[iEver_tagItemObject alloc] init];

    NSString *_pathUrl = [NSString stringWithFormat:@"item/queryByTagId/%d/%d",self.tagId,self.page];
    [[[content queryItemByTagId:nil
                             path:_pathUrl]
      deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(iEver_tagItemObject *object) {

         @strongify(self);
         if (object.resultCode == 1) {

             if (object.pageSize <self.page) {

                 self.tableView.showsInfiniteScrolling = NO;

             }else{

                 [self            reloadDatasouce:object._tagItemList];
                 [self.tableView  reloadData];
             }

             [GiFHUD  dismiss];
         }
         else{

             [GiFHUD  dismiss];
             NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:object.resultCode];
             [SVProgressHUD showImage:Nil status:codeStr];
         }
     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    id object = [self.dataSourceArray objectAtIndex:indexPath.row];
    return [iEver_tagItem_Cell heightForObject:object atIndexPath:indexPath tableView:tableView];

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    iEver_tagItemList_object *object = (iEver_tagItemList_object *)[self.dataSource itemAtIndexPath:indexPath];
    iEver_ItemDetailViewController *_itemlDetailViewController = [[iEver_ItemDetailViewController alloc]init];
    _itemlDetailViewController.type_id = object.T_ID;
    [self.navigationController pushViewController:_itemlDetailViewController animated:YES];
    IOS7POP;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}

#pragma mark -
#pragma mark UIInterfaceOrientation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return UIInterfaceOrientationPortrait;
}

- (BOOL)shouldAutorotate{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{

    return UIInterfaceOrientationPortrait;
}


@end
