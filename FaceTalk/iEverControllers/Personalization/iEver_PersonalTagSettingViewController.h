//
//  iEver_PersonalTagSettingViewController.h
//  FaceTalk
//
//  Created by 开仲 司 on 15/3/14.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEverBaseViewController.h"

@interface iEver_PersonalTagSettingViewController : iEverBaseViewController

@property (copy, nonatomic) NSString *userIsCustom;
@property (strong) id delegate;
@end
