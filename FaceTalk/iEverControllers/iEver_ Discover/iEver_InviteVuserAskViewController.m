//
//  iEver_InviteVuserAskViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/5/27.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_InviteVuserAskViewController.h"

#import "iEver_AskViewController.h"

#import "iEver_invitVuserCell.h"
#import "iEver_inviteVuserObject.h"

#import "REMenu.h"

@interface iEver_InviteVuserAskViewController ()
{

    UILabel *userTypeLable;
    NSString *fetchTypeStr;
    int categoryId;

    NSMutableArray *categoryMenuItemArray;
}

@property (strong, nonatomic) REMenu *menu;

@end

@implementation iEver_InviteVuserAskViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    /* NO show pull refresh */
    self.tableView.showsPullToRefresh = NO;
    self.tableView.showsInfiniteScrolling = NO;
    self.page = 1;

    [self addNavBar];

    fetchTypeStr = @"queryAll";
    categoryId = 0;

    categoryMenuItemArray = [[NSMutableArray alloc] init];

    /* 获取数据 */
    [self fetchData];

}


-(void)addNavBar {

    /* 标题“邀请” */
    UIView *centerBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 90, 44)];
    UILabel *askTitleLable = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 40, 25)];
    askTitleLable.backgroundColor = CLEAR;
    askTitleLable.font = TextBigFont;
    askTitleLable.textColor = BLACK;
    askTitleLable.text = @"邀请";
    [centerBarView addSubview:askTitleLable];

    /* 标题傍边的选择回答类型 */
    userTypeLable = [[UILabel alloc] initWithFrame:CGRectMake(45, 12, 45, 25)];
    userTypeLable.backgroundColor = CLEAR;
    userTypeLable.font = TextFont;
    userTypeLable.textColor = BLACK;
    userTypeLable.text = @"全 部";
    [centerBarView addSubview:userTypeLable];

    /* 标题傍边的指示小箭头 */
    UIImage *VuserCenterNav_arrow = [UIImage imageNamed:@"VuserCenterNav_arrow"];
    UIImageView *titleSignImageView = [[UIImageView alloc] initWithFrame:CGRectMake(87, 25, VuserCenterNav_arrow.size.width, VuserCenterNav_arrow.size.height)];
    titleSignImageView.image = VuserCenterNav_arrow;
    [centerBarView addSubview:titleSignImageView];

    centerBarView.backgroundColor = CLEAR;
    self.navigationItem.titleView = centerBarView;

    centerBarView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(TapcenterBarView:)];
    tapGesture.numberOfTapsRequired = 1;
    [centerBarView addGestureRecognizer:tapGesture];

    /* 导航条右按钮 */
    UIButton *rightBarBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBarBtn.frame = CGRectMake(0, 0, 44, 44);
    [rightBarBtn setTitle:@"确定" forState:UIControlStateNormal];
    [rightBarBtn setTitleColor:MY_PURPLECOLOR forState:UIControlStateNormal];
    rightBarBtn.titleLabel.font = TextFont;
    rightBarBtn.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        [self.delegate delegateWithInviteVC];
        [self.navigationController popViewControllerAnimated:YES];


    return [RACSignal empty];

    }];

    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarBtn];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    
}

- (void)TapcenterBarView:(UITapGestureRecognizer *)gesture {

    if (_menu.isOpen)
        return [_menu close];

    _menu = [[REMenu alloc] initWithItems:categoryMenuItemArray];

    [_menu showFromNavigationController:self.navigationController];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataSourceArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *reuseIdetify = @"InvitVuserCell";
    iEver_invitVuserCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdetify];
    if (!cell) {
        cell = [[iEver_invitVuserCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdetify];
    }

    if ([self.dataSourceArray count] > 0) {
        iEver_userList_object *content_object = (iEver_userList_object *)[self.dataSourceArray objectAtIndex:indexPath.row];
        [cell setObject:content_object];
    }
    return cell;
}


#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    iEver_userList_object *content_object = (iEver_userList_object *)[self.dataSourceArray objectAtIndex:indexPath.row];

    return [iEver_invitVuserCell heightForObject:content_object atIndexPath:indexPath tableView:tableView];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    iEver_invitVuserCell *cell = (iEver_invitVuserCell*)[tableView cellForRowAtIndexPath:indexPath];

    iEver_userList_object *content_object = (iEver_userList_object *)[self.dataSourceArray objectAtIndex:indexPath.row];


        if (content_object.isSelect) {
            content_object.isSelect = NO;
            [cell setChecked:YES];

            /* 删除邀请用户 */
            [self.delegate.inviteVuserID removeObject:[NSNumber numberWithInt:content_object.user_id]];
            [self.delegate.inviteVuserHeadimage removeObject:content_object.headImg];

        }else {

            if (self.delegate.inviteVuserID.count > 2) {

                UIAlertView *alterView = [[UIAlertView alloc] initWithTitle:@"提示"
                                                                    message:@"一次只能邀请三个达人回答您的问题"
                                                                   delegate:nil
                                                          cancelButtonTitle:nil
                                                          otherButtonTitles:@"确定", nil];
                [alterView show];

                return;
                
            }
            content_object.isSelect = YES;
            [cell setChecked:NO];
            /* 添加邀请用户 */
            [self.delegate.inviteVuserID addObject:[NSNumber numberWithInt:content_object.user_id]];
            [self.delegate.inviteVuserHeadimage addObject:content_object.headImg];
        }

}


//刷新数据
///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)fetchData
{

    @weakify(self);
    iEver_inviteVuserObject *inviteVuserObject = [[iEver_inviteVuserObject alloc] init];
    NSString *_pathUrl;
    if ([fetchTypeStr isEqualToString:@"queryAll"]) {
        _pathUrl = [NSString stringWithFormat:@"/expertUser/queryAll"];
    }else {
        _pathUrl = [NSString stringWithFormat:@"/expertUser/queryByCategoryId/%d",categoryId];
    }
    [[[inviteVuserObject expertUserQueryAll:nil path:_pathUrl] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(iEver_inviteVuserObject *object) {

         @strongify(self);
         if (self.page > 1) {

             self.tableView.showsInfiniteScrolling = NO;
         }else{

             if ([fetchTypeStr isEqualToString:@"queryAll"]) {

                 [self creatMenuItem:object._categoryList];
             }

             [self addDataToDataSource:object._userList];
             [self.tableView reloadData];

         }
         
     }];
}

-(void)creatMenuItem:(NSMutableArray *)array {

    for (int i = 0; i < array.count; i ++) {
        iEver_categoryList_object *categoryObject = array[i];

        REMenuItem *item = [[REMenuItem alloc] initWithTitle:categoryObject.categoryName
                                                    subtitle:nil
                                                       image:nil
                                            highlightedImage:nil
                                                      action:^(REMenuItem *item) {
                                                          NSLog(@"Item: %@", item);

                                                          userTypeLable.text = item.title;

                                                          categoryId = item.tag;

                                                          fetchTypeStr = @"queryByCategoryId";

                                                          [self.dataSourceArray removeAllObjects];
                                                          [self.tableView setContentOffset:CGPointMake(0,0) animated:NO];
                                                          self.page = 1;
                                                          [self fetchData];
                                                      }];

        item.tag = categoryObject.category_id;

        [categoryMenuItemArray addObject:item];
    }

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
