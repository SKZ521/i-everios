//
//  iEver_VuserHomeViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/6/4.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_VuserHomeViewController.h"

#import "iEver_VuserShareArticleViewController.h"
#import "iEver_VuserAnsweredQuestionViewController.h"

#import "iEver_UserMessageObject.h"
#import "iEver_AskViewController.h"

#define kScreen_Height [UIScreen mainScreen].bounds.size.height
#define kScreen_Width [UIScreen mainScreen].bounds.size.width
@interface iEver_VuserHomeViewController ()<UIScrollViewDelegate>
{
    UIView *infoParentView; // 放达人信息背景图
    UIView *infoView;       // 放用户信息
    UILabel *titleLabel;    // 上划后保留在头部的达人昵称

    UIView *segmentView;    // 放置分享、问答按钮的视图
    UIButton *shareButton;  // 分享按钮
    UIButton *aqButton;     // 回答问题按钮
    UIView *highLightView;  // 标记选中的线
    UIScrollView *baseScrollView;

    iEver_VuserShareArticleViewController *shareViewController;
    iEver_VuserAnsweredQuestionViewController *aqViewController;
}
@property(nonatomic, retain)iEver_personMessage_object *person_object;
@property(nonatomic, copy)NSString *userId;
@end

@implementation iEver_VuserHomeViewController

- (instancetype)initWithUserInfo:(iEver_personMessage_object *)userInfo userId:(NSString *)uId
{
    if (self = [super init])
    {
        self.person_object = userInfo;
        self.userId = uId;
    }
    return self;
}

#define kShareButtonTag 6808
#define kAqButtonTag 6809
- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.view setBackgroundColor:WHITE];

    /* 信息区 */
    // 父视图
    CGRect infoRect = CGRectMake(.0f, .0f, kScreen_Width, 212.0f);
    infoParentView = [[UIView alloc] initWithFrame:infoRect];
    [self.view addSubview:infoParentView];

    // 背景图
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:infoParentView.bounds];
    [bgView setImage:[UIImage imageNamed:@"V_home_bg"]];
    [infoParentView addSubview:bgView];

    // 承载用户信息
    infoView = [[UIView alloc] initWithFrame:CGRectMake(20.0f, 20.0f, CGRectGetWidth(infoRect)-40.0f, CGRectGetHeight(infoRect)-40.0f)];
    [infoParentView addSubview:infoView];

    // 头像
    UIImageView *iconView = [[UIImageView alloc] initWithFrame:CGRectMake((CGRectGetWidth(infoView.frame)-77.0f)/2, .0f, 77.0f, 77.0f)];
    [iconView setImageWithURL:[NSURL URLWithString:_person_object.headImg] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
    iconView.layer.cornerRadius = 77.0f/2;
    iconView.layer.masksToBounds = YES;
    [iconView setBackgroundColor:CLEAR];
    [infoView addSubview:iconView];

    // name label
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake((CGRectGetWidth(infoView.frame)-200.0f)/2, CGRectGetMaxY(iconView.frame)+20.0f, 200.0f, 30.0f)];
    [nameLabel setBackgroundColor:CLEAR];
    [nameLabel setTextColor:WHITE];
    [nameLabel setTextAlignment:NSTextAlignmentCenter];
    nameLabel.font = TextBigBoldFont;
    nameLabel.adjustsFontSizeToFitWidth = YES;
    nameLabel.text = _person_object.nickName;
    [infoView addSubview:nameLabel];

    // tag label
    UILabel *tagLabel = [[UILabel alloc] initWithFrame:CGRectMake((CGRectGetWidth(infoView.frame)-200.0f)/2, CGRectGetMaxY(nameLabel.frame)+20.0f, 200.0f, 30.0f)];
    [tagLabel setBackgroundColor:CLEAR];
    [tagLabel setTextColor:WHITE];
    [tagLabel setTextAlignment:NSTextAlignmentCenter];
    tagLabel.font = TextFont;
    tagLabel.adjustsFontSizeToFitWidth = YES;
    tagLabel.text = _person_object.intro;
    [infoView addSubview:tagLabel];

    // 返回按钮
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(11.0f, 20.0f, 40.0f, 40.0f)];
    [backButton setImage:[UIImage imageNamed:@"NAV_return"] forState:UIControlStateNormal];
    backButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        [self.navigationController popViewControllerAnimated:YES];
        return [RACSignal empty];
    }];
    [self.view addSubview:backButton];

    // title label
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(42.0f, 25, CGRectGetWidth(infoRect)-82.0f, 30.0f)];
    [titleLabel setBackgroundColor:CLEAR];
    [titleLabel setTextColor:WHITE];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    titleLabel.font = TextBigFont;
    titleLabel.adjustsFontSizeToFitWidth = YES;
    titleLabel.text = _person_object.nickName;
    [self.view addSubview:titleLabel];
    titleLabel.alpha = .0f;

    __weak iEver_VuserHomeViewController *weakself = self;
    if (_person_object.userType == 20)
    {
        // 1.创建基本UI架构
        // V标
        UIImageView *bedgeView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(iconView.frame)-20.0f, CGRectGetMaxY(iconView.frame)-20.0f, 14.0f, 14.0f)];
        [bedgeView setImage:[UIImage imageNamed:@"V_home_bedge"]];
        [bedgeView setBackgroundColor:CLEAR];
        [infoView addSubview:bedgeView];

        // 放置按钮
        segmentView = [[UIView alloc] initWithFrame:CGRectMake(.0f, CGRectGetHeight(infoRect), kScreen_Width, 47.0f)];
        [segmentView setBackgroundColor:CLEAR];
        [self.view addSubview:segmentView];

        // 经验分享按钮
        shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
        shareButton.tag = kShareButtonTag;
        [shareButton setFrame:CGRectMake(11.0f, .0f, (CGRectGetWidth(segmentView.frame)-11.0f*2-1.0f-2*2)/2, 45.0f)];
        [shareButton setBackgroundColor:CLEAR];
        [shareButton setTitle:[NSString stringWithFormat:@"经验分享(%d)",_person_object.articleTotal] forState:UIControlStateNormal];
        [shareButton setTitleColor:[UIColor colorWithRed:126.0f/255.0f green:106.0f/255.0f blue:162.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
        shareButton.titleLabel.font = TextFont;
        [shareButton addTarget:self action:@selector(segmentButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [segmentView addSubview:shareButton];

        // 竖线
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(11.0f+CGRectGetWidth(shareButton.frame)+2.0f, 2.0f, 1.0f, 40.0f)];
        [lineView setBackgroundColor:[UIColor colorWithRed:242.0f/255.0f green:242.0f/255.0f blue:242.0f/255.0f alpha:1.0f]];
        [segmentView addSubview:lineView];

        // 问答按钮
        aqButton = [UIButton buttonWithType:UIButtonTypeCustom];
        aqButton.tag = kAqButtonTag;
        [aqButton setFrame:CGRectMake(CGRectGetMinX(lineView.frame)+2, .0f, CGRectGetWidth(shareButton.frame), 45.0f)];
        [aqButton setBackgroundColor:CLEAR];
        [aqButton setTitle:[NSString stringWithFormat:@"回答问题(%d)",_person_object.answerTotal] forState:UIControlStateNormal];
        [aqButton setTitleColor:BLACK forState:UIControlStateNormal];
        aqButton.titleLabel.font = TextFont;
        [aqButton addTarget:self action:@selector(segmentButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [segmentView addSubview:aqButton];

        // 标记选中的横线
        highLightView = [[UIView alloc] initWithFrame:CGRectMake(11.0f, CGRectGetMaxY(shareButton.frame), CGRectGetWidth(shareButton.frame), 2.0f)];
        [highLightView setBackgroundColor:[UIColor colorWithRed:126.0f/255.0f green:106.0f/255.0f blue:162.0f/255.0f alpha:1.0f]];
        [segmentView addSubview:highLightView];

        // 承载数据列表的滚动面板
        baseScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(.0f, CGRectGetMaxY(segmentView.frame), kScreen_Width, kScreen_Height-CGRectGetMaxY(segmentView.frame)-49.0f)];
        [baseScrollView setContentSize:CGSizeMake(CGRectGetWidth(baseScrollView.frame)*2, CGRectGetHeight(baseScrollView.frame))];
        baseScrollView.delegate = self;
        baseScrollView.pagingEnabled = YES;
        baseScrollView.showsHorizontalScrollIndicator = NO;
        [baseScrollView setBackgroundColor:CLEAR];
        [self.view addSubview:baseScrollView];

        // 2.添加子控制器
        shareViewController = [[iEver_VuserShareArticleViewController alloc] initWithUserId:_userId];
        shareViewController.navigationController = self.navigationController;
        shareViewController.upBlock = ^{
            [weakself hideInfoView];
        };
        shareViewController.downBlock = ^{
            [weakself showInfoView];
        };
        [self addChildViewController:shareViewController];
        [shareViewController resetViewFrameWithRect:CGRectMake(.0f, .0f, CGRectGetWidth(baseScrollView.frame), CGRectGetHeight(baseScrollView.frame))];
        [baseScrollView addSubview:shareViewController.view];

        aqViewController = [[iEver_VuserAnsweredQuestionViewController alloc] init];
        aqViewController.navigationController = self.navigationController;
        aqViewController.upBlock = ^{
            [weakself hideInfoView];
        };
        aqViewController.downBlock = ^{
            [weakself showInfoView];
        };
        [self addChildViewController:aqViewController];
        [aqViewController resetViewFrameWithRect:CGRectMake(CGRectGetWidth(baseScrollView.frame), .0f, CGRectGetWidth(baseScrollView.frame), CGRectGetHeight(baseScrollView.frame))];
        [baseScrollView addSubview:aqViewController.view];

        // ASK按钮
        UIButton *askButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [askButton setFrame:CGRectMake(CGRectGetWidth(infoRect)-27.0f-8.0f, CGRectGetHeight(infoRect)/2, 27.0f, 27.0f)];
        [askButton setBackgroundColor:CLEAR];
        [askButton setImage:[UIImage imageNamed:@"askAnswerNav_ask"] forState:UIControlStateNormal];
        askButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
            iEver_AskViewController *_askMeViewController = [[iEver_AskViewController alloc]init];

            _askMeViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:_askMeViewController animated:YES];
            IOS7POP;
            return [RACSignal empty];
        }];
        [self.view addSubview:askButton];
    } else {
        aqViewController = [[iEver_VuserAnsweredQuestionViewController alloc] init];
        aqViewController.navigationController = self.navigationController;
        aqViewController.upBlock = ^{
            [weakself hideInfoView];
        };
        aqViewController.downBlock = ^{
            [weakself showInfoView];
        };
        [self addChildViewController:aqViewController];
        [aqViewController resetViewFrameWithRect:CGRectMake(.0f, CGRectGetMaxY(infoParentView.frame), CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-CGRectGetMaxY(infoParentView.frame)-49.0f)];
        [self.view addSubview:aqViewController.view];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    self.navigationController.navigationBarHidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    self.navigationController.navigationBarHidden = NO;
}
#pragma mark - Private Methods
// 经验分享/问答
- (void)segmentButtonClick:(id)sender
{
    UIButton *button = (UIButton *)sender;
    if (button.tag == kShareButtonTag)
    {
        [shareButton setTitleColor:[UIColor colorWithRed:126.0f/255.0f green:106.0f/255.0f blue:162.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
        [aqButton setTitleColor:BLACK forState:UIControlStateNormal];

        [UIView animateWithDuration:.25f animations:^{
            [baseScrollView setContentOffset:CGPointMake(.0f, .0f)];

            CGRect tempFrame = highLightView.frame;
            tempFrame.origin.x = CGRectGetMinX(shareButton.frame);
            highLightView.frame = tempFrame;
        }];

    } else {
        [shareButton setTitleColor:BLACK forState:UIControlStateNormal];
        [aqButton setTitleColor:[UIColor colorWithRed:126.0f/255.0f green:106.0f/255.0f blue:162.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];

        [UIView animateWithDuration:.25f animations:^{
            [baseScrollView setContentOffset:CGPointMake(CGRectGetWidth(baseScrollView.frame), .0f)];

            CGRect tempFrame = highLightView.frame;
            tempFrame.origin.x = CGRectGetMinX(aqButton.frame);
            highLightView.frame = tempFrame;
        }];
    }
}

// 下划显示达人信息
- (void)showInfoView
{
    if (!titleLabel.alpha) return;

    [UIView animateWithDuration:.35f animations:^{
        infoView.alpha = 1.0f;
        titleLabel.alpha = .0f;

        CGRect tempFrame = infoParentView.frame;
        tempFrame.origin.y = .0f;
        infoParentView.frame = tempFrame;

        if (_person_object.userType == 20)
        {
            tempFrame = segmentView.frame;
            tempFrame.origin.y = CGRectGetMaxY(infoParentView.frame);
            segmentView.frame = tempFrame;

            tempFrame = baseScrollView.frame;
            tempFrame.origin.y = CGRectGetMaxY(segmentView.frame);
            tempFrame.size.height = kScreen_Height-CGRectGetMaxY(segmentView.frame)-49.0f;
            baseScrollView.frame = tempFrame;

            [shareViewController resetViewFrameWithRect:CGRectMake(.0f, .0f, CGRectGetWidth(baseScrollView.frame), CGRectGetHeight(baseScrollView.frame))];
        } else {
            [aqViewController resetViewFrameWithRect:CGRectMake(.0f, CGRectGetMaxY(infoParentView.frame), CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-CGRectGetMaxY(infoParentView.frame)-49.0f)];
        }
    }];
}

// 上划隐藏达人信息
- (void)hideInfoView
{
    if (titleLabel.alpha) return;

    [UIView animateWithDuration:.35f animations:^{
        infoView.alpha = .0f;
        titleLabel.alpha = 1.0f;

        CGRect tempFrame = infoParentView.frame;
        tempFrame.origin.y = -148.0f;
        infoParentView.frame = tempFrame;

        if (_person_object.userType == 20)
        {
            tempFrame = segmentView.frame;
            tempFrame.origin.y = 64.0f;
            segmentView.frame = tempFrame;

            tempFrame = baseScrollView.frame;
            tempFrame.origin.y = CGRectGetMaxY(segmentView.frame);
            tempFrame.size.height = kScreen_Height-CGRectGetMaxY(segmentView.frame)-49.0f;
            baseScrollView.frame = tempFrame;

            [shareViewController resetViewFrameWithRect:CGRectMake(.0f, .0f, CGRectGetWidth(baseScrollView.frame), CGRectGetHeight(baseScrollView.frame))];
        } else {
            [aqViewController resetViewFrameWithRect:CGRectMake(.0f, CGRectGetMaxY(infoParentView.frame), CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame)-CGRectGetMaxY(infoParentView.frame)-49.0f)];
        }
    }];
}
#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSInteger index = fabs(scrollView.contentOffset.x) / scrollView.frame.size.width;
    if (index == 0)
    {
        [shareButton setTitleColor:[UIColor colorWithRed:126.0f/255.0f green:106.0f/255.0f blue:162.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
        [aqButton setTitleColor:BLACK forState:UIControlStateNormal];

        [UIView animateWithDuration:.25f animations:^{
            CGRect tempFrame = highLightView.frame;
            tempFrame.origin.x = CGRectGetMinX(shareButton.frame);
            highLightView.frame = tempFrame;
        }];
    } else {
        [shareButton setTitleColor:BLACK forState:UIControlStateNormal];
        [aqButton setTitleColor:[UIColor colorWithRed:126.0f/255.0f green:106.0f/255.0f blue:162.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];

        [UIView animateWithDuration:.25f animations:^{
            CGRect tempFrame = highLightView.frame;
            tempFrame.origin.x = CGRectGetMinX(aqButton.frame);
            highLightView.frame = tempFrame;
        }];
    }
}

@end
