//
//  iEver_AnswerViewController.h
//  FaceTalk
//
//  Created by kevin on 15/6/1.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEverBaseViewController.h"

@interface iEver_AnswerViewController : iEverBaseViewController

@property (nonatomic, assign) int                q_id;              /* 问题_id */
@property (nonatomic, assign) int                aUserType;         /* 用户类型id */

@end
