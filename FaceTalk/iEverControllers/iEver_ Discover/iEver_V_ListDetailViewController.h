//
//  iEver_V_ListDetailViewController.h
//  FaceTalk
//
//  Created by 开仲 司 on 14/12/22.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEverBaseTableViewController.h"
#import "iEver_discoverObject.h"
#import <MediaPlayer/MediaPlayer.h>
@class iEver_V_PersonCenterViewController;
@interface iEver_V_ListDetailViewController : iEverBaseTableViewController
@property (nonatomic, assign) int                        type_id;       /* id */
@property (nonatomic, retain) UIImage                    *image;
@property (nonatomic, retain) iEver_discoverContentList_object *list_Object;
@property (nonatomic, retain) iEver_V_PersonCenterViewController *delegate;
@property (nonatomic, assign) int                        didSelectIndexPath;/* 选中cell的indexpath  */

@end
