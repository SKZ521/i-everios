//
//  iEver_invitVuserCell.h
//  FaceTalk
//
//  Created by 开仲 司 on 15/5/27.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "SSBaseTableCell.h"

#import "iEver_inviteVuserObject.h"

@interface iEver_invitVuserCell : SSBaseTableCell

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;

@property (nonatomic, strong) iEver_userList_object *object;

- (void)setChecked:(BOOL)checked;

@end
