//
//  iEver_AskAnswerDetailViewController.h
//  FaceTalk
//
//  Created by kevin on 15/5/31.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEverBaseTableViewController.h"

@interface iEver_AskAnswerDetailViewController : iEverBaseTableViewController

@property (nonatomic, assign) int                question_id;           /* id */

-(void)insertCommentToAnswerComment;                                    /* 点击回答的评论，进行评论 */

@property (nonatomic, assign) int               comment_qId;            /* 回答评论 问题id */
@property (nonatomic, assign) int               comment_aId;            /* 回答评论 回答id */
@property (nonatomic, assign) int               comment_atUserId;       /* 回答评论 的atUser ID */
@property (nonatomic, copy  ) NSString       *toAnswerCommentName;      /* 回答评论 用户名 */

@end
