//
//  iEver_AnswerYouViewController.h
//  FaceTalk
//
//  Created by 开仲 司 on 14/11/25.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEverBaseViewController.h"

@interface iEver_AnswerYouViewController : iEverBaseViewController
@property (nonatomic, assign) int                        userId;       /* id */
@property (nonatomic, copy)   NSString                   *name;        /* 问题者昵称 */
@property (nonatomic, copy)   NSString                   *question;    /* 问题 */
@property (nonatomic, copy)   NSString                   *image;       /* 问题者头像 */
@property (strong)  id delegate;
@end
