//
//  iEver_inviteVuserObject.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/5/27.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_inviteVuserObject.h"

@implementation iEver_inviteVuserObject

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }

    /* quesList object  */
    NSMutableArray *array_userList_object = [[NSMutableArray alloc] init];
    self.pageSize                         = [[attributes objectForKey:@"pageSize"] integerValue];
    self.resultCode                       = [[attributes objectForKey:@"resultCode"] integerValue];
    array_userList_object                 = [attributes objectForKey:@"userList"];
    self._userList                        = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_userList_object count]; i++) {

        iEver_userList_object *quest_object = [[iEver_userList_object alloc] initWithAttributes:[array_userList_object objectAtIndex:i]];
        [self._userList addObject:quest_object];
    }

    /* categoryList object  */
    NSMutableArray *array_categoryList_object = [[NSMutableArray alloc] init];
    array_categoryList_object                 = [attributes objectForKey:@"categoryList"];
    self._categoryList                        = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_categoryList_object count]; i++) {

        iEver_categoryList_object *quest_object = [[iEver_categoryList_object alloc] initWithAttributes:[array_categoryList_object objectAtIndex:i]];
        [self._categoryList addObject:quest_object];
    }



    return self;
}

/*  expertQuestion data mothod */
- (RACSignal *)expertUserQueryAll:(NSDictionary *)dic path:(NSString *)pathUrl{

    /* restful request URL /expertQuestion/queryAll/{qCategoryId}/{qStatus}/{currPage} */
    return [[[iEver_AppDotNetAPIClient sharedClient] enqueueRequestWithMethod:@"GET"
                                                                         path:pathUrl
                                                                   parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                iEver_inviteVuserObject * object = [[iEver_inviteVuserObject alloc] initWithAttributes:responseObject];
                return object;
            }];
    
}
@end


/* userList object  */
@implementation iEver_userList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.user_id              = [[attributes objectForKey:@"id"] integerValue];
    self.nickName             = [attributes objectForKey:@"nickName"];
    self.headImg              = [attributes objectForKey:@"headImg"];
    self.intro                = [attributes objectForKey:@"intro"];
    self.answerTotal          = [[attributes objectForKey:@"answerTotal"] integerValue];
    self.pvAllTotal           = [[attributes objectForKey:@"pvAllTotal"] integerValue];
    self.articleTotal         = [[attributes objectForKey:@"articleTotal"] integerValue];
    self.sortLevel            = [[attributes objectForKey:@"sortLevel"] integerValue];
    self.isSelect             = NO;

    return self;
}

@end


/* categoryList object  */
@implementation iEver_categoryList_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.category_id          = [[attributes objectForKey:@"id"] integerValue];
    self.categoryName         = [attributes objectForKey:@"categoryName"];
    self.sortLevel            = [[attributes objectForKey:@"sortLevel"] integerValue];

    return self;
}

@end

