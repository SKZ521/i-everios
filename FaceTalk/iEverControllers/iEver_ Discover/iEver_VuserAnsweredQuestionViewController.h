//
//  iEver_VuserAnsweredQuestionViewController.h
//  FaceTalk
//
//  Created by 开仲 司 on 15/3/25.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEverBaseTableViewController.h"
@class iEver_VuserCenterViewController;

@class iEver_V_userObject;

typedef void (^ScrollUpBlock)();
typedef void (^ScrollDownBlock)();

@interface iEver_VuserAnsweredQuestionViewController : iEverBaseTableViewController

@property(nonatomic,strong)iEver_V_userObject *object;

@property(strong)UINavigationController *navigationController;

@property(strong)iEver_VuserCenterViewController *superController;

@property(nonatomic, copy)ScrollUpBlock upBlock;
@property(nonatomic, copy)ScrollDownBlock downBlock;

- (instancetype)initWithUserId:(NSString *)aId;

- (void)resetViewFrameWithRect:(CGRect)rect;
@end
