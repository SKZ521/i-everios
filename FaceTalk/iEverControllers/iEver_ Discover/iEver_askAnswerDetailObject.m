//
//  iEver_askAnswerDetailObject.m
//  FaceTalk
//
//  Created by kevin on 15/5/31.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_askAnswerDetailObject.h"

@implementation iEver_askAnswerDetailObject

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    
    /* answerDetailList object  */
    NSMutableArray *array_answerDetailList_object = [[NSMutableArray alloc] init];
    self.answerPageSize                   = [[attributes objectForKey:@"answerPageSize"] integerValue];
    self.resultCode                       = [[attributes objectForKey:@"resultCode"] integerValue];
    array_answerDetailList_object                 = [attributes objectForKey:@"answerDetailList"];
    self._answerDetailList                        = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_answerDetailList_object count]; i++) {
        
        iEver_answer_object *answer_object = [[iEver_answer_object alloc] initWithAttributes:[array_answerDetailList_object objectAtIndex:i]];
        [self._answerDetailList addObject:answer_object];
    }
    
    /* categoryList object  */
    self.questionObject = [[iEver_question_object alloc] initWithAttributes:[attributes objectForKey:@"question"]];
    
    return self;
}

/*  expertQuestionQueryById data mothod */
- (RACSignal *)expertQuestionQueryById:(NSDictionary *)dic path:(NSString *)pathUrl{
    
    /* restful request URL /expertQuestion/queryById/{questionId}/{currPage} */
    return [[[iEver_AppDotNetAPIClient sharedClient] enqueueRequestWithMethod:@"GET"
                                                                         path:pathUrl
                                                                   parameters:dic
             ]
            map:^id(NSDictionary *responseObject) {
                if ([responseObject isKindOfClass:[NSNull class]]) {
                    return nil;
                }
                iEver_askAnswerDetailObject * object = [[iEver_askAnswerDetailObject alloc] initWithAttributes:responseObject];
                return object;
            }];
    
}
@end


/* question object  */
@implementation iEver_question_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.question_id                 = [[attributes objectForKey:@"id"] integerValue];
    self.qCategoryId                 = [[attributes objectForKey:@"qCategoryId"] integerValue];
    self.qUserId                     = [[attributes objectForKey:@"qUserId"] integerValue];
    self.qNickName                   = [attributes objectForKey:@"qNickName"];
    self.qHeadImg                    = [attributes objectForKey:@"qHeadImg"];
    self.qTitle                      = [attributes objectForKey:@"qTitle"];
    self.qContent                    = [attributes objectForKey:@"qContent"];
    self.answerTotal                 = [[attributes objectForKey:@"answerTotal"] integerValue];
    self.qStatus                     = [[attributes objectForKey:@"qStatus"] integerValue];
    self.releaseTime                 = [[attributes objectForKey:@"releaseTime"] longLongValue];
    
    /* quesPicList object  */
    NSMutableArray *array_quesPicList_object = [[NSMutableArray alloc] init];
    array_quesPicList_object                 = [attributes objectForKey:@"quesPicList"];
    self._quesPicList                        = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_quesPicList_object count]; i++) {
        
        iEver_quesPic_object *answerPic_object = [[iEver_quesPic_object alloc] initWithAttributes:[array_quesPicList_object objectAtIndex:i]];
        [self._quesPicList addObject:answerPic_object];
    }
    
    
    return self;
}

@end


/* quesPic object  */
@implementation iEver_quesPic_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.pic_id               = [[attributes objectForKey:@"id"] integerValue];
    self.imgUrl               = [attributes objectForKey:@"imgUrl"];
    
    return self;
}

@end



/* answer object  */
@implementation iEver_answer_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.answer_id              = [[attributes objectForKey:@"id"] integerValue];
    self.qId_id                 = [[attributes objectForKey:@"qId"] integerValue];
    self.userId                 = [[attributes objectForKey:@"userId"] integerValue];
    self.aNickName              = [attributes objectForKey:@"aNickName"];
    self.aHeadImg               = [attributes objectForKey:@"aHeadImg"];
    self.userType               = [[attributes objectForKey:@"userType"] integerValue];
    self.content                = [attributes objectForKey:@"content"];
    self.commentTotal           = [[attributes objectForKey:@"commentTotal"] integerValue];
    /* isPraise是否点赞(大于1为已点赞)  这里用 bool 类型_selected 将isPraise转化 */
    if ([[attributes objectForKey:@"isPraise"] integerValue] >0) {
        self._selected            = YES;
    }else{
        self._selected            = NO;
    }
    
    self.praiseTotal            = [[attributes objectForKey:@"praiseTotal"] integerValue];
    self.createTime             = [[attributes objectForKey:@"createTime"] longLongValue];

    /* answerPicList object  */
    NSMutableArray *array_answerPicList_object = [[NSMutableArray alloc] init];
    array_answerPicList_object                 = [attributes objectForKey:@"answerPicList"];
    self._answerPicList                        = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_answerPicList_object count]; i++) {
        
        iEver_answerPic_object *answerPic_object = [[iEver_answerPic_object alloc] initWithAttributes:[array_answerPicList_object objectAtIndex:i]];
        [self._answerPicList addObject:answerPic_object];
    }
    
    /* answerCommentList object  */
    NSMutableArray *array_answerCommentList_object = [[NSMutableArray alloc] init];
    array_answerCommentList_object                 = [attributes objectForKey:@"answerCommentList"];
    self._answerCommentList                        = [[NSMutableArray alloc] initWithCapacity:0];
    for (int i = 0; i < [array_answerCommentList_object count]; i++) {
        
        iEver_answerComment_object *answerComment_object = [[iEver_answerComment_object alloc] initWithAttributes:[array_answerCommentList_object objectAtIndex:i]];
        [self._answerCommentList addObject:answerComment_object];
    }

    return self;
}

@end

/* answerPicList object  */
@implementation iEver_answerPic_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.pic_id              = [[attributes objectForKey:@"id"] integerValue];
    self.imgUrl               = [attributes objectForKey:@"imgUrl"];
    
    return self;
}

@end


/* answerComment object  */
@implementation iEver_answerComment_object

- (id)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.answerComment_id              = [[attributes objectForKey:@"id"] integerValue];
    self.userId                        = [[attributes objectForKey:@"userId"] integerValue];
    self.userNickName                  = [attributes objectForKey:@"userNickName"];
    self.atUserId                      = [[attributes objectForKey:@"atUserId"] integerValue];
    self.atUserNickName                = [attributes objectForKey:@"atUserNickName"];
    self.commentContent                = [attributes objectForKey:@"commentContent"];
    self.createTime                    = [[attributes objectForKey:@"createTime"] longLongValue];
    
    return self;
}

@end


