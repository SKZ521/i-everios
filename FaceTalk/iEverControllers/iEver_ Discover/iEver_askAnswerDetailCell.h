//
//  iEver_askAnswerDetailCell.h
//  FaceTalk
//
//  Created by kevin on 15/6/1.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "SSBaseTableCell.h"
#import "iEver_askAnswerDetailObject.h"

@interface iEver_askAnswerDetailCell : SSBaseTableCell

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;

@property (nonatomic, strong) iEver_answer_object *object;

@end
