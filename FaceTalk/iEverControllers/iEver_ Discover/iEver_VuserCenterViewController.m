//
//  iEver_VuserCenterViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/3/25.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_VuserCenterViewController.h"
#import "iEver_V_person_ViewController.h"
#import "iEver_VuserShareArticleViewController.h"
#import "iEver_VuserAnsweredQuestionViewController.h"
#import "iEver_LoginViewController.h"
#import "iEver_AskMeViewController.h"
#import "iEver_MemberHomePageHeaderView.h"
#import "iEver_V_userObject.h"

@interface iEver_VuserCenterViewController () <ViewPagerDataSource, ViewPagerDelegate>

{

    UIButton *ASKButton;
    UILabel *label_tab1;
    UILabel *label_tab2;
    iEver_V_userObject *_object;
    iEver_MemberHomePageHeaderView *headerView;
    iEver_VuserShareArticleViewController *_VuserShareArticleVC;
    iEver_VuserAnsweredQuestionViewController *_VuserAnsweredQuestionVC;
    
    int   _VuserNumber;

    UIButton *leftButton;

    __block  BOOL openOrClose;

    CGFloat headView_Y;

}

@end

@implementation iEver_VuserCenterViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

        self.title = @"大V说";

        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@""
                                                        image:[UIImage imageNamed:@"tabbar_discover_normal"]
                                                selectedImage:[UIImage imageNamed:@"tabbar_discover_click"]];

    }
    return self;
}


- (void)viewWillAppear:(BOOL)animated
{

    [super viewWillAppear:animated];
    ASKButton.hidden = NO;
    self.navigationController.navigationBarHidden = NO;
    [iEverAppDelegate shareAppDelegate].tabBarController.tabBar.hidden = NO;
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];

}

- (void)viewDidAppear:(BOOL)animated{

    [super viewDidAppear:YES];

    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [GiFHUD  dismiss];
    [super viewWillDisappear:animated];
    ASKButton.hidden = YES;
    
}

/* ask */
-(void)ASKVuserButton{
    
    if (!ASKButton) {
        ASKButton = [UIButton buttonWithType:UIButtonTypeCustom];
    }
    ASKButton.frame  = CGRectMake(ScreenWidth -50, ScreenHeight -200, 44, 44);
    ASKButton.hidden = NO;
    [ASKButton setImage:[UIImage imageNamed:@"VuserCenter_ask"] forState:UIControlStateNormal];
    [[iEverAppDelegate shareAppDelegate].tabBarController.view addSubview:ASKButton];
    [[iEverAppDelegate shareAppDelegate].tabBarController.view bringSubviewToFront:ASKButton];
    ASKButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        

        iEver_AskMeViewController *_askMeViewController = [[iEver_AskMeViewController alloc]init];
        
        iEver_discover_userList_object *user_Object = [_object._userList objectAtIndex:_VuserNumber];
        _askMeViewController.userId = user_Object.User_id;
        _askMeViewController.image  = user_Object.headImg;
        _askMeViewController.name   =user_Object.nickName;
        _askMeViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:_askMeViewController animated:YES];
        IOS7POP;
        
        return [RACSignal empty];
    }];
    
}

-(void)headViewAnimations{

    CGRect frame_headView;
    frame_headView = headerView.frame;
    frame_headView.origin.x = 0.0;
    frame_headView.origin.y = headView_Y;
    frame_headView.size.width = ScreenWidth;
    frame_headView.size.height = 377.5;


    CGContextRef context = UIGraphicsGetCurrentContext();
    [UIView beginAnimations:nil context:context];
    [UIView setAnimationDuration:.6];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:nil];

    headerView.frame = frame_headView;

    [UIView commitAnimations];

}

-(void)closeVuserInfoView{

    [leftButton setImage:[UIImage imageNamed:@"VuserCenter_nav_down"] forState:UIControlStateNormal];

    openOrClose = NO;

    [self.view endEditing:YES];

    headView_Y = - 377.5;
    [self headViewAnimations];

    iEver_discover_userList_object * userObject = (iEver_discover_userList_object *)[_object._userList objectAtIndex:_VuserNumber];

    self.title                   = userObject.nickName;

    self.y0    = 0.0;;
    self.y     = 0.0 + 44.0;
    self.hight = self.view.frame.size.height  - 40;

    self.dataSource = self;
    self.delegate   = self;

    [self reloadData];
}

-(void)openVuserInfoView{

    [leftButton setImage:[UIImage imageNamed:@"VuserCenter_nav_up"] forState:UIControlStateNormal];

    openOrClose = YES;

    [self.view endEditing:YES];

    headView_Y = 0.0;
    [self headViewAnimations];

    self.title                   = @"大V说";

    self.y0    = 0.0 + 377.5;
    self.y     = 0.0 + 44.0 + 377.5;
    self.hight = self.view.frame.size.height - 377.5  - 44;

    self.dataSource = self;
    self.delegate   = self;

    [self reloadData];
}


- (void)addBar
{

    @weakify(self);

    leftButton = [UIButton buttonWithType:UIButtonTypeCustom];

    leftButton.frame = CGRectMake(0, 0, 22, 22);
    [leftButton setImage:[UIImage imageNamed:@"VuserCenter_nav_up"] forState:UIControlStateNormal];


    [[leftButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *sender) {

        @strongify(self);

        if (openOrClose) {

            [leftButton setImage:[UIImage imageNamed:@"VuserCenter_nav_down"] forState:UIControlStateNormal];

            openOrClose = NO;

            [self.view endEditing:YES];

            headView_Y = - 377.5;
            [self headViewAnimations];

            iEver_discover_userList_object * userObject = (iEver_discover_userList_object *)[_object._userList objectAtIndex:_VuserNumber];
            self.title = userObject.nickName;

            self.y0    = 0.0;;
            self.y     = 0.0 + 44.0;
            self.hight = self.view.frame.size.height  - 40;

            self.dataSource = self;
            self.delegate   = self;
            
            [self reloadData];

        }else if(!openOrClose){

            [leftButton setImage:[UIImage imageNamed:@"VuserCenter_nav_up"] forState:UIControlStateNormal];


            openOrClose = YES;

            [self.view endEditing:YES];

            headView_Y = 0.0;
            [self headViewAnimations];

            self.title = @"大V说";

            self.y0    = 0.0 + 377.5;
            self.y     = 0.0 + 44.0 + 377.5;
            self.hight = self.view.frame.size.height - 377.5  - 44;

            self.dataSource = self;
            self.delegate   = self;
            
            [self reloadData];
        }

    }];
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    //self.navigationItem.leftBarButtonItem = leftBarButton;

    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:nil];
    [rightBarButton    setTintColor:WHITE];
    rightBarButton.rac_command      = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

         @strongify(self);
        iEver_V_person_ViewController *V_personViewController = [[iEver_V_person_ViewController alloc] init];
        V_personViewController.hidesBottomBarWhenPushed       = YES;
        [self.navigationController pushViewController:V_personViewController animated:YES];
        IOS7POP;

        return [RACSignal empty];

    }];
    self.navigationItem.rightBarButtonItem  = leftBarButton;
}

-(void)creatBaseView{

    headerView = [[iEver_MemberHomePageHeaderView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 377.5)];
    headerView.backgroundColor = WHITE;
    [self.view addSubview:headerView];

    _VuserShareArticleVC         = [[iEver_VuserShareArticleViewController alloc] init];
    _VuserShareArticleVC.superController = self;
    _VuserAnsweredQuestionVC     = [[iEver_VuserAnsweredQuestionViewController alloc] init];
    _VuserAnsweredQuestionVC.superController = self;

    label_tab1                   = [UILabel new];
    label_tab1.backgroundColor   = [UIColor clearColor];
    label_tab1.font              = TextFont;
    label_tab1.text              = @"经验分享(00000)";
    label_tab1.textAlignment     = NSTextAlignmentCenter;
    label_tab1.textColor         = [UIColor blackColor];
    [label_tab1 sizeToFit];

    label_tab2                   = [UILabel new];
    label_tab2.backgroundColor   = [UIColor clearColor];
    label_tab2.font              = TextFont;
    label_tab2.text              = @"回答问题(00000)";
    label_tab2.textAlignment     = NSTextAlignmentCenter;
    label_tab2.textColor         = [UIColor blackColor];
    [label_tab2 sizeToFit];
}

- (void)viewDidLoad {

    // Do any additional setup after loading the view.
    

    [self creatBaseView];

    self.dataSource = self;
    self.delegate = self;

    [self addBar];

    [self fetchData];
    
    _VuserNumber = 0;

    headView_Y = 0.0;

    openOrClose = YES;

    [self headViewAnimations];

    [super viewDidLoad];
}

-(void)delegateMethod{

    [self fetchData];
}

- (void)selectVuserFetchData:(int)VuserNumber{
    

    _VuserNumber = VuserNumber;

    [_VuserShareArticleVC selectVuserShareFetchData:VuserNumber];


    iEver_discover_userList_object * userObject = (iEver_discover_userList_object *)[_object._userList objectAtIndex:_VuserNumber];

    label_tab1.text              = [NSString stringWithFormat:@"经验分享(%d)",userObject.articleTotal];
    label_tab2.text              = [NSString stringWithFormat:@"回答问题(%d)",userObject.answerTotal];

}


- (void)fetchData
{

    @weakify(self);
    [GiFHUD show];

    iEver_V_userObject *content = [[iEver_V_userObject alloc] init];

    [[[content queryOrderByAnswer:nil
                             path:nil]
      deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(iEver_V_userObject *object) {

         @strongify(self);

         _object = object;

         if (object.resultCode == 1) {
             
             [self ASKVuserButton];

             [headerView setObject:object];

             [_VuserShareArticleVC setObject:object];

             [_VuserAnsweredQuestionVC setObject:object];

             iEver_discover_userList_object * userObject = (iEver_discover_userList_object *)[object._userList objectAtIndex:_VuserNumber];

             label_tab1.text              = [NSString stringWithFormat:@"经验分享(%d)",userObject.articleTotal];
             label_tab2.text              = [NSString stringWithFormat:@"回答问题(%d)",userObject.answerTotal];

              [self loadBootstrap];

         }
         else{

             iEver_LoginViewController *_loginViewController = [[iEver_LoginViewController alloc]init];
             _loginViewController.delegate                   = self;
             _loginViewController.hidesBottomBarWhenPushed   = YES;
             [self.navigationController pushViewController:_loginViewController animated:YES];

         }

         [GiFHUD  dismiss];
     }];

}

-(void)loadBootstrap {
    NSString *V_previousVersion = [[NSUserDefaults standardUserDefaults] objectForKey:V_PREVIOUSVERSION];
    if (!V_previousVersion) {
        V_previousVersion = @"0";
    }
    if ([V_CURRENTVERSION compare:V_previousVersion options:NSNumericSearch]) {
    //newVersionFound
    [[NSUserDefaults standardUserDefaults] setObject:V_CURRENTVERSION forKey:V_PREVIOUSVERSION];
    [[NSUserDefaults standardUserDefaults] synchronize];

    UIImage *main_Bootstrap = [UIImage imageNamed:@"V_Bootstrap"];
    UIImageView *image_Bootstrap = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,ScreenWidth, ScreenHeight)];
    image_Bootstrap.userInteractionEnabled = YES;
    image_Bootstrap.image = main_Bootstrap;

    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(100, ScreenHeight - 60, 120, 40);
    button.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        image_Bootstrap.hidden = YES;
        return [RACSignal empty];
    }];
    [image_Bootstrap addSubview:button];

    [[iEverAppDelegate shareAppDelegate].window addSubview:image_Bootstrap];

    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



#pragma mark - ViewPagerDataSource
- (NSUInteger)numberOfTabsForViewPager:(ViewPagerController *)viewPager {
    return 2;
}
- (UIView *)viewPager:(ViewPagerController *)viewPager viewForTabAtIndex:(NSUInteger)index {

    UILabel *label;
    if (index == 0) {

        label = label_tab1;

    }
    else{

        label = label_tab2;

    }
    return label;
}
- (UIViewController *)viewPager:(ViewPagerController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index {

    UIViewController * viewController;
    if (index == 0) {

        _VuserShareArticleVC.navigationController = self.navigationController;
        viewController = _VuserShareArticleVC;

    }else if(index == 1){

        viewController = _VuserAnsweredQuestionVC;


//        [_VuserAnsweredQuestionVC selectVuserShareFetchData:_VuserNumber];

    }

    return viewController;
}

#pragma mark - ViewPagerDelegate
- (CGFloat)viewPager:(ViewPagerController *)viewPager valueForOption:(ViewPagerOption)option withDefault:(CGFloat)value {

    switch (option) {
        case ViewPagerOptionStartFromSecondTab:
            return 0.0;
            break;
        case ViewPagerOptionCenterCurrentTab:
            return 1.0;
            break;
        case ViewPagerOptionTabLocation:
            return 0.0;
             break;

            break;
        default:
            break;
    }

    return value;
}
- (UIColor *)viewPager:(ViewPagerController *)viewPager colorForComponent:(ViewPagerComponent)component withDefault:(UIColor *)color {

    switch (component) {
        case ViewPagerIndicator:
            return ORANGECOLOR;
            break;
        case ViewPagerTabsView:
            return SEABLUE;
            break;

        default:
            break;
    }

    return color;
}


#pragma mark -
#pragma mark UIInterfaceOrientation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return UIInterfaceOrientationPortrait;
}

- (BOOL)shouldAutorotate{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{

    return UIInterfaceOrientationPortrait;
}
@end
