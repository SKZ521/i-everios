//
//  iEver_AskAswerPagesTableViewController.h
//  FaceTalk
//
//  Created by 开仲 司 on 15/5/23.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iEverBaseTableViewController.h"

typedef NS_ENUM(NSInteger, iEverPageShowType)
{
    iEverPageShowTypeDefault = 0,   // 侧边栏进入
};

@interface iEver_AskAswerPagesTableViewController : iEverBaseTableViewController

@property (assign) iEverPageShowType currentType;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil rootVC:(UIViewController *)rootVC;

@end
