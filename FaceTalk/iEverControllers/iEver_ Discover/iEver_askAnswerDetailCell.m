//
//  iEver_askAnswerDetailCell.m
//  FaceTalk
//
//  Created by kevin on 15/6/1.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_askAnswerDetailCell.h"

#import "iEver_AskAnswerDetailViewController.h"

#import "MJPhotoBrowser.h"
#import "MJPhoto.h"

/* 点赞model */
#import "iEver_likeActionObject.h"

/* 点赞撒花效果 */
#import "MCFireworksButton.h"
/* 字符串响应动作和标签化字符效果 */
#import "NSString+WPAttributedMarkup.h"
#import "WPHotspotLabel.h"
#import "WPAttributedStyleAction.h"
/* 自定义view寻找当前VC */
#import "UIView+FindViewController.h"

@interface iEver_askAnswerDetailCell () {
    
    NSMutableArray *urlArray; //相册URL
    NSMutableArray *imageViewArray;//问题图片
}

@property (nonatomic, strong) UIView        *bottomView;                  /* 整体背景 */

@property (nonatomic, strong) UIView        *centerView;                  /* 中间背景 */
@property (nonatomic, strong) UIButton      *photoButton;                 /* 问者头像按钮 */
@property (nonatomic, strong) UIImageView   *userTypeSignImageView;       /* 用户类型 */
@property (nonatomic, strong) UILabel       *nameLable;                   /* 问者昵称 */
@property (nonatomic, strong) UILabel       *answerTimeLabel;             /* 回复时间 */
@property (nonatomic, strong) UILabel       *contentLabel;                /* 问题内容 */
@property (nonatomic, strong) UIImageView   *Q_image1;                    /* 问题图片1 */
@property (nonatomic, strong) UIImageView   *Q_image2;                    /* 问题图片2 */
@property (nonatomic, strong) UIImageView   *Q_image3;                    /* 问题图片3 */
@property (nonatomic, strong) UIImageView   *Q_image4;                    /* 问题图片4 */
@property (nonatomic, strong) UIImageView   *Q_image5;                    /* 问题图片5 */
@property (nonatomic, strong) UIImageView   *Q_image6;                    /* 问题图片6 */
@property (nonatomic, strong) UIImageView   *Q_image7;                    /* 问题图片7 */
@property (nonatomic, strong) UIImageView   *Q_image8;                    /* 问题图片8 */
@property (nonatomic, strong) UIImageView   *Q_image9;                    /* 问题图片9 */

/* 点赞 回复按钮  */

@property (nonatomic, strong) iEver_answer_object *list_object;

@property (nonatomic, strong) MCFireworksButton      *likeButton;           /* 喜欢按钮 */
@property (nonatomic, strong) UILabel                *likeLabel;            /* 喜欢标签 */
@property (nonatomic, strong) UIButton               *replyButton;          /* 回复按钮 */
@property (nonatomic, strong) UILabel                *replyLabel;           /* 回复标签 */

@property (nonatomic, strong) UIImageView            *arrowImage;           /* 箭头图片 */
@property (nonatomic, strong) UIView                 *baseCommentView;      /* 用户评论灰色背景 */

/* 评论的评论第一条 */
@property (nonatomic, strong) UILabel                *nameLable_1;          /* 用户评论 */
@property (nonatomic, strong) UILabel                *timeLabel_1;          /* 发布时间 */
@property (nonatomic, strong) WPHotspotLabel         *contentLabel_1;       /* 消息内容 */
@property (nonatomic, strong) UIImageView            *lineView_1;           /* 视图横线 */

/* 评论的评论第二条 */
@property (nonatomic, strong) UILabel                *nameLable_2;          /* 用户评论 */
@property (nonatomic, strong) UILabel                *timeLabel_2;          /* 发布时间 */
@property (nonatomic, strong) WPHotspotLabel         *contentLabel_2;       /* 消息内容 */
@property (nonatomic, strong) UIImageView            *lineView_2;           /* 视图横线 */

/* 评论的评论第三条 */
@property (nonatomic, strong) UILabel                *nameLable_3;          /* 用户评论 */
@property (nonatomic, strong) UILabel                *timeLabel_3;          /* 发布时间 */
@property (nonatomic, strong) WPHotspotLabel         *contentLabel_3;       /* 消息内容 */
@property (nonatomic, strong) UIImageView            *lineView_3;           /* 视图横线 */

@property (nonatomic, strong) WPHotspotLabel         *queryAllCommLabel;    /* 查看更多评论_可点击标签 */

@property (nonatomic, strong) UIImageView            *lineView;             /* 视图横线 */


@end


@implementation iEver_askAnswerDetailCell


/* 相册浏览模式 */
- (void)tapImage:(UITapGestureRecognizer *)tap
{
    int count = urlArray.count;
    NSMutableArray *photos = [NSMutableArray arrayWithCapacity:count];
    for (int i = 0; i < count; i++) {
        // 更换高清尺寸
        NSString *url = [NSString stringWithFormat:@"%@?imageMogr2/thumbnail/640x",[urlArray objectAtIndex:i]];
        MJPhoto *photo = [[MJPhoto alloc] init];
        photo.url = [NSURL URLWithString:url]; // 图片路径
        photo.srcImageView = imageViewArray[i]; // 来源于哪个UIImageView
        [photos addObject:photo];
    }
    
    // 2.显示相册
    MJPhotoBrowser *browser = [[MJPhotoBrowser alloc] init];
    browser.currentPhotoIndex = tap.view.tag; // 弹出相册时显示的第一张图片是？
    browser.photos = photos; // 设置所有的图片
    [browser show];
}

- (void) configureCell {
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.bottomView];
        [self.contentView addSubview:self.centerView];
        
        [self.centerView addSubview:self.photoButton];
        [self.centerView addSubview:self.userTypeSignImageView];
        [self.centerView addSubview:self.nameLable];
        [self.centerView addSubview:self.answerTimeLabel];
        [self.centerView addSubview:self.contentLabel];
        [self.centerView addSubview:self.Q_image1];
        [self.centerView addSubview:self.Q_image2];
        [self.centerView addSubview:self.Q_image3];
        [self.centerView addSubview:self.Q_image4];
        [self.centerView addSubview:self.Q_image5];
        [self.centerView addSubview:self.Q_image6];
        [self.centerView addSubview:self.Q_image7];
        [self.centerView addSubview:self.Q_image8];
        [self.centerView addSubview:self.Q_image9];
        
        
        /* 点赞 回复按钮及对回复评论 */
        [self.contentView addSubview:self.likeButton];
        [self.contentView addSubview:self.likeLabel];
        [self.contentView addSubview:self.replyButton];
        [self.contentView addSubview:self.replyLabel];
        
        [self.contentView addSubview:self.arrowImage];
        [self.contentView addSubview:self.baseCommentView];
        
        [self.baseCommentView addSubview:self.nameLable_1];
        [self.baseCommentView addSubview:self.timeLabel_1];
        [self.baseCommentView addSubview:self.contentLabel_1];
        [self.baseCommentView addSubview:self.lineView_1];
        
        [self.baseCommentView addSubview:self.nameLable_2];
        [self.baseCommentView addSubview:self.timeLabel_2];
        [self.baseCommentView addSubview:self.contentLabel_2];
        [self.baseCommentView addSubview:self.lineView_2];
        
        [self.baseCommentView addSubview:self.nameLable_3];
        [self.baseCommentView addSubview:self.timeLabel_3];
        [self.baseCommentView addSubview:self.contentLabel_3];
        [self.baseCommentView addSubview:self.lineView_3];
        
        [self.baseCommentView addSubview:self.queryAllCommLabel];
        
        [self.contentView addSubview:self.lineView];
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)awakeFromNib
{
    // Initialization code
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    /* 视图横线 */
    CGRect r = self.contentView.bounds;
    CGFloat x = 15.0;
    CGFloat y = r.size.height - 1;
    CGFloat height = 1.0;
    CGFloat width = 290.0;
    self.lineView.frame = CGRectMake(x, y, width, height);
    
    
}
- (void)setObject:(iEver_answer_object *)object
{
    
    self.list_object = object;
    
    self.Q_image1.hidden = YES;
    self.Q_image2.hidden = YES;
    self.Q_image3.hidden = YES;
    self.Q_image4.hidden = YES;
    self.Q_image5.hidden = YES;
    self.Q_image6.hidden = YES;
    self.Q_image7.hidden = YES;
    self.Q_image8.hidden = YES;
    self.Q_image9.hidden = YES;
    
    urlArray = [[NSMutableArray alloc] init];
    imageViewArray = [[NSMutableArray alloc] init];
    
    /* Initialization size */
    CGFloat x = 0.0;
    CGFloat y = 0.0;
    CGFloat width = 0.0;
    CGFloat height = 0.0;
    
    CGFloat answerView_SizeHeight = 0.0;
    CGFloat content1_SizeHeight = 0.0;
    CGFloat content2_SizeHeight = 0.0;
    CGFloat content3_SizeHeight = 0.0;
    CGFloat tryDetailArrow_SizeHeight = 0.0;
    
    /* 中间view --答者头像 */
    x = 15.0;
    y = 15.0;
    width =  30.0;
    height = 30.0;
    [self.photoButton setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/60x",object.aHeadImg]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
    self.photoButton.frame = CGRectMake(x, y, width, height);
    
    /* 中间view --答者昵称 */
    x = 55.0;
    y = 25.0;
    width = 150.0;
    height = 15.0;
    self.nameLable.frame = CGRectMake(x, y, width, height);
    self.nameLable.text = object.aNickName;
    
    /* 中间view --回答时间 */
    x = 185.0;
    y = 25.0;
    width = 120.0;
    height = 15.0;
    self.answerTimeLabel.frame = CGRectMake(x, y, width, height);
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM月dd日 hh:mm"];
    NSString *dateLoca = [NSString stringWithFormat:@"%lld",object.createTime/1000];
    NSTimeInterval time =[dateLoca doubleValue];
    NSDate *detaildate = [NSDate dateWithTimeIntervalSince1970:time];
    self.answerTimeLabel.text = [formatter stringFromDate:detaildate];
    
    /* 中间view --回答内容 */
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
    CGSize contentSize =[object.content boundingRectWithSize:CGSizeMake(275.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
    x = 15.0;
    y = 60.0;
    width = 275.0;
    height = contentSize.height;
    self.contentLabel.frame = CGRectMake(x, y, width, height);
    self.contentLabel.text = object.content;
    
    /* 答者包含图片 */
    
    int totalLoc = 3; //总列数
    int totalRow = (object._answerPicList.count - 1) / totalLoc + 1; //总行数
    CGFloat imageViewW = 80;
    CGFloat imageViewH = 80;
    CGFloat margin = 5;
    
    if ([object._answerPicList count] > 0) {
        
        NSArray *imageView_array =@[self.Q_image1,
                                    self.Q_image2,
                                    self.Q_image3,
                                    self.Q_image4,
                                    self.Q_image5,
                                    self.Q_image6,
                                    self.Q_image7,
                                    self.Q_image8,
                                    self.Q_image9];
        
        for (int i = 0; i < [object._answerPicList count]; i++) {
            
            iEver_answerPic_object *_answerPic_object = [object._answerPicList objectAtIndex:i];
            
            [urlArray addObject:_answerPic_object.imgUrl];
            
            int row = i / totalLoc;
            int loc = i % totalLoc;
            CGFloat imageViewX = 15 + ( margin + imageViewW ) * loc;
            CGFloat imageViewY = self.contentLabel.frame.origin.y + self.contentLabel.frame.size.height + 10 + ( margin + imageViewH ) * row;
            
            UIImageView *imageview = imageView_array[i];
            [imageViewArray addObject:imageview];
            imageview.hidden = NO;
            [imageview setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/160x",_answerPic_object.imgUrl]] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
            imageview.frame = CGRectMake(imageViewX, imageViewY, imageViewW, imageViewH);
            imageview.tag = i;
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapImage:)];
            tapGesture.numberOfTapsRequired = 1;
            imageview.userInteractionEnabled = YES;
            [imageview addGestureRecognizer:tapGesture];
        }
    }
    
    /* 答案 view */
    x = 0.0;
    y = 0.0;
    width =  ScreenWidth;
    height = (object._answerPicList.count == 0) ? self.contentLabel.frame.origin.y + self.contentLabel.frame.size.height + 10.0 + 10.0 : (self.contentLabel.frame.origin.y + self.contentLabel.frame.size.height + 10.0) + ( margin + imageViewH ) * totalRow + 10.0 + 10.0;
    answerView_SizeHeight = height;
    self.centerView.frame = CGRectMake(x, y, width, height);
    
    
    /*  点赞按钮 */
    UIImage *detail_commentLike       = [UIImage imageNamed:@"detail_commentLike"];
    UIImage *detail_commentLike_click = [UIImage imageNamed:@"detail_commentLike_click"];
    x = 200.0 - (55.0 - detail_commentLike.size.width)/2;
    y =  answerView_SizeHeight + 5 - (22 - detail_commentLike.size.height)/2;
    width = 55.0;
    height = 22.0;
    
    if (object._selected) {
        [self.likeButton setImage:detail_commentLike_click forState:UIControlStateNormal];
    }else{
        [self.likeButton setImage:detail_commentLike forState:UIControlStateNormal];
    }
    self.likeButton.frame = CGRectMake(x, y, width, height);
    
    /* 喜欢数量 */
    x = 215.0;
    y = answerView_SizeHeight + 3;
    width = 50;
    height = 15;
    self.likeLabel.text = [NSString stringWithFormat:@"赞(%d)",object.praiseTotal];
    self.likeLabel.frame = CGRectMake(x, y, width, height);
    
    /*  回复评论按钮 */
    UIImage *detail_commentReplay = [UIImage imageNamed:@"detail_commentReplay"];
    x = 260.0 - (55.0 - detail_commentReplay.size.width)/2;
    y = answerView_SizeHeight + 5 - (22 - detail_commentReplay.size.height)/2;
    width = 55.0;
    height = 22.0;
    [self.replyButton setImage:detail_commentReplay forState:UIControlStateNormal];
    [self.replyButton setImage:detail_commentReplay forState:UIControlStateHighlighted];
    self.replyButton.frame = CGRectMake(x, y, width, height);
    
    
    /* 回复标签 */
    x = 275.0;
    y = answerView_SizeHeight + 3;
    width = 40;
    height = 15;
    self.replyLabel.text = @"回复";
    self.replyLabel.frame = CGRectMake(x, y, width, height);
    
    
    /* 评论的评论最多展示三条 更多显示查看更多*/
    /* 评论的评论没有 所有评论相关的子view隐藏 */
    if (object.commentTotal == 0) {
        
        self.arrowImage.hidden = YES;
        self.baseCommentView.hidden = YES;
        
        self.nameLable_1.hidden = YES;
        self.timeLabel_1.hidden = YES;
        self.contentLabel_1.hidden = YES;
        self.lineView_1.hidden = YES;
        
        self.nameLable_2.hidden = YES;
        self.timeLabel_2.hidden = YES;
        self.contentLabel_2.hidden = YES;
        self.lineView_2.hidden = YES;
        
        self.nameLable_3.hidden = YES;
        self.timeLabel_3.hidden = YES;
        self.contentLabel_3.hidden = YES;
        self.lineView_3.hidden = YES;
        
        self.queryAllCommLabel.hidden = YES;
        
        /* cell 背景块 */
        x = 0.0;
        y = 0.0;
        height = answerView_SizeHeight + 25.0; //如果没有回复 因为有了点赞和回复按钮，需要添加25像素的间距
        width = 320.0;
        self.bottomView.frame = CGRectMake(x, y, width, height);
        
    }
    
    /* 评论的评论包含一条 */
    else if (object.commentTotal == 1) {
        
        self.arrowImage.hidden = NO;
        self.baseCommentView.hidden = NO;
        
        self.nameLable_1.hidden = NO;
        self.timeLabel_1.hidden = NO;
        self.contentLabel_1.hidden = NO;
        self.lineView_1.hidden = YES;
        
        {
            
            /* 评论的评论小箭头 */
            UIImage *tryDetailArrow = [UIImage imageNamed:@"tryDetailArrow"];
            x = 35.0;
            y = y + height; //小箭头参照的 y & height 回复标签
            width = tryDetailArrow.size.width;
            height = tryDetailArrow.size.height;
            tryDetailArrow_SizeHeight = tryDetailArrow.size.height;
            self.arrowImage.image = tryDetailArrow;
            self.arrowImage.frame = CGRectMake(x, y, width, height);
            
            /* 获取第一条评论数据 */
            iEver_answerComment_object *_answerComment_object_1 = [object._answerCommentList objectAtIndex:0];
            
            /* 评论的评论第一条 用户昵称区域 */
            x = 10.0;
            y = 10.0;
            width = 180.0;
            height = 15.0;
            NSDictionary* name1_Style = @{@"gray": [UIColor lightGrayColor],
                                          @"blue": [UIColor blueColor]};
            
            NSString *name1_strStyle = [NSString stringWithFormat:@"<gray>%@ 回复</gray><blue>%@</blue>",_answerComment_object_1.userNickName,_answerComment_object_1.atUserNickName];
            self.nameLable_1.attributedText = [name1_strStyle attributedStringWithStyleBook:name1_Style];
            self.nameLable_1.frame = CGRectMake(x, y, width, height);
            
            /* 评论的评论第一条 时间 */
            x = 205.0;
            y = 10.0;
            width = 75.0;
            height = 15.0;
            NSString *replyCommentTimeStr_1 = [iEver_Global commentIntervalSinceNow:_answerComment_object_1.createTime/1000];
            self.timeLabel_1.text = replyCommentTimeStr_1;
            self.timeLabel_1.frame = CGRectMake(x, y, width, height);
            
            /* 评论的评论第一条 回复评论 */
            NSDictionary* contentLabel_StyleDic = @{@"insertComment":[WPAttributedStyleAction styledActionWithAction:^{
                
                
                iEver_AskAnswerDetailViewController *superVC = (iEver_AskAnswerDetailViewController *)[self firstAvailableUIViewController];
                
                superVC.comment_qId = object.qId_id;
                superVC.comment_aId = object.answer_id;
                superVC.comment_atUserId = _answerComment_object_1.userId;
                superVC.toAnswerCommentName = _answerComment_object_1.userNickName;
                [superVC insertCommentToAnswerComment];
                
                NSLog(@"insertComment");
                
            }]};
            NSDictionary * contentdic1 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
            CGSize contentSize1 =[_answerComment_object_1.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic1 context:nil].size;
            x = 10.0;
            y = y + height + 10.0;
            width = ScreenWidth - 15.0 - 15.0 - 20.0;
            height = contentSize1.height;
            content1_SizeHeight = contentSize1.height;
            NSString *contentLabel_strStyle = [NSString stringWithFormat:@"<insertComment>%@</insertComment>",_answerComment_object_1.commentContent];
            self.contentLabel_1.attributedText = [contentLabel_strStyle attributedStringWithStyleBook:contentLabel_StyleDic];
            self.contentLabel_1.frame = CGRectMake(x, y, width, height);
            
        }
        
        
        /* 评论的评论灰色块 */
        x = 15.0;
        y = self.arrowImage.frame.origin.y + tryDetailArrow_SizeHeight;
        width = ScreenWidth - 15.0 - 15.0;
        height = self.contentLabel_1.frame.origin.y + self.contentLabel_1.frame.size.height + 10.0;
        self.baseCommentView.frame = CGRectMake(x, y, width, height);
        
        
        
        self.nameLable_2.hidden = YES;
        self.timeLabel_2.hidden = YES;
        self.contentLabel_2.hidden = YES;
        self.lineView_2.hidden = YES;
        
        self.nameLable_3.hidden = YES;
        self.timeLabel_3.hidden = YES;
        self.contentLabel_3.hidden = YES;
        self.lineView_3.hidden = YES;
        
        self.queryAllCommLabel.hidden = YES;
        
        /* cell 背景块 */
        x = 0.0;
        y = 0.0;
        height = answerView_SizeHeight + 25.0 + 35.0 + content1_SizeHeight + 10.0 + 10.0;
        width = 320.0;
        self.bottomView.frame = CGRectMake(x, y, width, height);
        
        
    }
    
    /* 评论的评论包含二条 */
    else if (object.commentTotal == 2) {
        
        self.arrowImage.hidden = NO;
        self.baseCommentView.hidden = NO;
        
        self.nameLable_1.hidden = NO;
        self.timeLabel_1.hidden = NO;
        self.contentLabel_1.hidden = NO;
        self.lineView_1.hidden = NO;
        
        {
            
            /* 评论的评论小箭头 */
            UIImage *tryDetailArrow = [UIImage imageNamed:@"tryDetailArrow"];
            x = 35.0;
            y = y + height ;
            width = tryDetailArrow.size.width;
            height = tryDetailArrow.size.height;
            tryDetailArrow_SizeHeight = tryDetailArrow.size.height;
            self.arrowImage.image = tryDetailArrow;
            self.arrowImage.frame = CGRectMake(x, y, width, height);
            
            /* 第一条评论数据 */
            iEver_answerComment_object *_answerComment_object_1 = [object._answerCommentList objectAtIndex:0];
            
            /* 评论的评论第一条 用户昵称区域 */
            x = 10.0;
            y = 10.0;
            width = 180.0;
            height = 15.0;
            NSDictionary* name1_Style = @{@"gray": [UIColor lightGrayColor],
                                          @"blue": [UIColor blueColor]};
            
            NSString *name1_strStyle = [NSString stringWithFormat:@"<gray>%@ 回复</gray><blue>%@</blue>",_answerComment_object_1.userNickName,_answerComment_object_1.atUserNickName];
            self.nameLable_1.attributedText = [name1_strStyle attributedStringWithStyleBook:name1_Style];
            self.nameLable_1.frame = CGRectMake(x, y, width, height);
            
            /* 评论的评论第一条 时间 */
            x = 205.0;
            y = 10.0;
            width = 75.0;
            height = 15.0;
            NSString *replyCommentTimeStr_1 = [iEver_Global commentIntervalSinceNow:_answerComment_object_1.createTime/1000];
            self.timeLabel_1.text = replyCommentTimeStr_1;
            self.timeLabel_1.frame = CGRectMake(x, y, width, height);
            
            /* 评论的评论第一条 回复评论 */
            NSDictionary* contentLabel_StyleDic = @{@"insertComment":[WPAttributedStyleAction styledActionWithAction:^{
                
                iEver_AskAnswerDetailViewController *superVC = (iEver_AskAnswerDetailViewController *)[self firstAvailableUIViewController];
                
                superVC.comment_qId = object.qId_id;
                superVC.comment_aId = object.answer_id;
                superVC.comment_atUserId = _answerComment_object_1.userId;
                superVC.toAnswerCommentName = _answerComment_object_1.userNickName;
                [superVC insertCommentToAnswerComment];

                NSLog(@"insertComment");
                
            }]};
            NSDictionary * contentdic1 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
            CGSize contentSize1 =[_answerComment_object_1.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic1 context:nil].size;
            x = 10.0;
            y = y + height + 10.0;
            width = ScreenWidth - 15.0 - 15.0 - 20.0;
            height = contentSize1.height;
            content1_SizeHeight = contentSize1.height;
            NSString *contentLabel_strStyle = [NSString stringWithFormat:@"<insertComment>%@</insertComment>",_answerComment_object_1.commentContent];
            self.contentLabel_1.attributedText = [contentLabel_strStyle attributedStringWithStyleBook:contentLabel_StyleDic];
            self.contentLabel_1.frame = CGRectMake(x, y, width, height);
            
            
            /* 第一条评论的评论下的分割线 当有第二条评论的评论才会出现 */
            x = 0.0;
            y = self.contentLabel_1.frame.origin.y + self.contentLabel_1.frame.size.height + 9.0;
            width = ScreenWidth - 15.0 - 15.0;
            height = 1.0;
            self.lineView_1.frame = CGRectMake(x, y, width, height);
            
            
        }
        
        
        self.nameLable_2.hidden = NO;
        self.timeLabel_2.hidden = NO;
        self.contentLabel_2.hidden = NO;
        self.lineView_2.hidden = YES;
        
        {
            /* 第二条评论数据 */
            iEver_answerComment_object *_answerComment_object_2 = [object._answerCommentList objectAtIndex:1];
            
            /* 评论的评论第二条 用户昵称区域 */
            x = 10.0;
            y = self.lineView_1.frame.origin.y + 1.0 + 10.0;
            width = 180.0;
            height = 15.0;
            NSDictionary* name2_Style = @{@"gray": [UIColor lightGrayColor],
                                          @"blue": [UIColor blueColor]};
            
            NSString *name2_strStyle = [NSString stringWithFormat:@"<gray>%@ 回复</gray><blue>%@</blue>",_answerComment_object_2.userNickName,_answerComment_object_2.atUserNickName];
            self.nameLable_2.attributedText = [name2_strStyle attributedStringWithStyleBook:name2_Style];
            self.nameLable_2.frame = CGRectMake(x, y, width, height);
            
            
            /* 评论的评论第二条 时间 */
            x = 205.0;
            y = self.lineView_1.frame.origin.y + 1.0 + 10.0;
            width = 75.0;
            height = 15.0;
            NSString *replyCommentTimeStr_2 = [iEver_Global commentIntervalSinceNow:_answerComment_object_2.createTime/1000];
            self.timeLabel_2.text = replyCommentTimeStr_2;
            self.timeLabel_2.frame = CGRectMake(x, y, width, height);
            
            
            /* 评论的评论第二条 评论评论 */
            NSDictionary* contentLabel_StyleDic = @{@"insertComment":[WPAttributedStyleAction styledActionWithAction:^{
                
                
                iEver_AskAnswerDetailViewController *superVC = (iEver_AskAnswerDetailViewController *)[self firstAvailableUIViewController];
                
                superVC.comment_qId = object.qId_id;
                superVC.comment_aId = object.answer_id;
                superVC.comment_atUserId = _answerComment_object_2.userId;
                superVC.toAnswerCommentName = _answerComment_object_2.userNickName;
                [superVC insertCommentToAnswerComment];
                
                NSLog(@"insertComment");
                
            }]};
            NSDictionary * contentdic2 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
            CGSize contentSize2 =[_answerComment_object_2.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic2 context:nil].size;
            x = 10.0;
            y = y + height + 10.0;
            width = ScreenWidth - 15.0 - 15.0 - 20.0;
            height = contentSize2.height;
            content2_SizeHeight = contentSize2.height;
            self.contentLabel_2.frame = CGRectMake(x, y, width, height);
            NSString *contentLabel_strStyle = [NSString stringWithFormat:@"<insertComment>%@</insertComment>",_answerComment_object_2.commentContent];
            self.contentLabel_2.attributedText = [contentLabel_strStyle attributedStringWithStyleBook:contentLabel_StyleDic];
            
        }
        
        
        
        self.nameLable_3.hidden = YES;
        self.timeLabel_3.hidden = YES;
        self.contentLabel_3.hidden = YES;
        self.lineView_3.hidden = YES;
        
        self.queryAllCommLabel.hidden = YES;
        
        /* 评论的评论 背景灰色块 */
        x = 15.0;
        y = self.arrowImage.frame.origin.y + tryDetailArrow_SizeHeight;
        width = ScreenWidth - 15.0 - 15.0;
        height = self.contentLabel_2.frame.origin.y + self.contentLabel_2.frame.size.height + 10.0;
        self.baseCommentView.frame = CGRectMake(x, y, width, height);
        
        
        /* cell 背景块 */
        x = 0.0;
        y = 0.0;
        height = answerView_SizeHeight + 25.0 + 35.0 + content1_SizeHeight  + 35.0 + content2_SizeHeight + 10.0 + 10.0 + 10.0;
        width = 320.0;
        self.bottomView.frame = CGRectMake(x, y, width, height);
        
    }
    
    /* 评论的评论包含三条 */
    else if (object.commentTotal == 3) {
        
        self.arrowImage.hidden = NO;
        self.baseCommentView.hidden = NO;
        
        self.nameLable_1.hidden = NO;
        self.timeLabel_1.hidden = NO;
        self.contentLabel_1.hidden = NO;
        self.lineView_1.hidden = NO;
        
        {
            
            /* 评论的评论小箭头 */
            UIImage *tryDetailArrow = [UIImage imageNamed:@"tryDetailArrow"];
            x = 35.0;
            y = y + height;
            width = tryDetailArrow.size.width;
            height = tryDetailArrow.size.height;
            tryDetailArrow_SizeHeight = tryDetailArrow.size.height;
            self.arrowImage.image = tryDetailArrow;
            self.arrowImage.frame = CGRectMake(x, y, width, height);
            
            /* 第一条评论数据 */
            iEver_answerComment_object *_answerComment_object_1 = [object._answerCommentList objectAtIndex:0];
            
            /* 评论的评论第一条 用户昵称区域 */
            x = 10.0;
            y = 10.0;
            width = 180.0;
            height = 15.0;
            NSDictionary* name1_Style = @{@"gray": [UIColor lightGrayColor],
                                          @"blue": [UIColor blueColor]};
            
            NSString *name1_strStyle = [NSString stringWithFormat:@"<gray>%@ 回复</gray><blue>%@</blue>",_answerComment_object_1.userNickName,_answerComment_object_1.atUserNickName];
            self.nameLable_1.attributedText = [name1_strStyle attributedStringWithStyleBook:name1_Style];
            self.nameLable_1.frame = CGRectMake(x, y, width, height);
            
            /* 评论的评论第一条 时间 */
            x = 205.0;
            y = 10.0;
            width = 75.0;
            height = 15.0;
            NSString *replyCommentTimeStr_1 = [iEver_Global commentIntervalSinceNow:_answerComment_object_1.createTime/1000];
            self.timeLabel_1.text = replyCommentTimeStr_1;
            self.timeLabel_1.frame = CGRectMake(x, y, width, height);
            
            
            /* 评论的评论第一条 回复评论 */
            NSDictionary* contentLabel_StyleDic = @{@"insertComment":[WPAttributedStyleAction styledActionWithAction:^{
                
                iEver_AskAnswerDetailViewController *superVC = (iEver_AskAnswerDetailViewController *)[self firstAvailableUIViewController];
                
                superVC.comment_qId = object.qId_id;
                superVC.comment_aId = object.answer_id;
                superVC.comment_atUserId = _answerComment_object_1.userId;
                superVC.toAnswerCommentName = _answerComment_object_1.userNickName;
                [superVC insertCommentToAnswerComment];

                NSLog(@"insertComment");
                
            }]};
            NSDictionary * contentdic1 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
            CGSize contentSize1 =[_answerComment_object_1.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic1 context:nil].size;
            x = 10.0;
            y = y + height + 10.0;
            width = ScreenWidth - 15.0 - 15.0 - 20.0;
            height = contentSize1.height;
            content1_SizeHeight = contentSize1.height;
            NSString *contentLabel_strStyle = [NSString stringWithFormat:@"<insertComment>%@</insertComment>",_answerComment_object_1.commentContent];
            self.contentLabel_1.attributedText = [contentLabel_strStyle attributedStringWithStyleBook:contentLabel_StyleDic];
            self.contentLabel_1.frame = CGRectMake(x, y, width, height);
            
            
            /* 第一条评论的评论下的分割线 当有第二条评论的评论才会出现 */
            x = 0.0;
            y = self.contentLabel_1.frame.origin.y + self.contentLabel_1.frame.size.height + 9.0;
            width = ScreenWidth - 15.0 - 15.0;
            height = 1.0;
            self.lineView_1.frame = CGRectMake(x, y, width, height);
            
            
        }
        
        
        self.nameLable_2.hidden = NO;
        self.timeLabel_2.hidden = NO;
        self.contentLabel_2.hidden = NO;
        self.lineView_2.hidden = NO;
        
        {
            
            /* 第二条评论数据 */
            iEver_answerComment_object *_answerComment_object_2 = [object._answerCommentList objectAtIndex:1];
            
            /* 评论的评论第二条 用户昵称区域 */
            x = 10.0;
            y = self.lineView_1.frame.origin.y + 1.0 + 10.0;
            width = 180.0;
            height = 15.0;
            NSDictionary* name2_Style = @{@"gray": [UIColor lightGrayColor],
                                          @"blue": [UIColor blueColor]};
            
            NSString *name2_strStyle = [NSString stringWithFormat:@"<gray>%@ 回复</gray><blue>%@</blue>",_answerComment_object_2.userNickName,_answerComment_object_2.atUserNickName];
            self.nameLable_2.attributedText = [name2_strStyle attributedStringWithStyleBook:name2_Style];
            self.nameLable_2.frame = CGRectMake(x, y, width, height);
            
            
            /* 评论的评论第二条 时间 */
            x = 205.0;
            y = self.lineView_1.frame.origin.y + 1.0 + 10.0;
            width = 75.0;
            height = 15.0;
            NSString *replyCommentTimeStr_2 = [iEver_Global commentIntervalSinceNow:_answerComment_object_2.createTime/1000];
            self.timeLabel_2.text = replyCommentTimeStr_2;
            self.timeLabel_2.frame = CGRectMake(x, y, width, height);
            
            
            /* 评论的评论第二条 评论评论 */
            NSDictionary* contentLabel_StyleDic = @{@"insertComment":[WPAttributedStyleAction styledActionWithAction:^{
                
                iEver_AskAnswerDetailViewController *superVC = (iEver_AskAnswerDetailViewController *)[self firstAvailableUIViewController];
                
                superVC.comment_qId = object.qId_id;
                superVC.comment_aId = object.answer_id;
                superVC.comment_atUserId = _answerComment_object_2.userId;
                superVC.toAnswerCommentName = _answerComment_object_2.userNickName;
                [superVC insertCommentToAnswerComment];
                NSLog(@"insertComment");
                
            }]};
            NSDictionary * contentdic2 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
            CGSize contentSize2 =[_answerComment_object_2.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic2 context:nil].size;
            x = 10.0;
            y = y + height + 10.0;
            width = ScreenWidth - 15.0 - 15.0 - 20.0;
            height = contentSize2.height;
            content2_SizeHeight = contentSize2.height;
            self.contentLabel_2.frame = CGRectMake(x, y, width, height);
            NSString *contentLabel_strStyle = [NSString stringWithFormat:@"<insertComment>%@</insertComment>",_answerComment_object_2.commentContent];
            self.contentLabel_2.attributedText = [contentLabel_strStyle attributedStringWithStyleBook:contentLabel_StyleDic];
            
            /* 第二条评论的评论下的分割线 当有第三条评论的评论才会出现 */
            x = 0.0;
            y = self.contentLabel_2.frame.origin.y + self.contentLabel_2.frame.size.height + 9.0;
            width = ScreenWidth - 15.0 - 15.0;
            height = 1.0;
            self.lineView_2.frame = CGRectMake(x, y, width, height);
            
        }
        
        
        
        self.nameLable_3.hidden = NO;
        self.timeLabel_3.hidden = NO;
        self.contentLabel_3.hidden = NO;
        self.lineView_3.hidden = NO;
        
        {
            /* 第二条评论数据 */
            iEver_answerComment_object *_answerComment_object_3 = [object._answerCommentList objectAtIndex:2];
            
            /* 评论的评论第三条 用户昵称区域 */
            x = 10.0;
            y = self.lineView_2.frame.origin.y + 1.0 + 10.0;
            width = 180.0;
            height = 15.0;
            NSDictionary* name3_Style = @{@"gray": [UIColor lightGrayColor],
                                          @"blue": [UIColor blueColor]};
            
            NSString *name3_strStyle = [NSString stringWithFormat:@"<gray>%@ 回复</gray><blue>%@</blue>",_answerComment_object_3.userNickName,_answerComment_object_3.atUserNickName];
            self.nameLable_3.attributedText = [name3_strStyle attributedStringWithStyleBook:name3_Style];
            self.nameLable_3.frame = CGRectMake(x, y, width, height);
            
            
            /* 评论的评论第三条 时间 */
            x = 205.0;
            y = self.lineView_2.frame.origin.y + 1.0 + 10.0;
            width = 75.0;
            height = 15.0;
            NSString *replyCommentTimeStr_3 = [iEver_Global commentIntervalSinceNow:_answerComment_object_3.createTime/1000];
            self.timeLabel_3.text = replyCommentTimeStr_3;
            self.timeLabel_3.frame = CGRectMake(x, y, width, height);
            
            /* 评论的评论第一条 回复评论 */
            NSDictionary* contentLabel_StyleDic = @{@"insertComment":[WPAttributedStyleAction styledActionWithAction:^{
                
                iEver_AskAnswerDetailViewController *superVC = (iEver_AskAnswerDetailViewController *)[self firstAvailableUIViewController];
                
                superVC.comment_qId = object.qId_id;
                superVC.comment_aId = object.answer_id;
                superVC.comment_atUserId = _answerComment_object_3.userId;
                superVC.toAnswerCommentName = _answerComment_object_3.userNickName;
                [superVC insertCommentToAnswerComment];
                NSLog(@"insertComment");
                
            }]};
            NSDictionary * contentdic3 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
            CGSize contentSize3 =[_answerComment_object_3.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic3 context:nil].size;
            x = 10.0;
            y = y + height + 10.0;
            width = ScreenWidth - 15.0 - 15.0 - 20.0;
            height = contentSize3.height;
            content3_SizeHeight = contentSize3.height;
            self.contentLabel_3.frame = CGRectMake(x, y, width, height);
            NSString *contentLabel_strStyle = [NSString stringWithFormat:@"<insertComment>%@</insertComment>",_answerComment_object_3.commentContent];
            self.contentLabel_3.attributedText = [contentLabel_strStyle attributedStringWithStyleBook:contentLabel_StyleDic];
            
        }
        
        
        self.queryAllCommLabel.hidden = YES;
        
        /* 评论的评论 背景灰色块 */
        x = 15.0;
        y = self.arrowImage.frame.origin.y + tryDetailArrow_SizeHeight;
        width = ScreenWidth - 15.0 - 15.0;
        height = self.contentLabel_3.frame.origin.y + self.contentLabel_3.frame.size.height + 10.0;
        self.baseCommentView.frame = CGRectMake(x, y, width, height);
        
        
        /* cell 背景块 */
        x = 0.0;
        y = 0.0;
        height = answerView_SizeHeight + 25.0 + 35.0 + content1_SizeHeight  + 35.0 + content2_SizeHeight + 35.0 + content3_SizeHeight + 10.0 + 10.0 + 10.0 + 10.0 ;
        width = 320.0;
        self.bottomView.frame = CGRectMake(x, y, width, height);
        
        
    }
    
    /* 评论的评论超过三条 */
    else if (object.commentTotal > 3){
        
        self.arrowImage.hidden = NO;
        self.baseCommentView.hidden = NO;
        
        self.nameLable_1.hidden = NO;
        self.timeLabel_1.hidden = NO;
        self.contentLabel_1.hidden = NO;
        self.lineView_1.hidden = NO;
        
        {
            
            /* 评论的评论小箭头 */
            UIImage *tryDetailArrow = [UIImage imageNamed:@"tryDetailArrow"];
            x = 35.0;
            y = y + height;
            width = tryDetailArrow.size.width;
            height = tryDetailArrow.size.height;
            tryDetailArrow_SizeHeight = tryDetailArrow.size.height;
            self.arrowImage.image = tryDetailArrow;
            self.arrowImage.frame = CGRectMake(x, y, width, height);
            
            /* 第一条评论数据 */
            iEver_answerComment_object *_answerComment_object_1 = [object._answerCommentList objectAtIndex:0];
            
            /* 评论的评论第一条 用户昵称区域 */
            x = 10.0;
            y = 10.0;
            width = 180.0;
            height = 15.0;
            NSDictionary* name1_Style = @{@"gray": [UIColor lightGrayColor],
                                          @"blue": [UIColor blueColor]};
            
            NSString *name1_strStyle = [NSString stringWithFormat:@"<gray>%@ 回复</gray><blue>%@</blue>",_answerComment_object_1.userNickName,_answerComment_object_1.atUserNickName];
            self.nameLable_1.attributedText = [name1_strStyle attributedStringWithStyleBook:name1_Style];
            self.nameLable_1.frame = CGRectMake(x, y, width, height);
            
            /* 评论的评论第一条 时间 */
            x = 205.0;
            y = 10.0;
            width = 75.0;
            height = 15.0;
            NSString *replyCommentTimeStr_1 = [iEver_Global commentIntervalSinceNow:_answerComment_object_1.createTime/1000];
            self.timeLabel_1.text = replyCommentTimeStr_1;
            self.timeLabel_1.frame = CGRectMake(x, y, width, height);
            
            
            /* 评论的评论第一条 回复评论 */
            NSDictionary* contentLabel_StyleDic = @{@"insertComment":[WPAttributedStyleAction styledActionWithAction:^{
                
                iEver_AskAnswerDetailViewController *superVC = (iEver_AskAnswerDetailViewController *)[self firstAvailableUIViewController];
                
                superVC.comment_qId = object.qId_id;
                superVC.comment_aId = object.answer_id;
                superVC.comment_atUserId = _answerComment_object_1.userId;
                superVC.toAnswerCommentName = _answerComment_object_1.userNickName;
                [superVC insertCommentToAnswerComment];
                
                NSLog(@"insertComment");
                
            }]};
            NSDictionary * contentdic1 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
            CGSize contentSize1 =[_answerComment_object_1.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic1 context:nil].size;
            x = 10.0;
            y = y + height + 10.0;
            width = ScreenWidth - 15.0 - 15.0 - 20.0;
            height = contentSize1.height;
            content1_SizeHeight = contentSize1.height;
            NSString *contentLabel_strStyle = [NSString stringWithFormat:@"<insertComment>%@</insertComment>",_answerComment_object_1.commentContent];
            self.contentLabel_1.attributedText = [contentLabel_strStyle attributedStringWithStyleBook:contentLabel_StyleDic];
            self.contentLabel_1.frame = CGRectMake(x, y, width, height);
            
            
            /* 第一条评论的评论下的分割线 当有第二条评论的评论才会出现 */
            x = 0.0;
            y = self.contentLabel_1.frame.origin.y + self.contentLabel_1.frame.size.height + 9.0;
            width = ScreenWidth - 15.0 - 15.0;
            height = 1.0;
            self.lineView_1.frame = CGRectMake(x, y, width, height);
            
            
        }
        
        
        self.nameLable_2.hidden = NO;
        self.timeLabel_2.hidden = NO;
        self.contentLabel_2.hidden = NO;
        self.lineView_2.hidden = NO;
        
        {
            
            /* 第二条评论数据 */
            iEver_answerComment_object *_answerComment_object_2 = [object._answerCommentList objectAtIndex:1];
            
            /* 评论的评论第二条 用户昵称区域 */
            x = 10.0;
            y = self.lineView_1.frame.origin.y + 1.0 + 10.0;
            width = 180.0;
            height = 15.0;
            NSDictionary* name2_Style = @{@"gray": [UIColor lightGrayColor],
                                          @"blue": [UIColor blueColor]};
            
            NSString *name2_strStyle = [NSString stringWithFormat:@"<gray>%@ 回复</gray><blue>%@</blue>",_answerComment_object_2.userNickName,_answerComment_object_2.atUserNickName];
            self.nameLable_2.attributedText = [name2_strStyle attributedStringWithStyleBook:name2_Style];
            self.nameLable_2.frame = CGRectMake(x, y, width, height);
            
            
            /* 评论的评论第二条 时间 */
            x = 205.0;
            y = self.lineView_1.frame.origin.y + 1.0 + 10.0;
            width = 75.0;
            height = 15.0;
            NSString *replyCommentTimeStr_2 = [iEver_Global commentIntervalSinceNow:_answerComment_object_2.createTime/1000];
            self.timeLabel_2.text = replyCommentTimeStr_2;
            self.timeLabel_2.frame = CGRectMake(x, y, width, height);
            
            
            /* 评论的评论第二条 评论评论 */
            NSDictionary* contentLabel_StyleDic = @{@"insertComment":[WPAttributedStyleAction styledActionWithAction:^{
                
                iEver_AskAnswerDetailViewController *superVC = (iEver_AskAnswerDetailViewController *)[self firstAvailableUIViewController];
                
                superVC.comment_qId = object.qId_id;
                superVC.comment_aId = object.answer_id;
                superVC.comment_atUserId = _answerComment_object_2.userId;
                superVC.toAnswerCommentName = _answerComment_object_2.userNickName;
                [superVC insertCommentToAnswerComment];
                NSLog(@"insertComment");
                
            }]};
            NSDictionary * contentdic2 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
            CGSize contentSize2 =[_answerComment_object_2.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic2 context:nil].size;
            x = 10.0;
            y = y + height + 10.0;
            width = ScreenWidth - 15.0 - 15.0 - 20.0;
            height = contentSize2.height;
            content2_SizeHeight = contentSize2.height;
            self.contentLabel_2.frame = CGRectMake(x, y, width, height);
            NSString *contentLabel_strStyle = [NSString stringWithFormat:@"<insertComment>%@</insertComment>",_answerComment_object_2.commentContent];
            self.contentLabel_2.attributedText = [contentLabel_strStyle attributedStringWithStyleBook:contentLabel_StyleDic];
            
            /* 第二条评论的评论下的分割线 当有第三条评论的评论才会出现 */
            x = 0.0;
            y = self.contentLabel_2.frame.origin.y + self.contentLabel_2.frame.size.height + 9.0;
            width = ScreenWidth - 15.0 - 15.0;
            height = 1.0;
            self.lineView_2.frame = CGRectMake(x, y, width, height);
            
        }
        
        
        
        self.nameLable_3.hidden = NO;
        self.timeLabel_3.hidden = NO;
        self.contentLabel_3.hidden = NO;
        self.lineView_3.hidden = NO;
        
        {
            
            /* 第三条评论数据 */
            iEver_answerComment_object *_answerComment_object_3 = [object._answerCommentList objectAtIndex:2];
            /* 评论的评论第三条 用户昵称区域 */
            x = 10.0;
            y = self.lineView_2.frame.origin.y + 1.0 + 10.0;
            width = 180.0;
            height = 15.0;
            NSDictionary* name3_Style = @{@"gray": [UIColor lightGrayColor],
                                          @"blue": [UIColor blueColor]};
            
            NSString *name3_strStyle = [NSString stringWithFormat:@"<gray>%@ 回复</gray><blue>%@</blue>",_answerComment_object_3.userNickName,_answerComment_object_3.atUserNickName];
            self.nameLable_3.attributedText = [name3_strStyle attributedStringWithStyleBook:name3_Style];
            self.nameLable_3.frame = CGRectMake(x, y, width, height);
            
            
            /* 评论的评论第三条 时间 */
            x = 205.0;
            y = self.lineView_2.frame.origin.y + 1.0 + 10.0;
            width = 75.0;
            height = 15.0;
            NSString *replyCommentTimeStr_3 = [iEver_Global commentIntervalSinceNow:_answerComment_object_3.createTime/1000];
            self.timeLabel_3.text = replyCommentTimeStr_3;
            self.timeLabel_3.frame = CGRectMake(x, y, width, height);
            
            
            /* 评论的评论第一条 回复评论 */
            NSDictionary* contentLabel_StyleDic = @{@"insertComment":[WPAttributedStyleAction styledActionWithAction:^{
                
                iEver_AskAnswerDetailViewController *superVC = (iEver_AskAnswerDetailViewController *)[self firstAvailableUIViewController];
                
                superVC.comment_qId = object.qId_id;
                superVC.comment_aId = object.answer_id;
                superVC.comment_atUserId = _answerComment_object_3.userId;
                superVC.toAnswerCommentName = _answerComment_object_3.userNickName;
                [superVC insertCommentToAnswerComment];
                NSLog(@"insertComment");
                
            }]};
            NSDictionary * contentdic3 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
            CGSize contentSize3 =[_answerComment_object_3.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic3 context:nil].size;
            x = 10.0;
            y = y + height + 10.0;
            width = ScreenWidth - 15.0 - 15.0 - 20.0;
            height = contentSize3.height;
            content3_SizeHeight = contentSize3.height;
            self.contentLabel_3.frame = CGRectMake(x, y, width, height);
            NSString *contentLabel_strStyle = [NSString stringWithFormat:@"<insertComment>%@</insertComment>",_answerComment_object_3.commentContent];
            self.contentLabel_3.attributedText = [contentLabel_strStyle attributedStringWithStyleBook:contentLabel_StyleDic];
            
            /* 第三条评论的评论下的分割线 当有超过三条评论的评论才会出现 */
            x = 0.0;
            y = self.contentLabel_3.frame.origin.y + self.contentLabel_3.frame.size.height + 9.0;
            width = ScreenWidth - 15.0 - 15.0;
            height = 1.0;
            self.lineView_3.frame = CGRectMake(x, y, width, height);
            
        }
        
        NSDictionary* queryAllCommLabel_StyleDic = @{@"queryAllComm":[WPAttributedStyleAction styledActionWithAction:^{
            
//            iEver_itemCommentReplyDetailViewController *_CommentDetailViewController = [[iEver_itemCommentReplyDetailViewController alloc] init];
//            _CommentDetailViewController.comment_id = object.Comment_id;
//            _CommentDetailViewController.itemCommentList_object = object;
//            UIViewController *superVC = (UIViewController *)[self firstAvailableUIViewController];
//            [superVC.navigationController pushViewController:_CommentDetailViewController animated:YES];
//            if ([superVC.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
//                superVC.navigationController.interactivePopGestureRecognizer.delegate = nil;
//            }
        }]};
        
        /* 第三条评论的评论下的查看更多评论评论 当有超过三条评论的评论才会出现 */
        x = 0.0;
        y = self.lineView_3.frame.origin.y + 1.0 ;
        width = ScreenWidth - 15.0 - 15.0 - 20.0;
        height = 38.0;
        self.queryAllCommLabel.frame = CGRectMake(x, y, width, height);
        NSString *queryAllCommLabel_strStyle = [NSString stringWithFormat:@"<queryAllComm>查看全部评论（%d）</queryAllComm>",object.commentTotal];
        self.queryAllCommLabel.attributedText = [queryAllCommLabel_strStyle attributedStringWithStyleBook:queryAllCommLabel_StyleDic];
        self.queryAllCommLabel.hidden = NO;
        
        
        /* 评论的评论 背景灰色块 */
        x = 15.0;
        y = self.arrowImage.frame.origin.y + tryDetailArrow_SizeHeight;
        width = ScreenWidth - 15.0 - 15.0;
        height = self.contentLabel_3.frame.origin.y + self.contentLabel_3.frame.size.height + 38.0 + 10.0 ;
        self.baseCommentView.frame = CGRectMake(x, y, width, height);
        
        
        /* cell 背景块 */
        x = 0.0;
        y = 0.0;
        height = answerView_SizeHeight + 25.0 + 35.0 + content1_SizeHeight  + 35.0 + content2_SizeHeight + 35.0 + content3_SizeHeight + 10.0  + 38.0 + 10.0 + 10.0 + 10.0;
        width = 320.0;
        self.bottomView.frame = CGRectMake(x, y, width, height);
    }
    
    self.bottomView.backgroundColor = WHITE;
    
    
    NSLog(@"self.bottomView.frame.size.height----->%f",self.bottomView.frame.size.height);
    
    
}
+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
    iEver_answer_object *content_object = (iEver_answer_object *)object;
    
    CGFloat cellHieght = 0.0;
    
    CGFloat centerViewHieght = 0.0;
    
    /* 答案区域：内容和图片高度 */
    
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
    CGSize contentSize =[content_object.content boundingRectWithSize:CGSizeMake(275.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
    
    int totalRow = (content_object._answerPicList.count - 1) / 3 + 1; //总行数
    CGFloat imageViewH = 80;
    CGFloat margin = 5;
    
    centerViewHieght = (content_object._answerPicList.count == 0) ? ( 60.0 + contentSize.height  + 10.0 + 10.0 ): ((60.0 + contentSize.height + 10.0) + ( margin + imageViewH ) * totalRow + 10.0 + 10.0 );
    
    cellHieght = centerViewHieght;
    
    /* 答案评论区域：如果有评论 最多展示3条 */
    
    if (content_object.commentTotal == 0) {
        
        /* centerViewHieght 答案区域的高度；25.0 评论记录底部间距 */
        cellHieght = centerViewHieght + 25.0;
    }
    else if (content_object.commentTotal == 1) {
        
        iEver_answerComment_object *_answerComment_object_1 = [content_object._answerCommentList objectAtIndex:0];
        /* 评论的评论第一条 评论评论 */
        NSDictionary * contentdic1 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
        CGSize contentSize1 =[_answerComment_object_1.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic1 context:nil].size;
        
        /* centerViewHieght 答案区域的高度；25.0 评论记录底部间距/距离气泡 ；35.0 name部分占据的位置；contentSize1.height 第一条评论；10.0 第一条评论距离灰色色块间隔 ； 10 色块距离cell分割线 */
        cellHieght = centerViewHieght + 25.0 + 35.0 + contentSize1.height + 10.0 + 10.0;
        
    }
    else if (content_object.commentTotal == 2) {
        
        iEver_answerComment_object *_answerComment_object_1 = [content_object._answerCommentList objectAtIndex:0];
        /* 评论的评论第一条 评论评论 */
        NSDictionary * contentdic1 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
        CGSize contentSize1 =[_answerComment_object_1.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic1 context:nil].size;
        
        iEver_answerComment_object *_answerComment_object_2 = [content_object._answerCommentList objectAtIndex:1];
        /* 评论的评论第二条 评论评论 */
        NSDictionary * contentdic2 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
        CGSize contentSize2 =[_answerComment_object_2.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic2 context:nil].size;
        
        /* centerViewHieght 答案区域的高度；25.0 评论记录底部间距/距离气泡 ；35.0 name部分占据的位置；contentSize1.height 第一条评论；10.0 第一条评论距离灰色色块间隔 ； 10 色块距离cell分割线 */
        cellHieght = centerViewHieght + 25.0 + 35.0 + contentSize1.height  + 35.0 + contentSize2.height + 10.0 + 10.0 + 10.0;
    }
    else if (content_object.commentTotal == 3) {
        
        iEver_answerComment_object *_answerComment_object_1 = [content_object._answerCommentList objectAtIndex:0];
        /* 评论的评论第一条 评论评论 */
        NSDictionary * contentdic1 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
        CGSize contentSize1 =[_answerComment_object_1.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic1 context:nil].size;
        
        iEver_answerComment_object *_answerComment_object_2 = [content_object._answerCommentList objectAtIndex:1];
        /* 评论的评论第二条 评论评论 */
        NSDictionary * contentdic2 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
        CGSize contentSize2 =[_answerComment_object_2.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic2 context:nil].size;
        
        iEver_answerComment_object *_answerComment_object_3 = [content_object._answerCommentList objectAtIndex:2];
        /* 评论的评论第三条 评论评论 */
        NSDictionary * contentdic3 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
        CGSize contentSize3 =[_answerComment_object_3.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic3 context:nil].size;
        
        /* centerViewHieght 答案区域的高度；25.0 评论记录底部间距/距离气泡 ；35.0 name部分占据的位置；contentSize1.height 第一条评论；10.0 第一条评论距离灰色色块间隔 ； 10 色块距离cell分割线 */
        cellHieght = centerViewHieght + 25.0 + 35.0 + contentSize1.height  + 35.0 + contentSize2.height + 35.0 + contentSize3.height + 10.0 + 10.0+ 10.0 + 10.0;
    }
    
    else if (content_object.commentTotal > 3) {
        
        iEver_answerComment_object *_answerComment_object_1 = [content_object._answerCommentList objectAtIndex:0];
        /* 评论的评论第一条 评论评论 */
        NSDictionary * contentdic1 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
        CGSize contentSize1 =[_answerComment_object_1.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic1 context:nil].size;
        
        iEver_answerComment_object *_answerComment_object_2 = [content_object._answerCommentList objectAtIndex:1];
        /* 评论的评论第二条 评论评论 */
        NSDictionary * contentdic2 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
        CGSize contentSize2 =[_answerComment_object_2.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic2 context:nil].size;
        
        iEver_answerComment_object *_answerComment_object_3 = [content_object._answerCommentList objectAtIndex:2];
        /* 评论的评论第三条 评论评论 */
        NSDictionary * contentdic3 = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
        CGSize contentSize3 =[_answerComment_object_3.commentContent boundingRectWithSize:CGSizeMake(ScreenWidth - 15.0 - 15.0 - 20.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:contentdic3 context:nil].size;
        
        /* centerViewHieght 答案区域的高度；25.0 评论记录底部间距/距离气泡 ；35.0 name部分占据的位置；contentSize1.height 第一条评论；10.0 第一条评论距离灰色色块间隔 ； 10 色块距离cell分割线 */
        cellHieght = centerViewHieght + 25.0 + 35.0 + contentSize1.height  + 35.0 + contentSize2.height + 35.0 + contentSize3.height + 10.0  + 38.0 + 10.0+ 10.0 + 10.0;
    }

    
    
    return cellHieght;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */
# pragma mark - view getters


/* 整体背景视图 */
- (UIView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [[UIView alloc]init];
        _bottomView.backgroundColor = [UIColor whiteColor];
    }
    return _bottomView;
}

/* 中间背景视图 */
- (UIView *)centerView
{
    if (!_centerView) {
        _centerView = [[UIView alloc]init];
        _centerView.backgroundColor = [UIColor whiteColor];
    }
    return _centerView;
}

/* 头像按钮 */
- (UIButton *)photoButton
{
    if (!_photoButton){
        _photoButton = [UIButton buttonWithType:UIButtonTypeCustom];
        CALayer *photoButton = [_photoButton layer];
        [photoButton setMasksToBounds:YES];
        [photoButton setCornerRadius:30.0/2];
    }
    return _photoButton;
}

/* 问者昵称 */
- (UILabel *)nameLable
{
    if (!_nameLable){
        _nameLable = [[UILabel alloc] init];
        _nameLable.numberOfLines = 1;
        _nameLable.backgroundColor = CLEAR;
        _nameLable.textColor = MY_PURPLECOLOR;
        _nameLable.font = TextFont;
    }
    return _nameLable;
}

/* 回复时间 */
- (UILabel *)answerTimeLabel
{
    if (!_answerTimeLabel){
        _answerTimeLabel = [[UILabel alloc] init];
        _answerTimeLabel.backgroundColor = CLEAR;
        _answerTimeLabel.textColor = GRAY_LIGHT;
        _answerTimeLabel.textAlignment = NSTextAlignmentRight;
        _answerTimeLabel.font = TextDESCFonts;
    }
    return _answerTimeLabel;
}

/* 问题主题 */
- (UILabel *)contentLabel
{
    if (!_contentLabel){
        _contentLabel = [[UILabel alloc] init];
        _contentLabel.backgroundColor = CLEAR;
        _contentLabel.numberOfLines = 0;
        _contentLabel.textColor = BLACK;
        _contentLabel.font = TextFont;
    }
    return _contentLabel;
}

/* 问题图片1 */
-(UIImageView *)Q_image1{
    
    if (!_Q_image1) {
        _Q_image1 = [[UIImageView alloc] init];
        _Q_image1.backgroundColor = BLACK;
        _Q_image1.contentMode = UIViewContentModeScaleAspectFill;
        _Q_image1.hidden = YES;
    }
    return _Q_image1;
}

/* 问题图片2 */
-(UIImageView *)Q_image2{
    
    if (!_Q_image2) {
        _Q_image2 = [[UIImageView alloc] init];
        _Q_image2.backgroundColor = BLACK;
        _Q_image2.contentMode = UIViewContentModeScaleAspectFill;
        _Q_image2.hidden = YES;
    }
    return _Q_image2;
}


/* 问题图片3 */
-(UIImageView *)Q_image3{
    
    if (!_Q_image3) {
        _Q_image3 = [[UIImageView alloc] init];
        _Q_image3.backgroundColor = BLACK;
        _Q_image3.contentMode = UIViewContentModeScaleAspectFill;
        _Q_image3.hidden = YES;
    }
    return _Q_image3;
}


/* 问题图片4 */
-(UIImageView *)Q_image4{
    
    if (!_Q_image4) {
        _Q_image4 = [[UIImageView alloc] init];
        _Q_image4.backgroundColor = BLACK;
        _Q_image4.contentMode = UIViewContentModeScaleAspectFill;
        _Q_image4.hidden = YES;
    }
    return _Q_image4;
}


/* 问题图片5 */
-(UIImageView *)Q_image5{
    
    if (!_Q_image5) {
        _Q_image5 = [[UIImageView alloc] init];
        _Q_image5.backgroundColor = BLACK;
        _Q_image5.contentMode = UIViewContentModeScaleAspectFill;
        _Q_image5.hidden = YES;
    }
    return _Q_image5;
}

/* 问题图片6 */
-(UIImageView *)Q_image6{
    
    if (!_Q_image6) {
        _Q_image6 = [[UIImageView alloc] init];
        _Q_image6.backgroundColor = BLACK;
        _Q_image6.contentMode = UIViewContentModeScaleAspectFill;
        _Q_image6.hidden = YES;
    }
    return _Q_image6;
}

/* 问题图片7 */
-(UIImageView *)Q_image7{
    
    if (!_Q_image7) {
        _Q_image7 = [[UIImageView alloc] init];
        _Q_image7.backgroundColor = BLACK;
        _Q_image7.contentMode = UIViewContentModeScaleAspectFill;
        _Q_image7.hidden = YES;
    }
    return _Q_image7;
}

/* 问题图片8 */
-(UIImageView *)Q_image8{
    
    if (!_Q_image8) {
        _Q_image8 = [[UIImageView alloc] init];
        _Q_image8.backgroundColor = BLACK;
        _Q_image8.contentMode = UIViewContentModeScaleAspectFill;
        _Q_image8.hidden = YES;
    }
    return _Q_image8;
}

/* 问题图片9 */
-(UIImageView *)Q_image9{
    
    if (!_Q_image9) {
        _Q_image9 = [[UIImageView alloc] init];
        _Q_image9.backgroundColor = BLACK;
        _Q_image9.contentMode = UIViewContentModeScaleAspectFill;
        _Q_image9.hidden = YES;
    }
    return _Q_image9;
}


/*--------------------- this is 分割线  线面是回答问题的评论--------------------------*/

/*喜欢按钮*/
- (MCFireworksButton *)likeButton
{
    if (!_likeButton){
        _likeButton = [MCFireworksButton buttonWithType:UIButtonTypeCustom];
        self.likeButton.particleImage = [UIImage imageNamed:@"Sparkle"];
        self.likeButton.particleScale = 0.05;
        self.likeButton.particleScaleRange = 0.02;
        [_likeButton addTarget:self action:@selector(likeAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _likeButton;
}

-(void)likeAction:(id)sender{
    
    iEver_likeActionObject * content = [[iEver_likeActionObject alloc] init];
    if (!_list_object._selected) {
        
        NSDictionary *dic = @{@"answerId": [NSNumber numberWithInt:_list_object.answer_id]
                              };
        
        [[[content answerPraise:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
         subscribeNext:^(NSDictionary *object) {
             
             if ([[object objectForKey:@"resultCode"] integerValue] == 1) {
                 
                 _list_object._selected = !_list_object._selected;
                 _list_object.praiseTotal = _list_object.praiseTotal +1;
                 [self.likeButton popOutsideWithDuration:0.5];
                 [self.likeButton setImage:[UIImage imageNamed:@"detail_commentLike_click"] forState:UIControlStateNormal];
                 self.likeLabel.text = [NSString stringWithFormat:@"赞(%d)",_list_object.praiseTotal];
                 
                 [self.likeButton animate];
                 
             }else{
                 NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
                 [SVProgressHUD showImage:Nil status:codeStr];
             }
         }];
    }
    else {
        
        NSDictionary *dic = @{@"answerId": [NSNumber numberWithInt:_list_object.answer_id]
                              };
        
        [[[content delAnswerPraise:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
         subscribeNext:^(NSDictionary *object) {
             
             if ([[object objectForKey:@"resultCode"] integerValue] == 1) {
                 
                 _list_object._selected = !_list_object._selected;
                 _list_object.praiseTotal = _list_object.praiseTotal -1;
                 [self.likeButton popInsideWithDuration:0.4];
                 [self.likeButton setImage:[UIImage imageNamed:@"detail_commentLike"] forState:UIControlStateNormal];
                 self.likeLabel.text = [NSString stringWithFormat:@"赞(%d)",_list_object.praiseTotal];
                 
             }else{
                 NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
                 [SVProgressHUD showImage:Nil status:codeStr];
             }
         }];
    }
}

/* 评论点赞数量  */
- (UILabel *)likeLabel
{
    if (!_likeLabel){
        _likeLabel = [[UILabel alloc] init];
        _likeLabel.numberOfLines = 1;
        _likeLabel.backgroundColor = CLEAR;
        _likeLabel.textColor = MY_PURPLECOLOR;
        _likeLabel.font = TextFonts;
    }
    return _likeLabel;
}


/*回复按钮*/
- (UIButton *)replyButton
{
    if (!_replyButton){
        _replyButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_replyButton addTarget:self action:@selector(replyAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _replyButton;
}

-(void)replyAction:(id)sender{
    
    iEver_AskAnswerDetailViewController *superVC = (iEver_AskAnswerDetailViewController *)[self firstAvailableUIViewController];
    
    superVC.comment_qId = _list_object.qId_id;
    superVC.comment_aId = _list_object.answer_id;
    superVC.comment_atUserId = _list_object.userId;
    superVC.toAnswerCommentName = _list_object.aNickName;
    [superVC insertCommentToAnswerComment];
    
}

/* 评论回复  */
- (UILabel *)replyLabel
{
    if (!_replyLabel){
        _replyLabel = [[UILabel alloc] init];
        _replyLabel.numberOfLines = 1;
        _replyLabel.backgroundColor = CLEAR;
        _replyLabel.textColor = MY_PURPLECOLOR;
        _replyLabel.font = TextFonts;
    }
    return _replyLabel;
}

/* 评论下面箭头 */
- (UIImageView *)arrowImage
{
    if (!_arrowImage) {
        _arrowImage = [[UIImageView alloc]init];
        _arrowImage.backgroundColor = CLEAR;
    }
    return _arrowImage;
}

/* 对评论做评论的灰色背景 */
- (UIView *)baseCommentView
{
    if (!_baseCommentView) {
        _baseCommentView = [[UIView alloc]init];
        _baseCommentView.backgroundColor = TYR_BACKGRAYCOLOR;
    }
    return _baseCommentView;
}

/* 评论的评论者名称_1 */
- (UILabel *)nameLable_1
{
    if (!_nameLable_1){
        _nameLable_1 = [[UILabel alloc] init];
        _nameLable_1.numberOfLines = 1;
        _nameLable_1.backgroundColor = CLEAR;
        _nameLable_1.textColor = GRAY_LIGHT;
        _nameLable_1.textAlignment = NSTextAlignmentLeft;
        _nameLable_1.font = TextDESCFonts;
    }
    return _nameLable_1;
}

/* 评论的评论评论时间_1 */
- (UILabel *)timeLabel_1
{
    if (!_timeLabel_1){
        _timeLabel_1 = [[UILabel alloc] init];
        _timeLabel_1.numberOfLines = 0;
        _timeLabel_1.backgroundColor = CLEAR;
        _timeLabel_1.textColor = GRAY_LIGHT;
        _timeLabel_1.textAlignment = NSTextAlignmentRight;
        _timeLabel_1.font = TextDESCFonts;
    }
    return _timeLabel_1;
}

/* 评论的评论内容_1 */
- (WPHotspotLabel *)contentLabel_1
{
    if (!_contentLabel_1){
        _contentLabel_1 = [[WPHotspotLabel alloc] init];
        _contentLabel_1.numberOfLines = 0;
        _contentLabel_1.backgroundColor = CLEAR;
        _contentLabel_1.textColor = BLACK;
        _contentLabel_1.font = TextFont;
    }
    return _contentLabel_1;
}

/* 评论的评论视图横线_1 */
- (UIImageView *)lineView_1
{
    if (!_lineView_1) {
        _lineView_1 = [[UIImageView alloc]init];
        _lineView_1.image = [UIImage imageNamed:@"person_line"];
    }
    return _lineView_1;
}

/* 评论的评论者名称_2 */
- (UILabel *)nameLable_2
{
    if (!_nameLable_2){
        _nameLable_2 = [[UILabel alloc] init];
        _nameLable_2.numberOfLines = 1;
        _nameLable_2.backgroundColor = CLEAR;
        _nameLable_2.textColor = GRAY_LIGHT;
        _nameLable_2.textAlignment = NSTextAlignmentLeft;
        _nameLable_2.font = TextDESCFonts;
    }
    return _nameLable_2;
}

/* 评论的评论评论时间_2 */
- (UILabel *)timeLabel_2
{
    if (!_timeLabel_2){
        _timeLabel_2 = [[UILabel alloc] init];
        _timeLabel_2.numberOfLines = 0;
        _timeLabel_2.backgroundColor = CLEAR;
        _timeLabel_2.textColor = GRAY_LIGHT;
        _timeLabel_2.textAlignment = NSTextAlignmentRight;
        _timeLabel_2.font = TextDESCFonts;
    }
    return _timeLabel_2;
}

/* 评论的评论内容_2 */
- (WPHotspotLabel *)contentLabel_2
{
    if (!_contentLabel_2){
        _contentLabel_2 = [[WPHotspotLabel alloc] init];
        _contentLabel_2.numberOfLines = 0;
        _contentLabel_2.backgroundColor = CLEAR;
        _contentLabel_2.textColor = BLACK;
        _contentLabel_2.font = TextFont;
    }
    return _contentLabel_2;
}

/* 评论的评论视图横线_2 */
- (UIImageView *)lineView_2
{
    if (!_lineView_2) {
        _lineView_2 = [[UIImageView alloc]init];
        _lineView_2.image = [UIImage imageNamed:@"person_line"];
    }
    return _lineView_2;
}


/* 评论的评论者名称_3 */
- (UILabel *)nameLable_3
{
    if (!_nameLable_3){
        _nameLable_3 = [[UILabel alloc] init];
        _nameLable_3.numberOfLines = 1;
        _nameLable_3.backgroundColor = CLEAR;
        _nameLable_3.textColor = GRAY_LIGHT;
        _nameLable_3.textAlignment = NSTextAlignmentLeft;
        _nameLable_3.font = TextDESCFonts;
    }
    return _nameLable_3;
}

/* 评论的评论评论时间_3 */
- (UILabel *)timeLabel_3
{
    if (!_timeLabel_3){
        _timeLabel_3 = [[UILabel alloc] init];
        _timeLabel_3.numberOfLines = 0;
        _timeLabel_3.backgroundColor = CLEAR;
        _timeLabel_3.textColor = GRAY_LIGHT;
        _timeLabel_3.textAlignment = NSTextAlignmentRight;
        _timeLabel_3.font = TextDESCFonts;
    }
    return _timeLabel_3;
}

/* 评论的评论内容_3 */
- (WPHotspotLabel *)contentLabel_3
{
    if (!_contentLabel_3){
        _contentLabel_3 = [[WPHotspotLabel alloc] init];
        _contentLabel_3.numberOfLines = 0;
        _contentLabel_3.backgroundColor = CLEAR;
        _contentLabel_3.textColor = BLACK;
        _contentLabel_3.font = TextFont;
    }
    return _contentLabel_3;
}

/* 评论的评论视图横线_3 */
- (UIImageView *)lineView_3
{
    if (!_lineView_3) {
        _lineView_3 = [[UIImageView alloc]init];
        _lineView_3.image = [UIImage imageNamed:@"person_line"];
    }
    return _lineView_3;
}

/* 查看更多评论 */
- (WPHotspotLabel *)queryAllCommLabel
{
    if (!_queryAllCommLabel){
        _queryAllCommLabel = [[WPHotspotLabel alloc] init];
        _queryAllCommLabel.numberOfLines = 0;
        _queryAllCommLabel.backgroundColor = CLEAR;
        _queryAllCommLabel.textColor = BLACK;
        _queryAllCommLabel.font = TextFont;
        _queryAllCommLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _queryAllCommLabel;
}




/* 视图横线 */
- (UIImageView *)lineView
{
    if (!_lineView) {
        _lineView = [[UIImageView alloc]init];
        _lineView.image = [UIImage imageNamed:@"person_line"];
    }
    return _lineView;
}


@end
