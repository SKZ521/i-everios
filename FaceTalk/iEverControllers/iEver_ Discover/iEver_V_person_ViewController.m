//
//  iEver_V_person_ViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-10.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_V_person_ViewController.h"
#import "QQSectionHeaderView.h"
#import "iEver_V_groupUserObject.h"
#import "iEver_V_personCell.h"
#define HEADER_HEIGHT 40
@interface iEver_V_person_ViewController ()<QQSectionHeaderViewDelegate>
{

    iEver_V_groupUserObject *groupUserObject;
}
@property (nonatomic, strong) NSArray *images;
@end

@implementation iEver_V_person_ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"达人搜索";
    _images = @[@"discover_V_person_line1", @"discover_V_person_line2",@"discover_V_person_line3", @"discover_V_person_line4", @"discover_V_person_line5",@"discover_V_person_line1",@"discover_V_person_line2",@"discover_V_person_line3",@"discover_V_person_line4",@"discover_V_person_line5"];
    [self.tableView  setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    /* NO show pull refresh */
    self.tableView.showsPullToRefresh =NO;
    self.tableView.showsInfiniteScrolling = NO;
    [self fetchData];
}
- (void)fetchData
{

    //__block iEver_DiscoverViewController *weakSelf = self;
    [GiFHUD show];
    iEver_V_groupUserObject *content = [[iEver_V_groupUserObject alloc] init];
    [[[content queryByGroup:nil path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(iEver_V_groupUserObject *object) {

         groupUserObject = object;
         if ([groupUserObject._expertUserList count] >0) {
             iEver_V_userList_object *sectionObject = [groupUserObject._expertUserList objectAtIndex:0];
             sectionObject.opened = YES;
         }
         [self.tableView reloadData];
         [GiFHUD dismiss];

     }
     ];}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark -
#pragma mark Table view data source
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
	return HEADER_HEIGHT;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return [groupUserObject._expertUserList count]; // 分组数
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    iEver_V_userList_object *object = [groupUserObject._expertUserList objectAtIndex:section];
    if (object.opened) {
		return [object._userList count]; // 人员数

	}else {
		return 0;	// 不展开
	}
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier  = @"V_personCell";
    UITableViewCell *cell = nil;
	tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell = (iEver_V_personCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    // Configure the cell...
	if (!cell) {
        cell = [[iEver_V_personCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    iEver_V_userList_object *sectionObject = [groupUserObject._expertUserList objectAtIndex:indexPath.section];
    iEver_V_personCell *dic_cell = (iEver_V_personCell *)cell;
    iEver_V_user_object * object = [sectionObject._userList objectAtIndex:indexPath.row];
    [dic_cell setViewController:self];
    [dic_cell setObject:object];
	return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    iEver_V_userList_object *sectionObject = [groupUserObject._expertUserList objectAtIndex:indexPath.section];
    iEver_V_user_object * object = [sectionObject._userList objectAtIndex:indexPath.row];
    return [iEver_V_personCell heightForObject:object atIndexPath:indexPath tableView:tableView];
}


-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section {

    iEver_V_userList_object *object = [groupUserObject._expertUserList objectAtIndex:section];
	QQSectionHeaderView *sectionHeadView = [[QQSectionHeaderView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.self.tableView.bounds.size.width, HEADER_HEIGHT)
                                                                                title:object.categoryName
                                                                          imagestring:_images[section]
                                                                              section:section
                                                                            personNum:[object._userList count]
                                                                               opened:object.opened
                                                                             delegate:self] ;
	return sectionHeadView;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    iEver_V_userList_object *sectionObject = [groupUserObject._expertUserList objectAtIndex:indexPath.section];
    iEver_V_user_object * object = [sectionObject._userList objectAtIndex:indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma mark QQSectionHeaderView delegate
-(void)sectionHeaderView:(QQSectionHeaderView*)sectionHeaderView sectionClosed:(NSInteger)section
{
    iEver_V_userList_object *object = [groupUserObject._expertUserList objectAtIndex:section];
	object.opened = !object.opened;

	// 收缩+动画 (如果不需要动画直接reloaddata)
	NSInteger countOfRowsToDelete = [self.tableView numberOfRowsInSection:section];
    if (countOfRowsToDelete > 0) {
        object.indexPaths = [[NSMutableArray alloc] init];

        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [object.indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:section]];
        }
        [self.tableView deleteRowsAtIndexPaths:object.indexPaths withRowAnimation:UITableViewRowAnimationTop];
    }
}

-(void)sectionHeaderView:(QQSectionHeaderView*)sectionHeaderView sectionOpened:(NSInteger)section
{
    iEver_V_userList_object *object = [groupUserObject._expertUserList objectAtIndex:section];
	object.opened = !object.opened;

	// 展开+动画 (如果不需要动画直接reloaddata)
	if(object.indexPaths){
		[self.tableView insertRowsAtIndexPaths:object.indexPaths withRowAnimation:UITableViewRowAnimationTop];
	}else {

        [self.tableView reloadData];
    }
	object.indexPaths = nil;
}

@end
