//
//  iEver_AskAnswerDetailViewController.m
//  FaceTalk
//
//  Created by kevin on 15/5/31.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_AskAnswerDetailViewController.h"
#import "iEver_askAnswerDetailObject.h"

/* 插入回答评论 */
#import "iEver_commentObject.h"

#import "iEver_askAnswerDetailCell.h"

#import "MJPhotoBrowser.h"
#import "MJPhoto.h"

#import "NSString+WPAttributedMarkup.h"

#import "iEver_AnswerViewController.h"

@interface iEver_AskAnswerDetailViewController ()

{
    iEver_askAnswerDetailObject  *_datailObject;
    NSMutableArray *urlArray; //相册URL
    NSMutableArray *imageViewArray;//问题图片
    
}

/* 评论相关 */
@property (retain, nonatomic) UIView                    *mainView;
@property (retain, nonatomic) UIButton                  *bottomButton;
@property (retain, nonatomic) UITextView                *myTextView;
@property (nonatomic, strong) UILabel                   *commentLab;


@end

@implementation iEver_AskAnswerDetailViewController


/* 相册浏览模式 */
- (void)tapImage:(UITapGestureRecognizer *)tap
{
    int count = urlArray.count;
    NSMutableArray *photos = [NSMutableArray arrayWithCapacity:count];
    for (int i = 0; i < count; i++) {
        // 更换高清尺寸
        NSString *url = [NSString stringWithFormat:@"%@?imageMogr2/thumbnail/640x",[urlArray objectAtIndex:i]];
        MJPhoto *photo = [[MJPhoto alloc] init];
        photo.url = [NSURL URLWithString:url]; // 图片路径
        photo.srcImageView = imageViewArray[i]; // 来源于哪个UIImageView
        [photos addObject:photo];
    }
    
    // 2.显示相册
    MJPhotoBrowser *browser = [[MJPhotoBrowser alloc] init];
    browser.currentPhotoIndex = tap.view.tag; // 弹出相册时显示的第一张图片是？
    browser.photos = photos; // 设置所有的图片
    [browser show];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    /* NO show pull refresh */
    self.tableView.showsPullToRefresh = NO;
    self.tableView.showsInfiniteScrolling = NO;
    self.tableView.frame = CGRectMake(0, 0.0, ScreenWidth, ScreenHeight - BARHeight - NAVHeight);
    
    /* set tableview separtor none */
    [self.tableView  setSeparatorStyle:UITableViewCellSeparatorStyleNone];

    
    /* request data */
    [self fetchData];
    
    urlArray = [[NSMutableArray alloc] init];
    imageViewArray = [[NSMutableArray alloc] init];
    
    /* comment textview */
    [self creatReplyQusetionButton];
    
    /* 创建评论输入框 */
    [self creatTextView];
    
}


/*request data*/
-(void)fetchData{
    
     @weakify(self);

    iEver_askAnswerDetailObject *detail_Object = [[iEver_askAnswerDetailObject alloc] init];
    NSString *_pathUrl = [NSString stringWithFormat:@"/expertQuestion/queryById/%d/%d",self.question_id,self.page];
    [[[detail_Object expertQuestionQueryById:nil path:_pathUrl] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(iEver_askAnswerDetailObject *object) {
         
         _datailObject = object;
         @strongify(self);
         if (object.answerPageSize < self.page) {
             self.tableView.showsInfiniteScrolling = NO;
         }else{
             [self addDataToDataSource:object._answerDetailList];
             [self.tableView reloadData];
         }
         /* creat CoverView As TableViweHeader */
         if (self.page == 1) {
             [self creatCoverViewAsTableViweHeader:object];
         }
         
     }];
}

/* creat questionView As TableViweHeader */
-(void)creatCoverViewAsTableViweHeader:(iEver_askAnswerDetailObject *)object {
    
    /* Initialization size */
    CGFloat x = 0.0;
    CGFloat y = 0.0;
    CGFloat width = 0.0;
    CGFloat height = 0.0;
    
    /* custom tableView headerView */
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = WHITE;
    
    /* 提问者头像 */
    UIButton *photoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    CALayer *photolayer = [photoButton layer];
    [photolayer setMasksToBounds:YES];
    [photolayer setCornerRadius:30.0/2];
    [photoButton setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/60x",object.questionObject.qHeadImg]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
    x = 15.0;
    y = 15.0;
    width =  30.0;
    height = 30.0;
    photoButton.frame = CGRectMake(x, y, width, height);
    [headerView addSubview:photoButton];
    
    /* 用户昵称 */
    UILabel *nameLable = [[UILabel alloc] init];
    nameLable.numberOfLines = 1;
    nameLable.backgroundColor = CLEAR;
    nameLable.textColor = MY_PURPLECOLOR;
    nameLable.font = TextFont;
    nameLable.text = object.questionObject.qNickName;
    x = 55.0;
    y = 25.0;
    width = 150.0;
    height = 15.0;
    nameLable.frame = CGRectMake(x, y, width, height);
    [headerView addSubview:nameLable];
    
    /* 问题发布时间 */
    UILabel *creatTimeLable = [[UILabel alloc] init];
    creatTimeLable.backgroundColor = CLEAR;
    creatTimeLable.textColor = GRAY_LIGHT;
    creatTimeLable.textAlignment = NSTextAlignmentRight;
    creatTimeLable.font = TextDESCFonts;
    
    /* 格式创建时间  5月30日 10：00 */
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM月dd日 hh:mm"];
    NSString *dateLoca = [NSString stringWithFormat:@"%lld",object.questionObject.releaseTime/1000];
    NSTimeInterval time =[dateLoca doubleValue];
    NSDate *detaildate = [NSDate dateWithTimeIntervalSince1970:time];
    creatTimeLable.text = [formatter stringFromDate:detaildate];
    x = 175.0;
    y = 25.0;
    width = 120.0;
    height = 15.0;
    creatTimeLable.frame = CGRectMake(x, y, width, height);
    [headerView addSubview:creatTimeLable];
    
    /* 提问内容 */
    UILabel  *questionContentLabel = [[UILabel alloc] init];
    questionContentLabel.backgroundColor = CLEAR;
    questionContentLabel.numberOfLines = 0;
    questionContentLabel.textColor = BLACK;
    questionContentLabel.font = TextFont;
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
    CGSize contentSize =[object.questionObject.qContent boundingRectWithSize:CGSizeMake(275.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
    x = 15.0;
    y = 60.0;
    width = 275.0;
    height = contentSize.height;
    questionContentLabel.frame = CGRectMake(x, y, width, height);
    questionContentLabel.text = object.questionObject.qContent;
    [headerView addSubview:questionContentLabel];

    
    /* 问题包含图片 */
    
    int totalLoc = 3; //总列数
    int totalRow = (object.questionObject._quesPicList.count - 1) / totalLoc + 1; //总行数
    CGFloat imageViewW = 80;
    CGFloat imageViewH = 80;
    CGFloat margin = 5;
    
    if ([object.questionObject._quesPicList count] > 0) {
        
        for (int i = 0; i < [object.questionObject._quesPicList count]; i++) {
            
            UIImageView *questionImage = [[UIImageView alloc] init];
            questionImage.contentMode = UIViewContentModeScaleAspectFill;
            questionImage.backgroundColor = BLACK;
            int row = i / totalLoc;
            int loc = i % totalLoc;
            CGFloat imageViewX = 15 + ( margin + imageViewW ) * loc;
            CGFloat imageViewY = questionContentLabel.frame.origin.y + questionContentLabel.frame.size.height + 10 + ( margin + imageViewH ) * row;
          
            iEver_quesPic_object *_quesPic_object = [object.questionObject._quesPicList objectAtIndex:i];
            
            [urlArray addObject:_quesPic_object.imgUrl];
            [imageViewArray addObject:questionImage];
            
            [questionImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/160x",_quesPic_object.imgUrl]] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
            questionImage.frame = CGRectMake(imageViewX, imageViewY, imageViewW, imageViewH);
            questionImage.tag = i;
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapImage:)];
            tapGesture.numberOfTapsRequired = 1;
            questionImage.userInteractionEnabled = YES;
            [questionImage addGestureRecognizer:tapGesture];
            
            [headerView addSubview:questionImage];
        }
    }
    
    /* 灰色分割线 */
    x = 0.0 ;
    y = (object.questionObject._quesPicList.count == 0) ? questionContentLabel.frame.origin.y + questionContentLabel.frame.size.height + 10.0 : (questionContentLabel.frame.origin.y + questionContentLabel.frame.size.height + 10.0) + ( margin + imageViewH ) * totalRow + 10.0;
    width = ScreenWidth;
    height = 6.0;
    UIView *gray_view = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    gray_view.backgroundColor = TYR_BACKGRAYCOLOR;
    
    [headerView addSubview:gray_view];
    
    /* 回答量 */
    NSDictionary* styleDic = @{@"gray": [UIColor blackColor],
                                    @"blue": [UIColor redColor]};
    
    NSString *strStyle = [NSString stringWithFormat:@"<gray>回答</gray><blue>(%d)</blue>",[object._answerDetailList count]];
    
    UILabel *answerCountLable = [[UILabel alloc] init];
    answerCountLable.numberOfLines = 1;
    answerCountLable.backgroundColor = CLEAR;
    answerCountLable.textAlignment = NSTextAlignmentRight;
    answerCountLable.attributedText = [strStyle attributedStringWithStyleBook:styleDic];
    answerCountLable.font = TextFont;
    x = 20.0;
    y = gray_view.frame.origin.y + gray_view.frame.size.height + 15.0;
    width = 280.0;
    height = 15.0;
    answerCountLable.frame = CGRectMake(x, y, width, height);
    [headerView addSubview:answerCountLable];
    
    /* 回答下面的横线 */
    x = 20.0 ;
    y = answerCountLable.frame.origin.y + answerCountLable.frame.size.height + 15.0;
    width = ScreenWidth - 20.0 - 10.0;
    height = 1.0;
    UIImageView *downLine_image = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    downLine_image.image = [UIImage imageNamed:@"person_line"];
    [headerView addSubview:downLine_image];
    

    /* 中间view */
    x = 0.0;
    y = 0.0;
    width =  ScreenWidth;
    height =  downLine_image.frame.origin.y + downLine_image.frame.size.height;
    headerView.frame = CGRectMake(x, y, width, height);
    
    self.tableView.tableHeaderView = headerView;
}

-(void)creatReplyQusetionButton {

    /* --- bottomView replyBut  ------ */
    /* 底部回复view */
    UIImage    * detail_bottomView = [UIImage imageNamed:@"detail_bottomView"];
    UIView *replyView            = [[UIView alloc] initWithFrame:CGRectMake(0, ScreenHeight - BARHeight - NAVHeight, ScreenWidth, detail_bottomView.size.height)];
    replyView.backgroundColor    = TYR_BACKGRAYCOLOR;
    [self.view addSubview:replyView];
    
    /* 底部回复view 黑线 */
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 0.5)];
    lineView.backgroundColor = DAKEBLACK;
    [replyView addSubview:lineView];
    
    /* 底部回复图标 */
    UIImage *askAnswerDetail_answer = [UIImage imageNamed:@"askAnswerDetail_answer"];
    UIImageView *answerImage = [[UIImageView alloc] initWithFrame:CGRectMake(110.0, detail_bottomView.size.height/2 - askAnswerDetail_answer.size.height/2 , askAnswerDetail_answer.size.width, askAnswerDetail_answer.size.height)];
    answerImage.image = askAnswerDetail_answer;
    [replyView addSubview:answerImage];
    
    /* 底部回复字符 */
    UILabel *replyLable = [[UILabel alloc] initWithFrame:CGRectMake(110.0 + askAnswerDetail_answer.size.width + 5.0, detail_bottomView.size.height/2 - askAnswerDetail_answer.size.height/2 + 7, 60, 15)];
    replyLable.font = TextFont;
    replyLable.backgroundColor = CLEAR;
    replyLable.textColor = MY_PURPLECOLOR;
    replyLable.text = @"我要回答";
    [replyView addSubview:replyLable];
    
    
    UIButton *commentBut            = [UIButton buttonWithType:UIButtonTypeCustom];
    commentBut.backgroundColor      = CLEAR;
    commentBut.frame = replyView.bounds;
    commentBut.rac_command          = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        
        iEver_AnswerViewController *_AnswerViewController = [[iEver_AnswerViewController alloc]init];
        _AnswerViewController.q_id = _datailObject.questionObject.question_id;
        _AnswerViewController.aUserType = [[NSUserDefaults standardUserDefaults] integerForKey:@"userType"];
        [self.navigationController pushViewController:_AnswerViewController animated:YES];
        IOS7POP;
    
        return [RACSignal empty];
    }];
    [replyView addSubview:commentBut];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataSourceArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"askAnswerDetailCell";
    UITableViewCell *cell = nil;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell = (iEver_askAnswerDetailCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    // Configure the cell...
    if (!cell) {
        
        cell = [[iEver_askAnswerDetailCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    iEver_askAnswerDetailCell *askAnswerDetail_cell = (iEver_askAnswerDetailCell *)cell;
    iEver_answer_object * object = [self.dataSourceArray objectAtIndex:indexPath.row];
    [askAnswerDetail_cell setObject:object];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    iEver_answer_object * object = [self.dataSourceArray objectAtIndex:indexPath.row];
    return [iEver_askAnswerDetailCell heightForObject:object atIndexPath:indexPath tableView:tableView];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

// 根据键盘状态，调整_mainView的位置
- (void) changeContentViewPoint:(NSNotification *)notification{
    
    NSDictionary *userInfo  = [notification userInfo];
    NSValue *value          = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGFloat keyBoardEndY    = value.CGRectValue.origin.y;  // 得到键盘弹出后的键盘视图所在y坐标
    CGFloat kStateBarHeight = 0.0;
    
    NSNumber *duration      = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve         = [userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // 添加移动动画，使视图跟随键盘移动
    [UIView animateWithDuration:duration.doubleValue animations:^{
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationCurve:[curve intValue]];
        self.mainView.center = CGPointMake(self.mainView.center.x, keyBoardEndY - kStateBarHeight - self.mainView.bounds.size.height/2.0 - NAVHeight);   // keyBoardEndY的坐标包括了状态栏的高度，要减去  隐藏导航 NAVHeight
    }];
    
}


-(void)changeKeyboardWillHide:(NSNotification *)notification {
    
    self.bottomButton.hidden     = YES;
    [self.myTextView resignFirstResponder];
    self.mainView.frame          = CGRectMake(0, ScreenHeight - NAVHeight, ScreenWidth, 145.0);
    
}


-(void)insertCommentToAnswerComment {
    
    [self.view bringSubviewToFront:self.mainView];
    self.bottomButton.hidden    = NO;
    self.commentLab.text        = [NSString stringWithFormat:@"回复 %@",self.toAnswerCommentName];
    [self.myTextView becomeFirstResponder];
    
}

-(void)creatTextView {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeContentViewPoint:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    /* mainView 灰黑 BG*/
    self.bottomButton             = [UIButton buttonWithType:UIButtonTypeCustom];
    self.bottomButton.frame       = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
    self.bottomButton.hidden      = YES;
    self.bottomButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        
        self.bottomButton.hidden  = YES;
        [self.myTextView resignFirstResponder];
        self.mainView.frame       = CGRectMake(0, ScreenHeight, 320, 145.0);
        return [RACSignal empty];
    }];
    self.bottomButton.backgroundColor = BLACK;
    self.bottomButton.alpha           = 0.6;
    [self.view addSubview:self.bottomButton];
    
    /* mainView */
    self.mainView                     = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenHeight, 320, 145.0)];
    self.mainView.backgroundColor     = BackkGroundColor;
    [self.view addSubview:self.mainView];
    
    /* textViewImage */
    UIImageView *textViewImage       = [[UIImageView alloc] initWithFrame:CGRectMake(15, 50, 290, 80)];
    textViewImage.image              = [UIImage imageNamed:@"commentContentBG"];
    [self.mainView addSubview:textViewImage];
    
    /* myTextView */
    self.myTextView                  = [[UITextView alloc]initWithFrame:CGRectMake(15, 50, 290, 80)];
    self.myTextView.font             = TextFont;
    self.myTextView.backgroundColor  = CLEAR;
    [self.mainView addSubview:self.myTextView];
    
    /* cancelBut */
    UIButton *cancelBut              = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBut.frame                  = CGRectMake(10, 10, 30, 30);
    [cancelBut setBackgroundImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateNormal];
    [cancelBut setBackgroundImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateHighlighted];
    cancelBut.rac_command            = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        
        self.bottomButton.hidden     = YES;
        [self.myTextView resignFirstResponder];
        self.mainView.frame          = CGRectMake(0, ScreenHeight, 320, 145.0);
        return [RACSignal empty];
    }];
    [self.mainView addSubview:cancelBut];
    
    /* commentLab */
    self.commentLab             = [[UILabel alloc] initWithFrame:CGRectMake(40, 10, 240, 25)];
    self.commentLab.text                 = @"评 论";
    self.commentLab.font                 = TextFont;
    self.commentLab.textAlignment        = NSTextAlignmentCenter;
    [self.mainView addSubview:self.commentLab];
    
    /* sendBut */
    UIButton *sendBut               = [UIButton buttonWithType:UIButtonTypeCustom];
    sendBut.frame                   = CGRectMake(280, 10, 30, 30);
    [sendBut setBackgroundImage:[UIImage imageNamed:@"send"] forState:UIControlStateNormal];
    [sendBut setBackgroundImage:[UIImage imageNamed:@"send"] forState:UIControlStateHighlighted];
    sendBut.rac_command             = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        
        /* insert Comment */
        [self insertCommentRequest];
        return [RACSignal empty];
    }];
    [self.mainView addSubview:sendBut];
    
}
/* insert Comment */
-(void)insertCommentRequest{
    
    
    NSString *temp = [self.myTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if ([temp isEqualToString:@""]) {
        [SVProgressHUD showImage:Nil status:@"请输评论内容"];
        return ;
    }
    
    [SVProgressHUD showWithStatus:@"提交中..." maskType:SVProgressHUDMaskTypeBlack];
    iEver_commentObject * content = [[iEver_commentObject alloc] init];
    
    NSString *_pathUrl = [NSString stringWithFormat:@"/answerComment/insert"];
    NSDictionary *dic = @{@"qId": [NSNumber numberWithInt:self.comment_qId],
                         @"aId": [NSNumber numberWithInt:self.comment_aId],
                         @"atUserId": [NSNumber numberWithInt:self.comment_atUserId],
                         @"commentContent": self.myTextView.text
                };
    
    [[[content insertAnswerComment:dic path:_pathUrl] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(NSDictionary *object) {
         if ([[object objectForKey:@"resultCode"] integerValue] == 1) {
             [SVProgressHUD showImage:Nil status:@"提交成功"];
             
             self.page = 1;
             
             [self.tableView triggerPullToRefresh];
             
         }else{
             NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
             [SVProgressHUD showImage:Nil status:codeStr];
         }
         
     }];
    self.bottomButton.hidden = YES;
    [self.myTextView resignFirstResponder];
    self.commentLab.text = @"评 论";
    self.myTextView.text = @"";
    self.mainView.frame = CGRectMake(0, ScreenHeight, 320, 145.0);
}



@end
