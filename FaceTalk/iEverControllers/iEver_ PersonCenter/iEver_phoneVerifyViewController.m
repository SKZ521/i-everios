//
//  iEver_phoneVerifyViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 14/11/26.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_phoneVerifyViewController.h"
#import "iEver_UserMessageObject.h"
@interface iEver_phoneVerifyViewController ()


@property (strong) IBOutlet UIScrollView *scrollView;
@property (strong) IBOutlet UITextField *userPhone_textField;
@property (strong) IBOutlet UITextField *certificationCode_textField;
@property(nonatomic ,copy)NSString *verifyCode;
- (IBAction)requestCertificationCodeAction:(id)sender;
- (IBAction)nextAction:(id)sender;
@end
@implementation iEver_phoneVerifyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"手机验证";
}
- (IBAction)requestCertificationCodeAction:(id)sender{

    UIButton *CertifieyButton =(UIButton *)sender;

    if (![[iEver_Global Instance] validateMobile:self.userPhone_textField.text]) {
        [SVProgressHUD showImage:Nil status:@"请输入正确的手机号"];
        return;
    }

    /*Confirmation code  request mothod*/
    [self requestCertificationCodeInterface:CertifieyButton];

    
}
-(void)requestCertificationCodeInterface:(UIButton *)sender{

    iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
    NSDictionary *dic = @{@"mobilePhone": self.userPhone_textField.text,
                          };

    [[[content updateMobile:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(NSDictionary *object) {
         if ([[object objectForKey:@"resultCode"] integerValue] == 1) {

             __block int timeout= 60; //倒计时时间
             dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
             dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
             dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0);
             dispatch_source_set_event_handler(_timer, ^{
                 if(timeout<=0){
                     dispatch_source_cancel(_timer);
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [sender setTitle:@"发送验证码" forState:UIControlStateNormal];
                         sender.userInteractionEnabled = YES;
                     });
                 }else{
                     int seconds = timeout % 100;
                     NSString *strTime = [NSString stringWithFormat:@"%.2d", seconds];
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [sender setTitle:[NSString stringWithFormat:@"%@秒后重发",strTime] forState:UIControlStateNormal];
                         sender.userInteractionEnabled = NO;
                     });
                     timeout--;
                 }
             });
             dispatch_resume(_timer);
         }else{

             NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
             [SVProgressHUD showImage:Nil status:codeStr];
         }

     }];
}
- (IBAction)nextAction:(id)sender{


    if (![[iEver_Global Instance] validateMobile:self.userPhone_textField.text]) {
        [SVProgressHUD showImage:Nil status:@"请输入正确的手机号"];
        return;
    }

    iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
    NSDictionary *dic = @{@"mobilePhone": self.userPhone_textField.text,
                          @"verifyCode": self.certificationCode_textField.text,
                          };

    [[[content updateMobileVerify:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(NSDictionary *object) {
         if ([[object objectForKey:@"resultCode"] integerValue] == 1) {
             [SVProgressHUD showImage:Nil status:@"修改成功"];
             [self.navigationController popViewControllerAnimated:YES];
         }else{
             NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
             [SVProgressHUD showImage:Nil status:codeStr];
         }
     }];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end
