//
//  iEver_fixPersonMessageViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-9.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_fixPersonMessageViewController.h"
#import "iEver_UserMessageObject.h"
@interface iEver_fixPersonMessageViewController ()<UITextViewDelegate>{

    int gender;
}
@property (strong) IBOutlet UITextField  *name_text;
@property (strong) IBOutlet UIView       *name_view;

@property (strong) IBOutlet UITextField  *mail_text;
@property (strong) IBOutlet UIView       *mail_view;

@property (strong) IBOutlet UILabel      *man_lable;
@property (strong) IBOutlet UILabel      *women_lable;
@property (strong) IBOutlet UIView       *gender_view;

@property (strong) IBOutlet UITextView   *info_text;
@property (strong) IBOutlet UILabel      *infoNum_lable;
@property (strong) IBOutlet UILabel      *make_lable;
@property (strong) IBOutlet UIView       *info_view;


@property (strong) IBOutlet UIButton     *submitButton;


- (IBAction)buttonAction:(id)sender;
@end

@implementation iEver_fixPersonMessageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    gender = -1;
    self.title = self.intoType;

    if ([self.intoType isEqualToString:@"修改昵称"]) {
        self.name_view.hidden   = NO;
        self.gender_view.hidden = YES;
        self.info_view.hidden   = YES;
        self.mail_view.hidden   = YES;
        self.name_text.text     = self.text;
    }else if ([self.intoType isEqualToString:@"修改性别"]){

        self.name_view.hidden   = YES;
        self.gender_view.hidden = NO;
        self.info_view.hidden   = YES;
        self.mail_view.hidden   = YES;
        if ([self.text isEqualToString:@"男"]) {
            gender =1;
            self.man_lable.textColor   = [UIColor redColor];
            self.women_lable.textColor = [UIColor blackColor];
        }else{
            gender =0;
            self.man_lable.textColor   = [UIColor blackColor];
            self.women_lable.textColor = [UIColor redColor];
        }
    }else if ([self.intoType isEqualToString:@"修改简介"]){

        if(self.text.length>0){
            self.make_lable.hidden  = YES;
        }
        [self.info_text becomeFirstResponder];
        self.info_text.text         = self.text;
        self.name_view.hidden       = YES;
        self.gender_view.hidden     = YES;
        self.info_view.hidden       = NO;
        self.mail_view.hidden       = YES;
    }
    else if ([self.intoType isEqualToString:@"修改邮箱"]){

        [self.info_text becomeFirstResponder];
        self.mail_text.text      = self.text;
        self.name_view.hidden    = YES;
        self.gender_view.hidden  = YES;
        self.info_view.hidden    = YES;
        self.mail_view.hidden    = NO;
    }

    /* fix custom text Font */

    [self customButtonFont];
}

-(void)customButtonFont {

    self.name_text.font = TextFont;
    self.mail_text.font = TextFont;
    self.info_text.font = TextFont;
    self.submitButton.titleLabel.font = Title_V_Fonts;
    self.submitButton.backgroundColor = MY_PURPLECOLOR;
    CALayer *coverButton = [self.submitButton layer];
    [coverButton setMasksToBounds:YES];
    [coverButton setCornerRadius:5.0];

    self.infoNum_lable.font =  TextDESCFonts;
    self.make_lable.font  = TextFont;

    self.women_lable.font = TextDESCFonts;
    self.man_lable.font = TextDESCFonts;
    
}


- (IBAction)buttonAction:(id)sender{

    UIButton *button  = (UIButton *)sender;
	NSInteger index   = button.tag - 10;
    switch (index) {
        case 0:// 性别 男
        {
            gender =1;
            self.man_lable.textColor   = [UIColor redColor];
            self.women_lable.textColor = [UIColor blackColor];

        }
            break;
        case 1:// 性别 女
        {
            gender =0;
            self.man_lable.textColor   = [UIColor blackColor];
            self.women_lable.textColor = [UIColor redColor];

        }
            break;
        case 2:// 提交
        {

            if ([self.intoType isEqualToString:@"修改昵称"]) {
                if (self.name_text.text.length == 0) {
                    [SVProgressHUD showImage:Nil status:@"请输入正确的昵称"];
                    return;
                }
                [SVProgressHUD showWithStatus:@"提交中..." maskType:SVProgressHUDMaskTypeBlack];
                iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
                NSDictionary *dic = @{@"nickName": self.name_text.text,
                                      };

                [[[content updateNickName:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
                 subscribeNext:^(NSDictionary *object) {

                     if ([[object objectForKey:@"resultCode"] integerValue] == 1) {
                         [SVProgressHUD showImage:Nil status:@"修改成功"];
                         [self.navigationController popViewControllerAnimated:YES];
                     }else{
                         NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
                         [SVProgressHUD showImage:Nil status:codeStr];
                     }
                     
                 }];

            }else if ([self.intoType isEqualToString:@"修改性别"]){
                if (gender <0) {
                    [SVProgressHUD showImage:Nil status:@"请选择正确的性别"];
                    return;
                }
                [SVProgressHUD showWithStatus:@"提交中..." maskType:SVProgressHUDMaskTypeBlack];
                iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
                NSDictionary *dic = @{@"gender": [NSNumber numberWithInt:gender],
                                      };

                [[[content updateGender:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
                 subscribeNext:^(NSDictionary *object) {

                     if ([[object objectForKey:@"resultCode"] integerValue] == 1) {
                         [SVProgressHUD showImage:Nil status:@"修改成功"];
                         [self.navigationController popViewControllerAnimated:YES];
                     }else{
                         NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
                         [SVProgressHUD showImage:Nil status:codeStr];
                     }
                     
                 }];
            }else if ([self.intoType isEqualToString:@"修改简介"]){
                [SVProgressHUD showWithStatus:@"提交中..." maskType:SVProgressHUDMaskTypeBlack];
                iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
                NSDictionary *dic = @{@"intro": self.info_text.text,
                                      };

                [[[content updateIntro:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
                 subscribeNext:^(NSDictionary *object) {

                     if ([[object objectForKey:@"resultCode"] integerValue] == 1) {
                         [SVProgressHUD showImage:Nil status:@"修改成功"];
                         [self.navigationController popViewControllerAnimated:YES];
                     }else{
                         NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
                         [SVProgressHUD showImage:Nil status:codeStr];
                     }

                 }];
            }
            else if ([self.intoType isEqualToString:@"修改邮箱"]){
                [SVProgressHUD showWithStatus:@"提交中..." maskType:SVProgressHUDMaskTypeBlack];
                iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
                NSDictionary *dic = @{@"email": self.mail_text.text,
                                      };

                [[[content updateEmail:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
                 subscribeNext:^(NSDictionary *object) {

                     if ([[object objectForKey:@"resultCode"] integerValue] == 1) {
                         [SVProgressHUD showImage:Nil status:@"修改成功"];
                         [self.navigationController popViewControllerAnimated:YES];
                     }else{
                         NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
                         [SVProgressHUD showImage:Nil status:codeStr];
                     }
                     
                 }];
            }

        }
            break;

        default:
            break;
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UITextView Delegate Methods

- (void)textViewDidChange:(UITextView *)textView {

    NSInteger number = [textView.text length];
    if (number == 0) {
        self.make_lable.hidden = NO;
    }
    if (number > 0) {
        self.make_lable.hidden = YES;
    }
    if (number > 20) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"字符个数不能大于20" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alert show];
        textView.text = [textView.text substringToIndex:20];
        number = 20;
    }
    self.infoNum_lable.text = [NSString stringWithFormat:@"%d/20个字",number];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollview
{
    [self.name_text resignFirstResponder];
    [self.mail_text resignFirstResponder];
    [self.info_text resignFirstResponder];
}
@end
