//
//  iEver_EditUserMessageViewController.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-5.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEverBaseViewController.h"
#import "iEver_UserMessageObject.h"
@interface iEver_EditUserMessageViewController : iEverBaseViewController
@property (retain , nonatomic)iEver_personMessage_object *content_object;
@end
