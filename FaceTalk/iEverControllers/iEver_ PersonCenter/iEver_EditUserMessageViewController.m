//
//  iEver_EditUserMessageViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-5.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_EditUserMessageViewController.h"
#import "iEver_UserMessageObject.h"
#import "iEver_fixPersonMessageViewController.h"
#import "SJAvatarBrowser.h"
#import "QiniuSimpleUploader.h"
#import "iEver_UserMessageObject.h"
#import "iEver_phoneVerifyViewController.h"
#import "iEver_PersonalTagSettingViewController.h"

@interface iEver_EditUserMessageViewController ()<UITableViewDataSource,UITableViewDelegate>
{

}
@property (strong) IBOutlet UITableView           *tableView;
@end

@implementation iEver_EditUserMessageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

}
- (void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    [self fetchData];

}
- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"个人中心";

    /* request data */
    [self fetchData];

    //tableView------------------
    _tableView= [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - 64) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
   	[self.view addSubview:_tableView];

    
}
/*request data*/
-(void)fetchData{

    @weakify(self);
    iEver_UserMessageObject *detail_Object = [[iEver_UserMessageObject alloc] init];
    [[[detail_Object queryUserById:nil path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(iEver_personMessage_object *object) {
         @strongify(self);
         self.content_object = object;
         [_tableView reloadData];
     }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"MydetailCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        UIImage *image = [UIImage imageNamed:@"person_line"];
        UIImageView *cellIconIView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 44, 300, 1)];
        cellIconIView.image = image;
        [cell addSubview:cellIconIView];

            if (indexPath.row == 0) {

                UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(30, 10, 250, 24)];
                lable.textAlignment = NSTextAlignmentRight;
                lable.tag = 12;
                lable.textColor = GRAY_LIGHT;
                lable.font = TextFont;
                [cell.contentView addSubview:lable];
            }
            else  if (indexPath.row == 1) {

                UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(30, 10, 250, 24)];
                lable.textAlignment = NSTextAlignmentRight;
                lable.tag = 13;
                lable.textColor = GRAY_LIGHT;
                lable.font = TextFonts;
                [cell.contentView addSubview:lable];
            }
            else  if (indexPath.row == 2) {

                UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(30, 10, 250, 24)];
                lable.textAlignment = NSTextAlignmentRight;
                lable.tag = 14;
                lable.textColor = GRAY_LIGHT;
                lable.font = TextFont;
                [cell.contentView addSubview:lable];
            }
            else  if (indexPath.row == 3) {

                UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(30, 10, 200, 24)];
                lable.textAlignment = NSTextAlignmentRight;
                lable.tag = 15;
                lable.textColor = GRAY_LIGHT;
                lable.font = TextFont;
                [cell.contentView addSubview:lable];

                UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(240, 13, 40, 18)];
                imageView.tag = 25;
                [cell.contentView addSubview:imageView];
            }
        
            else if(indexPath.row == 4) {

                UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(30, 10, 200, 24)];
                lable.textAlignment = NSTextAlignmentRight;
                lable.tag = 16;
                lable.textColor = GRAY_LIGHT;
                lable.font = TextFont;
                [cell.contentView addSubview:lable];

                UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(240, 13, 40, 18)];
                imageView.tag = 26;
                [cell.contentView addSubview:imageView];

            }
        }
        cell.detailTextLabel.text = nil;

            NSArray *title = @[@"性别",@"个性定制",@"简介",@"邮箱",@"手机"];
            cell.textLabel.text = title[indexPath.row];
            switch (indexPath.row) {

                case 0: {

                    cell.detailTextLabel.text = nil;
                    UILabel *lable = (UILabel *)[cell.contentView viewWithTag:12];
                    if (self.content_object.gender == 0) {
                        lable.text = @"女";
                    }else {
                        lable.text = @"男";
                    }

                }
                    break;
                case 1: {
                    cell.detailTextLabel.text = nil;
                    UILabel *lable = (UILabel *)[cell.contentView viewWithTag:13];
                    lable.text = self.content_object.feature;
                }
                    break;
                case 2: {

                    cell.detailTextLabel.text = nil;
                    UILabel *lable = (UILabel *)[cell.contentView viewWithTag:14];
                    lable.text = self.content_object.intro;
                }
                    break;
                case 3: {
                    cell.detailTextLabel.text = nil;
                    UILabel *lable = (UILabel *)[cell.contentView viewWithTag:15];
                    lable.text = self.content_object.email;

                    UIImageView *imageview = (UIImageView *)[cell.contentView viewWithTag:25];
                    if (self.content_object.bindStatus == 10 ||self.content_object.bindStatus == 30) {
                        imageview.image = [UIImage imageNamed:@"person_comfirmed"];
                    }else{
                        imageview.image = [UIImage imageNamed:@"person_discomfirmed"];
                    }


                }
                    break;

                case 4:{

                    cell.detailTextLabel.text = nil;
                    UILabel *lable = (UILabel *)[cell.contentView viewWithTag:16];
                    lable.text = self.content_object.mobilePhone;
                    UIImageView *imageview = (UIImageView *)[cell.contentView viewWithTag:26];
                    if (self.content_object.bindStatus == 20 ||self.content_object.bindStatus == 30) {
                        imageview.image = [UIImage imageNamed:@"person_comfirmed"];
                    }else{
                        imageview.image = [UIImage imageNamed:@"person_discomfirmed"];
                    }

                }break;

                default:
                    break;
            }
    cell.textLabel.font = TextFont;
    return cell;
    
}
 - (void)handleSingleFingerEvent:(UITapGestureRecognizer *)sender
 {
     if (sender.numberOfTapsRequired == 1) {
          [SJAvatarBrowser showImage:(UIImageView*)sender.view];
     }
}
/*  10:更换头像；11：修改昵称；12：修改性别；13：修改简介；14：修改邮箱；15：修改手机号码 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

        switch (indexPath.row) {
            case 0:{
                iEver_fixPersonMessageViewController *_fixUserMessageViewController = [[iEver_fixPersonMessageViewController alloc]init];
                if (self.content_object.gender == 0) {
                    _fixUserMessageViewController.text = @"女";
                }else{
                    _fixUserMessageViewController.text = @"男";
                }
                _fixUserMessageViewController.intoType = @"修改性别";
                [self.navigationController pushViewController:_fixUserMessageViewController animated:YES];
                IOS7POP;

            }break;

            case 1:{

                iEver_PersonalTagSettingViewController *_PersonalTagSettingViewController = [[iEver_PersonalTagSettingViewController alloc]init];
                if (self.content_object.feature.length >0) {

                    _PersonalTagSettingViewController.userIsCustom =  @"DID";
                }else
                {
                    _PersonalTagSettingViewController.userIsCustom =  @"NOT";

                }
                _PersonalTagSettingViewController.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:_PersonalTagSettingViewController animated:YES];
                IOS7POP;

            }break;

            case 2:{

                iEver_fixPersonMessageViewController *_fixUserMessageViewController = [[iEver_fixPersonMessageViewController alloc]init];
                _fixUserMessageViewController.text = self.content_object.intro;
                _fixUserMessageViewController.intoType = @"修改简介";
                [self.navigationController pushViewController:_fixUserMessageViewController animated:YES];
                IOS7POP;



            }break;

            case 3:{

                if (self.content_object.bindStatus == 10 ||self.content_object.bindStatus == 30) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"邮箱已经验证通过" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                    [alert show];
                    return;
                }
                else if ((self.content_object.registerType == 10 || self.content_object.registerType == 30 )&& (self.content_object.bindStatus == 0 ||self.content_object.bindStatus == 20)){
                    //邮箱验证接口
                    [SVProgressHUD showWithStatus:@"提交中..." maskType:SVProgressHUDMaskTypeBlack];
                    iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
                    [[[content sendBindEmail:nil path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
                     subscribeNext:^(NSDictionary *object) {

                         if ([[object objectForKey:@"resultCode"] integerValue] == 1) {
                             [SVProgressHUD showImage:Nil status:@"绑定邮件发送成功"];
                         }else{
                             NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
                             [SVProgressHUD showImage:Nil status:codeStr];
                         }
                     }];

                }else if ((self.content_object.registerType == 20 || self.content_object.registerType == 30) && [self.content_object.email isEqualToString:@""]){

                    iEver_fixPersonMessageViewController *_fixUserMessageViewController = [[iEver_fixPersonMessageViewController alloc]init];

                    _fixUserMessageViewController.text = self.content_object.email;
                    _fixUserMessageViewController.intoType = @"修改邮箱";
                    [self.navigationController pushViewController:_fixUserMessageViewController animated:YES];
                    IOS7POP;

                }else if (self.content_object.registerType == 20 && ![self.content_object.email isEqualToString:@""]){

                    //邮箱验证接口
                    [SVProgressHUD showWithStatus:@"提交中..." maskType:SVProgressHUDMaskTypeBlack];
                    iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
                    [[[content sendBindEmail:nil path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
                     subscribeNext:^(NSDictionary *object) {

                         if ([[object objectForKey:@"resultCode"] integerValue] == 1) {
                             [SVProgressHUD showImage:Nil status:@"绑定邮件发送成功"];
                         }else{
                             NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
                             [SVProgressHUD showImage:Nil status:codeStr];
                         }
                     }];
                }



            }break;

            case 4:{

                if (self.content_object.bindStatus == 20 ||self.content_object.bindStatus == 30) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"手机已经验证通过" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                    [alert show];
                    return;
                }else if ( self.content_object.bindStatus == 0 ||self.content_object.bindStatus == 10){
                    /* 进入手机验证环节 */
                    iEver_phoneVerifyViewController *_phoneVerifyViewController = [[iEver_phoneVerifyViewController alloc]init];
                    [self.navigationController pushViewController:_phoneVerifyViewController animated:YES];
                    IOS7POP;
                }



            }break;

            default:
                break;
        }

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 45.0;
}


@end
