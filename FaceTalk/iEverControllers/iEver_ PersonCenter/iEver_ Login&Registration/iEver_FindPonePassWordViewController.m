//
//  iEver_FindPonePassWordViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/2/12.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_FindPonePassWordViewController.h"
#import "iEver_UserMessageObject.h"
@interface iEver_FindPonePassWordViewController ()

@property (strong) IBOutlet UIScrollView *scrollView;
@property (strong) IBOutlet UITextField  *userPhone_textField;
@property (strong) IBOutlet UITextField  *certificationCode_textField;
@property (strong) IBOutlet UITextField  *userPassWord_textField;
@property (strong) IBOutlet UITextField  *userPassWordV_textField;
@property (strong) IBOutlet UIButton     *C_button;
@property (strong) IBOutlet UIButton     *login_button;
@property(nonatomic ,copy)NSString       *verifyCode;

- (IBAction)requestCertificationCodeAction:(id)sender;
- (IBAction)nextAction:(id)sender;

@end

@implementation iEver_FindPonePassWordViewController

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    /* scrollView  */
    [self backScrollView];
}
-(void)backScrollView{

    _scrollView.scrollEnabled = YES;
    CGSize newSize = CGSizeMake(self.view.frame.size.width, 700.0);
    [_scrollView setContentSize:newSize];
    
}
- (void)viewDidLoad {

    [super viewDidLoad];
    _C_button.backgroundColor = yanzheng_N;
    self.title = @"手机找回密码";

    /* fix custom text Font */

    [self customButtonFont];
}

-(void)customButtonFont {

    self.userPhone_textField.font = TitleFont;
    self.userPassWord_textField.font = TitleFont;
    self.userPassWordV_textField.font = TitleFont;
    self.certificationCode_textField.font = TitleFont;

    self.login_button.titleLabel.font = Title_V_Fonts;
    self.login_button.backgroundColor = MY_PURPLECOLOR;
    CALayer *nextButton = [self.login_button layer];
    [nextButton setMasksToBounds:YES];
    [nextButton setCornerRadius:5.0];

    CALayer *certifiButton = [self.C_button layer];
    [certifiButton setMasksToBounds:YES];
    [certifiButton setCornerRadius:10.0];

    self.C_button.titleLabel.font = TextDESCFonts;
    self.C_button.backgroundColor = yanzheng_N;

    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)requestCertificationCodeAction:(id)sender{

    UIButton *CertifieyButton =(UIButton *)sender;

    if (![[iEver_Global Instance] validateMobile:self.userPhone_textField.text]) {
        [SVProgressHUD showImage:Nil status:@"请输入正确的手机号"];
        return;
    }

    /*Confirmation code  request mothod*/
    [self requestCertificationCodeInterface:CertifieyButton];


}

-(void)requestCertificationCodeInterface:(UIButton *)sender{

    __block int timeout= 60; //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0);
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout<=0){
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                [sender setTitle:@"发送验证码" forState:UIControlStateNormal];
                sender.userInteractionEnabled = YES;
                sender.backgroundColor = yanzheng_N;
            });
        }else{
            int seconds = timeout % 100;
            NSString *strTime = [NSString stringWithFormat:@"%.2d", seconds];
            dispatch_async(dispatch_get_main_queue(), ^{
                [sender setTitle:[NSString stringWithFormat:@"%@秒后重发",strTime] forState:UIControlStateNormal];
                sender.backgroundColor = yanzheng_C;
                sender.userInteractionEnabled = NO;
            });
            timeout--;
        }
    });
    dispatch_resume(_timer);

    iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
    NSDictionary *dic = @{@"mobilePhone": self.userPhone_textField.text,
                          };

    [[[content send2mobile:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(NSDictionary *object) {
         if ([[object objectForKey:@"resultCode"] integerValue] == 1) {

             [SVProgressHUD showImage:Nil status:@"验证码已经发送您手机，请注意查收"];
         }else{
             NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
             [SVProgressHUD showImage:Nil status:codeStr];
         }

     }];


}
- (IBAction)nextAction:(id)sender{


    if (![[iEver_Global Instance] validateMobile:self.userPhone_textField.text]) {
        [SVProgressHUD showImage:Nil status:@"请输入正确的手机号"];
        return;
    }

    if ( self.certificationCode_textField.text.length == 0) {
        [SVProgressHUD showImage:Nil status:@"输入验证码"];
        return;
    }

    if (self.userPassWord_textField.text.length > 20) {
        [SVProgressHUD showImage:Nil status:@"密码太长"];
        return;
    }
    if (self.userPassWord_textField.text.length < 6) {
        [SVProgressHUD showImage:Nil status:@"密码太短"];
        return;
    }
    if (![self.userPassWord_textField.text isEqualToString:self.userPassWordV_textField.text]) {
        [SVProgressHUD showImage:Nil status:@"两次输入不一致"];
        return;
    }

    iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
    NSDictionary *dic = @{@"mobilePhone": self.userPhone_textField.text,
                          @"verifyCode": self.certificationCode_textField.text,
                          @"password": self.userPassWord_textField.text,

                          };

    [[[content forgotUpdatePasswd:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(NSDictionary *object) {
         if ([[object objectForKey:@"resultCode"] integerValue] == 1) {

             [[NSUserDefaults standardUserDefaults] setObject:self.userPhone_textField.text forKey:@"account"];
             [[NSUserDefaults standardUserDefaults] synchronize];
             [SVProgressHUD showImage:Nil status:@"密码成功找回"];
             [self.navigationController popViewControllerAnimated:YES];

         }else{
             NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
             [SVProgressHUD showImage:Nil status:codeStr];
             return;
         }
     }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark -
#pragma mark UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollview
{
    [self.userPhone_textField resignFirstResponder];
    [self.certificationCode_textField resignFirstResponder];
    [self.userPassWord_textField resignFirstResponder];
    [self.userPassWordV_textField resignFirstResponder];
}
@end
