//
//  iEver_RegisterViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-15.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_RegisterViewController.h"
#import "iEver_OfficialListViewController.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "iEver_UserMessageObject.h"
#import "QiniuSimpleUploader.h"
#define photo_modeHeight 358
#define photo_mode_viewY   ScreenHeight - photo_modeHeight - 44 - 20

@interface iEver_RegisterViewController ()<UIImagePickerControllerDelegate,
UINavigationControllerDelegate,QiniuUploadDelegate>
{
    UIImage *headImage;
    NSString *fileName;

    BOOL uploadSucceeded;
}
@property (strong, nonatomic) QiniuSimpleUploader *sUploader;
@property (strong) IBOutlet UIScrollView      *scrollView;
@property (strong) IBOutlet UIImageView       *userPicture_imageView;
@property (strong) IBOutlet UITextField       *userName_textField;
@property (strong) IBOutlet UITextField       *userMail_textField;
@property (strong) IBOutlet UITextField       *userPassword_textField;
@property (strong) IBOutlet UIView            *photo_mode_view;
@property (strong) IBOutlet UIButton          *background_button;

@property (strong) IBOutlet UIButton          *comple_button;

@property (strong) IBOutlet UILabel      *selectPic_lable;

@property (strong) IBOutlet UILabel      *photo_lable;
@property (strong) IBOutlet UILabel      *pic_lable;

@property (strong) IBOutlet UIButton     *defaultPic_01;
@property (strong) IBOutlet UIButton     *defaultPic_02;
@property (strong) IBOutlet UIButton     *defaultPic_03;
@property (strong) IBOutlet UIButton     *defaultPic_04;
@property (strong) IBOutlet UIButton     *defaultPic_05;
@property (strong) IBOutlet UIButton     *defaultPic_06;
@property (strong) IBOutlet UIButton     *defaultPic_07;
@property (strong) IBOutlet UIButton     *defaultPic_08;
@property (strong) IBOutlet UIButton     *defaultPic_09;
@property (strong) IBOutlet UIButton     *defaultPic_10;

- (IBAction)registerAction:(id)sender;
- (IBAction)takePhotoAction:(id)sender;
- (IBAction)backgroundButtonPhotoAction:(id)sender;
- (IBAction)takeDefaultPhotoAction:(id)sender;
@end

@implementation iEver_RegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    /* scrollView  */
    [self backScrollView];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"邮箱注册";
    CALayer *coverImage = [_userPicture_imageView layer];
    [coverImage setMasksToBounds:YES];
    [coverImage setCornerRadius:150.0/2];

    /*user image modify notification */
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(headImageModifySusseed:)
                                                 name:@"headImageModifySusseedNotification"
                                               object:nil];

    /* fix custom text Font */

    [self customButtonFont];
}

-(void)customButtonFont {

    self.userName_textField.font = TitleFont;
    self.userMail_textField.font = TitleFont;
    self.userPassword_textField.font = Title_Font;
    self.comple_button.titleLabel.font = Title_V_Fonts;
    self.comple_button.backgroundColor = MY_PURPLECOLOR;
    CALayer *coverButton = [self.comple_button layer];
    [coverButton setMasksToBounds:YES];
    [coverButton setCornerRadius:5.0];
    self.selectPic_lable.font = Title_Font;

    self.photo_lable.font = TextFonts;
    self.pic_lable.font = TextFonts;

    CALayer *defaultPic_01_layer = [self.defaultPic_01 layer];
    [defaultPic_01_layer setMasksToBounds:YES];
    [defaultPic_01_layer setCornerRadius:5.0];

    CALayer *defaultPic_02_layer = [self.defaultPic_02 layer];
    [defaultPic_02_layer setMasksToBounds:YES];
    [defaultPic_02_layer setCornerRadius:5.0];

    CALayer *defaultPic_03_layer = [self.defaultPic_03 layer];
    [defaultPic_03_layer setMasksToBounds:YES];
    [defaultPic_03_layer setCornerRadius:5.0];

    CALayer *defaultPic_04_layer = [self.defaultPic_04 layer];
    [defaultPic_04_layer setMasksToBounds:YES];
    [defaultPic_04_layer setCornerRadius:5.0];

    CALayer *defaultPic_05_layer = [self.defaultPic_05 layer];
    [defaultPic_05_layer setMasksToBounds:YES];
    [defaultPic_05_layer setCornerRadius:5.0];

    CALayer *defaultPic_06_layer = [self.defaultPic_06 layer];
    [defaultPic_06_layer setMasksToBounds:YES];
    [defaultPic_06_layer setCornerRadius:5.0];

    CALayer *defaultPic_07_layer = [self.defaultPic_07 layer];
    [defaultPic_07_layer setMasksToBounds:YES];
    [defaultPic_07_layer setCornerRadius:5.0];

    CALayer *defaultPic_08_layer = [self.defaultPic_08 layer];
    [defaultPic_08_layer setMasksToBounds:YES];
    [defaultPic_08_layer setCornerRadius:5.0];

    CALayer *defaultPic_09_layer = [self.defaultPic_09 layer];
    [defaultPic_09_layer setMasksToBounds:YES];
    [defaultPic_09_layer setCornerRadius:5.0];

    CALayer *defaultPic_10_layer = [self.defaultPic_10 layer];
    [defaultPic_10_layer setMasksToBounds:YES];
    [defaultPic_10_layer setCornerRadius:5.0];

    
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)backScrollView{

    _scrollView.scrollEnabled = YES;
    CGSize newSize = CGSizeMake(self.view.frame.size.width, 700.0);
    [_scrollView setContentSize:newSize];

}
#pragma mark
#pragma mark - user take a photo as user Head picture
- (IBAction)takePhotoAction:(id)sender{

    [self Mode_viewAppear];
    [self.userMail_textField resignFirstResponder];
    [self.userName_textField resignFirstResponder];
    [self.userPassword_textField resignFirstResponder];
    
}
#pragma mark
#pragma mark - photo view appear/disappear
- (void)Mode_viewDisappear
{
    self.background_button.hidden = YES;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    self.photo_mode_view.frame = CGRectMake(0, ScreenHeight, ScreenWidth, photo_modeHeight);
    [UIView commitAnimations];
}

- (void)Mode_viewAppear
{
    self.background_button.hidden = NO;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    self.photo_mode_view.frame = CGRectMake(0, photo_mode_viewY, ScreenWidth, photo_modeHeight);
    [UIView commitAnimations];
}

#pragma mark
#pragma mark - Mail Register Action
- (IBAction)registerAction:(id)sender{

    if (fileName.length == 0) {
        [SVProgressHUD showImage:Nil status:@"请添加您的头像"];
        return;
    }
    if (self.userName_textField.text.length == 0 ) {
        [SVProgressHUD showImage:Nil status:@"昵称为空"];
        return;
    }
    if (self.userName_textField.text.length < 2 ) {
        [SVProgressHUD showImage:Nil status:@"昵称太短"];
        return;
    }
    if (self.userName_textField.text.length > 15 ) {
        [SVProgressHUD showImage:Nil status:@"昵称太长"];
        return;
    }
    if (![iEver_Global validateEmail:self.userMail_textField.text]) {
        [SVProgressHUD showImage:Nil status:@"请输入正确的邮箱"];
        return;
    }
    if (self.userPassword_textField.text.length > 20) {
        [SVProgressHUD showImage:Nil status:@"密码太长"];
        return;
    }
    if ( self.userPassword_textField.text.length < 6) {
        [SVProgressHUD showImage:Nil status:@"密码太短"];
        return;
    }
    /*Confirmation right */
    [self submitRegisterRequest];
}

-(void)submitRegisterRequest{

    BOOL isNetworkWorking = [[iEver_Global Instance] isExistenceNetwork];
    if (!isNetworkWorking) {
        UIAlertView *myalert = [[UIAlertView alloc] initWithTitle:@"警告"
                                                          message:NetWorkError
                                                         delegate:nil
                                                cancelButtonTitle:@"确认"
                                                otherButtonTitles:nil];
        [myalert show];
    }else {

        [SVProgressHUD showWithStatus:@"提交中..." maskType:SVProgressHUDMaskTypeBlack];
        iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
        NSDictionary *dic = @{@"nickName": self.userName_textField.text,
                              @"email": self.userMail_textField.text,
                              @"password": self.userPassword_textField.text,
                              @"headImg": fileName,
                              @"pushToken": [APService registrationID],
                              @"deviceType": @"ios"
                              };

        [[[content emailRegister:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
         subscribeNext:^(NSDictionary *object) {
             if ([[object objectForKey:@"resultCode"] integerValue] == 1) {

                 BOOL isLogin = YES;
                 NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                 [defaults setBool:isLogin forKey:@"isLogin"];
                 [defaults setObject:[object objectForKey:@"loginKey"] forKey:@"loginKey"];
                 [defaults setObject:[object objectForKey:@"userId"] forKey:@"userId"];
                 [defaults setInteger:[[object objectForKey:@"userType"] integerValue] forKey:@"userType"];
                 [defaults setObject:self.userName_textField.text forKey:@"account"];
                 [defaults synchronize];

                 if ([self.delegate respondsToSelector:@selector(delegateMethod)]) {
                     [self.delegate delegateMethod];
                 }
                 [SVProgressHUD showImage:Nil status:@"注册成功"];
                 [self.navigationController popToRootViewControllerAnimated:YES];
             }else{
                 NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
                 [SVProgressHUD showImage:Nil status:codeStr];
             }
             
         }];
    }


}

#pragma mark
#pragma mark - backgroundButton Action
/**
 *   @param    tag = 10 背景按钮 ;11 取消;12 拍照; 13 相册;
 *
 */
- (IBAction)backgroundButtonPhotoAction:(id)sender{

    NSLog(@"sender----->%@",sender);
    UIButton *button = (UIButton *)sender;
	NSInteger index = button.tag - 10;
    switch (index) {
        case 0:
        {

            [self Mode_viewDisappear];

        }
            break;
        case 1:
        {

            [self Mode_viewDisappear];
        }
            break;
        case 2:
        {

            [self Mode_viewDisappear];
            
            UIImagePickerController *myPicker  =[[UIImagePickerController alloc] init];
            myPicker.delegate = self;
            myPicker.allowsEditing = YES;
            myPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:myPicker animated:YES completion:^{
            }];
        }
            break;
        case 3:
        {

            [self Mode_viewDisappear];
            UIImagePickerController *myPicker  =[[UIImagePickerController alloc] init];
            myPicker.delegate = self;
            myPicker.allowsEditing = YES;
            myPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:myPicker animated:YES completion:^{
            }];
        }
            break;

            
        default:
            break;
    }
    
    
}
/**
 *   @param    默认头像 1-10 tag;
 *             那么： http://iever.qiniudn.com/img/user/headImg_3.png
 */
- (IBAction)takeDefaultPhotoAction:(id)sender{

    [self Mode_viewDisappear];
    UIButton *button = (UIButton *)sender;
    NSInteger index = button.tag - 20;
    NSString *photoStr = [NSString stringWithFormat:@"p%d",index];
    NSString *fileNameStr = [NSString stringWithFormat:@"headImg_%d.png",index];
    fileName = fileNameStr;
    _userPicture_imageView.image = [UIImage imageNamed:photoStr];

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -

- (void)uploadContent:(NSDictionary *)info
{
    [SVProgressHUD showWithStatus:@"上传中..." maskType:SVProgressHUDMaskTypeBlack];
     iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
    @weakify(self);
    NSString *_pathUrl = [NSString stringWithFormat:@"qiniu/getUpToken/userHeadImg/png"];
    [[content fetchPictureSiteToken:nil path:_pathUrl] subscribeNext:^(NSDictionary *dic) {
        if ([[dic objectForKey:@"resultCode"] integerValue] == 1) {
            @strongify(self);

            self.sUploader = [QiniuSimpleUploader uploaderWithToken:[dic objectForKey:@"uptoken"]];
            self.sUploader.delegate = self;

            UIImage *image = [info objectForKey: @"UIImagePickerControllerEditedImage"];
            NSString *key = [NSString stringWithFormat:@"%@", [dic objectForKey:@"fileName"]];
            NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:key];

            NSData *data = UIImageJPEGRepresentation(image, 1);
            [data writeToFile:filePath atomically:YES];
            [self.sUploader uploadFile:filePath key:[dic objectForKey:@"fullPath"] extra:nil];
            fileName = [dic objectForKey:@"fileName"];
        }else{

            NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[dic objectForKey:@"resultCode"] integerValue]];
            [SVProgressHUD showImage:Nil status:codeStr];
        }
    }];
}

#pragma mark -

- (void)uploadSucceeded:(NSString *)filePath ret:(NSDictionary *)ret
{
    uploadSucceeded = YES;
    [SVProgressHUD showSuccessWithStatus:@"上传成功"];

}

- (void)uploadFailed:(NSString *)filePath error:(NSError *)error
{
    fileName = @"";
    _userPicture_imageView.image = [UIImage imageNamed:@"fail_pic"];
    uploadSucceeded = NO;
    [SVProgressHUD showErrorWithStatus:@"上传失败"];
    iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
    NSString *_pathUrl = [NSString stringWithFormat:@"qiniu/uploadErr"];
    NSDictionary *dic = @{@"error_log": error
                          };
    [[content uploadErr:dic path:_pathUrl] subscribeNext:^(NSDictionary *dic) {
    }];
}


#pragma mark -
#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController: (UIImagePickerController *)picker didFinishPickingMediaWithInfo: (NSDictionary *)info
{
    // for iOS7
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {

        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        //来自照相机的image，那么先保存
        UIImage *original_image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        UIImageWriteToSavedPhotosAlbum(original_image, self,
                                       nil,
                                       nil);
    }

    //获得图片,并上传
    UIImage *image = [info objectForKey: @"UIImagePickerControllerEditedImage"];
    [self uploadContent:info];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"headImageModifySusseedNotification" object:image];
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];

}

/*user image modify notification  mothod*/
-(void)headImageModifySusseed:(NSNotification *)info{

    headImage = [info object];
    _userPicture_imageView.image = headImage;


}

#pragma mark -
#pragma mark - textField Delegate
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{

    if (textField == self.userName_textField) {

        if (self.userName_textField.text.length == 0 ) {
            return YES;
        }
        /*Confirmation userName code write here */
    }
    if (textField == self.userMail_textField) {

        if (self.userMail_textField.text.length == 0 ) {
            return YES;
        }
        /*Confirmation userMail code write here */
    }

   return YES;

}

#pragma mark -
#pragma mark UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollview
{
    [self.userName_textField resignFirstResponder];
    [self.userMail_textField resignFirstResponder];
    [self.userPassword_textField resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}
@end
