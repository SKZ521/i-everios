//
//  iEver_PhoneRegister_NextViewController.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-15.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEverBaseViewController.h"
@class iEver_MainViewController;
@interface iEver_PhoneRegister_NextViewController : iEverBaseViewController
@property(nonatomic ,copy)NSString *iphoneNum;
@property(nonatomic ,copy)NSString *verifyCode;
@property (strong) id delegate;
@end
