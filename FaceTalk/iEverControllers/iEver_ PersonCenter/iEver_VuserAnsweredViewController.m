//
//  iEver_VuserAnsweredViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 14/11/21.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_VuserAnsweredViewController.h"
#import "iEver_V_answeredQuestionObject.h"
#import "iEver_V_answeredCell.h"
@interface iEver_VuserAnsweredViewController ()

@end

@implementation iEver_VuserAnsweredViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"已回答问题";
    /* NO show pull refresh */
    self.tableView.showsPullToRefresh =NO;
    self.tableView.showsInfiniteScrolling = NO;

    /* set default backgroundView */
    self.tableView.backgroundView = ({
        UIImageView *_backImageView = [[UIImageView alloc] initWithFrame:self.view.frame];
        _backImageView.image = [UIImage imageNamed:@"backImage"];
        _backImageView;
    });
    [self.tableView  setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    /* request data */
    [self fetchData];
}
/*request data*/
-(void)fetchData{

    @weakify(self);
    iEver_V_answeredQuestionObject *question_Object = [[iEver_V_answeredQuestionObject alloc] init];
    int userId =  [[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] intValue];
    NSString *_pathUrl = [NSString stringWithFormat:@"/expertQuestion/queryAnswered/%d/%d",userId,self.page];
    [[[question_Object queryAnsweredByUser:nil path:_pathUrl] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(iEver_V_answeredQuestionObject *object) {

         @strongify(self);
         [self addDataToDataSource:object._quesList];
         [self.tableView reloadData];
         
     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataSourceArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier  = @"VUserAnsweredCell";
    UITableViewCell *cell = nil;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell = (iEver_V_answeredCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    // Configure the cell...
    if (!cell) {
        cell = [[iEver_V_answeredCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    iEver_V_answeredCell *dic_cell = (iEver_V_answeredCell *)cell;
    iEver_V_quesList_object * object = [self.dataSourceArray objectAtIndex:indexPath.row];
    [dic_cell setViewController:self.navigationController];
    [dic_cell setObject:object];
    return cell;

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    id object = [self.dataSourceArray objectAtIndex:indexPath.row];
    return [iEver_V_answeredCell heightForObject:object atIndexPath:indexPath tableView:tableView];

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

@end
