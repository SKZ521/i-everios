//
//  iEver_fixPersonMessageViewController.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-9.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEverBaseViewController.h"

@interface iEver_fixPersonMessageViewController : iEverBaseViewController
@property (copy, nonatomic) NSString *intoType;
@property (copy, nonatomic) NSString *text;
@end
