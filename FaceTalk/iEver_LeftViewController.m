//
//  iEver_LeftViewController.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/5/18.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_LeftViewController.h"
#import "iEver_UserMessageObject.h"
#import "iEver_MYcell.h"

#import "iEver_EditUserMessageViewController.h"
#import "iEver_PersonalTagSettingViewController.h"
#import "iEver_SettingViewController.h"

#import "iEver_OfficialListViewController.h"
#import "iEver_normalUserAnsweredQesViewController.h"
#import "iEver_normalUserUnAnswerQesViewController.h"
#import "iEver_VuserAnsweredViewController.h"
#import "iEver_VuserUnAnswerdeViewController.h"
#import "iEver_VuserShareArticleViewController.h"
#import "iEver_MY_ItemTryViewController.h"
#import "iEver_MessageViewController.h"
#import "iEver_LoginViewController.h"

#import "iEver_AskAswerPagesTableViewController.h"
#import "iEver_VuserAnsweredQuestionViewController.h"

#import "JBKenBurnsView.h"

#import "QiniuSimpleUploader.h"

#import "iEver_ColorTagsView.h"


#define DRAWERWidth         48.0f
#define USERPHOTOHieght     190.0f
#define BASEUSERHieght      140.0f
#define ENTERUserCHieght    52.0f
#define ENTERSettingHieght  70.0f

#define TableFootViewHieght 133.0f



@interface iEver_LeftViewController ()  <KenBurnsViewDelegate,UIImagePickerControllerDelegate,
UINavigationControllerDelegate,QiniuUploadDelegate,UITextFieldDelegate>

{

    UITableView *table;
    NSMutableArray *tableDataArray;
    UILabel      *tagStr;

    UIImage *headImage;
    NSString *fileName;
    UIImageView *headImageView;
    UITextField *userName;
    UILabel *userType;
    UIButton *enterLoginButton;
    BOOL uploadSucceeded;


}

@property(nonatomic ,retain) KenBurnsView *userPhotoBGImageView;
@property (strong, nonatomic) QiniuSimpleUploader *sUploader;
@end

@implementation iEver_LeftViewController

/*request data*/
- (void)fetchData
{
    iEver_UserMessageObject *detail_Object = [[iEver_UserMessageObject alloc] init];
    [[[detail_Object queryUserById:nil path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(iEver_personMessage_object *object) {

         /* 用户头像 */
         [headImageView setImageWithURL:[NSURL URLWithString:object.headImg] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];

         /* 用户昵称 */
         NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
         CGSize textSize     =[object.nickName boundingRectWithSize:CGSizeMake(250.0, 20)
                                                           options:NSStringDrawingUsesLineFragmentOrigin
                                                        attributes:tdic context:nil].size;
         userName.text = object.nickName;
         UIImage *person_fixMessage = [UIImage imageNamed:@"person_fixMessage"];
         userName.frame = CGRectMake((ScreenWidth  - DRAWERWidth)/2 - textSize.width/2, headImageView.frame.origin.y + headImageView.frame.size.height + 20.0, textSize.width + person_fixMessage.size.width, 20);

         UIImageView *image=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"person_fixMessage"]];
         userName.rightView=image;
         userName.rightViewMode = UITextFieldViewModeAlways;

         UIView *dzBaseView = (UIView *)[self.tableView.tableHeaderView viewWithTag:115];

         UIView *titleView = (UIView *)[dzBaseView viewWithTag:1150];
         iEver_ColorTagsView *tagView = (iEver_ColorTagsView *)[dzBaseView viewWithTag:1151];
         UIButton *personTag_button = (UIButton *)[dzBaseView viewWithTag:1152];
         if (object.feature && [object.feature length]>0)
         {
             // 已定制
             titleView.hidden = NO;
             tagView.hidden = NO;
             personTag_button.hidden = YES;

             NSArray *tags = [object.feature componentsSeparatedByString:@" "];
             [tagView setTags:tags];

             NSArray *colors = @[[UIColor colorWithRed:252.0f/255.0f green:199.0f/255.0f blue:11.0f/255.0f alpha:1.0f],
                                 [UIColor colorWithRed:187.0f/255.0f green:166.0f/255.0f blue:248.0f/255.0f alpha:1.0f],
                                 [UIColor colorWithRed:253.0f/255.0f green:166.0f/255.0f blue:202.0f/255.0f alpha:1.0f],
                                 [UIColor colorWithRed:104.0f/255.0f green:231.0f/255.0f blue:209.0f/255.0f alpha:1.0f],
                                 [UIColor colorWithRed:102.0f/255.0f green:73.0f/255.0f blue:150.0f/255.0f alpha:1.0f],
                                 [UIColor colorWithRed:228.0f/255.0f green:.0f/255.0f blue:101.0f/255.0f alpha:1.0f],
                                 [UIColor colorWithRed:255.0f/255.0f green:110.0f/255.0f blue:16.0f/255.0f alpha:1.0f]];
             if ([colors count]>=[tags count])
             {
                 [tagView setColors:colors];
             }
         } else {
             // 未定制
             titleView.hidden = YES;
             tagView.hidden = YES;
             personTag_button.hidden = NO;
         }
     }];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isLogin"]) {
        userName.hidden = NO;
        userType.hidden = NO;
        enterLoginButton.hidden = YES;
    }else {

        userName.hidden = YES;
        userType.hidden = YES;
        enterLoginButton.hidden = NO;
        headImageView.image = [UIImage imageNamed:@"defualt_icon"];
    }
    
   
}
- (void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];

    /* request data */
    [self fetchData];

    [UIView animateWithDuration:.3 animations:^{
        if (iOS7) {
            NSLog(@"setNeedsStatusBarAppearanceUpdate");
            [self setNeedsStatusBarAppearanceUpdate];
        }
    }];


}

- (void)viewDidLoad {

    [super viewDidLoad];

    /* NO show pull refresh */
    self.tableView.showsPullToRefresh = NO;
    self.tableView.showsInfiniteScrolling = NO;

    [self.tableView  setSeparatorStyle:UITableViewCellSeparatorStyleNone];

    NSArray * titleArray                 = @[@"我的消息",@"我的试用",@"赞过的文章",@"我的回答",@"我的提问"];
    NSArray * imageArray                 = @[@"MY_icon1",@"MY_icon2",@"MY_icon3",@"MY_icon6",@"MY_icon7"];
    NSArray * lineIsHiddenArray          = @[[NSNumber numberWithBool:NO],[NSNumber numberWithBool:YES],[NSNumber numberWithBool:NO],[NSNumber numberWithBool:YES],[NSNumber numberWithBool:YES]];
    tableDataArray = [[NSMutableArray alloc] initWithCapacity:5];

    for (int i = 0; i < 5 ; i++) {

        iEver_personMessage_object *content_object = [[iEver_personMessage_object alloc] init];
        content_object.section4Title               = titleArray[i];
        content_object.section4image               = imageArray[i];
        content_object.lineHidden                  = [lineIsHiddenArray[i] boolValue];
        [tableDataArray addObject:content_object];
    }

    self.tableView.frame = CGRectMake(0.0, USERPHOTOHieght, ScreenWidth, ScreenHeight - USERPHOTOHieght);

    self.view.backgroundColor = WHITE;

    /* 用户头像区域 */
    [self userMassageAreaViewAsSelfHeaderView];

    /* 用户个人中心和用户定制区域 */
    [self userCenterAndPersonalizationAsTableViweHeader];


    /* 用户个人中心和用户定制区域 */
    [self settingAsTableViweFooter];

    /*user image modify notification */
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(headImageModifySusseed:) name:@"headImageModifySusseedNotification" object:nil];
}

/*user image modify notification  mothod*/
-(void)headImageModifySusseed:(NSNotification *)info{

    headImage = [info object];
    headImageView.image = headImage;
}

-(void)userMassageAreaViewAsSelfHeaderView{


    /* 背景闪动的图片 */

    self.userPhotoBGImageView = [[KenBurnsView alloc] initWithFrame:CGRectMake(0.0, 0.0, ScreenWidth - DRAWERWidth, USERPHOTOHieght)];

    self.userPhotoBGImageView.layer.borderColor = [UIColor blackColor].CGColor;
    self.userPhotoBGImageView.delegate = self;

    NSArray *myImages = [NSArray arrayWithObjects:
                         [UIImage imageNamed:@"leftMyViewBg1"],
                         [UIImage imageNamed:@"leftMyViewBg2"],
                         [UIImage imageNamed:@"leftMyViewBg3"], nil];

    [self.userPhotoBGImageView animateWithImages:myImages
                 transitionDuration:5
                               loop:YES
                        isLandscape:YES];


    [self.view addSubview:self.userPhotoBGImageView];


    /* 一层白色背景 leftMyViewWhiteBg */

    UIImage *leftMyViewWhiteBg = [UIImage imageNamed:@"leftMyViewWhiteBg"];

    UIImageView *whiteBgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, USERPHOTOHieght - leftMyViewWhiteBg.size.height, ScreenWidth, leftMyViewWhiteBg.size.height)];
    whiteBgImageView.image = leftMyViewWhiteBg;

    [self.view addSubview:whiteBgImageView];
    

    /* 用户头像 */

    headImageView = [[UIImageView alloc] initWithFrame:CGRectMake((ScreenWidth  - DRAWERWidth)/2 - 70.0/2, 35.0, 70.0, 70.0)];
    headImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapTuserPhotoAction:)];
    tapGesture.numberOfTapsRequired = 1;
    [headImageView addGestureRecognizer:tapGesture];
    headImageView.backgroundColor = CLEAR;
    CALayer *photoLayer= [headImageView layer];
    [photoLayer setBorderWidth:1.0];
    [photoLayer setBorderColor:[WHITE CGColor]];
    [photoLayer setMasksToBounds:YES];
    [photoLayer setCornerRadius:70.0/2];
    headImageView.contentMode = UIViewContentModeScaleAspectFill;

    [self.view addSubview:headImageView];

    /* 用户昵称 */

    userName = [[UITextField alloc] init];
    userName.textColor = BLACK;
    userName.textAlignment = NSTextAlignmentCenter;
    userName.font = TextFont;
    userName.backgroundColor = CLEAR;
    userName.keyboardType = UIKeyboardTypeDefault;
    userName.delegate = self;

    [self.view addSubview:userName];


    /* 如果用户没有登录 显示“立刻登录” */

    enterLoginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    enterLoginButton.frame = CGRectMake((ScreenWidth  - DRAWERWidth)/2 - 200/2, USERPHOTOHieght - 70.0, 200, 40);
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@"立刻登录"];
    NSRange strRange = {0,[str length]};
    [str addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:strRange];
    [enterLoginButton setAttributedTitle:str forState:UIControlStateNormal];
    [enterLoginButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    enterLoginButton.titleLabel.font = TextFont;
    enterLoginButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        iEver_LoginViewController *_LoginViewController = [[iEver_LoginViewController alloc]init];
        _LoginViewController.navBarType = kCloseButton;
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:_LoginViewController];
        [self presentViewController:nav animated:YES completion:nil];
        return [RACSignal empty];
    }];

    [self.view addSubview:enterLoginButton];

    /* 用户类型 */

    userType = [[UILabel alloc] initWithFrame:CGRectMake((ScreenWidth  - DRAWERWidth)/2 - 200.0/2, USERPHOTOHieght - 40.0, 200.0, 20)];
    userType.numberOfLines = 1;
    userType.textColor = LINE_GRAY;
    userType.textAlignment = NSTextAlignmentCenter;
    userType.font = TextFont;
    userType.backgroundColor = CLEAR;
    userType.text = @"普通会员";

    [self.view addSubview:userType];
    

}

/* 选择图片方式 */
- (void)tapTuserPhotoAction:(UITapGestureRecognizer *)gesture {

    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isLogin"]) {

        @weakify(self);

        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                 delegate:nil
                                                        cancelButtonTitle:@"取消"
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:@"拍照",@"从相册选择", nil];
        [actionSheet.rac_buttonClickedSignal subscribeNext:^(id x) {
            @strongify(self);

            NSInteger aIndex = [x integerValue];

            UIImagePickerController *myPicker  =[[UIImagePickerController alloc] init];
            myPicker.delegate = self;
            myPicker.allowsEditing = YES;

            if (aIndex == 0) {
                myPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:myPicker animated:YES completion:^{

                }];
            }else if (aIndex == 1) {
                myPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                [self presentViewController:myPicker animated:YES completion:^{
                    
                }];
            }
        }];
        
        [actionSheet showInView:self.view.window];
    }
    else{

        iEver_LoginViewController *_LoginViewController = [[iEver_LoginViewController alloc]init];
        _LoginViewController.navBarType = kCloseButton;
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:_LoginViewController];
        [self presentViewController:nav animated:YES completion:nil];

    }
    [userName resignFirstResponder];


}


-(void)userCenterAndPersonalizationAsTableViweHeader{

    /* Initialization */
    CGFloat x = 0.0;
    CGFloat y = 0.0;
    CGFloat width = 0.0;
    CGFloat height = 0.0;

    /* 个人中心user center */
    x = 0.0 ;
    y = 0.0;
    width = ScreenWidth - DRAWERWidth;
    height = BASEUSERHieght;
    UIView *tableHeadBaseView = [[UIView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    tableHeadBaseView.backgroundColor = WHITE;


    /* 个人中心上横线 */
    x = 15.0 ;
    y = 0.0;
    width = ScreenWidth - DRAWERWidth - 2 * 15.0;
    height = 1.0;
    UIImageView *upLine_image = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    upLine_image.image = [UIImage imageNamed:@"person_line"];

    [tableHeadBaseView addSubview:upLine_image];

    /* 个人中心 小图标 */
    UIImage *MY_icon1 = [UIImage imageNamed:@"MY_icon0"];
    x = 15.0 ;
    y = 15.0;
    width = MY_icon1.size.width;
    height = MY_icon1.size.height;
    UIImageView *userC_image = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    userC_image.image = MY_icon1;

    [tableHeadBaseView addSubview:userC_image];


    /* “个人中心” */
    x = 56.0 ;
    y = 16.0;
    width = 100;
    height = 20;
    UILabel *userCenterLable = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    userCenterLable.numberOfLines = 1;
    userCenterLable.textColor = BLACK;
    userCenterLable.font = TextFont;
    userCenterLable.text = @"个人中心";
    userCenterLable.backgroundColor = CLEAR;

    [tableHeadBaseView addSubview:userCenterLable];

    /* 个人中心 arrow 小图标 */
    UIImage *VuserCenter_arrow = [UIImage imageNamed:@"VuserCenter_arrow"];
    x = 235.0 ;
    y = 20.0;
    width = VuserCenter_arrow.size.width;
    height = VuserCenter_arrow.size.height;
    UIImageView *userCArrow_image = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    userCArrow_image.image = VuserCenter_arrow;

    [tableHeadBaseView addSubview:userCArrow_image];


    /* 进入个人中心按钮 */
    x = 0.0 ;
    y = 0.0;
    width = ScreenWidth;
    height = ENTERUserCHieght;
    UIButton *enterUserCenterButton = [UIButton buttonWithType:UIButtonTypeCustom];
    enterUserCenterButton.frame = CGRectMake(x, y, width, height);
    enterUserCenterButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isLogin"]) {

            iEver_EditUserMessageViewController *_EditUserMessageViewController = [[iEver_EditUserMessageViewController alloc]init];
            _EditUserMessageViewController.navBarType = kCloseButton;
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:_EditUserMessageViewController];
            [self presentViewController:nav animated:YES completion:nil];
        }
        else{

            iEver_LoginViewController *_LoginViewController = [[iEver_LoginViewController alloc]init];
            _LoginViewController.navBarType = kCloseButton;
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:_LoginViewController];
            [self presentViewController:nav animated:YES completion:nil];

            }


        return [RACSignal empty];
    }];

    [tableHeadBaseView addSubview:enterUserCenterButton];


    /* 个人中心下横线 */
    x = 15.0 ;
    y = ENTERUserCHieght;
    width = ScreenWidth - DRAWERWidth - 2 * 15.0;
    height = 1.0;
    UIImageView *downLine_image = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    downLine_image.image = [UIImage imageNamed:@"person_line"];

    [tableHeadBaseView addSubview:downLine_image];


    /* 个性定制区域 */
    x = 15.0f;
    y = ENTERUserCHieght;
    width = ScreenWidth - DRAWERWidth - 2 * 15.0;
    height = BASEUSERHieght - ENTERUserCHieght;
    UIView *personTagBaseView = [[UIView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    personTagBaseView.tag = 115;
    personTagBaseView.backgroundColor = CLEAR;
    [tableHeadBaseView addSubview:personTagBaseView];

    // 已定制
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(personTagBaseView.frame.size.width/2 - 99.0f/2, 5.0f, 99.0f, 25.0f)];
    [titleView setBackgroundColor:CLEAR];
    titleView.tag = 1150;
    titleView.hidden = YES;
    [personTagBaseView addSubview:titleView];

    UIImageView *dzImageView = [[UIImageView alloc] initWithFrame:CGRectMake(.0f, (CGRectGetHeight(titleView.frame)-14.0f)/2, 14.0f, 14.0f)];
    [dzImageView setImage:[UIImage imageNamed:@"MY_dz_icon"]];
    [dzImageView setBackgroundColor:CLEAR];
    [titleView addSubview:dzImageView];

    UILabel *dzLabel = [[UILabel alloc] initWithFrame:CGRectMake(14.0f, .0f, 60.0f, 25.0f)];
    dzLabel.textColor = BLACK;
    dzLabel.font = TextFont;
    dzLabel.text = @"个人定制";
    [dzLabel setTextAlignment:NSTextAlignmentCenter];
    dzLabel.backgroundColor = CLEAR;
    [titleView addSubview:dzLabel];

    UIButton *dzEditButton = [UIButton buttonWithType:UIButtonTypeCustom];
    dzEditButton.frame = CGRectMake(74.0f, .0f, 25.0f, 25.0f);
    [dzEditButton setBackgroundImage:[UIImage imageNamed:@"MY_edit"] forState:UIControlStateNormal];
    dzEditButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        iEver_PersonalTagSettingViewController *_PersonalTagSettingViewController = [[iEver_PersonalTagSettingViewController alloc]init];
        _PersonalTagSettingViewController.navBarType = kCloseButton;
        _PersonalTagSettingViewController.userIsCustom = @"DID";
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:_PersonalTagSettingViewController];
        [self presentViewController:nav animated:YES completion:nil];
        return [RACSignal empty];
    }];
    [titleView addSubview:dzEditButton];

    iEver_ColorTagsView* tagList = [[iEver_ColorTagsView alloc] initWithFrame:CGRectMake(.0f, CGRectGetMaxY(titleView.frame)+5.0f, CGRectGetWidth(personTagBaseView.frame), CGRectGetHeight(personTagBaseView.frame)-CGRectGetHeight(titleView.frame))];
    [tagList setAutomaticResize:YES];
    tagList.tag = 1151;
    tagList.hidden = YES;
    // Customisation
    [tagList setTagBackgroundColor:[UIColor clearColor]];
    [tagList setBorderWidth:.5f];
    [tagList setTextShadowColor:[UIColor clearColor]];
    [tagList setViewOnly:YES];
    [personTagBaseView addSubview:tagList];

    // 未定制
    UIImage *MY_dingzi = [UIImage imageNamed:@"MY_dingzih"];
    [personTagBaseView addSubview:({
        x = .0f;
        y = personTagBaseView.frame.size.height/2 - MY_dingzi.size.height/2;
        width = MY_dingzi.size.width;
        height = MY_dingzi.size.height;

        UIButton *personTag_button = [UIButton buttonWithType:UIButtonTypeCustom];
        personTag_button.tag = 1152;
        personTag_button.frame = CGRectMake(x, y, width, height);
        [personTag_button setBackgroundImage:MY_dingzi forState:UIControlStateNormal];
        [personTag_button setBackgroundImage:MY_dingzi forState:UIControlStateHighlighted];
        personTag_button.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isLogin"]) {
                iEver_PersonalTagSettingViewController *_PersonalTagSettingViewController = [[iEver_PersonalTagSettingViewController alloc]init];
                _PersonalTagSettingViewController.navBarType = kCloseButton;
                _PersonalTagSettingViewController.userIsCustom = @"NOT";
                UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:_PersonalTagSettingViewController];
                [self presentViewController:nav animated:YES completion:nil];
            }
            else{

                iEver_LoginViewController *_LoginViewController = [[iEver_LoginViewController alloc]init];
                _LoginViewController.navBarType = kCloseButton;
                UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:_LoginViewController];
                [self presentViewController:nav animated:YES completion:nil];
                
            }
            return [RACSignal empty];
        }];
        personTag_button;
    })];

    /* 个人定制下横线 */
    x = 15.0 ;
    y = CGRectGetHeight(tableHeadBaseView.frame)-1.0f;
    width = ScreenWidth - DRAWERWidth - 2 * 15.0;
    height = 1.0;
    UIImageView *dzdownLine_image = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    dzdownLine_image.image = [UIImage imageNamed:@"person_line"];

    [tableHeadBaseView addSubview:dzdownLine_image];

    self.tableView.tableHeaderView = tableHeadBaseView;
}

-(void)settingAsTableViweFooter {

    /* Initialization */
    CGFloat x = 0.0;
    CGFloat y = 0.0;
    CGFloat width = 0.0;
    CGFloat height = 0.0;

    /* 设置区域背景区域 */
    x = 0.0 ;
    y = 0.0;
    width = ScreenWidth - DRAWERWidth;
    height = TableFootViewHieght;
    UIView *tableFootBaseView = [[UIView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    tableFootBaseView.backgroundColor = WHITE;


    /* 灰色分割线 */
    x = 0.0 ;
    y = 0.0;
    width = ScreenWidth - DRAWERWidth;
    height = 6.0;
    UIView *gray_view = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    gray_view.backgroundColor = TYR_BACKGRAYCOLOR;

    [tableFootBaseView addSubview:gray_view];

    /* 设置 小图标 */
    UIImage *MY_set = [UIImage imageNamed:@"MY_set"];
    x = 20.0 ;
    y = 30.0;
    width = MY_set.size.width;
    height = MY_set.size.height;
    UIImageView *setting_image = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    setting_image.image = MY_set;

    [tableFootBaseView addSubview:setting_image];


    /* “设置” */
    x = 56.0 ;
    y = 32.0;
    width = 100;
    height = 20;
    UILabel *settingLable = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    settingLable.numberOfLines = 1;
    settingLable.textColor = BLACK;
    settingLable.font = TextFont;
    settingLable.text = @"设置";
    settingLable.backgroundColor = CLEAR;

    [tableFootBaseView addSubview:settingLable];

    /* 个人中心 arrow 小图标 */
    UIImage *VuserCenter_arrow = [UIImage imageNamed:@"VuserCenter_arrow"];
    x = 235.0 ;
    y = 35.0;
    width = VuserCenter_arrow.size.width;
    height = VuserCenter_arrow.size.height;
    UIImageView *settingArrow_image = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    settingArrow_image.image = VuserCenter_arrow;

    [tableFootBaseView addSubview:settingArrow_image];


    /* 进入设置中心按钮 */
    x = 0.0 ;
    y = 6.0;
    width = ScreenWidth;
    height = ENTERUserCHieght;
    UIButton *enterSettingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    enterSettingButton.frame = CGRectMake(x, y, width, height);
    enterSettingButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {

        iEver_SettingViewController *_SettingViewController = [[iEver_SettingViewController alloc]init];
        _SettingViewController.navBarType = kCloseButton;
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:_SettingViewController];
        [self presentViewController:nav animated:YES completion:nil];
        return [RACSignal empty];
    }];

    [tableFootBaseView addSubview:enterSettingButton];

    /* 设置下横线 */
    x = 15.0 ;
    y = ENTERSettingHieght;
    width = ScreenWidth - DRAWERWidth - 2 * 15.0;
    height = 1.0;
    UIImageView *downLine_image = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    downLine_image.image = [UIImage imageNamed:@"person_line"];

    [tableFootBaseView addSubview:downLine_image];


    /* 版本信息 */
    x = 0.0 ;
    y = 90.0;
    width = ScreenWidth - DRAWERWidth;
    height = 20;
    UILabel *copyRightLable = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    copyRightLable.numberOfLines = 1;
    copyRightLable.textColor = LINE_GRAY;
    copyRightLable.font = TextDESCFonts;
    copyRightLable.textAlignment = NSTextAlignmentCenter;
    copyRightLable.text = [NSString stringWithFormat:@"美课美妆 %@V",CURRENTShortVERSION];
    copyRightLable.backgroundColor = CLEAR;

    [tableFootBaseView addSubview:copyRightLable];


    self.tableView.tableFooterView = tableFootBaseView;
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

     return tableDataArray.count;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"LeftMYcell";
    iEver_MYcell *cell = (iEver_MYcell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    // Configure the cell...
    if (!cell) {

        cell = [[iEver_MYcell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];

    }

    iEver_MYcell *my_cell = (iEver_MYcell *)cell;
    iEver_personMessage_object *content_object = (iEver_personMessage_object *)[tableDataArray objectAtIndex:indexPath.row];
    [my_cell setObject:content_object];

    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [iEver_MYcell heightForObject:nil atIndexPath:indexPath tableView:tableView];

}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isLogin"]) {

        if (indexPath.row == 0) {
            iEver_MessageViewController *_messageViewController = [[iEver_MessageViewController alloc]init];
            _messageViewController.navBarType = kCloseButton;
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:_messageViewController];
            [self presentViewController:nav animated:YES completion:nil];
        }
        else if (indexPath.row == 1) {

            iEver_MY_ItemTryViewController *_MY_ItemTryViewController = [[iEver_MY_ItemTryViewController alloc]init];
            _MY_ItemTryViewController.title = @"我的试用";
            _MY_ItemTryViewController.navBarType = kCloseButton;
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:_MY_ItemTryViewController];
            [self presentViewController:nav animated:YES completion:nil];

        }
        else if (indexPath.row == 2) {

            iEver_OfficialListViewController *_OfficialListViewController = [[iEver_OfficialListViewController alloc]init];
            _OfficialListViewController.userLike                          = 1;
            _OfficialListViewController.title                             = @"赞过的文章";
            _OfficialListViewController.category_id                       = 1;
            _OfficialListViewController.navBarType = kCloseButton;
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:_OfficialListViewController];
            [self presentViewController:nav animated:YES completion:nil];


        }
        else if (indexPath.row == 3) {
            // 我的回答
            iEver_VuserAnsweredQuestionViewController *_normalUserAnsweredQesViewController = [[iEver_VuserAnsweredQuestionViewController alloc]initWithUserId:@"1726"/*[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]*/];
            _normalUserAnsweredQesViewController.title = @"我的回答";
            _normalUserAnsweredQesViewController.navBarType = kCloseButton;
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:_normalUserAnsweredQesViewController];
            [self presentViewController:nav animated:YES completion:nil];
        }
        else if (indexPath.row == 4) {
            // 我的提问
            iEver_AskAswerPagesTableViewController *_normalUserUnAnswerQesViewController = [[iEver_AskAswerPagesTableViewController alloc] initWithNibName:nil bundle:nil];
            _normalUserUnAnswerQesViewController.title = @"我的提问";
            _normalUserUnAnswerQesViewController.currentType = iEverPageShowTypeDefault;
            _normalUserUnAnswerQesViewController.navBarType = kCloseButton;
            _normalUserUnAnswerQesViewController.page = 1;
            [_normalUserUnAnswerQesViewController fetchData];
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:_normalUserUnAnswerQesViewController];
            [self presentViewController:nav animated:YES completion:nil];
        }

    }
    else{

        iEver_LoginViewController *_LoginViewController = [[iEver_LoginViewController alloc]init];
        _LoginViewController.navBarType = kCloseButton;
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:_LoginViewController];
        [self presentViewController:nav animated:YES completion:nil];

    }

    [userName resignFirstResponder];

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

- (void)uploadContent:(NSDictionary *)info
{
    [SVProgressHUD showWithStatus:@"上传中..." maskType:SVProgressHUDMaskTypeBlack];
    iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
    @weakify(self);
    NSString *_pathUrl = [NSString stringWithFormat:@"qiniu/getUpToken/userHeadImg/png"];
    [[content fetchPictureSiteToken:nil path:_pathUrl] subscribeNext:^(NSDictionary *dic) {
        if ([[dic objectForKey:@"resultCode"] integerValue] == 1) {
            @strongify(self);

            self.sUploader = [QiniuSimpleUploader uploaderWithToken:[dic objectForKey:@"uptoken"]];
            self.sUploader.delegate = self;

            UIImage *image = [info objectForKey: @"UIImagePickerControllerEditedImage"];
            NSString *key = [NSString stringWithFormat:@"%@", [dic objectForKey:@"fileName"]];
            NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:key];

            NSData *data = UIImageJPEGRepresentation(image, 1);
            [data writeToFile:filePath atomically:YES];
            [self.sUploader uploadFile:filePath key:[dic objectForKey:@"fullPath"] extra:nil];
            fileName = [dic objectForKey:@"fileName"];
        }else{

            NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[dic objectForKey:@"resultCode"] integerValue]];
            [SVProgressHUD showImage:Nil status:codeStr];
        }
    }];
}

#pragma mark -
#pragma mark - qiniu load image delegate

- (void)uploadSucceeded:(NSString *)filePath ret:(NSDictionary *)ret
{
    uploadSucceeded = YES;
    iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
    NSDictionary *dic = @{@"headImg": fileName};
    [[[content fixUserPictureSiteToken:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(NSDictionary *object) {
         if ([[object objectForKey:@"resultCode"] integerValue] == 1) {
             [SVProgressHUD showSuccessWithStatus:@"上传成功"];
             [self fetchData];
         }else{
             NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
             [SVProgressHUD showImage:Nil status:codeStr];
         }
     }];
}

- (void)uploadFailed:(NSString *)filePath error:(NSError *)error
{
    fileName = @"";
    headImageView.image = [UIImage imageNamed:@"fail_pic"];
    uploadSucceeded = NO;
    [SVProgressHUD showErrorWithStatus:@"上传失败"];
    DLog(@"uploadSucceeded filePath: %@; error: %@", filePath, error);
    iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
    NSString *_pathUrl = [NSString stringWithFormat:@"qiniu/uploadErr"];
    NSDictionary *dic = @{@"error_log": error
                          };
    [[content uploadErr:dic path:_pathUrl] subscribeNext:^(NSDictionary *dic) {
    }];
    
}

#pragma mark -
#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {

    [userName resignFirstResponder];

    if (userName.text.length == 0) {
        [SVProgressHUD showImage:Nil status:@"请输入正确的昵称"];
        return YES;
    }
    [SVProgressHUD showWithStatus:@"提交中..." maskType:SVProgressHUDMaskTypeBlack];
    iEver_UserMessageObject * content = [[iEver_UserMessageObject alloc] init];
    NSDictionary *dic = @{@"nickName": userName.text,
                          };

    [[[content updateNickName:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(NSDictionary *object) {

         if ([[object objectForKey:@"resultCode"] integerValue] == 1) {
             [SVProgressHUD showImage:Nil status:@"修改成功"];
             [self.navigationController popViewControllerAnimated:YES];
         }else{
             NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
             [SVProgressHUD showImage:Nil status:codeStr];
         }

     }];

    return YES;
}

#pragma mark -
#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController: (UIImagePickerController *)picker didFinishPickingMediaWithInfo: (NSDictionary *)info
{

    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        UIImage *original_image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        UIImageWriteToSavedPhotosAlbum(original_image, self,
                                       nil,
                                       nil);
    }
    UIImage *image = [info objectForKey: @"UIImagePickerControllerEditedImage"];
    [self uploadContent:info];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"headImageModifySusseedNotification" object:image];
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark - Status Bar

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation
{
    return UIStatusBarAnimationSlide;

}
#pragma mark - shouldAutorotateToInterfaceOrientation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)shouldAutorotate
{
    return NO;
}
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;//只支持这一个方向(正常的方向)
}
@end
