//
//  main.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-11.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "iEverAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([iEverAppDelegate class]));
    }
}
