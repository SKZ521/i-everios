//
//  iEver_MYcell.h
//  FaceTalk
//
//  Created by 开仲 司 on 15/4/2.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "SSBaseTableCell.h"
#import "iEver_UserMessageObject.h"
@interface iEver_MYcell : SSBaseTableCell
+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;
@property (nonatomic, strong) iEver_personMessage_object *object;
@end
