//
//  iEver_PerCener_nana_cell.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-16.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_PerCener_nana_cell.h"
#import "iEver_likeActionObject.h"
#import "iEver_V_ListDetailViewController.h"
#import "iEver_AskMeViewController.h"
#import "iEver_V_TagList.h"

@interface iEver_PerCener_nana_cell (){

}
@property (nonatomic, strong) UIView                         *bottomView;                /* 背景视图 */
@property (nonatomic, strong) UIView                         *baseColorView;             /* 背景视图 */
@property (nonatomic, strong) UILabel                        *browseLabel;               /* 浏览次数 */
@property (nonatomic, strong) UILabel                        *articleTitleLabel;         /* 文章标题 */
@property (nonatomic, strong) UIImageView                    *browseImageView;           /*浏览图片*/
@property (nonatomic, strong) UIImageView                    *startImageView;            /*播放图片*/
@property (nonatomic, strong) iEver_V_TagList     *tagList;          /* 文章所属标签 */
@property (nonatomic, strong) iEver_V_PersonCenterViewController *navigation; /*cell VC*/
@property (nonatomic, strong) iEver_discoverContentList_object *discover_object;

@end

@implementation iEver_PerCener_nana_cell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)setViewController:(iEver_V_PersonCenterViewController *)MVNavigation{

    self.navigation = MVNavigation;
}
- (void) configureCell {

}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
		self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = WHITE;
        [self.contentView addSubview:self.bottomView];
        [self.contentView addSubview:self.coverImage];
        [self.contentView addSubview:self.baseColorView];
        [self.contentView addSubview:self.tagList];
        [self.contentView addSubview:self.browseLabel];
        [self.contentView addSubview:self.articleTitleLabel];
        [self.contentView addSubview:self.browseLabel];
        [self.contentView addSubview:self.browseImageView];
        [self.contentView addSubview:self.startImageView];

    }
    return self;
}
- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews
{
	[super layoutSubviews];
    /* 单元格frame */
    //const CGRect r = self.contentView.bounds;

    
}
- (void)setObject:(iEver_discoverContentList_object *)object
{
    _discover_object = object;

    CGFloat x = 0.0;
    CGFloat y = 0.0;
    CGFloat width = 0.0;
    CGFloat height = 0.0;

    /* 文章封面图片 */

    x = 5.0;
    y = 5.0;
    width = 310.0;
    height = 310.0;
    self.coverImage.frame = CGRectMake(x, y, width, height);
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.coverImage.bounds byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight cornerRadii:CGSizeMake(5, 5)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.coverImage.bounds;
    maskLayer.path = maskPath.CGPath;
    self.coverImage.layer.mask = maskLayer;
    self.coverImage.layer.masksToBounds = YES;
    [self.coverImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/620x",object.coverImg]] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];


    /* 封面标题 */
    x = 20.0;
    y = self.coverImage.frame.size.height - 50.0;
    width = 280.0;
    height = 20.0;
    self.articleTitleLabel.frame = CGRectMake(x, y, width, height);
    self.articleTitleLabel.text = object.articleTitle;

    /* 文章所属标签 */
    x = 20.0;
    y = self.articleTitleLabel.frame.origin.y + self.articleTitleLabel.frame.size.height + 10;
    width = 280.0;
    height = 18.0;
    NSMutableArray *mutableArray_ID = [[NSMutableArray alloc] init];
    for (iEver_V_PersonList_tagsList_object *tagObject in  object._tagsList) {
        [mutableArray_ID addObject:[NSNumber numberWithInt:tagObject.tagId]];
    }
    self.tagList.IDArray                 = mutableArray_ID;
    NSMutableArray *mutableArray     = [[NSMutableArray alloc] init];
    for (iEver_V_PersonList_tagsList_object *tagObject in  object._tagsList) {
        [mutableArray addObject:[NSString stringWithFormat:@"#%@#",tagObject.tagName]];
    }
    /*编辑标签*/
    self.tagList.frame = CGRectMake(x, y, width, height);
    [self.tagList setTags:mutableArray];

    /*颜色视图*/
    x = 5;
    y = ScreenWidth - 69;
    width = ScreenWidth - 10;
    height = 64;
    self.baseColorView.frame = CGRectMake(x, y, width, height);


    /*背景视图*/
    x = 0;
    y = 0;
    width = ScreenWidth;
    height = ScreenWidth;
    self.bottomView.frame = CGRectMake(x, y, width, height);



    /*浏览标示图片*/
    x = 10.0;
    y = 15;
    width = 17.0;
    height = 11.0;
    self.browseImageView.frame = CGRectMake(x, y, width, height);

    /*浏览量*/
    x = 32;
    y = 10;
    width = 295.0;
    height = 20.0;
    self.browseLabel.text = [NSString stringWithFormat:@"%d",object.pvTotal];
    self.browseLabel.frame = CGRectMake(x, y, width, height);

    /*播放图片*/
    UIImage *main_list_play_big = [UIImage imageNamed:@"main_list_play_big"];
    x = ScreenWidth/2 - main_list_play_big.size.width/2;
    y = 120;
    width = main_list_play_big.size.width;
    height = main_list_play_big.size.height;
    self.startImageView.frame = CGRectMake(x, y, width, height);


    /* 浏览标示图片 */
    if (object.videoLink.length > 0 || object.mVideoLink.length > 0) {

        self.browseImageView.image = [UIImage imageNamed:@"main_list_video"];

        self.startImageView.hidden = NO;


    }else {

        self.browseImageView.image = [UIImage imageNamed:@"main_list_number"];

        self.startImageView.hidden = YES;

    }




}

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{

   	return ScreenWidth;


}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
# pragma mark - view getters
/*背景视图*/
- (UIView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [[UIView alloc]init];
        _bottomView.backgroundColor = WHITE;
    }
    return _bottomView;
}

/*背景视图*/
- (UIView *)baseColorView
{
    if (!_baseColorView) {
        _baseColorView = [[UIView alloc]init];
        _baseColorView.backgroundColor = SpecialBannerCOLOR_1;
        _baseColorView.alpha = 0.6;
    }
    return _baseColorView;
}


/* 文章类型标签 */
- (iEver_V_TagList *)tagList
{
	if (!_tagList){
		_tagList = [[iEver_V_TagList alloc] init];
	}
	return _tagList;
}

/*封面图片*/
- (UIImageView *)coverImage
{
    if (!_coverImage) {
        _coverImage = [[UIImageView alloc]init];
    }
    return _coverImage;
}

/* 封面标题 */
- (UILabel *)articleTitleLabel
{
	if (!_articleTitleLabel){
		_articleTitleLabel = [[UILabel alloc] init];
		_articleTitleLabel.backgroundColor = CLEAR;
        _articleTitleLabel.textColor = BLACK;
        _articleTitleLabel.font = Title_V_Fonts;
	}
	return _articleTitleLabel;
}


/*播放按钮*/
-(UIImageView *)startImageView{

    if (!_startImageView){
        _startImageView       = [[UIImageView alloc] init];
        _startImageView.image = [UIImage imageNamed:@"main_list_play_big"];
    }
    return _startImageView;
}

/*浏览图片标示*/
- (UIImageView *)browseImageView
{
    if (!_browseImageView) {
        _browseImageView             = [[UIImageView alloc]init];
        _browseImageView.contentMode = UIViewContentModeScaleToFill;

    }
    return _browseImageView;
}

/*浏览*/
- (UILabel *)browseLabel
{
    if (!_browseLabel){
        _browseLabel                  = [[UILabel alloc] init];
        _browseLabel.backgroundColor  = CLEAR;
        _browseLabel.textColor        = WHITE;
        _browseLabel.textAlignment    = NSTextAlignmentLeft;
        _browseLabel.shadowColor      = GRAY_LIGHT;
        _browseLabel.font             = TextDESCFonts;
    }
    return _browseLabel;
}

@end
