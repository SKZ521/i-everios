//
//  iEver_person_FourSection_cell.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-4.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_person_FourSection_cell.h"
@interface iEver_person_FourSection_cell()
@property (nonatomic, strong) UIView        *bottom_View;         /* 视图背景*/
@property (nonatomic, strong) UILabel       *text_Name;           /* 设置定制信息标签 */
@property (nonatomic, strong) UIImageView   *arrow_image;         /* 箭头图片 */
@property (nonatomic, strong) UIImageView   *icon_image;          /* 图标图片 */
@property (nonatomic, strong) UIImageView   *lineView;            /* 视图横线 */
@property (nonatomic, strong) UILabel       *message;            /* 设置定制信息标签 */
@end
@implementation iEver_person_FourSection_cell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) configureCell {
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
		self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = CLEAR;
        self.backgroundColor = CLEAR;
        [self.contentView addSubview:self.bottom_View];
        [self.contentView addSubview:self.text_Name];
        [self.contentView addSubview:self.arrow_image];
        [self.contentView addSubview:self.icon_image];
        [self.contentView addSubview:self.lineView];
        [self.contentView addSubview:self.message];

    }
    return self;
}
- (void)awakeFromNib
{
    // Initialization code
}
- (void)layoutSubviews
{
	[super layoutSubviews];
    /*单元格frame */
    //const CGRect r = self.contentView.bounds;

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setObject:(iEver_personMessage_object *)object
{
    CGFloat x = 0.0;
    CGFloat y = 0.0;
    CGFloat width = 0.0;
    CGFloat height = 0.0;

    /* 白色圆角地图 */
    x = 5.0;
    y = 0.0;
    width = 310.0;
    height = 45.0;
    self.bottom_View.frame = CGRectMake(x, y, width, height);

    /* 图标 */
    x = 20.0;
    y = 7.5;
    width = 30;
    height = 30;
    self.icon_image.image = [UIImage imageNamed:object.section4image];
    self.icon_image.frame = CGRectMake(x, y, width, height);

    /* 用户名称 */
    x = 60.0;
    y = 12.0;
    width = 150.0;
    height = 25.0;
    self.text_Name.frame = CGRectMake(x, y, width, height);
    self.text_Name.text = object.section4Title;

    /* 箭头图片 */
    x = 298.0;
    y = 16.0;
    width = 10.0;
    height = 16.0;
    self.arrow_image.frame = CGRectMake(x, y, width, height);

    /* 版本号 或者缓存数据 */
    x = 188.0;
    y = 10.0;
    width = 80.0;
    height = 25.0;
    self.message.frame = CGRectMake(x, y, width, height);

    /* 视图横线 */
    x = 15.0;
    y = 44.0;
    height = 1.0;
    width = 290.0;
    self.lineView.frame = CGRectMake(x, y, width, height);

    if (object.section4EndCell == 0) {
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bottom_View.bounds byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight cornerRadii:CGSizeMake(5, 5)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.bottom_View.bounds;
        maskLayer.path = maskPath.CGPath;
        self.bottom_View.layer.mask=maskLayer;
        self.bottom_View.layer.masksToBounds=YES;
        self.lineView.hidden = NO;
    }
    else if (object.section4EndCell == 1) {

        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bottom_View.bounds byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight cornerRadii:CGSizeMake(0, 0)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.bottom_View.bounds;
        maskLayer.path = maskPath.CGPath;
        self.bottom_View.layer.mask=maskLayer;
        self.bottom_View.layer.masksToBounds=YES;
        self.lineView.hidden = NO;
    }
    else if (object.section4EndCell == 2) {
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bottom_View.bounds byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight cornerRadii:CGSizeMake(0, 0)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.bottom_View.bounds;
        maskLayer.path = maskPath.CGPath;
        self.bottom_View.layer.mask=maskLayer;
        self.bottom_View.layer.masksToBounds=YES;
        self.lineView.hidden = NO;
    }
//    else if (object.section4EndCell == 3){
//
//        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bottom_View.bounds byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight cornerRadii:CGSizeMake(0, 0)];
//        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
//        maskLayer.frame = self.bottom_View.bounds;
//        maskLayer.path = maskPath.CGPath;
//        self.bottom_View.layer.mask=maskLayer;
//        self.bottom_View.layer.masksToBounds=YES;
//        self.lineView.hidden = NO;
//        NSDictionary *appInfo = [[NSBundle mainBundle] infoDictionary];
//        NSString *currentVersion = [appInfo objectForKey:@"CFBundleVersion"];
//        self.message.text = [NSString stringWithFormat:@"%@V",currentVersion];
//    }
    else if (object.section4EndCell == 3) {
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bottom_View.bounds byRoundingCorners:UIRectCornerBottomLeft|UIRectCornerBottomRight cornerRadii:CGSizeMake(5, 5)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.bottom_View.bounds;
        maskLayer.path = maskPath.CGPath;
        self.bottom_View.layer.mask=maskLayer;
        self.bottom_View.layer.masksToBounds=YES;
        self.lineView.hidden = YES;

        float imageCache  = [[SDImageCache sharedImageCache] getSize];
        self.message.text = [NSString stringWithFormat:@"%.2FM",(imageCache / 1024.0 / 1024.0)];
    }


}

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
	return 45.0;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */
# pragma mark - view getters

/* 底部白色视图 */
- (UIView *)bottom_View
{
    if (!_bottom_View) {
        _bottom_View = [[UIView alloc]init];
        _bottom_View.backgroundColor = WHITE;
    }
    return _bottom_View;
}

/* 用户昵称 */
- (UILabel *)text_Name
{
	if (!_text_Name){
		_text_Name = [[UILabel alloc] init];
		_text_Name.numberOfLines = 1;
		_text_Name.backgroundColor = CLEAR;
		_text_Name.textColor = LIST_TITLE;
		_text_Name.font = TextFont;
	}
	return _text_Name;
}

/* 版本号 或者缓存数据 */
- (UILabel *)message
{
	if (!_message){
		_message = [[UILabel alloc] init];
		_message.numberOfLines = 1;
		_message.backgroundColor = CLEAR;
		_message.textColor = LIST_TITLE;
        _message.textAlignment = NSTextAlignmentRight;
		_message.font = TextFont;
	}
	return _message;
}
/* 图标图片 */
- (UIImageView *)icon_image
{
    if (!_icon_image) {
        _icon_image = [[UIImageView alloc]init];
    }
    return _icon_image;
}
/* 箭头图片 */
- (UIImageView *)arrow_image
{
    if (!_arrow_image) {
        _arrow_image = [[UIImageView alloc]init];
        _arrow_image.contentMode = UIViewContentModeScaleAspectFill;
        _arrow_image.image = [UIImage imageNamed:@"tryItem_detail_arrow"];

    }
    return _arrow_image;
}

/* 视图横线 */
- (UIImageView *)lineView
{
    if (!_lineView) {
        _lineView = [[UIImageView alloc]init];
        _lineView.image = [UIImage imageNamed:@"person_line"];
    }
    return _lineView;
}
@end
