//
//  iEver_discoverUpdateCell.h
//  FaceTalk
//
//  Created by 开仲 司 on 14/12/10.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "SSBaseTableCell.h"
#import "iEver_V_userObject.h"
@interface iEver_discoverUpdateCell : SSBaseTableCell
+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;
@property (nonatomic, strong) iEver_discover_userList_object   *object;
@end
