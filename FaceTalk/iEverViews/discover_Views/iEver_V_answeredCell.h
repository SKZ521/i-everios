//
//  iEver_V_answeredCell.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-18.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "SSBaseTableCell.h"
#import "iEver_V_answeredQuestionObject.h"
@interface iEver_V_answeredCell : SSBaseTableCell

@property (nonatomic, strong) iEver_V_quesList_object   *object;

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;

- (void)setViewController:(UINavigationController *)MVNavigation;

@end
