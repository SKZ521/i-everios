//
//  iEver_V_personCell.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-11.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_V_personCell.h"
#import "iEver_V_person_ViewController.h"
#import "iEver_AskMeViewController.h"
@interface iEver_V_personCell (){

}
@property (nonatomic, strong) UIButton      *photoButton;      /* 头像按钮 */
@property (nonatomic, strong) UILabel       *nameLable;        /* 名称标签 */
@property (nonatomic, strong) UILabel       *answerNum;        /* 回答问题量 */
@property (nonatomic, strong) UILabel       *personInfo;       /* 用户简介 */
@property (nonatomic, strong) UIButton      *askMeButton;      /* askMe按钮 */
@property (nonatomic, strong) UIImageView   *lineImageview;    /* 分割线 */
@property (nonatomic, strong) iEver_V_person_ViewController *V_personVC; /*cell VC*/
@property (nonatomic, strong) iEver_V_user_object *V_person_object;
@end

@implementation iEver_V_personCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)setViewController:(iEver_V_person_ViewController *)mvc{

    self.V_personVC = mvc;
}
- (void) configureCell {

}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
		self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.photoButton];
        [self.contentView addSubview:self.nameLable];
        [self.contentView addSubview:self.answerNum];
        [self.contentView addSubview:self.personInfo];
        [self.contentView addSubview:self.askMeButton];
        [self.contentView addSubview:self.lineImageview];
    }
    return self;
}
- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews
{
	[super layoutSubviews];
    /* 单元格frame */
    const CGRect r = self.contentView.bounds;

    CGFloat x = 0.0;
    CGFloat y = 0.0;
    CGFloat width = 0.0;
    CGFloat height = 0.0;

    /* 达人头像按钮 */
    x = 10.0;
    y = 10.0;
    width = 45.0;
    height = 45.0;
    self.photoButton.frame = CGRectMake(x, y, width, height);

    /* 名称标签 */
    x = 62.0;
    y = 14.0;
    width = 100.0;
    height = 25.0;
    self.nameLable.frame = CGRectMake(x, y, width, height);

    /* askMe Button */
    x = 267.0;
    y = 10.0;
    width = 40.0;
    height = 40.0;
    self.askMeButton.frame = CGRectMake(x, y, width, height);

    /* 分割线 */
    x = 15.0;
    y = r.size.height - 1.0;
    width = 290.0;
    height = 1.0;
    self.lineImageview.frame = CGRectMake(x, y, width, height);

}
- (void)setObject:(iEver_V_user_object *)object
{

    _V_person_object = object;
    /* 达人头像 */
    [_photoButton setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/90x",object.headImg]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"defualt_icon"]];

    /* 达人名称 */
    self.nameLable.text = object.nickName;

    /* 回答问题量 */
    self.answerNum.text = [NSString stringWithFormat:@"回答%d问题",object.answerTotal];
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:self.answerNum.font, NSFontAttributeName,nil];
    CGSize contentSize =[self.answerNum.text boundingRectWithSize:CGSizeMake(100.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;

    /* 回答问题量 */
    CGFloat x = 160.0;
    CGFloat y = 16.0;
    CGFloat width = contentSize.width + 8.0;
    CGFloat height = contentSize.height + 4.0;
    self.answerNum.frame = CGRectMake(x, y, width, height);


    /* 个人简述 */
    self.personInfo.text = object.intro;
    NSDictionary * Pdic = [NSDictionary dictionaryWithObjectsAndKeys:self.personInfo.font, NSFontAttributeName,nil];
    CGSize PcontentSize =[self.personInfo.text boundingRectWithSize:CGSizeMake(195.0, 40.0) options:NSStringDrawingUsesLineFragmentOrigin  attributes:Pdic context:nil].size;
    x = 62.0;
    y = 45.0;
    width = PcontentSize.width;
    height = PcontentSize.height;
    self.personInfo.frame = CGRectMake(x, y, width, height);

}
+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
    iEver_V_user_object *contentObject = (iEver_V_user_object *)object;
    NSDictionary * Pdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
    CGSize PcontentSize =[contentObject.intro boundingRectWithSize:CGSizeMake(245.0, 40.0) options:NSStringDrawingUsesLineFragmentOrigin  attributes:Pdic context:nil].size;
    return 40.0 + PcontentSize.height + 15.0;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
# pragma mark - view getters
/*头像按钮*/
- (UIButton *)photoButton
{
	if (!_photoButton){
		_photoButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _photoButton.titleLabel.numberOfLines = 0;
        _photoButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        [_photoButton setTitleColor:BLACK forState:UIControlStateNormal];
        _photoButton.titleLabel.font = TextLIST_Fonts;
        CALayer *photoButton = [_photoButton layer];
        [photoButton setMasksToBounds:YES];
        [photoButton setCornerRadius:45.0/2];
        [photoButton setBorderColor:[[UIColor lightGrayColor] CGColor]];
        [_photoButton addTarget:self action:@selector(V_personCenter:) forControlEvents:UIControlEventTouchUpInside];
	}
	return _photoButton;
}
-(void)V_personCenter:(id)sender{

}
/*达人昵称*/
- (UILabel *)nameLable
{
	if (!_nameLable){
		_nameLable = [[UILabel alloc] init];
		_nameLable.numberOfLines = 1;
		_nameLable.backgroundColor = CLEAR;
		_nameLable.textColor = DETAIL_TITLE;
		_nameLable.font = TextLIST_TITLE_Fonts;
	}
	return _nameLable;
}
/* 回答问题量 */
- (UILabel *)answerNum
{
	if (!_answerNum){
		_answerNum = [[UILabel alloc] init];
		_answerNum.backgroundColor = [UIColor colorWithRed:217/255.f green:217/255.f blue:217/255.f alpha:1.00];
        _answerNum.textAlignment = NSTextAlignmentCenter;
        _answerNum.textColor = LIST_TITLE;
        _answerNum.font = TextFont;

        CALayer *answerNumLayer = [_answerNum layer];
        [answerNumLayer setMasksToBounds:YES];
        [answerNumLayer setCornerRadius:2.0];

	}
	return _answerNum;
}

/* 达人简述*/
- (UILabel *)personInfo
{
	if (!_personInfo){
		_personInfo = [[UILabel alloc] init];
		_personInfo.backgroundColor = CLEAR;
        _personInfo.lineBreakMode = NSLineBreakByTruncatingTail;
        _personInfo.numberOfLines = 0;
        _personInfo.textAlignment = NSTextAlignmentLeft;
        _personInfo.textColor = LIST_EDITOR;
        _personInfo.font = TextFont;
	}
	return _personInfo;
}

/* ask me button */
- (UIButton *)askMeButton
{
	if (!_askMeButton){
		_askMeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_askMeButton setImage:[UIImage imageNamed:@"V_list_ask"] forState:UIControlStateNormal];
        [_askMeButton setImage:[UIImage imageNamed:@"V_list_ask"] forState:UIControlStateHighlighted];
        [_askMeButton addTarget:self action:@selector(askMeAction:) forControlEvents:UIControlEventTouchUpInside];
	}
	return _askMeButton;
}
-(void)askMeAction:(id)sender{

    iEver_AskMeViewController *_askMeViewController = [[iEver_AskMeViewController alloc]init];
    _askMeViewController.userId = _V_person_object.User_id;
    _askMeViewController.image  = _V_person_object.headImg;
    _askMeViewController.name   =_V_person_object.nickName;
    _askMeViewController.hidesBottomBarWhenPushed = YES;
    [_V_personVC.navigationController pushViewController:_askMeViewController animated:YES];
    if ([_V_personVC.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        _V_personVC.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }

}
-(UIImageView *)lineImageview{

    if (!_lineImageview){

        _lineImageview = [[UIImageView alloc] init];
        _lineImageview.image = [UIImage imageNamed:@"person_line"];
    }
    return _lineImageview;
}
@end
