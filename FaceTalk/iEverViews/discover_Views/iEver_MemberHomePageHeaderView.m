//
//  iEver_MemberHomePageHeaderView.m
//  FaceTalk
//
//  Created by 开仲 司 on 15/3/25.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_MemberHomePageHeaderView.h"
#import "iEver_V_userObject.h"
#import "UIView+FindViewController.h"
#import "iEver_VuserCenterViewController.h"


@interface iEver_MemberHomePageHeaderView() <UIScrollViewDelegate>
{

    UIImageView *lastImage;
    UIImageView *currenctImage;

    iEver_V_userObject * _object;
    
}

@property (strong, nonatomic) UIScrollView *scrollView_SmallPhoto;
@property (strong, nonatomic) UIScrollView *scrollView_BigPhoto;

@end

@implementation iEver_MemberHomePageHeaderView

#define KScrollViewHeight_Small 80.0f
#define KuserPhoto_Small 50.0f

#define KScrollViewHeight_Big 305.0f
#define KuserPhoto_Big 146.0f

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

        [self initiaSubViews];
        

        }
    return self;
}

- (void)initiaSubViews
{
    
    UIImage *_Selectuser_left = [UIImage imageNamed:@"VuserCenter_Selectuser_left"];
    UIImage *_Selectuser_right = [UIImage imageNamed:@"VuserCenter_Selectuser_right"];
    
    UIImageView *_leftImageView = [[UIImageView alloc] initWithFrame:CGRectMake(16, 165.0, _Selectuser_left.size.width, _Selectuser_left.size.height)];
    
    _leftImageView.image = _Selectuser_left;
    
    UIImageView *_rightImageView = [[UIImageView alloc] initWithFrame:CGRectMake(ScreenWidth - 16 - _Selectuser_right.size.width, 165.0, _Selectuser_right.size.width, _Selectuser_right.size.height)];
    
    _rightImageView.image = _Selectuser_right;


    [self.baseView addSubview:self.scrollView_SmallPhoto];
    [self.baseView addSubview:self.scrollView_BigPhoto];
    
    [self.baseView addSubview:_leftImageView];
    [self.baseView addSubview:_rightImageView];
    
    [self addSubview:self.baseView];
}


- (void)setObject:(iEver_V_userObject *)object
{

    if (![object._userList count]) {
        return;
    }

    _object = object;
    for (int i = 0; i < [object._userList count]; i++) {

        iEver_discover_userList_object *user_Object = [object._userList objectAtIndex:i];
        [self addImageWithName:user_Object atPosition:i];

    }

    UIImage *selectBackImage = [UIImage imageNamed:@"VuserCenter_choose"];
    
    [self.scrollView_SmallPhoto setContentSize:CGSizeMake((selectBackImage.size.width ) * [object._userList count], KScrollViewHeight_Small)];

    for (int i = 0; i < [object._userList count]; i++) {

        iEver_discover_userList_object *user_Object = [object._userList objectAtIndex:i];
        [self addImageWithNameBigPhoto:user_Object atPosition:i];

    }

    [self.scrollView_BigPhoto setContentSize:CGSizeMake(ScreenWidth * [object._userList count], KScrollViewHeight_Big)];

}


- (void)addImageWithName:(iEver_discover_userList_object *)iAd atPosition:(int)position

{
    UIImage *selectBackImage = [UIImage imageNamed:@"VuserCenter_choose"];
    UIImage *nomalBackImage = [UIImage imageNamed:@"VuserCenter_unchoose"];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake((selectBackImage.size.width ) * position, 10.0, selectBackImage.size.width, selectBackImage.size.height)];
    
    
    imageView.tag = position + 1000;
    
    imageView.userInteractionEnabled = YES;
    
    UIButton *button     = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame         = CGRectMake((selectBackImage.size.width - KuserPhoto_Small) / 2, (selectBackImage.size.height - KuserPhoto_Small) / 2, KuserPhoto_Small, KuserPhoto_Small);
    CALayer *coverButton = [button layer];
    [coverButton setMasksToBounds:YES];
    [coverButton setCornerRadius:KuserPhoto_Small/2];
    button.tag           = position + 2000;
    [button setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/150x",iAd.headImg]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
    
    [[button rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *button) {

        lastImage = currenctImage;
        lastImage.image = nomalBackImage;
        
        currenctImage = (UIImageView *) [self viewWithTag:button.tag - 1000];
        currenctImage.image = selectBackImage;
        
        
        /* 点击头像 切换中间大头像 */
        
        [self.scrollView_BigPhoto setContentOffset:CGPointMake((button.tag - 2000) * ScreenWidth, 0.0) animated:NO];
        
        iEver_VuserCenterViewController *superVC = (iEver_VuserCenterViewController *)[self firstAvailableUIViewController];
        [superVC selectVuserFetchData:(button.tag - 2000)];

        
    }];
    
    if (position == 0) {
        
        imageView.image = selectBackImage;
        currenctImage   = imageView;
        
    }else {
    
        imageView.image = nomalBackImage;
        
    }
    
    [imageView addSubview:button];
    [self.scrollView_SmallPhoto addSubview:imageView];
}

- (void)addImageWithNameBigPhoto:(iEver_discover_userList_object *)iAd atPosition:(int)position

{

    UIView   *view       = [[UIView alloc] initWithFrame:CGRectMake(ScreenWidth * position , 0.0 , ScreenWidth , KScrollViewHeight_Big)];
    view.backgroundColor = CLEAR;

    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(87, 24.0, KuserPhoto_Big, KuserPhoto_Big)];
    CALayer *coverImage = [imageview layer];
    [coverImage setMasksToBounds:YES];
    [coverImage setCornerRadius:KuserPhoto_Big/2];
    imageview.tag          = position + 3000;
    [imageview setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/300x",iAd.headImg]]
                   placeholderImage:[UIImage imageNamed:@"defualt_icon"]];


    imageview.userInteractionEnabled = YES;
    //
    UISwipeGestureRecognizer *swipeLeftGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipeGesture:)];
    swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionUp;
    [view addGestureRecognizer:swipeLeftGesture];


    UILabel *name        = [[UILabel alloc] initWithFrame:CGRectMake(0, 209, ScreenWidth, 23)];
    name.numberOfLines   = 1;
    name.textAlignment   = NSTextAlignmentCenter;
    name.backgroundColor = CLEAR;
    name.textColor       = VNAME;
    name.font            = TextLIST_TITLE_Fonts;
    name.text            = iAd.nickName;

    UILabel *info        = [[UILabel alloc] init];
    info.lineBreakMode   = NSLineBreakByTruncatingTail;
    info.numberOfLines   = 0;
    info.textAlignment   = NSTextAlignmentCenter;
    info.textColor       = VINFO;
    info.font            = TextFont;
    info.text            = iAd.intro;

    NSDictionary * Pdic  = [NSDictionary dictionaryWithObjectsAndKeys:info.font, NSFontAttributeName,nil];
    CGSize PcontentSize  = [iAd.intro boundingRectWithSize:CGSizeMake(200.0, 40.0) options:NSStringDrawingUsesLineFragmentOrigin  attributes:Pdic context:nil].size;
    info.frame           = CGRectMake(60, 249, 200.0, PcontentSize.height);



    [view addSubview:imageview];
    [view addSubview:name];
    [view addSubview:info];
    [self.scrollView_BigPhoto addSubview:view];
}


-(void)handleSwipeGesture:(UIGestureRecognizer*)sender{

    iEver_VuserCenterViewController *superVC = (iEver_VuserCenterViewController *)[self firstAvailableUIViewController];

    [superVC closeVuserInfoView];

    
}
#pragma mark -
#pragma mark UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollview
{
    
    UIImage *selectBackImage = [UIImage imageNamed:@"VuserCenter_choose"];
    UIImage *nomalBackImage = [UIImage imageNamed:@"VuserCenter_unchoose"];


    if (scrollview == _scrollView_BigPhoto) {
        int page = floor(scrollview.contentOffset.x / ScreenWidth );
        [self.scrollView_SmallPhoto setContentOffset:CGPointMake(page * (selectBackImage.size.width ), 0.0) animated:YES];
        
        lastImage = currenctImage;
        lastImage.image = nomalBackImage;
        
        currenctImage = (UIImageView *) [self viewWithTag:page + 1000];
        currenctImage.image = selectBackImage;
        
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {

    if (scrollView == _scrollView_BigPhoto) {

        int page = floor( scrollView.contentOffset.x / ScreenWidth );

        if (page < 0) {
            page = 0;
        }
        if (page > [_object._userList count] - 1) {
            page = [_object._userList count] - 1;
        }

        iEver_VuserCenterViewController *superVC = (iEver_VuserCenterViewController *)[self firstAvailableUIViewController];
        [superVC selectVuserFetchData:page];
    }

}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{



    
}

#pragma mark -
#pragma mark layoutSubviews (update view)

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.scrollView_SmallPhoto.frame = CGRectMake(0, 0, ScreenWidth, KScrollViewHeight_Small);
    self.scrollView_BigPhoto.frame   = CGRectMake(0, KScrollViewHeight_Small, ScreenWidth, KScrollViewHeight_Big);
}

- (void)setSubViewPosition:(CGFloat)y
{
    if (y <= 0 ) {
        [self.baseView setFrame:CGRectMake(0, 0, ScreenWidth, 377.5)];
    } else {
        [self.baseView setFrame:CGRectMake(0, - y, ScreenWidth, 377.5)];
    }

}

#pragma mark -
#pragma mark SET
- (UIScrollView *)scrollView_SmallPhoto
{
    if (!_scrollView_SmallPhoto) {
        _scrollView_SmallPhoto = [[UIScrollView alloc] init];
        _scrollView_SmallPhoto.pagingEnabled = NO;
        _scrollView_SmallPhoto.showsHorizontalScrollIndicator = NO;
        _scrollView_SmallPhoto.showsVerticalScrollIndicator = NO;
        _scrollView_SmallPhoto.delegate = self;
    }

    return _scrollView_SmallPhoto;
}

- (UIScrollView *)scrollView_BigPhoto
{
    if (!_scrollView_BigPhoto) {
        _scrollView_BigPhoto = [[UIScrollView alloc] init];
        _scrollView_BigPhoto.pagingEnabled = YES;
        _scrollView_BigPhoto.showsHorizontalScrollIndicator = NO;
        _scrollView_BigPhoto.showsVerticalScrollIndicator = NO;
        _scrollView_BigPhoto.delegate = self;
        _scrollView_BigPhoto.userInteractionEnabled = YES;
    }

    return _scrollView_BigPhoto;
}

- (UIView *)baseView
{
    if (!_baseView) {
        _baseView = [[UIView alloc] init];
        _baseView.frame = CGRectMake(0, 0, ScreenWidth, 377.5);
        _baseView.backgroundColor = CLEAR;
    }

    return _baseView;
}


@end
