//
//  iEver_recommendExpertArticleCell_ArticleDetail.h
//  FaceTalk
//
//  Created by 开仲 司 on 15/5/6.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "SSBaseTableCell.h"
#import "iEver_mainDetail_Object.h"

@interface iEver_recommendExpertArticleCell_ArticleDetail : SSBaseTableCell

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;
@property (nonatomic, strong) iEver_mainDetail_Object *object;


@end
