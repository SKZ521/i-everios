//
//  iEver_V_answeredCell.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-11-18.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_V_answeredCell.h"
#import "MCFireworksButton.h"
#import "iEver_likeActionObject.h"
#import "SJAvatarBrowser.h"
@interface iEver_V_answeredCell (){

}
@property (nonatomic, strong) UIView        *bottomView;         /* 背景视图 */
@property (nonatomic, strong) UIButton      *Q_PhotoButton;      /* 问者头像按钮 */
@property (nonatomic, strong) UIImageView   *Q_TypeImage;        /* 问着标示图标 */
@property (nonatomic, strong) UILabel       *Q_nameLable;        /* 问者昵称 */
@property (nonatomic, strong) UILabel       *Q_TimeLable;        /* 提问时间 */
@property (nonatomic, strong) UILabel       *Q_InfoLable;        /* 问者标签 */
@property (nonatomic, strong) UILabel       *Q_contentLabel;     /* 问题 */
@property (nonatomic, strong) UIButton      *Q_DetailButton;     /* 问题评论详情 */
@property (nonatomic, strong) UIImageView   *Q_image1;           /* 问题图片1 */
@property (nonatomic, strong) UIImageView   *Q_image2;           /* 问题图片2 */
@property (nonatomic, strong) UIImageView   *Q_image3;           /* 问题图片3 */
@property (nonatomic, strong) UIImageView   *Q_image4;           /* 问题图片4 */
@property (nonatomic, strong) UIImageView   *Q_image5;           /* 问题图片5 */
@property (nonatomic, strong) UIImageView   *Q_image6;           /* 问题图片6 */
@property (nonatomic, strong) UIImageView   *Q_image7;           /* 问题图片7 */
@property (nonatomic, strong) UIImageView   *Q_image8;           /* 问题图片8 */
@property (nonatomic, strong) UIImageView   *Q_image9;           /* 问题图片9 */
@property (nonatomic, strong) UIButton      *A_PhotoButton;      /* 答者头像 */
@property (nonatomic, strong) UIImageView   *A_TypeImage;        /* 答者图片标示 */
@property (nonatomic, strong) UILabel       *A_nameLable;        /* 答者昵称 */
@property (nonatomic, strong) UILabel       *A_TimeLable;        /* 回答时间 */
@property (nonatomic, strong) UILabel       *A_InfoLable;        /* 答者标签 */
@property (nonatomic, strong) UILabel       *A_contentLabel;     /* 答案 */
@property (nonatomic, strong) UIImageView   *A_image1;           /* 答案图片1 */
@property (nonatomic, strong) UIImageView   *A_image2;           /* 答案图片2 */
@property (nonatomic, strong) UIImageView   *A_image3;           /* 答案图片3 */
@property (nonatomic, strong) UIImageView   *A_image4;           /* 答案图片4 */
@property (nonatomic, strong) UIImageView   *A_image5;           /* 答案图片5 */
@property (nonatomic, strong) UIImageView   *A_image6;           /* 答案图片6 */
@property (nonatomic, strong) UIImageView   *A_image7;           /* 答案图片7 */
@property (nonatomic, strong) UIImageView   *A_image8;           /* 答案图片8 */
@property (nonatomic, strong) UIImageView   *A_image9;           /* 答案图片9 */
@property (nonatomic, strong) MCFireworksButton *likeButton;     /* 喜欢按钮 */
@property (nonatomic, strong) UILabel       *likeNumLabel;       /* 喜欢次数 */
@property (nonatomic, strong) UIImageView   *CutoffRuleImage;    /* 问答之间分割线 */
@property (nonatomic, strong) UIImageView   *lineImage;          /* 单元格线 */
@property (nonatomic, strong) UINavigationController *navigation; /*cell VC*/
@property (nonatomic, strong) iEver_V_quesList_object *whole_object;
@end

@implementation iEver_V_answeredCell
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {

    }
    return self;
}

- (void)setViewController:(UINavigationController *)MVNavigation{

    self.navigation = MVNavigation;
}

- (void)configureCell
{

}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor redColor];

        [self.contentView addSubview:self.bottomView];
        [self.contentView addSubview:self.Q_PhotoButton];
        [self.contentView addSubview:self.Q_TypeImage];
        [self.contentView addSubview:self.Q_nameLable];
        [self.contentView addSubview:self.Q_TimeLable];
        [self.contentView addSubview:self.Q_InfoLable];
        [self.contentView addSubview:self.Q_DetailButton];
        [self.contentView addSubview:self.Q_contentLabel];
        [self.contentView addSubview:self.Q_DetailButton];

        [self.contentView addSubview:self.A_PhotoButton];
        [self.contentView addSubview:self.A_TypeImage];
        self.A_TypeImage.hidden = YES;
        [self.contentView addSubview:self.A_nameLable];
        [self.contentView addSubview:self.A_TimeLable];
        [self.contentView addSubview:self.A_InfoLable];
        [self.contentView addSubview:self.A_contentLabel];
//        [self.contentView addSubview:self.likeNumLabel];
//        self.likeNumLabel.hidden = YES;
        [self.contentView addSubview:self.likeButton];
        self.likeButton.hidden = YES;
        [self.contentView addSubview:self.lineImage];
        [self.contentView addSubview:self.CutoffRuleImage];
        self.CutoffRuleImage.hidden = YES;
        [self.contentView addSubview:self.Q_image1];
        [self.contentView addSubview:self.Q_image2];
        [self.contentView addSubview:self.Q_image3];
        [self.contentView addSubview:self.Q_image4];
        [self.contentView addSubview:self.Q_image5];
        [self.contentView addSubview:self.Q_image6];
        [self.contentView addSubview:self.Q_image7];
        [self.contentView addSubview:self.Q_image8];
        [self.contentView addSubview:self.Q_image9];
        [self.contentView addSubview:self.A_image1];
        [self.contentView addSubview:self.A_image2];
        [self.contentView addSubview:self.A_image3];
        [self.contentView addSubview:self.A_image4];
        [self.contentView addSubview:self.A_image5];
        [self.contentView addSubview:self.A_image6];
        [self.contentView addSubview:self.A_image7];
        [self.contentView addSubview:self.A_image8];
        [self.contentView addSubview:self.A_image9];
    }
    return self;
}

- (void)setObject:(iEver_V_quesList_object *)object
{
    _whole_object = object;

    self.Q_image1.hidden = YES;
    self.Q_image2.hidden = YES;
    self.Q_image3.hidden = YES;
    self.Q_image4.hidden = YES;
    self.Q_image5.hidden = YES;
    self.Q_image6.hidden = YES;
    self.Q_image7.hidden = YES;
    self.Q_image8.hidden = YES;
    self.Q_image9.hidden = YES;

    self.A_image1.hidden = YES;
    self.A_image2.hidden = YES;
    self.A_image3.hidden = YES;
    self.A_image4.hidden = YES;
    self.A_image5.hidden = YES;
    self.A_image6.hidden = YES;
    self.A_image7.hidden = YES;
    self.A_image8.hidden = YES;
    self.A_image9.hidden = YES;

    CGFloat x = 0.0;
    CGFloat y = 0.0;
    CGFloat width = 0.0;
    CGFloat height = 0.0;

    /* 问题者用户头像 */
    x = 11.0;
    y = 11.0;
    width = 30.0;
    height = 30.0;
    [self.Q_PhotoButton setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/60x",object.qHeadImg]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
    self.Q_PhotoButton.frame = CGRectMake(x, y, width, height);

    /* 问题A标示 */
    x = 11.0f;
    y = CGRectGetMaxY(_Q_PhotoButton.frame);
    width = 20.0f;
    height = 14.0f;
    self.Q_TypeImage.frame = CGRectMake(x, y, width, height);

    /* 问者昵称 */
    x = CGRectGetMaxX(_Q_PhotoButton.frame);
    y = 11.0;
    width = 200.0;
    height = 20.0;
    self.Q_nameLable.frame = CGRectMake(x, y, width, height);
    self.Q_nameLable.text = [NSString stringWithFormat:@"%@:",object.qNickName];

    /* 问题发布时间 */
    x = CGRectGetWidth(self.frame)-80.0f;
    y = 11.0f;
    width = 80.0f;
    height = 20.0f;
    self.Q_TimeLable.frame = CGRectMake(x, y, width, height);

    /* 提问者标签 */
    x = CGRectGetMaxX(_Q_PhotoButton.frame);
    y = CGRectGetMaxY(_Q_nameLable.frame);
    width = 200.0f;
    height = 20.0f;
    self.Q_InfoLable.frame = CGRectMake(x, y, width, height);

    /* 问题内容 */
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
    CGSize contentSize =[object.qContent boundingRectWithSize:CGSizeMake(220.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
    self.Q_contentLabel.text = object.qContent;
    x      = 50.0;
    y      = self.Q_nameLable.frame.origin.y + self.Q_nameLable.frame.size.height;
    width  = 220.0;
    height = contentSize.height;
    self.Q_contentLabel.frame = CGRectMake(x, y + 10.0, width, height);

    /* 问题图片 */
    x = 20.0;
    y = self.Q_contentLabel.frame.origin.y + self.Q_contentLabel.frame.size.height;
    width = 72.0f;
    height = 72.0f;
    if ([object._QquesPicList count] >0) {
        NSArray *imageView_array =@[self.Q_image1,self.Q_image2,self.Q_image3,self.Q_image4,self.Q_image5,self.Q_image6,self.Q_image7,self.Q_image8,self.Q_image9];
        NSInteger count = [object._QquesPicList count]<=[imageView_array count]?[object._QquesPicList count]:[imageView_array count];
        for (int i = 0; i < count; i++) {
            UIImageView *imageview = imageView_array[i];
            imageview.hidden = NO;
            [imageview setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/315x",[object._QquesPicList objectAtIndex:i]]] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
            imageview.frame = CGRectMake((52.5 + 10.0) * i + x, y + 10.0, width, height);
            UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapGesture:)];
            tapGesture.numberOfTapsRequired=1;
            imageview.userInteractionEnabled = YES;
            [imageview addGestureRecognizer:tapGesture];
        }

        y = self.Q_image1.frame.origin.y + self.Q_image1.frame.size.height;
    }else{

        y = self.Q_contentLabel.frame.origin.y + self.Q_contentLabel.frame.size.height;
    }

    return;
    /* 答案Q标示 */
    x = 12.0;
    y = self.CutoffRuleImage.frame.origin.y + self.CutoffRuleImage.frame.size.height + 12.0;
    width = 15.0;
    height = 15.0;
    self.A_TypeImage.frame = CGRectMake(x, y, width, height);

    /* 回答者用户头像 */
    x = 12.0;
    y = self.CutoffRuleImage.frame.origin.y + self.CutoffRuleImage.frame.size.height + 12.0;
    width = 30.0;
    height = 30.0;
    [self.A_PhotoButton setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/60x",object.aHeadImg]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
    self.A_PhotoButton.frame = CGRectMake(x, y, width, height);

    /* 答者昵称 */
    x = 50.0;
    y = self.CutoffRuleImage.frame.origin.y + self.CutoffRuleImage.frame.size.height + 10.0;
    width = 200.0;
    height = 20.0;
    self.A_nameLable.frame = CGRectMake(x, y, width, height);
    self.A_nameLable.text = [NSString stringWithFormat:@"%@ 回复:",object.aNickName];

    /* 答案 */
    CGSize A_contentSize =[object.aContent boundingRectWithSize:CGSizeMake(220.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
    self.A_contentLabel.text = object.aContent;
    x = 50.0;
    y = self.A_nameLable.frame.origin.y + self.A_nameLable.frame.size.height + 10.0;
    width = 220.0;
    height = A_contentSize.height;
    self.A_contentLabel.frame = CGRectMake(x, y, width, height);


    /* 答案图片 */
    x = 20.0;
    y = self.A_contentLabel.frame.origin.y + self.A_contentLabel.frame.size.height ;
    width = 72.0f;
    height = 72.0f;
    if ([object._AquesPicList count] >0) {

        NSArray *imageView_array =@[self.A_image1,self.A_image2,self.A_image3,self.A_image4,self.A_image5,self.A_image6,self.A_image7,self.A_image8,self.A_image9];
        NSInteger count = [object._AquesPicList count]<=[imageView_array count]?[object._AquesPicList count]:[imageView_array count];
        for (int i = 0; i < count; i++) {
            UIImageView *imageview = imageView_array[i];
            imageview.hidden = NO;
            [imageview setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/315x",[object._AquesPicList objectAtIndex:i]]] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
            imageview.frame = CGRectMake((52.5 + 10.0) * i + x, y+ 10.0, width, height);
            UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapGesture:)];
            imageview.userInteractionEnabled = YES;
            tapGesture.numberOfTapsRequired=1;
            [imageview addGestureRecognizer:tapGesture];
        }

        y = self.A_image1.frame.origin.y + self.A_image1.frame.size.height;
    }

    /* 单元格线 */
    x = 0.0;
    y = y + 9.0;
    width = 320.0;
    height = 1.0;
    self.lineImage.frame = CGRectMake(x, y, width, height);

}

- (void)handleTapGesture:(UIGestureRecognizer*)sender
{
    [SJAvatarBrowser showImage:(UIImageView*)sender.view];
}

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
    iEver_V_quesList_object *V_answer_object = (iEver_V_quesList_object *)object;

    // 上边缘到提问者昵称间隙
    CGFloat height = 11.0f;
    /* 昵称 */
    height += 20.0f;
    // 标签
    height += 20.0f;
    /* 问题内容 */
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
    CGSize contentSize =[V_answer_object.qContent boundingRectWithSize:CGSizeMake(220.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
    if (contentSize.height > 0.0) {
        height = height + contentSize.height + 10.0;
    }
    /* 图片 */
    NSInteger count = [V_answer_object._QquesPicList count]<=9?[V_answer_object._QquesPicList count]:9;
    if (count%3 == 0)
    {
        height += 72.0f*[V_answer_object._QquesPicList count]/3;
    } else {
        height += 72.0f*([V_answer_object._QquesPicList count]/3 + 1);
    }
    height += 20.0f;
    height += 8.0f;

    height += 20.0f;
    // 标签
    height += 20.0f;
    /* 问题内容 */
    tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
    contentSize =[V_answer_object.aContent boundingRectWithSize:CGSizeMake(220.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;
    if (contentSize.height > 0.0) {
        height = height + contentSize.height + 10.0;
    }
    /* 图片 */
    count = [V_answer_object._AquesPicList count]<=9?[V_answer_object._AquesPicList count]:9;
    if (count%3 == 0)
    {
        height += 72.0f*[V_answer_object._AquesPicList count]/3;
    } else {
        height += 72.0f*([V_answer_object._AquesPicList count]/3 + 1);
    }
    height += 20.0f;
    height += 8.0f;
   	return height;
}

# pragma mark - view getters
/*背景视图*/
- (UIView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [[UIView alloc]init];
        _bottomView.backgroundColor = TYR_BACKGRAYCOLOR;
    }
    return _bottomView;
}

/* 问者头像按钮 */
- (UIButton *)Q_PhotoButton
{
    if (!_Q_PhotoButton){
        _Q_PhotoButton = [UIButton buttonWithType:UIButtonTypeCustom];
        CALayer *photoButton = [_Q_PhotoButton layer];
        [photoButton setMasksToBounds:YES];
        [photoButton setCornerRadius:15.0];
    }
    return _Q_PhotoButton;
}

/* 问者标示图标 */
-(UIImageView *)Q_TypeImage{

    if (!_Q_TypeImage) {
        _Q_TypeImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"discover_V_Q"]];
    }
    return _Q_TypeImage;
}

/* 问者昵称 */
- (UILabel *)Q_nameLable
{
    if (!_Q_nameLable){
        _Q_nameLable = [[UILabel alloc] init];
        _Q_nameLable.numberOfLines = 1;
        _Q_nameLable.backgroundColor = CLEAR;
        _Q_nameLable.textColor = BLACK;
        _Q_nameLable.font = TitleFont;
    }
    return _Q_nameLable;
}

/* 提问时间 */
- (UILabel *)Q_TimeLable
{
    if (!_Q_TimeLable){
        _Q_TimeLable = [[UILabel alloc] init];
        _Q_TimeLable.numberOfLines = 1;
        _Q_TimeLable.backgroundColor = CLEAR;
        _Q_TimeLable.textColor = BLACK;
        _Q_TimeLable.font = TitleFont;
    }
    return _Q_TimeLable;
}

/* 问者昵称 */
- (UILabel *)Q_InfoLable
{
    if (!_Q_InfoLable){
        _Q_InfoLable = [[UILabel alloc] init];
        _Q_InfoLable.numberOfLines = 1;
        _Q_InfoLable.backgroundColor = CLEAR;
        _Q_InfoLable.textColor = BLACK;
        _Q_InfoLable.font = TitleFont;
    }
    return _Q_InfoLable;
}

- (UIButton *)Q_DetailButton
{
    if (!_Q_DetailButton)
    {
        _Q_DetailButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_Q_DetailButton setBackgroundColor:CLEAR];
    }
    return _Q_DetailButton;
}

/* 问题 */
- (UILabel *)Q_contentLabel
{
    if (!_Q_contentLabel){
        _Q_contentLabel = [[UILabel alloc] init];
        _Q_contentLabel.backgroundColor = CLEAR;
        _Q_contentLabel.numberOfLines = 0;
        _Q_contentLabel.textColor = BLACK;
        _Q_contentLabel.font = TextFont;
    }
    return _Q_contentLabel;
}

/* 答者头像 */
- (UIButton *)A_PhotoButton
{
    if (!_A_PhotoButton){
        _A_PhotoButton = [UIButton buttonWithType:UIButtonTypeCustom];
        CALayer *photoButton = [_A_PhotoButton layer];
        [photoButton setMasksToBounds:YES];
        [photoButton setCornerRadius:15.0];
    }
    return _A_PhotoButton;
}

/* 答者图片标示 */
-(UIImageView *)A_TypeImage{

    if (!_A_TypeImage) {
        _A_TypeImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"MY_VuserType"]];
    }
    return _A_TypeImage;
}

/* 答者昵称 */
- (UILabel *)A_nameLable
{
    if (!_A_nameLable){
        _A_nameLable = [[UILabel alloc] init];
        _A_nameLable.numberOfLines = 1;
        _A_nameLable.backgroundColor = CLEAR;
        _A_nameLable.textColor = V_COMMENTCOLOR;
        _A_nameLable.font = TextFont;
    }
    return _A_nameLable;
}

/* 答者昵称 */
- (UILabel *)A_TimeLable
{
    if (!_A_TimeLable){
        _A_TimeLable = [[UILabel alloc] init];
        _A_TimeLable.numberOfLines = 1;
        _A_TimeLable.backgroundColor = CLEAR;
        _A_TimeLable.textColor = V_COMMENTCOLOR;
        _A_TimeLable.font = TextFont;
    }
    return _A_TimeLable;
}

/* 答案 */
- (UILabel *)A_InfoLable
{
    if (!_A_InfoLable){
        _A_InfoLable = [[UILabel alloc] init];
        _A_InfoLable.backgroundColor = CLEAR;
        _A_InfoLable.numberOfLines = 0;
        _A_InfoLable.textColor = BLACK;
        _A_InfoLable.font = TextFont;
    }
    return _A_InfoLable;
}

 /*喜欢次数*/
- (UILabel *)likeNumLabel
{
    if (!_likeNumLabel){
        _likeNumLabel = [[UILabel alloc] init];
        _likeNumLabel.backgroundColor = CLEAR;
        _likeNumLabel.textColor = BLACK;
        _likeNumLabel.textAlignment = NSTextAlignmentRight;
        _likeNumLabel.font = TextFont;
    }
    return _likeNumLabel;
}

/*喜欢按钮*/
- (MCFireworksButton *)likeButton
{
    if (!_likeButton){
        _likeButton = [MCFireworksButton buttonWithType:UIButtonTypeCustom];
        self.likeButton.particleImage = [UIImage imageNamed:@"Sparkle"];
        self.likeButton.particleScale = 0.05;
        self.likeButton.particleScaleRange = 0.02;
        [_likeButton addTarget:self action:@selector(likeAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _likeButton;
}
-(void)likeAction:(id)sender{

//    iEver_likeActionObject * content = [[iEver_likeActionObject alloc] init];
//    if (!_whole_object._selected) {
//
//        NSDictionary *dic = @{@"businessId": [NSNumber numberWithInt:_whole_object.ques_id],
//                              @"type": [NSNumber numberWithInt:10],
//                              };
//
//        [[[content like:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
//         subscribeNext:^(NSDictionary *object) {
//
//             if ([[object objectForKey:@"resultCode"] integerValue] == 1) {
//
//                 _whole_object._selected = !_whole_object._selected;
//                 _whole_object.likeTotal = _whole_object.likeTotal +1;
//                 [self.likeButton popOutsideWithDuration:0.5];
//                 [self.likeButton setImage:[UIImage imageNamed:@"detail_commentLike_click"] forState:UIControlStateNormal];
//                 self.likeNumLabel.text = [NSString stringWithFormat:@"%d",_whole_object.likeTotal];
//                 [self.likeButton animate];
//
//             }else{
//                 NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
//                 [SVProgressHUD showImage:Nil status:codeStr];
//             }
//         }];
//    }
//    else {
//
//        NSDictionary *dic = @{@"businessId": [NSNumber numberWithInt:_whole_object.ques_id],
//                              @"type": [NSNumber numberWithInt:10],
//                              };
//
//        [[[content unlike:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
//         subscribeNext:^(NSDictionary *object) {
//
//             if ([[object objectForKey:@"resultCode"] integerValue] == 1) {
//
//                 _whole_object._selected = !_whole_object._selected;
//                 _whole_object.likeTotal = _whole_object.likeTotal -1;
//                 [self.likeButton popInsideWithDuration:0.4];
//                 [self.likeButton setImage:[UIImage imageNamed:@"detail_commentLike"] forState:UIControlStateNormal];
//                 self.likeNumLabel.text = [NSString stringWithFormat:@"%d",_whole_object.likeTotal];
//
//             }else{
//                 NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
//                 [SVProgressHUD showImage:Nil status:codeStr];
//             }
//         }];
//    }
}

/* 单元格分割线图片标示 */
-(UIImageView *)lineImage{

    if (!_lineImage) {
        _lineImage       = [[UIImageView alloc] init];
        _lineImage.image = [UIImage imageNamed:@"V_colorLine"];
        _lineImage.backgroundColor = CLEAR;
    }
    return _lineImage;
}

/* 问答之间分割线 */
-(UIImageView *)CutoffRuleImage{

    if (!_CutoffRuleImage) {
        _CutoffRuleImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"V_answerLine"]];
    }
    return _CutoffRuleImage;
}

/* 问题图片1 */
-(UIImageView *)Q_image1{

    if (!_Q_image1) {
        _Q_image1        = [[UIImageView alloc] init];
        _Q_image1.hidden = YES;
    }
    return _Q_image1;
}
/* 问题图片2 */
-(UIImageView *)Q_image2{

    if (!_Q_image2) {
        _Q_image2 = [[UIImageView alloc] init];
        _Q_image2.hidden = YES;
    }
    return _Q_image2;
}
/* 问题图片3 */
-(UIImageView *)Q_image3{

    if (!_Q_image3) {
        _Q_image3 = [[UIImageView alloc] init];
        _Q_image3.hidden = YES;
    }
    return _Q_image3;
}
/* 问题图片4 */
-(UIImageView *)Q_image4{

    if (!_Q_image4) {
        _Q_image4 = [[UIImageView alloc] init];
        _Q_image4.hidden = YES;
    }
    return _Q_image4;
}

/* 问题图片5 */
-(UIImageView *)Q_image5{

    if (!_Q_image5) {
        _Q_image5 = [[UIImageView alloc] init];
        _Q_image5.backgroundColor = BLACK;
        _Q_image5.contentMode = UIViewContentModeScaleAspectFill;
        _Q_image5.hidden = YES;
    }
    return _Q_image5;
}

/* 问题图片6 */
-(UIImageView *)Q_image6{

    if (!_Q_image6) {
        _Q_image6 = [[UIImageView alloc] init];
        _Q_image6.backgroundColor = BLACK;
        _Q_image6.contentMode = UIViewContentModeScaleAspectFill;
        _Q_image6.hidden = YES;
    }
    return _Q_image6;
}

/* 问题图片7 */
-(UIImageView *)Q_image7{

    if (!_Q_image7) {
        _Q_image7 = [[UIImageView alloc] init];
        _Q_image7.backgroundColor = BLACK;
        _Q_image7.contentMode = UIViewContentModeScaleAspectFill;
        _Q_image7.hidden = YES;
    }
    return _Q_image7;
}

/* 问题图片8 */
-(UIImageView *)Q_image8{

    if (!_Q_image8) {
        _Q_image8 = [[UIImageView alloc] init];
        _Q_image8.backgroundColor = BLACK;
        _Q_image8.contentMode = UIViewContentModeScaleAspectFill;
        _Q_image8.hidden = YES;
    }
    return _Q_image8;
}

/* 问题图片9 */
-(UIImageView *)Q_image9{

    if (!_Q_image9) {
        _Q_image9 = [[UIImageView alloc] init];
        _Q_image9.backgroundColor = BLACK;
        _Q_image9.contentMode = UIViewContentModeScaleAspectFill;
        _Q_image9.hidden = YES;
    }
    return _Q_image9;
}
/* 答案图片1 */
-(UIImageView *)A_image1{

    if (!_A_image1) {
        _A_image1 = [[UIImageView alloc] init];
        _A_image1.hidden = YES;
    }
    return _A_image1;
}
/* 答案图片2 */
-(UIImageView *)A_image2{

    if (!_A_image2) {
        _A_image2 = [[UIImageView alloc] init];
        _A_image2.hidden = YES;
    }
    return _A_image2;
}
/* 答案图片3*/
-(UIImageView *)A_image3{

    if (!_A_image3) {
        _A_image3 = [[UIImageView alloc] init];
        _A_image3.hidden = YES;
    }
    return _A_image3;
}
/* 答案图片4 */
-(UIImageView *)A_image4{

    if (!_A_image4) {
        _A_image4 = [[UIImageView alloc] init];
        _A_image4.hidden = YES;
    }
    return _A_image4;
}
/* 答案图片5 */
-(UIImageView *)A_image5{

    if (!_A_image5) {
        _A_image5 = [[UIImageView alloc] init];
        _A_image5.hidden = YES;
    }
    return _A_image5;
}
/* 答案图片6 */
-(UIImageView *)A_image6{

    if (!_A_image6) {
        _A_image6 = [[UIImageView alloc] init];
        _A_image6.hidden = YES;
    }
    return _A_image6;
}
/* 答案图片3*/
-(UIImageView *)A_image7{

    if (!_A_image7) {
        _A_image7 = [[UIImageView alloc] init];
        _A_image7.hidden = YES;
    }
    return _A_image7;
}
/* 答案图片8 */
-(UIImageView *)A_image8{

    if (!_A_image8) {
        _A_image8 = [[UIImageView alloc] init];
        _A_image8.hidden = YES;
    }
    return _A_image8;
}
/* 答案图片4 */
-(UIImageView *)A_image9{

    if (!_A_image9) {
        _A_image9 = [[UIImageView alloc] init];
        _A_image9.hidden = YES;
    }
    return _A_image9;
}

@end
