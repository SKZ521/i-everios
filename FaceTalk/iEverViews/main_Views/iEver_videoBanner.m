//
//  iEver_videoBanner.m
//  FaceTalk
//
//  Created by kevin on 15/3/29.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_videoBanner.h"
#import "UIView+FindViewController.h"
#import "iEver_mainContent.h"
#import "iEver_OfficialDetailViewController.h"
#import "UIView+FindViewController.h"
#import "iEver_V_ListDetailViewController.h"


@interface iEver_videoBanner()

@property (strong, nonatomic) UIScrollView *scrollView;

@end

@implementation iEver_videoBanner

#define KScrollViewHeight 94.0f

#define Kimage_Height 84.0f

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self initiaSubViews];
        
        
    }
    return self;
}

- (void)initiaSubViews
{
    
    [self.baseView addSubview:self.scrollView];
    
    [self addSubview:self.baseView];
}


- (void)setObject:(iEver_mainContent *)object
{
    if (![object._videoBannerList count]) {
        return;
    }
    
    for (int i = 0; i < [object._videoBannerList count]; i++) {
        
        iEver_videoBannerList_object *_videoBanner_Object = [object._videoBannerList objectAtIndex:i];
        [self addImageWithName:_videoBanner_Object atPosition:i];
        
    }
    
    [self.scrollView setContentSize:CGSizeMake(274/2 * [object._videoBannerList count], KScrollViewHeight)];
    
}


- (void)addImageWithName:(iEver_videoBannerList_object *)iAd atPosition:(int)position

{
    UIImage *main_list_TV         = [UIImage imageNamed:@"main_list_TV"];
    UIImage *main_list_play_small = [UIImage imageNamed:@"main_list_play_small"];
    
    
    UIView *coverBaseView = [[UIView alloc] initWithFrame:CGRectMake(274/2 * position, 0, 274/2, KScrollViewHeight)];
    
    UIImageView *coverImage = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, main_list_TV.size.width, main_list_TV.size.height)];
    
    [coverImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/180x/interlace/1",iAd.coverImg]] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
    
    UIImageView *tv_Image = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, main_list_TV.size.width, main_list_TV.size.height)];
    tv_Image.image =  main_list_TV;
    
    UIImageView * play_small = [[UIImageView alloc] initWithFrame:CGRectMake((tv_Image.frame.size.width/2 - main_list_play_small.size.width/2), (tv_Image.frame.size.height/2 - main_list_play_small.size.height/2), main_list_play_small.size.width, main_list_play_small.size.height)];
    play_small.image = main_list_play_small;
    

    UIButton *button     = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame         = CGRectMake( 5 , 5, main_list_TV.size.width, main_list_TV.size.height);
    [[button rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *button) {

        if (iAd.homeType == 10) {

            iEver_OfficialDetailViewController *_officialDetailViewController = [[iEver_OfficialDetailViewController alloc]init];
            _officialDetailViewController.type_id = iAd.articleId;
            _officialDetailViewController.hidesBottomBarWhenPushed = YES;
            UIViewController *superVC = (UIViewController *)[self firstAvailableUIViewController];
            [superVC.navigationController pushViewController:_officialDetailViewController animated:YES];
            if ([superVC.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
                superVC.navigationController.interactivePopGestureRecognizer.delegate = nil;
            }
        }else if (iAd.homeType == 17) {

            iEver_V_ListDetailViewController *_V_ListDetailViewController = [[iEver_V_ListDetailViewController alloc]init];
            _V_ListDetailViewController.type_id = iAd.articleId;
            _V_ListDetailViewController.hidesBottomBarWhenPushed = YES;
            UIViewController *superVC = (UIViewController *)[self firstAvailableUIViewController];
            [superVC.navigationController pushViewController:_V_ListDetailViewController animated:YES];
            if ([superVC.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
                superVC.navigationController.interactivePopGestureRecognizer.delegate = nil;
            }
        }
        
    }];
    
    [coverBaseView addSubview:coverImage];
    
    [coverBaseView addSubview:tv_Image];
    
    [coverBaseView addSubview:play_small];
    
    [coverBaseView addSubview:button];
    
    [self.scrollView addSubview:coverBaseView];
}


#pragma mark -
#pragma mark layoutSubviews (update view)

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.scrollView.frame = CGRectMake(0, 0, ScreenWidth, KScrollViewHeight);
}

#pragma mark -
#pragma mark SET
- (UIScrollView *)scrollView
{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.pagingEnabled = NO;
        _scrollView.backgroundColor = CLEAR;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
    }
    
    return _scrollView;
}


- (UIView *)baseView
{
    if (!_baseView) {
        _baseView = [[UIView alloc] init];
        _baseView.frame = CGRectMake(0, 0, ScreenWidth, KScrollViewHeight);
        _baseView.backgroundColor = CLEAR;
    }
    
    return _baseView;
}





+ (CGFloat)heightForVideoBanner{

    return KScrollViewHeight;
}
@end
