//
//  iEver_videoBanner.h
//  FaceTalk
//
//  Created by kevin on 15/3/29.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import <UIKit/UIKit.h>

@class iEver_mainContent;

@interface iEver_videoBanner : UIView

@property(nonatomic,strong)iEver_mainContent *object;

@property (strong, nonatomic) UIView          *baseView;

+ (CGFloat)heightForVideoBanner;

@end
