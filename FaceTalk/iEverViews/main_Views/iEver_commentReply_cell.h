//
//  iEver_commentReply_cell.h
//  FaceTalk
//
//  Created by 开仲 司 on 15/5/15.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "SSBaseTableCell.h"
#import "iEver_commentReply_object.h"

@interface iEver_commentReply_cell : SSBaseTableCell

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;

@property (nonatomic, strong) iEver_acList_object *object;

@end
