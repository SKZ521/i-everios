//
//  iEver_itemDetailCell_PicList.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-23.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "SSBaseTableCell.h"
#import "iEver_itemDetail_Object.h"

@interface iEver_itemDetailCell_PicList : SSBaseTableCell
+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;
@property (nonatomic, strong) iEver_detail_itemPicList_object *object;
@end
