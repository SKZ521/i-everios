//
//  iEver_itemDetail_recommendItem_cell.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-24.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "SSBaseTableCell.h"
#import "iEver_itemDetail_Object.h"
@class iEver_ItemDetailViewController;
@interface iEver_itemDetail_recommendItem_cell : SSBaseTableCell
+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;
- (void)setViewController:(iEver_ItemDetailViewController *)mvc;
@property (nonatomic, strong) iEver_itemDetail_Object *object;
@end
