//
//  iEver_tryItemDetail_cell.m
//  FaceTalk
//
//  Created by 开仲 司 on 14/11/27.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_tryItemDetail_cell.h"
@interface iEver_tryItemDetail_cell()
@property (nonatomic, strong) UIView        *bottomView;     /* 背景视图 */
@property (nonatomic, strong) UILabel       *label;      /* 用户名称 */
@end

@implementation iEver_tryItemDetail_cell
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void) configureCell {
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = CLEAR;
        self.contentView.backgroundColor = CLEAR;
        [self.contentView addSubview:self.bottomView];
        [self.contentView addSubview:self.label];
    }
    return self;
}
- (void)awakeFromNib
{
    // Initialization code
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    /*单元格frame */
    //const CGRect r = self.contentView.bounds;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}
- (void)setObject:(NSString *)object
{
    /* 用户图片 */
    CGFloat x = 15.0;
    CGFloat y = 5.0;
    CGFloat width = 290.0;
    CGFloat height = 36.0;
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
    CGSize contentSize =[object boundingRectWithSize:CGSizeMake(290.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;

    height = contentSize.height;
    self.label.frame = CGRectMake(x, y, width, height);
    self.label.text = object;

    /* 背景视图 */
    x = 0.0;
    y = 0.0;
    height = contentSize.height + 10.0;
    width = 320.0;
    self.bottomView.frame = CGRectMake(x, y, width, height);

}
+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
    /* 获取描述文字高度 */
    float return_height = 0.0;
    NSString *str = (NSString *)object;
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:14.0], NSFontAttributeName,nil];
    CGSize contentSize =[str boundingRectWithSize:CGSizeMake(290.0, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:tdic context:nil].size;

    return_height =  contentSize.height + 10.0;
    return return_height;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
/* 背景视图 */
- (UIView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [[UIView alloc]init];
        _bottomView.backgroundColor = WHITE;
    }
    return _bottomView;
}
/* 用户名称 */
- (UILabel *)label
{
    if (!_label){
        _label = [[UILabel alloc] init];
        _label.numberOfLines = 0;
        _label.backgroundColor = CLEAR;
        _label.textColor = BLACK;
        _label.font = TextFont;
    }
    return _label;
}
@end
