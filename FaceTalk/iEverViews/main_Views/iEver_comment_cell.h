//
//  iEver_comment_cell.h
//  FaceTalk
//
//  Created by 开仲 司 on 14/12/23.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "SSBaseTableCell.h"
#import "iEver_detailCommentObject.h"

@interface iEver_comment_cell : SSBaseTableCell

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;

@property (nonatomic, strong) iEver_articleCommentList_object *object;


@end
