//
//  iEver_itemRank_cell.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-28.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_itemRank_cell.h"
#import "iEver_likeActionObject.h"
#import "CWStarRateView.h"

@interface iEver_itemRank_cell()

@property (nonatomic, strong) UIImageView   *markImage;        /* 数字背景图片 */
@property (nonatomic, strong) UILabel       *markLabel;        /* 数字标示 */
@property (nonatomic, strong) UIImageView   *levelImage;       /* 升降图片 */
@property (nonatomic, strong) UIImageView   *itemImage;        /* 商品图片 */
@property (nonatomic, strong) UILabel       *titleLabel;       /* 商品标题 */
@property (nonatomic, strong) UILabel       *subTitleLabel;    /* 商品副标题 */
@property (nonatomic, strong) CWStarRateView *starView;        /* 评分视图 */
@property (nonatomic, strong) UILabel       *personTotalLabel; /* 使用人数 */
@property (nonatomic, strong) UILabel       *startGradeLabel;  /* 使用人数 */
@property (nonatomic, strong) UIImageView   *lineView;         /* 视图横线 */
@property (nonatomic, strong) iEver_itemTopList_object *itemTopList_object;

@end

@implementation iEver_itemRank_cell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void) configureCell {
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
		self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = CLEAR;
        [self.contentView addSubview:self.markImage];
        [self.markImage addSubview:self.markLabel];
        [self.contentView addSubview:self.itemImage];
        [self.contentView addSubview:self.levelImage];
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.subTitleLabel];
        [self.contentView addSubview:self.starView];
        [self.contentView addSubview:self.personTotalLabel];
        [self.contentView addSubview:self.startGradeLabel];
        [self.contentView addSubview:self.lineView];

    }
    return self;
}
- (void)awakeFromNib
{
    // Initialization code
}
- (void)layoutSubviews
{
	[super layoutSubviews];
    /*单元格frame */
    //const CGRect r = self.contentView.bounds;

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}
- (void)setObject:(iEver_itemTopList_object *)object
{
    _itemTopList_object = object;
    CGFloat x = 0.0;
    CGFloat y = 0.0;
    CGFloat width = 0.0;
    CGFloat height = 0.0;

    /* 数字背景图 */
    UIImage *rankList_rankSmallBox = [UIImage imageNamed:@"rankList_rankSmallBox"];
    x = 13.0;
    y = 25.0;
    width = rankList_rankSmallBox.size.width;
    height = rankList_rankSmallBox.size.height;
    self.markImage.image = rankList_rankSmallBox;
    self.markImage.frame = CGRectMake(x, y, width, height);

    /* 数字标示 */
    x = rankList_rankSmallBox.size.width/2 - 10;
    y = rankList_rankSmallBox.size.height/2 - 10;
    width = 20.0;
    height = 20.0;
    self.markLabel.frame = CGRectMake(x, y, width, height);
    self.markLabel.text  = [NSString stringWithFormat:@"%d",object.rowNum];

    /* 升降图片 */
    UIImage *rank_rise = [UIImage imageNamed:@"rank_rise"];
    UIImage *rank_parallel = [UIImage imageNamed:@"rank_parallel"];
    UIImage *rank_drop = [UIImage imageNamed:@"rank_drop"];
    x = rankList_rankSmallBox.size.width/2 - rank_rise.size.width/2;
    y = self.markImage.frame.origin.y + self.markImage.frame.size.height + 5;
    width = rank_rise.size.width;
    height = rank_rise.size.height;
    if (object.beforeLevel == 0) {
        self.levelImage.hidden        = YES;
    }else{

        if (object.sortLevel > object.beforeLevel) {
            self.levelImage.image             = rank_rise;
        }else if(object.sortLevel == object.beforeLevel){
            self.levelImage.image             = rank_parallel;
        }else if(object.sortLevel < object.beforeLevel){
            self.levelImage.image             = rank_drop;
        }

        self.levelImage.hidden         = NO;
    }
        self.levelImage.frame          = CGRectMake(x, y, width, height);

    /* 步骤图片 */
    x = self.markImage.frame.origin.x + self.markImage.frame.size.width;
    y = 10.0;
    width = 80.0;
    height = 80.0;
    self.itemImage.frame           = CGRectMake(x, y, width, height);
    [self.itemImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/160x",object.itemImg]] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];

    /* 商品标题 */
    x = self.itemImage.frame.origin.x + self.itemImage.frame.size.width + 4;
    y = 10.0;
    width = 170.0;
    height = 40.0;
    self.titleLabel.frame          = CGRectMake(x, y, width, height);
    self.titleLabel.text           = object.itemName;
    NSDictionary * name_dic        = [NSDictionary dictionaryWithObjectsAndKeys:self.titleLabel.font, NSFontAttributeName,nil];
    CGSize name_silver_Size        = [object.itemName boundingRectWithSize:CGSizeMake(width, height)
                                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                                attributes:name_dic
                                                                   context:nil].size;
    self.titleLabel.frame          = CGRectMake(x, y, width, name_silver_Size.height);

    /* 评分视图 */
    self.starView.scorePercent     = (object.startGrade *2)/10.f;

    /* 分数 */
    x = 190.0;
    y = 40.0;
    width = 80;
    height = 15;
    self.startGradeLabel.frame     = CGRectMake(x, y, width, height);
    self.startGradeLabel.text      = [NSString stringWithFormat:@"%.1f",object.startGrade];

    /* 使用人数 */
    x = 124.0;
    y = 51.0;
    width = 150.0;
    height = 25.0;
    self.personTotalLabel.frame    = CGRectMake(x, y, width, height);
    self.personTotalLabel.text     = [NSString stringWithFormat:@"%d人关注",object.pvTotal];


    /* 商品副标题 */
    x = 124.0;
    y = 68.0;
    width = 150.0;
    height = 25.0;
    self.subTitleLabel.frame       = CGRectMake(x, y, width, height);
    self.subTitleLabel.text        = [NSString stringWithFormat:@"RMB%d/%@",object.price,object.itemSpec];



    /* 视图横线 */
    x = 15.0;
    y = 99.0;
    height = 1.0;
    width = 290.0;
    self.lineView.frame            = CGRectMake(x, y, width, height);

}
+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
	return 100.0;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
# pragma mark - view getters

/* 数字标示背景图 */
- (UIImageView *)markImage
{
    if (!_markImage) {
        _markImage = [[UIImageView alloc]init];
        _markImage.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _markImage;
}


/* 数字标示 */
- (UILabel *)markLabel
{
	if (!_markLabel){
		_markLabel = [[UILabel alloc] init];
		_markLabel.numberOfLines = 1;
		_markLabel.backgroundColor = CLEAR;
		_markLabel.textColor = BLACK;
		_markLabel.font = TitleFont;
        _markLabel.textAlignment = NSTextAlignmentCenter;
	}
	return _markLabel;
}

/* 升降标示 */
- (UIImageView *)levelImage
{
    if (!_levelImage) {
        _levelImage = [[UIImageView alloc]init];
        _levelImage.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _levelImage;
}
/* 商品图片 */
- (UIImageView *)itemImage
{
    if (!_itemImage) {
        _itemImage = [[UIImageView alloc]init];
        _itemImage.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _itemImage;
}

/* 商品标题 */
- (UILabel *)titleLabel
{
	if (!_titleLabel){
		_titleLabel = [[UILabel alloc] init];
		_titleLabel.numberOfLines = 1;
		_titleLabel.backgroundColor = CLEAR;
		_titleLabel.textColor = BLACK;
		_titleLabel.font = Title_Font;
	}
	return _titleLabel;
}
/* 商品容量&价格 */
- (UILabel *)subTitleLabel
{
	if (!_subTitleLabel){
		_subTitleLabel = [[UILabel alloc] init];
        _subTitleLabel.numberOfLines = 1;
		_subTitleLabel.backgroundColor = CLEAR;
        _subTitleLabel.textColor = DAKEBLACK;
        _subTitleLabel.font = TextDESCFonts;
	}
	return _subTitleLabel;
}

/* 分数 */
- (UILabel *)startGradeLabel
{
    if (!_startGradeLabel){
        _startGradeLabel = [[UILabel alloc] init];
        _startGradeLabel.numberOfLines = 1;
        _startGradeLabel.backgroundColor = CLEAR;
        _startGradeLabel.textColor = RANK_UPDATE_GOLD;
        _startGradeLabel.font = TextFonts;
    }
    return _startGradeLabel;
}
/* 使用人数 */
- (UILabel *)personTotalLabel
{
	if (!_personTotalLabel){
		_personTotalLabel = [[UILabel alloc] init];
        _personTotalLabel.numberOfLines = 1;
		_personTotalLabel.backgroundColor = CLEAR;
        _personTotalLabel.textColor = GRAY_LIGHT;
        _personTotalLabel.font = TextFonts;
	}
	return _personTotalLabel;
}

/* 视图横线 */
- (UIImageView *)lineView
{
    if (!_lineView) {
        _lineView = [[UIImageView alloc]init];
        _lineView.image = [UIImage imageNamed:@"person_line"];
    }
    return _lineView;
}

/* 评分视图 */
- (CWStarRateView *)starView
{
    if (!_starView) {
        _starView = [[CWStarRateView alloc] initWithFrame:CGRectMake(124.0, 40, 65, 12) numberOfStars:5];

    }
    return _starView;
}
@end
