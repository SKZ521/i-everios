//
//  iEver_itemComment_cell.h
//  FaceTalk
//
//  Created by 开仲 司 on 15/5/16.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "SSBaseTableCell.h"
#import "iEver_itemDetailCommentObject.h"

@interface iEver_itemComment_cell : SSBaseTableCell

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;

@property (nonatomic, strong) iEver_itemCommentList_object *object;

@end
