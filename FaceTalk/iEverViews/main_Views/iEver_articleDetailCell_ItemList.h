//
//  iEver_articleDetailCell_ItemList.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-22.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "SSBaseTableCell.h"
#import "iEver_mainDetail_Object.h"
@class iEver_OfficialDetailViewController;

@interface iEver_articleDetailCell_ItemList : SSBaseTableCell
+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;
- (void)setViewController:(UINavigationController *)mvc;
@property (nonatomic, strong) iEver_detail_itemList_object *object;

@end
