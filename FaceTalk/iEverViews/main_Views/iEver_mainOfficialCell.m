//
//  iEver_mainOfficialCell.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-9-19.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_mainOfficialCell.h"
#import "iEver_mainContent.h"
#import "iEver_OfficialDetailViewController.h"
#import "MCFireworksButton.h"
#import "iEver_likeActionObject.h"
#import "DWTagList.h"

#import "iEver_OfficialListViewController.h"
#import "UIView+FindViewController.h"

// TabView for tabs, that provides un/selected state indicators
@class CecondCategoryAreaView;

@interface CecondCategoryAreaView : UIView
@property (nonatomic) UIColor *viewColor;
@end

@implementation CecondCategoryAreaView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)drawRect:(CGRect)rect {

    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGPoint sPoints[3];//坐标点
    
    sPoints[0] =CGPointMake(0.0, 0.0);//坐标1
    
    sPoints[1] =CGPointMake(70.0,0.0);//坐标2
    
    sPoints[2] =CGPointMake(70.0,70.0);//坐标3
    
    UIColor *clearColor=[UIColor clearColor];
    
    CGContextSetStrokeColorWithColor(context,clearColor.CGColor);
    
    CGContextSetFillColorWithColor(context,self.viewColor.CGColor);

    CGContextAddLines(context, sPoints, 3);//添加线
    
    CGContextClosePath(context);//封起来
    
    CGContextDrawPath(context, kCGPathFillStroke); //根据坐标绘制路径
    
    //执行绘画
    
    CGContextStrokePath(context);
    
    
}
@end

@class TitleAreaView;

@interface TitleAreaView : UIView
@property (nonatomic) UIColor *viewColor;
@end

@implementation TitleAreaView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}
- (void)drawRect:(CGRect)rect {
    
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGPoint sPoints[4];//坐标点
    
    sPoints[0] =CGPointMake(0.0, 0.0);//坐标1
    
    sPoints[1] =CGPointMake(300.0,0.0);//坐标2
    
    sPoints[2] =CGPointMake(300.0,63.0);//坐标3
    
    sPoints[3] =CGPointMake(0.0,63.0);//坐标3
    
    UIColor *clearColor=[UIColor clearColor];
    
    CGContextSetStrokeColorWithColor(context,clearColor.CGColor);
    
    //CGContextSetFillColorWithColor(context,self.viewColor.CGColor);

    CGContextSetFillColorWithColor(context,CLEAR.CGColor);

    CGContextAddLines(context, sPoints, 4);//添加线
    
    CGContextClosePath(context);//封起来
    
    CGContextDrawPath(context, kCGPathFillStroke); //根据坐标绘制路径
    
    //执行绘画
    
    CGContextStrokePath(context);
    
    
}
@end

static CGFloat cellHeight = 430.0f;


@interface iEver_mainOfficialCell (){

}
@property (nonatomic, strong) UIView                         *bottomView;           /*背景视图*/
@property (nonatomic, strong) UILabel                        *timeLable;            /*时间*/
@property (nonatomic, strong) UIImageView                    *browseImageView;      /*浏览图片*/
@property (nonatomic, strong) UILabel                        *browseLabel;          /*浏览量*/
@property (nonatomic, strong) UIImageView                    *startImageView;       /*播放图片*/

@property (nonatomic, strong) UIImageView                    *thirdCategoryView;             /*三级分类图片*/
@property (nonatomic, strong) UILabel                        *thirdCategoryLable;            /*三级分类*/

@property (nonatomic, strong) CecondCategoryAreaView         *secondCategoryAreaView;    /*二级分类背景*/
@property (nonatomic, strong) UILabel                        *secondCategoryLable;       /*二级分类name*/

@property (nonatomic, strong) TitleAreaView                  *titleAreaView;        /*背景视图*/
@property (nonatomic, strong) UILabel                        *titleLable;           /*标题*/
@property (nonatomic, strong) DWTagList                      *tagList;              /* 文章所属标签 */

@property (nonatomic, strong) UIButton                       *photoButtonClick;     /*头像按钮点击放大*/
@property (nonatomic, strong) UIImageView                    *placeholderImageView; /*默认图片*/


@property (nonatomic, strong) MCFireworksButton              *likeButton;           /*喜欢按钮*/
@property (nonatomic, strong) UIButton                       *shareButton;          /*分享按钮*/

@property (nonatomic, strong) UILabel                        *likeLable;            /*喜欢次数*/
@property (nonatomic, strong) UILabel                        *shareLable;           /*分享次数*/



@property (nonatomic, strong) iEver_mainContentList_object   *list_object;


@end
@implementation iEver_mainOfficialCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code Collection
    }
    return self;
}
- (void) configureCell {

}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
		self.selectionStyle              = UITableViewCellSelectionStyleNone;
        self.backgroundColor             = [UIColor clearColor];
        self.contentView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.bottomView];
        [self.contentView addSubview:self.timeLable];

        [self.contentView addSubview:self.thirdCategoryView];
        [self.contentView addSubview:self.thirdCategoryLable];

        [self.contentView addSubview:self.coverImage];
        [self.contentView addSubview:self.placeholderImageView];
        
        [self.contentView addSubview:self.secondCategoryAreaView];
        [self.contentView addSubview:self.secondCategoryLable];
        
        [self.contentView addSubview:self.browseImageView];
        [self.contentView addSubview:self.browseLabel];
        
        [self.contentView addSubview:self.startImageView];
        [self.contentView addSubview:self.titleAreaView];
        [self.contentView addSubview:self.titleLable];
        [self.contentView addSubview:self.tagList];
        
        [self.contentView addSubview:self.likeButton];
        [self.contentView addSubview:self.shareButton];
        
        [self.contentView addSubview:self.likeLable];
        [self.contentView addSubview:self.shareLable];
        
      
    }
    return self;
}
- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)layoutSubviews
{
	[super layoutSubviews];
    /*单元格frame */
    //const CGRect r = self.contentView.bounds;

    /*背景视图*/
    CGFloat x = 0.0;
    CGFloat y = 0.0;
    CGFloat width = 320.0;
    CGFloat height = 430.0;
    self.bottomView.frame = CGRectMake(x, y, width, height);

    /*三级分类图片*/
    UIImage *main_list_third = [UIImage imageNamed:@"main_list_third"];
    x = 5.0;
    y = 2.0;
    width = main_list_third.size.width;
    height = main_list_third.size.height;
    self.thirdCategoryView.frame = CGRectMake(x, y, width, height);

    /*三级分类*/
    x = 17.0;
    y = 0.0;
    width = 100.0;
    height = 12.0;
    self.thirdCategoryLable.frame = CGRectMake(x, y, width, height);

    
    /*时间标签*/
    x = 215.0;
    y = 0.0;
    width = 100.0;
    height = 12.0;
    self.timeLable.frame = CGRectMake(x, y, width, height);
    
    /*封面图片*/
    x = 0.0;
    y = 15.0;
    width = 320.0;
    height = 320.0;
    self.coverImage.frame = CGRectMake(x, y, width, height);
    
    /*二级分类颜色块*/
    x = 250.0;
    y = 15.0;
    width = 70.0;
    height = 70.0;
    self.secondCategoryAreaView.frame = CGRectMake(x, y, width, height);
    
    /*二级name*/
    x = 255.0;
    y = 20.0;
    width = 80.0;
    height = 40.0;
    self.secondCategoryLable.frame = CGRectMake(x, y, width, height);
    
    /*浏览标示图片*/
    x = 10.0;
    y = 320.0;
    width = 17.0;
    height = 11.0;
    self.browseImageView.frame = CGRectMake(x, y, width, height);
    
    /*浏览量*/
    x = 32;
    y = 315.0;
    width = 295.0;
    height = 20.0;
    self.browseLabel.frame = CGRectMake(x, y, width, height);
    
    /*播放图片*/
    UIImage *main_list_play_big = [UIImage imageNamed:@"main_list_play_big"];
    x = ScreenWidth/2 - main_list_play_big.size.width/2;
    y = 140;
    width = main_list_play_big.size.width;
    height = main_list_play_big.size.height;
    self.startImageView.frame = CGRectMake(x, y, width, height);
    
    
    /*标题区域*/
    x = 10;
    y = 235.0 + 15.0;
    width = 300;
    height = 63;
    self.titleAreaView.frame = CGRectMake(x, y, width, height);
    
    /*标题标签*/
    x = 22.0;
    y = 265.0;
    width = 276.0;
    height = 20.0;
    self.titleLable.frame = CGRectMake(x, y, width, height);
    


    /*头像按钮点击放大*/
    x = ScreenWidth - 70.0;
    y = 15.0;
    width = 70.0;
    height = 70.0;
    self.photoButtonClick.frame = CGRectMake(x, y, width, height);

    /*封面图片默认图片*/
    x = 0.0;
    y = 15.0;
    width = 320.0;
    height = 320.0;
    self.placeholderImageView.hidden = YES;
    self.placeholderImageView.frame = CGRectMake(x, y, width, height);
    
    /*喜欢按钮*/
    UIImage *main_list_like = [UIImage imageNamed:@"main_list_like"];
    x = 103;
    y = 355;
    width = main_list_like.size.width;
    height = main_list_like.size.height;
    self.likeButton.frame = CGRectMake(x, y, width, height);
    
    /*分享按钮*/
    UIImage *main_list_share = [UIImage imageNamed:@"main_list_share"];
    x = 180;
    y = 355;
    width = main_list_share.size.width;
    height = main_list_share.size.height;
    self.shareButton.frame = CGRectMake(x, y, width, height);
    
    /*喜欢次数*/
    x = 90;
    y = 355 + 50;
    width = 70;
    height = 20;
    self.likeLable.frame = CGRectMake(x, y, width, height);
    
    /*分享次数*/
    x = 165;
    y = 355 + 50;
    width = 70;
    height = 20;
    self.shareLable.frame = CGRectMake(x, y, width, height);
    
}

- (void)setObject:(iEver_mainContentList_object *)object
{

    _list_object = object;

    if (object.threeCategoryName.length > 0 ) {
        self.thirdCategoryLable.text = object.threeCategoryName;
        self.thirdCategoryLable.hidden = NO;
        self.thirdCategoryView.hidden = NO;

    }else{

        self.thirdCategoryLable.hidden = YES;
        self.thirdCategoryView.hidden = YES;
    }
    
    /* 时间标签 */
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY.MM.dd"];
    NSString *dateLoca = [NSString stringWithFormat:@"%lld",object.releaseTime/1000];
    NSTimeInterval time =[dateLoca doubleValue];
    NSDate *detaildate = [NSDate dateWithTimeIntervalSince1970:time];
    NSString *timestr = [formatter stringFromDate:detaildate];
    self.timeLable.text = timestr;


    /* 封面图片 */
    [self.coverImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/640x",object.coverImg]] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
    self.titleLable.text = object.articleTitle;
    
    /* 浏览标示图片 */
    if (object.videoLink.length > 0 || object.mVideoLink.length > 0) {
        
        self.browseImageView.image = [UIImage imageNamed:@"main_list_video"];
        
        self.startImageView.hidden = NO;
        
        
    }else {
    
        self.browseImageView.image = [UIImage imageNamed:@"main_list_number"];
        
        self.startImageView.hidden = YES;
        
    }
    
    /* 浏览量 */
    self.browseLabel.text = [NSString stringWithFormat:@"%d",object.pvTotal+ object.webPvTotal];
    
    /* 二级分类name */
    _secondCategoryLable.text = object.buttonName;
    
    /*编辑标签*/
    NSMutableArray *mutableArray_ID = [[NSMutableArray alloc] init];
    for (iEver_mainList_tagsList_object *tagObject in  object._tagsList) {
        [mutableArray_ID addObject:[NSNumber numberWithInt:tagObject.tagId]];
    }
    self.tagList.IDArray                 = mutableArray_ID;
    NSMutableArray *mutableArray         = [[NSMutableArray alloc] init];
    for (iEver_mainList_tagsList_object *tagObject in  object._tagsList) {
        [mutableArray addObject:[NSString stringWithFormat:@"#%@",tagObject.tagName]];
    }
    self.tagList.frame = CGRectMake(22.0, 290.0, 276.0, 18.0);
    [self.tagList setTags:mutableArray];
    
    [self.secondCategoryAreaView setViewColor:[[iEver_Global Instance] colorWithHexString:object.useCategoryColor]];

    self.titleAreaView.backgroundColor = [[iEver_Global Instance] colorWithHexString:object.useCategoryColor];

    [self.secondCategoryAreaView  setNeedsDisplay];

    if (object._selected) {
        [self.likeButton setImage:[UIImage imageNamed:@"main_list_liked"] forState:UIControlStateNormal];
    }else {
        [self.likeButton setImage:[UIImage imageNamed:@"main_list_like"] forState:UIControlStateNormal];
    }
    
    self.likeLable.text = [NSString stringWithFormat:@"%d喜欢",object.likeTotal];
    
    self.shareLable.text = [NSString stringWithFormat:@"%d分享",object.commentTotal];
  
}

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
	CGFloat h = cellHeight;
   	return h;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
# pragma mark - view getters
/*背景视图*/
- (UIView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [[UIView alloc]init];
        _bottomView.backgroundColor = [UIColor whiteColor];

    }
    return _bottomView;
}

/*时间标签*/
- (UILabel *)timeLable
{
    if (!_timeLable){
        _timeLable                 = [[UILabel alloc] init];
        _timeLable.backgroundColor = CLEAR;
        _timeLable.textColor       = LIST_EDITOR;
        _timeLable.textAlignment   = NSTextAlignmentRight;
        _timeLable.font            = TextDESCFonts;
    }
    return _timeLable;
}

/* 封面图片默认图片*/
- (UIImageView *)coverImage
{
    if (!_coverImage) {
        _coverImage             = [[UIImageView alloc]init];
        _coverImage.contentMode = UIViewContentModeScaleToFill;

    }
    return _coverImage;
}

/*三级分类图片*/
- (UIImageView *)thirdCategoryView
{
    if (!_thirdCategoryView) {
        _thirdCategoryView             = [[UIImageView alloc]init];
        _thirdCategoryView.image       = [UIImage imageNamed:@"main_list_third"];
        
    }
    return _thirdCategoryView;
}

/*三级分类*/
- (UILabel *)thirdCategoryLable
{
    if (!_thirdCategoryLable){
        _thirdCategoryLable                 = [[UILabel alloc] init];
        _thirdCategoryLable.backgroundColor = CLEAR;
        _thirdCategoryLable.textColor       = LIST_EDITOR;
        _thirdCategoryLable.textAlignment   = NSTextAlignmentLeft;
        _thirdCategoryLable.font            = TextDESCFonts;
    }
    return _thirdCategoryLable;
}


/*封面图片*/
- (UIImageView *)placeholderImageView
{
    if (!_placeholderImageView) {
        _placeholderImageView              = [[UIImageView alloc]init];
        _placeholderImageView.image        = [UIImage imageNamed:@"defualt_icon"];
        _placeholderImageView.contentMode  = UIViewContentModeScaleToFill;
        
    }
    return _placeholderImageView;
}

/*播放按钮*/
-(UIImageView *)startImageView{
    
    if (!_startImageView){
        _startImageView       = [[UIImageView alloc] init];
        _startImageView.image = [UIImage imageNamed:@"main_list_play_big"];
    }
    return _startImageView;
}

/*浏览图片标示*/
- (UIImageView *)browseImageView
{
    if (!_browseImageView) {
        _browseImageView             = [[UIImageView alloc]init];
        _browseImageView.contentMode = UIViewContentModeScaleToFill;
        
    }
    return _browseImageView;
}

/*浏览*/
- (UILabel *)browseLabel
{
    if (!_browseLabel){
        _browseLabel                  = [[UILabel alloc] init];
        _browseLabel.backgroundColor  = CLEAR;
        _browseLabel.textColor        = WHITE;
        _browseLabel.textAlignment    = NSTextAlignmentLeft;
        _browseLabel.shadowColor      = GRAY_LIGHT;
        _browseLabel.font             = TextDESCFonts;
    }
    return _browseLabel;
}



/*二级分类视图*/
- (CecondCategoryAreaView *)secondCategoryAreaView
{
    if (!_secondCategoryAreaView) {
        _secondCategoryAreaView = [[CecondCategoryAreaView alloc]init];
        
    }
    return _secondCategoryAreaView;
}

/*二级分类name*/
- (UILabel *)secondCategoryLable
{
    if (!_secondCategoryLable){
        _secondCategoryLable                  = [[UILabel alloc] init];
        _secondCategoryLable.backgroundColor  = CLEAR;
        _secondCategoryLable.textColor        = BLACK;
        _secondCategoryLable.textAlignment    = NSTextAlignmentCenter;
        _secondCategoryLable.font             = TextDESCFonts;
        _secondCategoryLable.transform = CGAffineTransformMakeRotation(M_PI/4);
    }
    return _secondCategoryLable;
}


/*背景视图*/
- (TitleAreaView *)titleAreaView
{
    if (!_titleAreaView) {
        _titleAreaView = [[TitleAreaView alloc] init];
        _titleAreaView.alpha = 0.7;
        
    }
    return _titleAreaView;
}
/*标题*/
- (UILabel *)titleLable
{
    if (!_titleLable){
        _titleLable = [[UILabel alloc] init];
        _titleLable.numberOfLines = 1;
        _titleLable.backgroundColor = CLEAR;
        _titleLable.textColor = BLACK;
        _titleLable.font = Title_BigFont;
        _titleLable.textAlignment = NSTextAlignmentLeft;
    }
    return _titleLable;
}
/* 文章类型 */
- (DWTagList *)tagList
{
    if (!_tagList){
        _tagList = [[DWTagList alloc] init];
    }
    return _tagList;
}

/*头像按钮点击区域放大*/
- (UIButton *)photoButtonClick
{
    if (!_photoButtonClick){
        _photoButtonClick.backgroundColor = CLEAR;
        _photoButtonClick = [UIButton buttonWithType:UIButtonTypeCustom];
        [_photoButtonClick addTarget:self action:@selector(queryByCategory:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _photoButtonClick;
}
-(void)queryByCategory:(id)sender{

    iEver_OfficialListViewController *_OfficialListViewController = [[iEver_OfficialListViewController alloc]init];
    _OfficialListViewController.category_id = _list_object.useCategoryId;
    _OfficialListViewController.categoryType = _list_object.categoryType;
    _OfficialListViewController.title = _list_object.buttonName;
    _OfficialListViewController.hidesBottomBarWhenPushed = YES;
    UIViewController *superVC = (UIViewController *)[self firstAvailableUIViewController];
    [superVC.navigationController pushViewController:_OfficialListViewController animated:YES];
    if ([superVC.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        superVC.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }

}


/*喜欢按钮*/
- (MCFireworksButton *)likeButton
{
    if (!_likeButton){
        _likeButton = [MCFireworksButton buttonWithType:UIButtonTypeCustom];
        self.likeButton.particleImage = [UIImage imageNamed:@"Sparkle"];
        self.likeButton.particleScale = 0.05;
        self.likeButton.particleScaleRange = 0.02;
        [_likeButton addTarget:self action:@selector(likeAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _likeButton;
}
-(void)likeAction:(id)sender{

    iEver_likeActionObject * content = [[iEver_likeActionObject alloc] init];
    if (!_list_object._selected) {

        NSDictionary *dic = @{@"businessId": [NSNumber numberWithInt:_list_object.Cover_id],
                              @"type": [NSNumber numberWithInt:10],
                              };

        [[[content like:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
         subscribeNext:^(NSDictionary *object) {

             if ([[object objectForKey:@"resultCode"] integerValue] == 1) {

                 _list_object._selected = !_list_object._selected;
                 _list_object.likeTotal = _list_object.likeTotal +1;
                 [self.likeButton popOutsideWithDuration:0.5];
                 [self.likeButton setImage:[UIImage imageNamed:@"main_list_liked"] forState:UIControlStateNormal];
                 self.likeLable.text = [NSString stringWithFormat:@"%d喜欢",_list_object.likeTotal];
                 [self.likeButton animate];

             }else{
                 NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
                 [SVProgressHUD showImage:Nil status:codeStr];
             }
         }];
    }
    else {

        NSDictionary *dic = @{@"businessId": [NSNumber numberWithInt:_list_object.Cover_id],
                              @"type": [NSNumber numberWithInt:10],
                              };

        [[[content unlike:dic path:nil] deliverOn:[RACScheduler mainThreadScheduler]]
         subscribeNext:^(NSDictionary *object) {

             if ([[object objectForKey:@"resultCode"] integerValue] == 1) {

                 _list_object._selected = !_list_object._selected;
                 _list_object.likeTotal = _list_object.likeTotal -1;
                 [self.likeButton popInsideWithDuration:0.4];
                 [self.likeButton setImage:[UIImage imageNamed:@"main_list_like"] forState:UIControlStateNormal];
                 self.likeLable.text = [NSString stringWithFormat:@"%d喜欢",_list_object.likeTotal];

             }else{
                 NSString *codeStr = [[iEver_Global Instance] confirmationResultCode:[[object objectForKey:@"resultCode"] integerValue]];
                 [SVProgressHUD showImage:Nil status:codeStr];
             }
         }];
    }
}


/*分享按钮*/
- (UIButton *)shareButton
{
    if (!_shareButton){
        _shareButton.backgroundColor = CLEAR;
        _shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_shareButton setImage:[UIImage imageNamed:@"main_list_share"] forState:UIControlStateNormal];
        [_shareButton setImage:[UIImage imageNamed:@"main_list_share"] forState:UIControlStateHighlighted];
        [_shareButton addTarget:self action:@selector(shareAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _shareButton;
}
-(void)shareAction:(id)sender{

    NSString *shareUrl = _list_object.webUrl;
    NSString *title = _list_object.articleTitle;
    UIImage *shareImage = _coverImage.image;

    //设置分享消息类型
    [UMSocialData defaultData].extConfig.wxMessageType = UMSocialWXMessageTypeWeb;

    //微信好友
    [UMSocialData defaultData].extConfig.wechatSessionData.url = shareUrl;
    [UMSocialData defaultData].extConfig.wechatSessionData.title = title;

    //微信朋友圈
    [UMSocialData defaultData].extConfig.wechatTimelineData.url = shareUrl;
    [UMSocialData defaultData].extConfig.wechatTimelineData.title = title;

    //Qzone QQ空间分享只支持图文分享（图片文字缺一不可）
    [UMSocialData defaultData].extConfig.qzoneData.url = shareUrl;
    [UMSocialData defaultData].extConfig.qzoneData.title = title;


    NSString *shareText = [NSString stringWithFormat:@"美课美妆给予你们：%@, 详情请见：%@",  title, shareUrl];            //分享内嵌文字

    //分享到新浪微博内容
    [UMSocialData defaultData].extConfig.sinaData.shareText = shareText;

    UIViewController *superVC = (UIViewController *)[self firstAvailableUIViewController];

    //如果得到分享完成回调，需要设置delegate为self
    [UMSocialSnsService presentSnsIconSheetView:superVC appKey:UMAPPKEY shareText:shareText shareImage:shareImage shareToSnsNames:@[UMShareToWechatSession, UMShareToWechatTimeline,UMShareToSina, UMShareToTencent ] delegate:nil];

}


/*喜欢次数*/
- (UILabel *)likeLable
{
    if (!_likeLable){
        _likeLable = [[UILabel alloc] init];
        _likeLable.numberOfLines = 1;
        _likeLable.backgroundColor = CLEAR;
        _likeLable.textColor = BLACK;
        _likeLable.font = TextDESCFonts;
        _likeLable.textAlignment = NSTextAlignmentCenter;
    }
    return _likeLable;
}

/*分享次数*/
- (UILabel *)shareLable
{
    if (!_shareLable){
        _shareLable = [[UILabel alloc] init];
        _shareLable.numberOfLines = 1;
        _shareLable.backgroundColor = CLEAR;
        _shareLable.textColor = BLACK;
        _shareLable.font = TextDESCFonts;
        _shareLable.textAlignment = NSTextAlignmentCenter;
    }
    return _shareLable;
}


@end
