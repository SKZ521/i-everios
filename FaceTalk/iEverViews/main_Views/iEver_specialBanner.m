//
//  iEver_specialBanner.m
//  FaceTalk
//
//  Created by kevin on 15/3/29.
//  Copyright (c) 2015年 iEver. All rights reserved.
//

#import "iEver_specialBanner.h"
#import "UIView+FindViewController.h"
#import "iEver_mainContent.h"
#import "iEver_OfficialDetailViewController.h"
#import "iEver_V_ListDetailViewController.h"


@interface iEver_specialBanner()<UIScrollViewDelegate,MCPagerViewDelegate> {

    iEver_mainContent * _object;

    NSInteger  _pages;
}

@property (strong, nonatomic) UIView          *baseView;

@property (strong, nonatomic) UIView          *scrollHeadView;

@property (strong, nonatomic) UIImageView     *arrowImageView;




@end

@implementation iEver_specialBanner

#define KBaseViewHeight 510.0
#define ColorViewYGap 15.0
#define ColorViewXGap 7.0
#define ColorViewHeight 472.0

#define ScrollViewXGap 9.0
#define ScrollViewYGap 0.0
#define ScrollViewWidth 288.0
#define ScrollViewImageHight 288.0

#define ScrollHeadViewWight 306.0
#define ScrollHeadViewHight 55.0



#define Kimage_Height 84.0f

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self initiaSubViews];
        
    }
    return self;
}

- (void)initiaSubViews
{
    
    [self.baseView addSubview:self.baseColorView];
    
    [self.baseColorView addSubview:self.scrollView];
    
    [self.baseColorView addSubview:self.scrollHeadView];
    
    [self.scrollHeadView addSubview:self.lastSpecialTitle];
    
    [self.scrollHeadView addSubview:self.crrentSpecialTitle];
    
    [self.scrollHeadView addSubview:self.nextSpecialTitle];
    
    [self.scrollHeadView addSubview:self.arrowImageView];
    
    [self addSubview:self.baseView];

    [self addSubview:self.pagerView];
}


- (void)setObject:(iEver_mainContent *)object
{
    
    if (![object._specialBannerList count]) {
        return;
    }
    
    self.baseColorView.backgroundColor = SpecialBannerCOLOR_1;
    
    _object = object;
    
    for (int i = 0; i < [object._specialBannerList count]; i++) {
        
        iEver_specialBannerList_object *_specialBanner_object = [object._specialBannerList objectAtIndex:i];
        [self addImageWithName:_specialBanner_object atPosition:i];
        
    }
    
    [self.scrollView setContentSize:CGSizeMake(ScrollViewWidth * [object._specialBannerList count], ColorViewHeight)];

    if ([object._specialBannerList count] == 1) {
        iEver_specialBannerList_object *_specialBanner_object1 = [object._specialBannerList objectAtIndex:0];
        self.crrentSpecialTitle.text = _specialBanner_object1.specialTitle;

    }


    if ([object._specialBannerList count] > 1) {
        iEver_specialBannerList_object *_specialBanner_object1 = [object._specialBannerList objectAtIndex:0];
        self.crrentSpecialTitle.text = _specialBanner_object1.specialTitle;
        iEver_specialBannerList_object *_specialBanner_object2 = [object._specialBannerList objectAtIndex:1];
        self.nextSpecialTitle.text = _specialBanner_object2.specialTitle;
        
    }

    UIImage *main_list_arrow  = [UIImage imageNamed:@"main_list_arrow"];
    self.arrowImageView.frame = CGRectMake(ScrollHeadViewWight/2 - main_list_arrow.size.width/2, ScrollHeadViewHight - main_list_arrow.size.height, main_list_arrow.size.width, main_list_arrow.size.height);


    // Pager
    _pages = [object._specialBannerList count];

    [_pagerView setImage:[UIImage imageNamed:@"main_list_point"]
        highlightedImage:[UIImage imageNamed:@"main_list_point_"]
                  forKey:@"a"];

    NSMutableString * string = [[NSMutableString alloc ] init];

    for (int i = 0; i < [object._specialBannerList count]; i ++ ) {
        [string appendFormat:@"a"];
    }

    [_pagerView setPattern:string];

    _pagerView.delegate = self;

     [self.pagerView setCenter:CGPointMake((self.scrollView.frame.size.width / 2.0 - _pages * 18 /2), self.scrollView.frame.size.height - 109)];
    
}


- (void)addImageWithName:(iEver_specialBannerList_object *)iAd atPosition:(int)position

{
    
    UIImage *main_list_number     = [UIImage imageNamed:@"main_list_number"];
    UIImage *main_list_video      = [UIImage imageNamed:@"main_list_video"];
    UIImage *main_list_sign       = [UIImage imageNamed:@"main_list_sign"];
    UIImage *main_list_play_big   = [UIImage imageNamed:@"main_list_play_big"];
    UIImage *main_list_sign_V     = [UIImage imageNamed:@"main_list_sign_V"];
    
    
    /* 封面图片 */
    UIImageView *coverImage = [[UIImageView alloc] initWithFrame:CGRectMake(ScrollViewWidth * position, ScrollHeadViewHight, ScrollViewWidth, ScrollViewWidth)];
    
    coverImage.userInteractionEnabled = YES;
    
    [coverImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/576x/interlace/1",iAd.coverImg]] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];
    
    [self.scrollView addSubview:coverImage];
    
    /* 浏览图片 */
    
    UIImageView * browseImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 20, main_list_number.size.width, main_list_number.size.height)];
    if (iAd.videoLink.length > 0 || iAd.mVideoLink.length > 0) {
        browseImage.image = main_list_video;
    }else {
        browseImage.image = main_list_number;
    }

    [coverImage addSubview:browseImage];
    
    /* 浏览次数 */
    
    UILabel *browseLable = [[UILabel alloc] initWithFrame:CGRectMake(10 + main_list_number.size.width + 5, 18, 60, 15)];
    browseLable.numberOfLines    = 1;
    browseLable.backgroundColor  = CLEAR;
    browseLable.textColor        = WHITE;
    browseLable.shadowColor      = GRAY_LIGHT;
    browseLable.font             = TextDESCFonts;
    browseLable.textAlignment    = NSTextAlignmentLeft;
    browseLable.text             = [NSString stringWithFormat:@"%d",iAd.pvTotal + iAd.webPvTotal];
    [coverImage addSubview:browseLable];

    /* 如果是视频 视频播放按钮 */;

    if (iAd.videoLink.length > 0) {

        UIImageView *main_list_play_big_image = [[UIImageView alloc] initWithFrame:CGRectMake((coverImage.frame.size.width  - main_list_play_big.size.width)/2, (coverImage.frame.size.height  - main_list_play_big.size.height)/2, main_list_play_big.size.width, main_list_play_big.size.height)];

        main_list_play_big_image.image = main_list_play_big;

        [coverImage addSubview:main_list_play_big_image];
    }
    
    /* 分类 */
    UIView *categoryView = [[UIView alloc] initWithFrame:CGRectMake(ScrollViewWidth * position, ScrollHeadViewHight + ScrollViewWidth + 22.0, ScrollViewWidth, main_list_sign.size.height)];
    
    [self.scrollView addSubview:categoryView];

    if (iAd.homeType == 10) {
        UIImageView *signImageView = [[UIImageView alloc] initWithFrame:CGRectMake((ScrollViewWidth - main_list_sign.size.width)/2,0.0, main_list_sign.size.width, main_list_sign.size.height)];

        signImageView.image = main_list_sign;

        [categoryView addSubview:signImageView];

        UILabel *rootCateLable = [[UILabel alloc] initWithFrame:CGRectMake(6.0, 2.0, main_list_sign.size.width/2 - 6.0 * 2, main_list_sign.size.height - 2.0 *2)];
        rootCateLable.numberOfLines = 1;
        rootCateLable.backgroundColor = CLEAR;
        rootCateLable.textColor = WHITE;
        rootCateLable.font = TextFont;
        rootCateLable.textAlignment = NSTextAlignmentCenter;

        rootCateLable.text = iAd.rootCategoryName;
        [signImageView addSubview:rootCateLable];

        UILabel *secondCateLable = [[UILabel alloc] initWithFrame:CGRectMake(main_list_sign.size.width/2 + 6.0, 2.0, main_list_sign.size.width/2 - 6.0 * 2, main_list_sign.size.height - 2.0 *2)];
        secondCateLable.numberOfLines = 1;
        secondCateLable.backgroundColor = CLEAR;
        secondCateLable.textColor = BLACK;
        secondCateLable.font = TextFont;
        secondCateLable.textAlignment = NSTextAlignmentCenter;

        secondCateLable.text = iAd.secondCategoryName;
        [signImageView addSubview:secondCateLable];

    }else if (iAd.homeType == 17) {

        UIImageView *signImageView_V = [[UIImageView alloc] initWithFrame:CGRectMake((ScrollViewWidth - main_list_sign_V.size.width)/2,0.0, main_list_sign_V.size.width, main_list_sign_V.size.height)];

        signImageView_V.image = main_list_sign_V;

        [categoryView addSubview:signImageView_V];

        UILabel *expertUserNickNameLable = [[UILabel alloc] initWithFrame:CGRectMake(6.0, 2.0, main_list_sign.size.width - 6.0 * 2, main_list_sign.size.height - 2.0 *2)];
        expertUserNickNameLable.numberOfLines = 1;
        expertUserNickNameLable.backgroundColor = CLEAR;
        expertUserNickNameLable.textColor = BLACK;
        expertUserNickNameLable.font = TextFont;
        expertUserNickNameLable.textAlignment = NSTextAlignmentCenter;

        expertUserNickNameLable.text = iAd.expertUserNickName;
        [signImageView_V addSubview:expertUserNickNameLable];


    }
    

    /* 时间 */
    UILabel * timeLable = [[UILabel alloc] initWithFrame:CGRectMake(ScrollViewWidth * position, categoryView.frame.origin.y + categoryView.frame.size.height + 10.0, ScrollViewWidth, 20.0)];
    timeLable.numberOfLines = 1;
    timeLable.backgroundColor = CLEAR;
    timeLable.textColor = TIME;
    timeLable.font = TextFont;
    timeLable.textAlignment = NSTextAlignmentCenter;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY.MM.dd"];
    NSString *dateLoca = [NSString stringWithFormat:@"%lld",iAd.bannerReleaseTime/1000];
    NSTimeInterval time =[dateLoca doubleValue];
    NSDate *detaildate = [NSDate dateWithTimeIntervalSince1970:time];
    NSString *timestr = [formatter stringFromDate:detaildate];
    timeLable.text = timestr;
    
    [self.scrollView addSubview:timeLable];
    
    /* 标题 */
    UILabel * titleLable = [[UILabel alloc] initWithFrame:CGRectMake(ScrollViewWidth * position, timeLable.frame.origin.y + timeLable.frame.size.height + 10.0, ScrollViewWidth, 20.0)];
    titleLable.numberOfLines = 1;
    titleLable.backgroundColor = CLEAR;
    titleLable.textColor = BLACK;
    titleLable.font = Title_BigFont;
    titleLable.textAlignment = NSTextAlignmentCenter;
    
    titleLable.text = iAd.articleTitle;
    
    [self.scrollView addSubview:titleLable];
    
    
    /* 交互按钮 */
    UIButton *button     = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame         = CGRectMake( 0 , 0, ScrollViewWidth, ScrollViewWidth);
    [[button rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *button) {

        if (iAd.homeType == 10) {
            iEver_OfficialDetailViewController *_officialDetailViewController = [[iEver_OfficialDetailViewController alloc]init];
            _officialDetailViewController.type_id = iAd.businessId;
            _officialDetailViewController.hidesBottomBarWhenPushed = YES;
            UIViewController *superVC = (UIViewController *)[self firstAvailableUIViewController];
            [superVC.navigationController pushViewController:_officialDetailViewController animated:YES];
            if ([superVC.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
                superVC.navigationController.interactivePopGestureRecognizer.delegate = nil;
            }
        }else if  (iAd.homeType == 17) {
            iEver_V_ListDetailViewController *_V_ListDetailViewController = [[iEver_V_ListDetailViewController alloc]init];
            _V_ListDetailViewController.type_id = iAd.businessId;
            _V_ListDetailViewController.hidesBottomBarWhenPushed = YES;
            UIViewController *superVC = (UIViewController *)[self firstAvailableUIViewController];
            [superVC.navigationController pushViewController:_V_ListDetailViewController animated:YES];
            if ([superVC.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
                superVC.navigationController.interactivePopGestureRecognizer.delegate = nil;
            }

        }
    }];
    [coverImage addSubview:button];
    
}

#pragma mark -
#pragma mark UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollview
{
    int page = floor(self.scrollView.contentOffset.x / ScrollViewWidth);
    _pagerView.page = page;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    int page = floor(self.scrollView.contentOffset.x  / ScrollViewWidth);

    if (page < 0) {
        page = 0;
    }
    if (page > [_object._specialBannerList count] - 1) {
        page = [_object._specialBannerList count] - 1;
    }
    
    if (page - 1 < 0) {
        self.lastSpecialTitle.text = @"";
    }else{
    
        iEver_specialBannerList_object *_specialBanner_object1 = [_object._specialBannerList objectAtIndex:page - 1];
        self.lastSpecialTitle.text = _specialBanner_object1.specialTitle;
    }
    

        iEver_specialBannerList_object *_specialBanner_object2 = [_object._specialBannerList objectAtIndex:page];
        self.crrentSpecialTitle.text = _specialBanner_object2.specialTitle;
    
    if (page + 1 > _object._specialBannerList.count - 1) {
        self.nextSpecialTitle.text = @"";
    }else{
    
        iEver_specialBannerList_object *_specialBanner_object3 = [_object._specialBannerList objectAtIndex:page + 1];
        self.nextSpecialTitle.text = _specialBanner_object3.specialTitle;
    }
    NSArray *colorTypes =@[SpecialBannerCOLOR_1,
                           SpecialBannerCOLOR_2,
                           SpecialBannerCOLOR_3,
                           SpecialBannerCOLOR_4,
                           SpecialBannerCOLOR_5,
                           SpecialBannerCOLOR_6,
                           SpecialBannerCOLOR_1,
                           SpecialBannerCOLOR_2,
                           SpecialBannerCOLOR_3,
                           SpecialBannerCOLOR_4,
                           SpecialBannerCOLOR_5,
                           SpecialBannerCOLOR_6];

    [UIView beginAnimations:nil context:nil];
    [UIView    setAnimationCurve: UIViewAnimationCurveLinear];
    [UIView    setAnimationDelegate:self];
    [UIView    setAnimationDuration:.6];
     self.baseColorView.backgroundColor = colorTypes[page];
    [UIView commitAnimations];


}

#pragma mark -
#pragma mark layoutSubviews (update view)

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.scrollView.frame = CGRectMake(ScrollViewXGap, ScrollViewYGap, ScrollViewWidth, ColorViewHeight);
}

#pragma mark -
#pragma mark SET
- (UIScrollView *)scrollView
{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.pagingEnabled = YES;
        _scrollView.backgroundColor = CLEAR;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.delegate = self;
    }
    
    return _scrollView;
}


- (UIView *)baseView
{
    if (!_baseView) {
        _baseView = [[UIView alloc] init];
        _baseView.frame = CGRectMake(0, 0, ScreenWidth, KBaseViewHeight);
        _baseView.backgroundColor = CLEAR;
    }
    
    return _baseView;
}

- (UIView *)baseColorView
{
    if (!_baseColorView) {
        _baseColorView = [[UIView alloc] init];
        _baseColorView.frame = CGRectMake(ColorViewXGap, ColorViewYGap, ScreenWidth - 2 * ColorViewXGap , ColorViewHeight);
        _baseColorView.backgroundColor = CLEAR;
    }
    
    return _baseColorView;
}


- (UIView *)scrollHeadView
{
    if (!_scrollHeadView) {
        _scrollHeadView = [[UIView alloc] init];
        _scrollHeadView.frame = CGRectMake(0, 0, ScrollHeadViewWight , ScrollHeadViewHight);
        _scrollHeadView.backgroundColor = CLEAR;
    }
    
    return _scrollHeadView;
}

- (UILabel *)lastSpecialTitle
{
    if (!_lastSpecialTitle){
        _lastSpecialTitle = [[UILabel alloc] initWithFrame:CGRectMake(ScrollViewXGap, 20 , 85, 15)];
        _lastSpecialTitle.numberOfLines = 1;
        _lastSpecialTitle.backgroundColor = CLEAR;
        _lastSpecialTitle.textColor = TIME;
        _lastSpecialTitle.font = Title_Font;
        _lastSpecialTitle.textAlignment = NSTextAlignmentLeft;
    }
    return _lastSpecialTitle;
}

- (UILabel *)crrentSpecialTitle
{
    if (!_crrentSpecialTitle){
        _crrentSpecialTitle = [[UILabel alloc] initWithFrame:CGRectMake(91, 15 , 124, 20)];
        _crrentSpecialTitle.numberOfLines = 1;
        _crrentSpecialTitle.backgroundColor = CLEAR;
        _crrentSpecialTitle.textColor = BLACK;
        _crrentSpecialTitle.font = Title_Big2_Font;
        _crrentSpecialTitle.textAlignment = NSTextAlignmentCenter;
    }
    return _crrentSpecialTitle;
}

- (UILabel *)nextSpecialTitle
{
    if (!_nextSpecialTitle){
        _nextSpecialTitle = [[UILabel alloc] initWithFrame:CGRectMake(212, 20 , 85, 15)];
        _nextSpecialTitle.numberOfLines = 1;
        _nextSpecialTitle.backgroundColor = CLEAR;
        _nextSpecialTitle.textColor = TIME;
        _nextSpecialTitle.font = Title_Font;
        _nextSpecialTitle.textAlignment = NSTextAlignmentRight;
    }
    return _nextSpecialTitle;
}

- (UIImageView *)arrowImageView
{
    if (!_arrowImageView) {
        _arrowImageView             = [[UIImageView alloc]init];
        _arrowImageView.contentMode = UIViewContentModeScaleToFill;
        _arrowImageView.image       = [UIImage imageNamed:@"main_list_arrow"];
        
    }
    return _arrowImageView;
}


+ (CGFloat)heightForSpecialBanner{
    
    return KBaseViewHeight;
}
- (MCPagerView *)pagerView
{
    if (!_pagerView) {
        _pagerView = [[MCPagerView alloc] init];


    }
    return _pagerView;
}
- (void)pageView:(MCPagerView *)pageView didUpdateToPage:(NSInteger)newPage
{

}
@end
