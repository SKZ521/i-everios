//
//  iEver_itemTry_cell.h
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-30.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "SSBaseTableCell.h"
#import "iEver_tryItemObject.h"

@interface iEver_itemTry_cell : SSBaseTableCell
+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;
@property (nonatomic, strong) iEver_itemTryList_object *object;
@end
