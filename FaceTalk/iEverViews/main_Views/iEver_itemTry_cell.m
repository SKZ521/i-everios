//
//  iEver_itemTry_cell.m
//  FaceTalk
//
//  Created by 开仲 司 on 14-10-30.
//  Copyright (c) 2014年 iEver. All rights reserved.
//

#import "iEver_itemTry_cell.h"

@interface iEver_itemTry_cell()
@property (nonatomic, strong) UIView        *bottomView;         /* 视图_base */
@property (nonatomic, strong) UIView        *halfBlackView;      /* 视图横线 */
@property (nonatomic, strong) UIImageView   *try_itemImage;      /* 商品图片 */
@property (nonatomic, strong) UILabel       *try_itemName;       /* 数字标示 */
@property (nonatomic, strong) UILabel       *tryNum;             /* 商品标题 */
@property (nonatomic, strong) UILabel       *applyNum;           /* 商品副标题 */
@property (nonatomic, strong) UILabel       *tryTime;            /* 使用人数 */

@property (nonatomic, strong) UIImageView   *apply_image;        /* 是否已经申请试用标示 */
@property (nonatomic, strong) UILabel       *tryNum_lable;       /* 试用数 */
@property (nonatomic, strong) UILabel       *applyNum_lable;     /* 申请数 */
@property (nonatomic, strong) UILabel       *tryTime_lable;      /* 剩余 */
@end


@implementation iEver_itemTry_cell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void) configureCell {
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
		self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.contentView.backgroundColor = CLEAR;
        self.backgroundColor = CLEAR;
        [self.contentView addSubview:self.bottomView];
        [self.contentView addSubview:self.try_itemImage];
        [self.contentView addSubview:self.halfBlackView];
        [self.contentView addSubview:self.try_itemName];
        [self.contentView addSubview:self.tryNum];
        [self.contentView addSubview:self.applyNum];
        [self.contentView addSubview:self.tryTime];
        [self.contentView addSubview:self.apply_image];
        [self.contentView addSubview:self.tryNum_lable];
        [self.contentView addSubview:self.applyNum_lable];
        [self.contentView addSubview:self.tryTime_lable];

    }
    return self;
}
- (void)awakeFromNib
{
    // Initialization code
}
- (void)layoutSubviews
{
	[super layoutSubviews];
    /*单元格frame */
    //const CGRect r = self.contentView.bounds;

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)setObject:(iEver_itemTryList_object *)object
{
    CGFloat x = 0.0;
    CGFloat y = 0.0;
    CGFloat width = 0.0;
    CGFloat height = 0.0;

    /* 试用活动图片 */
    x = 5.0;
    y = 5.0;
    width = 310.0;
    height = 155.0;
    self.try_itemImage.frame = CGRectMake(x, y, width, height);
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.try_itemImage.bounds byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight cornerRadii:CGSizeMake(5, 5)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.try_itemImage.bounds;
    maskLayer.path = maskPath.CGPath;
    self.try_itemImage.layer.mask = maskLayer;
    self.try_itemImage.layer.masksToBounds = YES;
    [self.try_itemImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?imageMogr2/thumbnail/620x",object.tryImg]] placeholderImage:[UIImage imageNamed:@"defualt_icon"]];


    /* 白色圆角地图 */
    x = 5.0;
    y = 5.0;
    width = 310.0;
    height = 200.0;
    CALayer *bottomViewLayer = [self.bottomView layer];
    [bottomViewLayer setMasksToBounds:YES];
    [bottomViewLayer setCornerRadius:5.0];
    self.bottomView.frame = CGRectMake(x, y, width, height);


    /* 半透明视图 */
    x = 5.0;
    y = 160.0 - 23.0;
    width = 310.0;
    height = 23.0;
    self.halfBlackView.alpha = 0.6;
    self.halfBlackView.frame = CGRectMake(x, y, width, height);


    /* 试用数 */
    x = 15.0;
    y = 135.0;
    width = 50.0;
    height = 25.0;
    self.tryNum_lable.frame = CGRectMake(x, y, width, height);
    self.tryNum_lable.text = @"试用数";

    /* 活动试用数 */
    x = 55.0;
    y = 135.0;
    width = 60.0;
    height = 25.0;
    self.tryNum.frame = CGRectMake(x, y, width, height);
    self.tryNum.text = [NSString stringWithFormat:@"%d份",object.tryNum];

    /* 申请数 */
    x = 100.0;
    y = 135.0;
    width = 50.0;
    height = 25.0;
    self.applyNum_lable.frame = CGRectMake(x, y, width, height);
    self.applyNum_lable.text = @"申请数";

    /* 申请人数 */
    x = 140.0;
    y = 135.0;
    width = 60.0;
    height = 25.0;
    self.applyNum.frame = CGRectMake(x, y, width, height);
    self.applyNum.text = [NSString stringWithFormat:@"%d人",object.applyTotal];
    

    /* 剩余 */
    x = 240.0;
    y = 135.0;
    width = 40;
    height = 25.0;
    self.tryTime_lable.frame = CGRectMake(x, y, width, height);
    self.tryTime_lable.text = @"剩余";


    /* 剩余时间 或者是否结束 */
    x = 270.0;
    y = 135.0;
    width = 50.0;
    height = 25.0;
    self.tryTime.frame = CGRectMake(x, y, width, height);
    NSString *str = [iEver_Global intervalSinceNow:object.endTime/1000];
    if ([str isEqualToString:@"已结束"]) {
        self.tryTime_lable.hidden = YES;
        self.tryTime.textColor = WHITE;
    }else{
        self.tryTime_lable.hidden = NO;
        self.tryTime.textColor = TRY_UPDATE_YELLOY;
    }
    self.tryTime.text = [NSString stringWithFormat:@"%@",str];


    /* 活动标题 */
    x = 15.0;
    y = 170.0;
    width = 240.0;
    height = 25.0;
    self.try_itemName.frame = CGRectMake(x, y, width, height);
    self.try_itemName.text = object.tryName;


       /* 是否申请 */
    UIImage *tryItem_applyed = [UIImage imageNamed:@"tryItem_applyed"];
    x = 250.0;
    y = 165.0;
    width = tryItem_applyed.size.width;
    height = tryItem_applyed.size.height;
    self.apply_image.frame = CGRectMake(x, y, width, height);

    if (object.isApply > 0) {
        self.apply_image.hidden = NO;
        self.apply_image.image = tryItem_applyed;
    }else{

        self.apply_image.hidden = YES;
    }


}

+ (CGFloat)heightForObject:(id)object atIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
	return 210.0;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

# pragma mark - view getters

/* 底部白色视图 */
- (UIView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [[UIView alloc]init];
        _bottomView.backgroundColor = WHITE;
    }
    return _bottomView;
}



/* 半黑视图 */
- (UIView *)halfBlackView
{
    if (!_halfBlackView) {
        _halfBlackView = [[UIView alloc]init];
        _halfBlackView.backgroundColor = BLACK;
    }
    return _halfBlackView;
}


/* 试用图片 */
- (UIImageView *)try_itemImage
{
    if (!_try_itemImage) {
        _try_itemImage = [[UIImageView alloc]init];
        _try_itemImage.contentMode = UIViewContentModeScaleAspectFill;

    }
    return _try_itemImage;
}


/* 试用活动标题 */
- (UILabel *)try_itemName
{
	if (!_try_itemName){
		_try_itemName = [[UILabel alloc] init];
		_try_itemName.numberOfLines = 1;
		_try_itemName.backgroundColor = CLEAR;
		_try_itemName.textColor = BLACK;
		_try_itemName.font = Title_V_Fonts;
	}
	return _try_itemName;
}

/* 活动试用数量 */
- (UILabel *)tryNum
{
	if (!_tryNum){
		_tryNum = [[UILabel alloc] init];
		_tryNum.numberOfLines = 1;
		_tryNum.backgroundColor = CLEAR;
		_tryNum.textColor = TRY_UPDATE_YELLOY;
		_tryNum.font = TextDESCFonts;
	}
	return _tryNum;
}
/* 活动申请数量 */
- (UILabel *)applyNum
{
	if (!_applyNum){
		_applyNum = [[UILabel alloc] init];
        _applyNum.numberOfLines = 1;
		_applyNum.backgroundColor = CLEAR;
        _applyNum.textColor = TRY_UPDATE_YELLOY;
        _applyNum.font = TextDESCFonts;
	}
	return _applyNum;
}
/* 活动剩余时间 是否结束 */
- (UILabel *)tryTime
{
	if (!_tryTime){
		_tryTime = [[UILabel alloc] init];
        _tryTime.numberOfLines = 1;
		_tryTime.backgroundColor = CLEAR;
        _tryTime.textColor = TRY_UPDATE_YELLOY;
        _tryTime.font = TextDESCFonts;
	}
	return _tryTime;
}


- (UILabel *)tryNum_lable
{
	if (!_tryNum_lable){
		_tryNum_lable = [[UILabel alloc] init];
		_tryNum_lable.numberOfLines = 1;
		_tryNum_lable.backgroundColor = CLEAR;
		_tryNum_lable.textColor = WHITE;
		_tryNum_lable.font = TextDESCFonts;
	}
	return _tryNum_lable;
}

- (UILabel *)applyNum_lable
{
	if (!_applyNum_lable){
		_applyNum_lable = [[UILabel alloc] init];
		_applyNum_lable.numberOfLines = 1;
		_applyNum_lable.backgroundColor = CLEAR;
		_applyNum_lable.textColor = WHITE;
		_applyNum_lable.font = TextDESCFonts;
	}
	return _applyNum_lable;
}

- (UILabel *)tryTime_lable
{
	if (!_tryTime_lable){
		_tryTime_lable = [[UILabel alloc] init];
		_tryTime_lable.numberOfLines = 1;
		_tryTime_lable.backgroundColor = CLEAR;
		_tryTime_lable.textColor = WHITE;
		_tryTime_lable.font = TextDESCFonts;
	}
	return _tryTime_lable;
}

/* 试用图片 */
- (UIImageView *)apply_image
{
    if (!_apply_image) {
        _apply_image = [[UIImageView alloc]init];
        _apply_image.contentMode = UIViewContentModeScaleAspectFill;

    }
    return _apply_image;
}
@end
